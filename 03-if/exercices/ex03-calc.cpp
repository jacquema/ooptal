// Cours 03 exercice 6
// Demander à l'utilisateur d'entrer deux nombres entiers, puis une opération, sous la forme d'un caractère parmis `+`, `-`, `*`, `/`.   
// Afficher le résultat de l'application de l'opération aux deux nombres.   
// Traiter les différents cas d'erreur (mauvais caractère, division par 0...).

#include <iostream>

int main()
{
    // operateur
    char op;

    // nombres
    int n1, n2;

    /* lecture des données */
    std::cout << "Opération : ";
    std::cin >> op;

    std::cout << "Nombre 1 : ";
    std::cin >> n1;

    std::cout << "Nombre 2 : ";
    std::cin >> n2;

    // résultat
    double r;

    if (op == '+')
    {
        r = n1 + n2;
    }
    else if (op == '-')
    {
        r = n1 - n2;
    }
    else if (op == '*')
    {
        r = n1 * n2;
    }
    else if (op == '/')
    {
        if (n2 == 0)
        {
            std::cerr << "Erreur: division par 0" << std::endl;
            return 2; 
        }

        r = (double) n1 / n2;
    }
    else
    {
        std::cout << "Désolé je ne sais pas calculer ";
        std::cout << n1 << ' ' << op << ' ' << n2 << std::endl;
        return 3;
    }

    /* impression du résultat */
    std::cout << n1 << op << n2 << " = " << r << std::endl;
    return 0;
}
