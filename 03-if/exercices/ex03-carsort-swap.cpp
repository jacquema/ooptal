// exercice 3
// solution avec en onversant c1 et c2



#include <iostream>
 
 int main()
{
    // caractères en entrée
    char c1, c2;

    // lecture des données 
    std::cout << "Entrer le premier  caractère: ";
    std::cin >> c1;

    std::cout << "Entrer le deuxième caractère: ";
    std::cin >> c2;

	// test et permutation de c1 et c2
    if (c1 > c2)
    {
        // permute c1 et c2 (c1 et c2 peuvent être modifiées!)
        char tmp = c1;
        c1 = c2;
        c2 = tmp;
    }

    // impression du résultat 
    std::cout << "Dans l'ordre: " << c1 << " et " << c2 << std::endl;

    return 0;
}
