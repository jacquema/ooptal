// exercice 1

#include <iostream>

int main()
{
    int i;

    /* lecture des données */
    std::cout << "Entrer un entier: ";
    std::cin >> i;

    /* impression de la parité */
    if (i % 2 == 0)
    {
        std::cout << i << " est pair" << std::endl;
    }
    else
    {
        std::cout << i << " est impair" << std::endl;
    }

    return 0;
}
