// exercice 3
// avec else

#include <iostream>
 
 int main()
{
    // caractères en entrée
    char c1, c2;

    // lecture des données 
    std::cout << "Entrer le premier  caractère: ";
    std::cin >> c1;

    std::cout << "Entrer le deuxième caractère: ";
    std::cin >> c2;

    // test 
    if (c1 <= c2)
        std::cout << "Dans l'ordre: " << c1 << " et " << c2 << std::endl;
	else
        std::cout << "Dans l'ordre: " << c2 << " et " << c1 << std::endl;

    return 0;
}