// exercice 5

#include <iostream>

int main()
{
    // caractère en entrée
    char c;

    /* lecture des données */
    std::cout << "Entrer un caractère alphabétique: ";
    std::cin >> c;

	// analyse de cas
    if ('a' <= c && c <= 'z')      // c minuscule
		c -= 32; // conversion en majuscule
    else if ('A' <= c && c <= 'Z') // c majuscule
		c += 32; // conversion en minuscule
    else   // defaut                           
    {
        std::cerr << "Erreur: le caractère " << c << " n'est pas alphabétique" << std::endl;
        return 1; 
    }

    // impression de c apres conversion
	std::cout << "après conversion: " << c << std::endl;

    return 0;
}
