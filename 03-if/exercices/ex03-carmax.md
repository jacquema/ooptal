

> **Exercice 2**.  
> Demander à l'utilisateur deux caractères (type `char`) et afficher le plus grand des deux, suivant l'ordre ASCII (cf. cours02 pour la table ASCII).

