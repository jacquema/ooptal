# Exercices - cours 03

## Instructions conditionnelles



> **Exercice 1**.  
> Demander à l'utilisateur un entier et afficher si il est pair ou impair. 



> **Exercice 2**.  
> Demander à l'utilisateur deux caractères (type `char`) et afficher le plus grand des deux, suivant l'ordre ASCII (cf. cours02 pour la table ASCII).



> **Exercice 3**.  
>
> - Demander à l'utilisateur deux caractères  (type `char`) et les afficher dans l'ordre ASCII,
>   avec un `if`-`else`
> - Même question avec un simple `if` (sans `else`).



> **Exercice 4**.
>
> Considérons les instructions `if`-`else` imbriquées suivantes:
>
> ```C++
> bool participation, felicitations;
> double notes;
> 
> if (notes > 10)
> {
> 	if (participation)
> 	{
> 		felicitations = true;
> 	}
> 	else felicitations = false;
> }
> else felicitations = false;
> ```
>
> - écrire une seule instruction `if`-`else` (sans imbrication) qui donnera le même résultat.
> - même question sans  `if`-`else`  du tout!



> **Exercice 5**.  (analyse par cas)
> Demander à l'utilisateur un caractère alphabétique  (type `char`).  
> Si le caractère est une majuscule, afficher la minuscule correspondante (utiliser la table ASCII) 
> Si le caractère est une minuscule, afficher la majuscule correspondante.  
> Si le caractère n'est pas alphabétique, afficher un message d'erreur.



> **Exercice 6**. **DEVOIR MAISON** (interpreteur de calculs entiers simples)
> Demander à l'utilisateur d'entrer deux nombres entiers, puis une opération, sous la forme d'un caractère parmis `+`, `-`, `*`, `/`.   
> Afficher le résultat de l'application de l'opération aux deux nombres.   
> On traitera les différents cas d'erreur (mauvais caractère, division par 0...).

