

> **Exercice 4**.
>
> Considérons les instructions `if`-`else` imbriquées suivantes:
>
> ```C++
> bool participation, felicitations;
> double notes;
> 
> if (notes > 10)
> {
> 	if (participation)
> 	{
> 		felicitations = true;
> 	}
> 	else felicitations = false;
> }
> else felicitations = false;
> ```
>
> - écrire une seule instruction `if`-`else` (sans imbrication) qui donnera le même résultat.

```C++
if ((notes > 10) && participation)
  felicitations = true;
else
  felicitations = false;
```

> - même question sans  `if`-`else`  du tout!

```C++
felicitations = (notes > 10) && participation;
```

