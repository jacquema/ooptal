// exercice 3
// solution avec 2 nouvelles variables (sans else)

#include <iostream>
 
 int main()
{
    // caractères en entrée
    char c1, c2;

    // lecture des données 
    std::cout << "Entrer le premier  caractère: ";
    std::cin >> c1;

    std::cout << "Entrer le deuxième caractère: ";
    std::cin >> c2;

    // variables auxiliaires: caractères en sortie
    char co1 = c1;
    char co2 = c2;

	// c1 et c2 ne sont pas modifiées
    if (c1 > c2)
    {
        co1 = c2;
        co2 = c1;
    }

    // impression du résultat 
    std::cout << "Dans l'ordre: " << co1 << " et " << co2 << std::endl;

    return 0;
}
