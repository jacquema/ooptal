// exercice 2 
// solution 2 avec une troisième variable

#include <iostream>

int main()
{
    char c1, c2;

    /* lecture des données */
    std::cout << "Entrer le premier caractère: ";
    std::cin >> c1;

    std::cout << "Entrer le deuxième caractère: ";
    std::cin >> c2;

    char bigger;

    /* estimation du plus grand des deux */
    if (c1 >= c2)
        bigger = c1;
    else
        bigger = c2;

    /* impression du résultat */
    std::cout << "Le plus grand caractère est: " << bigger << std::endl;

    return 0;
}

