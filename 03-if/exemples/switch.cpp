/* 
 * switch.cpp
 */

#include <iostream>

int main()
{
	int x;

    std::cout << "Entrer un entier: ";
    std::cin >> x;

	switch (x) 
	{
	case 1:
		std::cout << " est 1"  << std::endl;
		break;

	case 2:
		std::cout << " est 2" << std::endl;
		break;

	default:
		std::cout << " n'est ni 1 ni 2" << std::endl;
	}

	return 0;
}