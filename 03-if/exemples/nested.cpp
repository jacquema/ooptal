
#include <iostream>

int main()
{
	int x;

    std::cout << "Entrer un entier: ";
    std::cin >> x;

	if (x > 0)
		std::cout << " est positif" << std::endl;
	else if (x < 0)  
		std::cout << " est négatif" << std::endl;
	else 
		std::cout << " est nul" << std::endl;

	return 0;
}