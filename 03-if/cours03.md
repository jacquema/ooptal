# Cours de programmation objet

INALCO - Master TAL première année

---

# Cours 03 Instructions conditionnelles

jeudi 03 octobre 2024  
Florent Jacquemard   `florent.jacquemard@inria.fr`

sources: 

- cours de Marie-Anne Moreaux, INALCO
- cours de Christian Casteyde
  - http://casteyde.christian.free.fr/cpp/cours/online/c1039.html (`if`)
  - http://casteyde.christian.free.fr/cpp/cours/online/x1159.html (`switch` `case`)

- https://cplusplus.com/doc/tutorial/control/

  

## Structure du flot de contrôle 

Dans un programme, les instructions représentant les opérations permettant de parvenir au résultat voulu, doivent être énoncées dans un certain ordre.

De façon implicite, le mode d'exécution des instructions est **séquentiel** : les instructions sont exécutées, de la première à la dernière, l'une après l'autre dans l'ordre où elles sont écrites. Quel que soit la situation engendrée par l'exécution des instructions, l'ordre est complètement pré-déterminé.
Dans ce cas, le *flot de contrôle* est linéaire.

Or dans de nombreux cas, on veut que l'exécution d'une action ou d'un groupe d'actions soit **conditionnée** par la réalisation ou non d'une situation particulière. L'exécution n'est alors plus strictement séquentielle, la condition introduit un **branchement** dans le *flot de contrôle*  du programme.



## Les instructions conditionnelles

En plus des instructions représentant des actions élémentaires, on introduit des consignes pour contrôler l'ordre dans lequel doivent s'enchaîner les instructions successives. Ces consignes s'appellent les *instructions conditionnelles*.

Elles contiendront toujours au moins deux parties:

- une partie exprimant une **condition** sur la situation dans laquelle on se trouve. Cette partie est en général exprimée par un prédicat, représenté par une expression booléenne, caractérisant l’état de certaines variables à partir duquel le processeur devra faire des choix.
- une partie représentant la **direction** (branche) à prendre dans le flot de contrôle (suivant l'évaluation de la condition), c'est à dire quelle instruction ou bloc d'instructions  doivent être exécutés,  selon la situation.

Une instruction de contrôle va donc faire dépendre l'exécution ou non de certaines instructions de la réalisation ou non d'une condition.

> **Exemple**:  pgcd 
> dans l'algorithme d'Euclide pour le calcul du pgcd présenté au premier cours, 
> une condition teste si le reste est nul (afin d'arêter le calcul).
>
> ![](img/pgcd.png)



Avant d'écrire une instruction conditionnelle, sous forme de test, il faut :

- déterminer tous les cas qui sont susceptibles d'apparaître,
- vérifier qu'ils sont bien tous mutuellement exclusifs.

Durant l'exécution de l'algorithme par la machine, celle-ci est capable d'évaluer la condition, c'est-à-dire déterminer si elle est vraie ou fausse, et sur la base de cette évaluation de choisir quelle est l'instruction ou le groupe d'instructions à exécuter.



Dans les paragraphes suivants sont présentés des cas particuliers d'instructions de contrôle appellées *instructions conditionnelles*, qui introduisent un branchement suivant l'évaluation d'une condition.



## Alternative

Une alternative permet de choisir entre 2 directions possibles selon une condition décrite par un prédicat (expression évaluée en un booléen).

```c++
if (expression bool)
{
    // bloc d'instructions (branche true)
} 
else
{
    // bloc d'instructions (branche false)
}
// [suite]
```

- Si la situation décrite par l'expression est vraie,  
  alors c'est le premier bloc, "branche `true`", qui est réalisé, puis on passe à `[suite]`.   
  La branche  `false` est ignorée.

- Si la situation décrite par l'expression  est fausse,  
  alors c'est le second bloc, branche `false`, qui est réalisé, puis on passe à `[suite]`.   
  La branche  `true` est ignorée.

Les 2 situations s'excluent mutuellement, c'est-à-dire que si l'une est possible, l'autre est impossible.  
En d'autres termes, l'action `false` n'est réalisé que si la négation du prédicat est vraie.



> **Exemple**:  cf. [`positif.cpp`](positif.cpp)
>
> ```c++
> if (x > 0)
> {
>     std::cout << "x est positif";
>     std::cout << std::endl;
> }
> else 
> {
>     std::cout << "x est négatif ou nul";
>     std::cout << std::endl;
> }
> ```
>



Les `{` et `}` autour des blocs d'instructions sont facultatives pour un bloc qui ne contient 
qu'une instruction.



> **Exemple**:  
>
> le code de l'exemple précédent peut s'écrire sans accolades en utilisant des instructions *one-liners*:
>
> ```C++
> if (x > 0)
> 	std::cout << "x est positif" << std::endl;
> else
> 	std::cout << "x est négatif ou nul" << std::endl;
> ```



On peut imbriqué des `if-else` : chacune des branches peut contenir des `if-else`:

> **Exemple**: `if-else` imbriqués.
>
> cf. [`nested.cpp`](exemples/nested.cpp)
>
> ```C++
> 	if (x >= 0)
> 	{
> 		if (x > 0)
> 			std::cout << " est positif" << std::endl;
> 		else 
> 			std::cout << " est nul" << std::endl;
> 	}
> 	else   // x < 0
> 		std::cout << " est négatif" << std::endl;
> ```
>
> Les accolades sont facultatives ici, car la branche `true` du premier `if` ne contient qu'une instruction,
> qui est elle-même un second `if-else`.
>
> Ici, ces accolades ont été ajoutées pour des raisons de lisibilité.



L'indentation n'est pas significative dans la syntaxe C++ (contrairement à Python par exemple).
Elle est donc **optionnelle**, utilisée uniquement pour une question de lisibilité du code - il est donc quand même recommandée de bien indenter le code.

En C++, ce sont les accolades `{` , `}` qui servent à délimiter les blocs de code.



## Condition simple

Une condition simple permet d'indiquer qu'une action doit être réalisée si la situation décrite par le prédicat est réalisée.

```c++
if (expression bool)
{
    // bloc d'instructions (branche `true`)
}
// [suite]  
```

- Si la situation décrite par l'expression est vraie  
  alors l'action du bloc, branche `true`,  est réalisée puis on passe à `[suite]`. 
- Si la situation décrite par  l'expression est fausse, alors on passe directement à `[suite]`.   
  La branche `true` est ignorée.

On peut considérer la condition simple comme un cas particulier de l'alternative:

```c++
if (expression bool)
{
	// branche `true`
}  
else
{
  // rien!
}
// [suite]
```

Un `if` sans `else` pourra typiquement être employé pour tester une pre-condition sur des données, avant de faire un calcul, pour vérifier certaines hypotheses. Par exemple, qu'une variable est non nulle avant de faire une division.



## Sélection `if`, `else-if`, `else`

Une sélection permet de choisir parmi plusieurs cas possibles lequel correspond à la situation qui se présente et de réaliser l'action qui lui correspond.

```c++
if (expression bool 1)
{
    // bloc d'instructions (branche 1)
}
else if (expression bool 2)
{
    // bloc d'instructions (branche 2)
}
...
else
{
    // bloc d'instructions (branche n)
}
// [suite]
```

L'instruction fait une **analyse par cas**:

- Si la situation correspond à celle décrite par l'une des expressions,
  l'action de la branche correspondante est réalisée puis on passe à la `[suite]`.
- Tous les cas sont **mutuellement exclusifs** de fait: 
  l'expression 2 ne sera testée que si l'expression 1 est fausse (et donc dans ce cas la branche 1 est ignorée), et ainsi de suite.
- Le dernier cas (celui de la branche `n`) est en général le cas par défaut. 



> **Exemple**:  cf.  [`exemples/selection.cpp`](exemples/selection.cpp)
>
> ```c++
> if (x > 0)
>   	std::cout << "x est positif" << std::endl;
> else if (x < 0)  
>   	std::cout << "x est négatif" << std::endl;
> else 
>   	std::cout << "x est nul" << std::endl;
> ```
>



On peut voir cette instruction comme un raccourci syntaxique pour des alternatives (`if`, `else`) imbriquées.

> **Exemple**:   La forme imbriquée de l'exemple précédent est:
>
> ```c++
> if (x > 0)
> 	std::cout << "x est positif" << std::endl;
> else 
> {
>   if (x < 0)  
> 		std::cout << "x est négatif" << std::endl;
> 	else 
> 		std::cout << "x est nul" << std::endl;
> }
> ```
>
> Encore un fois, les accolades sont facultatives.



En effet, chaque nouveau cas va ajouter un niveau d'imbrication supplémentaire. Si il y a de nombreux cas, le code deviendra difficile à lire.



## Sélection `switch`

Lorsque la condition est une comparaison d'égalité entre 2 valeurs entières, on peut utiliser une instruction `switch() {}`, plutôt qu'une sélection par suite d'instructions `if {}` `else if {}`.

La forme de cette instruction conditionnelle est la suivante:

```c++
switch (expression)
{
    case v1 :
        // bloc d'instructions (branche 1)
        break;
    case v2 :
         // bloc d'instructions (branche 2)
         break;
    ...
    case vn :
         // bloc d'instructions (branche n)
        break;
    default :
        // bloc d'instructions par défault
}
// [suite]
```

L'expression qui suit le mot clé `switch` (entre `()`) est évaluée. Elle doit être d'un type **entier**.
Le résultat de cette évaluation est alors comparée à chacune des valeurs entières `v1`, `v2`,..., `vn`  qui suivent les `case`, dans l'ordre.

À la première valeur `vi` égale à l'expression, le bloc d'instruction suivant est exécuté, jusqu'au `break`. Puis le contrôle passe à l'instruction suivant le `switch`  (ligne `[suite]`).

Si aucune des valeurs `v1`, `v2`,..., `vn`  n'est égale à l'expression, le bloc suivant `default` est exécuté, 
dans le cas où le label `default` est présent (il n'est pas obligatoire). En l'absence de `default`, le contrôle passe directement à `[suite]`.

Attention, si `break` n'est pas présent, toutes les instructions suivant le bloc sélectionné (après les `case` ou `default`)  jusqu'à `[suite]` sont exécutées!



L'instruction suivante

```c++
switch (x)
{
    case v1 :
        // bloc d'instructions (branche 1)
        break;
    case v2 :
         // bloc d'instructions (branche 2)
         break;
    ...
    case vn :
         // bloc d'instructions (branche n)
        break;
    default :
        // bloc d'instructions par défault
}
```

est équivalente à (quand `x` est une variable entiere):

```c++
if (x == v1)
{
   // bloc d'instructions (branche 1)
}
else if (x == v2)
{
   // bloc d'instructions (branche 2)
} 
  ...
else if (x == vn)
{
   // bloc d'instructions (branche n)
} 
else
{
  // bloc d'instructions par défault  
}
```



> **Exemple**: cf. [`exemples/switch.cpp`](exemples/switch.cpp)
>
> ```c++
> switch (x) 
> {
>   case 1:
>     std::cout << "x est 1";
>     break;
> 
>   case 2:
>     std::cout << "x est 2";
>     break;
> 
>   default:
>     std::cout << "x n'est ni 1 ni 2";
> }
> ```
>
> Noter la présence des `break`; sans eux dans le cas où `x == 1`, les trois messages seront affichés.
>
> L'instruction `switch` ci-dessus est équivalente à
>
> ```c++
> if (x == 1) 
>  	std::cout << "x est 1";
> else if (x == 2)
> 	std::cout << "x est 2";
> else 
> 	std::cout << "x n'est ni 1 ni 2";
> ```
>

