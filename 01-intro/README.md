# Cours de programmation objet

INALCO - Master TAL première année

---

# Cours 01 Introduction
jeudi 19 septembre 2024
Florent Jacquemard
`florent.jacquemard@inria.fr`



**plan**

- généralités sur la programmation et les langages
  les algorithmes...
- exemple de programme C++
  éléments de syntaxe
- environnement de développement
  prise en main des outils



**sources**: 

- cours M1 TAL de M.-Anne Moreaux (INALCO)



## Programme

Un programme est une suite d’instructions destinée à faire réaliser une tâche à un ordinateur.

- **Programme source** : 
  le code écrit par un programmeur dans un *langage de programmation*,
  enregistré dans un fichier au format « texte brut ».
  Le language de programmation est lisible et compréhensible par humain.

- **Programme exécutable** : 
  instructions directement exécutables par la machine (processeur), 
  présentes en mémoire centrale.
  Le langage est compréhensible par le processeur (*language machine*).



## Language machine
Language exécuté directement par un processeur, fait d'instructions élémentaires.

Ces instructions peuvent être des types suivants:  

- Copier le contenu d’un emplacement mémoire vers un autre emplacement, 
- Comparer le contenu d’un emplacement mémoire avec une valeur définie, 
- Effectuer les opérations arithmétiques élémentaires,
- Envoyer ou recevoir des données depuis un périphérique

Chaque modèle de processeur possède son propre langage = jeu d'instructions, 
par exemple:

- `x86`, `AMD64`
- `ARM`
- `RISC-V`



## Languages de haut niveau
Un langage de programmation à haut niveau d'abstraction, est orienté vers les problème à résoudre,
faisant *abstraction*  du matériel sur lequel il est exécuté (instruction du processeur et gestion de la mémoire).

- en général, indépendants de la machine, 
- peut parfois contenir des instructions propres à un système particulier.



**Architecture Linux**

​                                                                  (le haut niveau)

![width:50px height:50px](img/IBM-linux-th-562146794.jpeg)

​                                                                   (le bas niveau)



## Languages de bas niveau
Un langage de programmation est dit de bas niveau lorsqu'il offre peu d'abstraction par rapport au language machine.

Ces langages sont adaptés à l'écriture de logiciels **critiques** en terme de **performances**:

- informatique embarquée
- pilotes (*drivers*)
- systèmes exploitation (*OS*)
- moteur de jeux vidéos

Langage `assembleur` (`ASM`): très proche d'un langage machine, mais les programmes peuvent être écrits par des humains.
Ils contiennent une version symbolique des instructions, ainsi que des constantes, commentaires, étiquettes, macros...



## Gestion de la mémoire

Les langages `C` et `C++` ont des caratéristiques à la fois de languages de haut niveau et bas niveau.

- ils sont (relativement) indépendants de la machine 
  (le même code peut être compilé sur différentes machines)
- mais la **mémoire** est gérée explicitement par le programmeur, qui doit l'allouer (comme ressource) et penser à la libérer lorsqu’elle n’est plus utilisée, ce qui implique toute une réflexion sur la durée de vie des entités du programme.

La mémoire est en effet une partie essentielle du matériel sur lequel un programme est executé.
Sa gestion en tant que ressource est plus ou moins automatisée suivant les langages de programmation.



   (le haut niveau)

| gestion de la mémoire |  langages |
|-----------------------|-----------|
| automatique (GC) | `C#`, `Java`,  `Python`, `Ruby`, `LISP` |
| semi-automatique (ownership, ARC) | `Rust`, `Objective-C`, `Swift` |
| manuelle              | `C`, `C++` |

   (le bas niveau)



Mécanismes de gestion de la mémoire:

- GC: Garbage Collection (périodique)
- ARC: automatic reference counting
  (détection automatique et libération des objets inutiles)



## Langages compilés
Afin d'être exécuté sur une machine, un programme source est converti en langage machine, par un *compilateur*.

![width:100px height:10px](img/compilo.png)

Une fois le programme compilé en **exécutable** (code machine), ce dernier peut être exécuté directement par le système d'exploitation. 

L'exécutable n'est pas **portable** (il est réservé à l'architecture matérielle de la machine **cible**), contrairement au code source. Pour executer sur une autre machine, il faut recompiler.

La compilation prend un certain temps, mais ce temps est ensuite "économisé" à l'exécution: le compilateur optimise le code machine pour qu'il soit efficace.

Languages compilés:

- `C`, `C++`
- `Objective-C`
- `Swift`
- `Rust`



voir:  [Compiler Explorer](https://godbolt.org/)  https://godbolt.org/



## Langages interprétés

Le programme source est exécuté directement par un *interpreteur* (sans phase de compilation).

![](img/interpreter.png)

L'interpréteur fait souvent une traduction du programme source dans une représentation intermédiaire (*code objet*), immédiatement exécuté (*Read-Eval-Print-Loop*) 

- `Python`, `Perl`
- `JavaScript`, `PHP`, `Ruby` 
- `LISP`, `OCaml`, `Haskell`
- `MATLAB`, `R`, `Maple`, `Mathematica`

L'usage d'un interpreteur offre plus de **souplesse** pour la programmation mais l'exécution est plus lente que celle de programme machine issu d'un compilateur.

En théorie, tout langage de programmation peut être compilé ou interprété.  
Par exemple, il existe des compilateurs pour `OCaml` et `Haskell`.

Le [classement](https://en.wikipedia.org/wiki/List_of_programming_languages_by_type#Machine_languages) des languages ci-dessus correspond à l'usage le plus courant.



## Machine virtuelle
Il existe aussi des langage **semi-interprétés**: Le programme source est traduit vers une représentation intermédiaire (*bytecode*) qui est interpretée par une une *machine virtuelle* (*runtime*).

![height:10px](img/VM.png)

Le code source comme le bytecode sont portables. En revanche, la machine virtuelle (interprete de bytecode) est propre à l'architecture d'une machine cible.

Langages semi-interprétés:

- `Java` (JVM)
- `Scala`
- `OCaml`
- `CPython` 



## Languages spécialisés
`C++` est considéré comme un langage **généraliste**.

Certains autres langages sont dédiés à un **usage** spécifique (DSL Domain Specific Language)

- `PHP`, `Javascript` (Web dynamique)
- `arduino` (pédagogique électronique, au-dessus de `C` / `C++`)
- `Esterel`, `Lustre` (synchrone, pour systèmes réactifs / embarqués)
- `MATLAB`, `R` (calcul numérique, statistiques)



## Langage orienté objet

`C++` est un langage de programmation orienté objet, qui une méthode (**paradigme**) de programmation centré sur une notion d'objets et leurs interactions. D'autre langages orienté objet sont `Java`, `Python`, `C#`, `Swift`, `Scala`, `JavaScript`...

Cela dit, `C++` n’est pas un langage purement orienté-objet: on peut y définir des fonctions en dehors d'objets (fonctions *libres*), voire même écrire des programmes complets sans définir d'objets (on parle de programmation impérative). C'est d'ailleurs ce que nous ferons durant tout le premier semestre!



## Bref historique `C++`

- **1972 : Langage C**  
  AT&T Bell Labs
- **1983 : Objective C**   
  NeXt puis Apple  
  Extension objet du C.
  Syntaxe inhabituelle inspirée de Smalltalk
- **1985 : C++**  
  AT&T Bell Labs   
  Extension object du C par Bjarne Stroustrup 
- **1991 : Python**  
  Guido van Rossum (CWI)   
  Vise simplicité et rapidité d'écriture  
  Interprété, typage dynamique (= à l'exécution) 
- **1995 : Java**  
  Sun Microsystems, puis Oracle  
  Simplification du C++, purement objet  
  Egalement inspiré de Smalltalk, ADA, etc.  
- **2001: C#**  
  Microsoft  
  Originellement proche de Java pour .NET  
  Egalement inspiré de C++, Delphi, etc.
- **2009: Go**
  Google
  langage pour la programmation système influencé par `C`
- **2010: Rust**   
  Mozilla   (pour le moteur `Gecko` de Firefox)    
  proche de `C` and `C++` avec focus sécurité et performance  
- **2011: C++11**    
  Consortium international ISO   
  Révision majeure, suivie de `C++14`, `C++17`, `C++20`,  `C++23`, `C++26` ...   
- **2014: Swift**   
  Apple  
  Successeur d'Objective C  
- **2022 Carbon**  
  Google  
  expérimental. "successeur de C++"



## Classement des langages par popularité 

*e.g.*  liste IEEE de *Top Programming Languages*

https://spectrum.ieee.org/top-programming-languages-2024

popularité auprès des membres de l'IEEE (2023)

![width:50px height:50px](img/IEEETPL2023-spectrum.png)

demande des employeurs (2023)

![width:50px height:50px](img/IEEETPL2023-jobs.png)





## Classement des langages par frugalité

référence:

> Rui Pereiraa et al.  
> Ranking Programming Languages by Energy Efficiency  
> in Science of Computer Programming, Elsevier
> https://doi.org/10.1016/j.scico.2021.102609

<img src="img/CryptoMode-C-Programming-Languages.png" style="zoom:67%;" />


source: https://greenlab.di.uminho.pt/wp-content/uploads/2017/10/sleFinal.pdf

La gestion très fine de la mémoire, et de composants matériel de la machine, fait de `C++` un langage potentiellement très performant, donc intéressant pour des domaines critiques : systèmes d’exploitation, systèmes embarqués, pilotes, moteurs de recherche, jeux vidéos, moteurs de rendu 3D, *etc*.



## Documentation et standard

De plus, `C++` est un langage très bien documenté: un standard spécifie précisément tout ce qui est supporté ou pas (*undefined behaviour*) par les compilateurs. Cela garantie une certaine assurance aux programmeurs (pas de surprises) et explique aussi la popularité de ce langage.

- https://en.cppreference.com
- https://cplusplus.com/reference/
- standard: https://isocpp.org/std/the-standard

de nombreux autres sources communautaires

- FAQ: https://isocpp.org/faq

- guidelines: https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines

  

## Programmation
C'est l'art d'écrire un programme source, 
qui est l'encodage d'un algorithme dans un langage de programmation. 

Le language de programmation est «intelligible» par l'humain, mais 
le programme source doit aussi être «lisible» par le système informatique. 
Sa **syntaxe** est donc définie formellement (par une grammaire).

La programmation se fait généralement en plusieurs étapes.

**Conception et écriture du programme** :

- Définir un **algorithme**
  - spécifier les données et les résultats attendus
  - définir la structure logique de la solution 
    sans se préoccuper de contraintes techniques

> ex. écriture d'un discour (idées fortes, enchainement, trame... vs police, mise en page)

- Écrire un texte **encodant** l’algorithme dans un certain langage de programmation.  
  Ce texte est le *programme source*.
- Transformer ce programme source en programme exécutable. 
  via un outil: *interpreteur* ou *compilateur*.
- Faire fonctionner effectivement son programme. 
  *exécution*.



## Algorithme
structure logique d'une méthode pour la résolution d'un problème.

Le mot algorithme provient du nom latinisé du mathématicien perse Al-Khawarizmi (~ 780- 850).
Le titre de l’un de ses ouvrages ( الكتاب المختصر في حساب الجبر والمقابلة) est à l’origine du mot algèbre. 

![Image-Al-Kitāb_al-muḫtaṣar_fī_ḥisāb_al-ğabr_wa-l-muqābala](/Users/jacquema/Library/Mobile Documents/com~apple~CloudDocs/Enseignement/INALCO/01-intro/img/Image-Al-Kitāb_al-muḫtaṣar_fī_ḥisāb_al-ğabr_wa-l-muqābala.jpg)

Un autre de ses ouvrages a initié la diffusion de l’utilisation des chiffres arabes au Moyen-Orient et en Occident.



## Exemple: Algorithme d'Euclide 
Exemple d'algorithme mathématique pour le calcul du PGCD de 2 entiers naturels non nuls

- diviser le plus grand des deux par le plus petit
- diviser le plus petit par le reste de la division précédente
- recommencer jusqu'à ce que le reste devienne nul
- le PGCD est le dernier reste non nul ou le premier diviseur si le premier reste est nul.



## Algorithme d'Euclide (2)

Il existe plusieurs formats pour présenter les algorithmes.

Autre présentation diagrammatique de l'algorithme d'Euclide:

![](img/pgcd.png)



## Algorithme d'Euclide (3)

autre présentation, 
dans une syntaxe proche d'un langage de programmation.

```cs
euclide(a, b)
  tant que b ≠ 0
     t := b; 
     b := a modulo b; 
     a := t; 
retourner a
```



## Algorithme (2)

Un algorithme est la spécification d'une méthode de résolution décrite en terme d'une suite finie de règles présentée dans un ordre déterminé qui est l'ordre d'exécution.  
L'exécution de cette succession d'étapes doit se faire en un temps fini.

Formalisation de la notion d'algorithme :
Une suite d'actions que devra effectuer un automate pour parvenir en un temps fini à un résultat déterminé par
- une précondition, spécifiée par l'état des données au début
- une postcondition, spécifiée par l'état des données à la fin

La suite d'opérations sera composée d'actions élémentaires, exprimées par les instructions. Ces actions élémentaires sont celles que l’automate peut réaliser.



## Algorithme (3)

Un algorithme constitue une méthode de résolution d'un problème. Il ne doit donc pas être conçu pour obtenir un résultat sur une donnée particulière, mais pour obtenir le résultat représentant la solution du problème sur n'importe quelles données de même type.

Il peut exister différents algorithmes pour parvenir à la résolution d'un problème donné. 
Il faut ensuite les évaluer selon leurs qualités (temps ou espace de calcul utilisé - ***complexité***)

Certains problèmes n'ont pas de solutions algorithmiques. Il est donc nécessaire de déterminer théoriquement s'il existe une solution calculable pour le problème que l'on cherche à résoudre.
En 1936, Alan Turing étudie les liens entre la notion d’algorithme et celle de raisonnement mathématique. Cela l’a conduit a imaginé un procédé de calcul, la machine de Turing, et à suggérer que ce procédé peut être universel, c’est-à-dire capable d’exécuter tous les algorithmes possibles.



## Programmation (suite)

Écrire un programme qui fonctionnera effectivement suppose de

- connaître le langage de programmation utilisé 
  (lexique, syntaxe, sémantique)
- connaître son environnement de programmation 
  et sa mise en œuvre dans le système d'exploitation utilisé
  - où et comment sont rangés les fichiers : code source, données, résultats
  - quelles sont les commandes à utiliser
    - pour vérifier l'organisation de ses fichiers
    - pour transformer le code source en code exécutable
- savoir/comprendre comment s'articule le programme et le système d'exploitation.



## Vérification, correction

Une fois le programme source écrit et exécuté,  le travail n'est malheureusement pas terminé.

- tester (exécution avec données d'entrée bien choisies)
- corriger quand le test n'est pas concluant 
  difficulté: faire le lien entre le code source et le code (machine) réellement éxécuté 
  via un *debuggeur*
- améliorer (gestion mémoire, performances...)  
  analyse statique (*linter*, *méthodes formelles*) ou dynamique (*profiler*...)



## Collaboration et Ré-utiliation

Modifier, améliorer, compléter un programme écrit par d'autres.

- Être capable de lire, c'est-à-dire de comprendre du code source.
  Déterminer quelle est la structure logique de l'algorithme exprimé par ce code.
- Être capable d'écrire du code source lisible,  
  par d'autres ou par soit-même plus tard...

  - adoption d'un style (*styleguide*), 
    exemple: https://google.github.io/styleguide/cppguide.html
  - documentation de code (commentaires)
  - spécification (formelle)

- écrire du code évolutif et réutilisable
  → programmation orientée objet



## Bonjour l'exemple 

exemple *hello world* : 
programme imprimant un message sur la sortie standard 
la sortie standard est l'écran                                        


le code source suivant est disponible dans : `exemples/bonjour.cpp` 

```cs
/* 
 * Bonjour 
 * Exercice de compilation
 */

#include <iostream>

int main(void)
{
    std::cout << "Bonjour!" << std::endl;
    return 0;
} // main
```



## Contenu du programme source

```cpp
/* 
 * Bonjour 
 * Exercice de compilation
 */

#include <iostream>

int main()
{
    std::cout << "Bonjour" << std::endl;
    return 0;
} // main
```

- le texte entre `/*` et `*/`est un *commentaire*.
  Il est ignoré par le compilateur.  
  
  Il en est de même du texte après `//` et jusqu'à la fin de la ligne 
  Les lignes vides aussi sont ignorées.
  
- avec `#include <iostream>`
  le préprocesseur recopie le contenu du fichier d'entête (*header*) `iostream.hpp` de la bibliothèque standard.
  Ce fichier spécifie les objets de la librairie que l'on a le droit d'utiliser dans le code source, 
  par exemple ici `std::cout`,    `std::endl` et l'opérateur `<<`  (décrits plus bas).

```g++ -E bonjour.cpp```
affiche le nouveau fichier source obtenu par précompilation.
C'est ce fichier qui est ensuite compilé.

- `int main()` est la déclaration d'une fonction, 
  essentiellement un bloc de code auquel on a donné le nom `main`.

  - son type de retour est `int` (entiers)
  - son nom `main`
  - elle a 0 paramètre (entre les `()`)

- `main` a un rôle particulier en `C` et `C++`:
  c'est cette fonction qui est appelée quand le programme est exécuté
  (quelle que soit sa position dans le code).

- entre `{` et `}` se trouve la définition de la fonction `main`.
  De manière générale, les  `{` et `}` sont utilisées pour délimiter un bloc de code.

- `std::cout << "Bonjour" << std::endl;` est une déclaration (expression produisant un résultat).
  - `std::cout` est un *flot de sortie* (*flux*), pouvant afficher des caractères (*character output device*).
    en général la fenêtre du terminal.
    Il est défini dans la bibliothèque `iostream` qui a été incluse.
  - `<<`  est un *opérateur* binaire qui envoie ce qui est à sa droite sur le flot qui est à sa gauche.
  - `"Bonjour"` est une *chaîne de caractères* (type `string`).
  - `std::endl` est un *manipulateur* qui insère (dans le flot de sortie) un caractère de retour à la ligne et force l'affichage (*flush*).
  
- le préfixe `std::` fait référence à un espace de nom (**namespace**) pour caractériser des éléments de la librairie standard. `cout` est un symbole (objet) défini dans cette librairie standard.
  Le but des espace de noms explicite est d'éviter les conflits de nom: lorsque plusieurs librairie définissent des objets différents avec le même nom. 



## Compilation C++ 
Construction du programme exécutable.

dans un terminal:

```> g++ -c bonjour.cpp```

- précompilation et 
- création d'un fichier objet `bonjour.o`
  à partir du fichier source `bonjour.cpp`

```> g++ bonjour.o```

- éditeur de liens qui intégre `bonjour.o` avec d'autres fichiers object de la bibliothèque standard.
- création du programme exécutable `a.out`  

```> ./a.out```

- lancement de l'execution du programme executable  `a.out`  
  (affichage dans le terminal)



**nommage de l'executable**: 
à la deuxième étape, l'option `-o` permet de donner un nom à l'exécutable.  

```> g++ -c bonjour.cpp```

```> g++ -o bonjour bonjour.o```  ou   ```> g++ bonjour.o -o bonjour```
l'executable s'appelle `bonjour`

 ```> ./bonjour```  



**raccourci**: 

lorsqu'on a un seul fichier source comme ici, on peut appeler la compilation et l'édition de liens en une seule commande.

```> g++ -o bonjour bonjour.cpp```  ou  ```> g++ bonjour.cpp -o bonjour```

 ```> ./bonjour```  

ou avec le nom d'executable par défaut (`a.out`)

```> g++ bonjour.cpp```

 ```> ./a.out```  



**autres options**:

- `-std=c++11` ou `c++11` ou `c++14` ou `c++17` ou `c++20` ou `c++23` ou `c++26`
  standard C++ (numéro de révision majeure du langage)
- `-Wall`  `-W`  : affichage de messages de warning
- `-Werror` : transforme les warnings en erreurs
  donc tant qu’il y a des warnings, le programme ne compile pas
- `-O3` optimisation (le code produit sera plus rapide)
- `--help`
- `-g` : ajout d'information de debuggage à l'executable



## Code Objet

Le but du compilateur est de produire du langage machine (executable par le processeur).

Le code objet est un code intermédiaire, composé 

- du code machine,
- d'annotations permettant d'identifier des *blocs* de code extérieurs
  (table de symboles).

Les bibliothèques contiennent des fichiers (*modules*) de code objet.
Le fichier `bonjour.o` produit ci-dessus par `g++ -c bonjour.cpp` contient aussi du code objet.

Les noms des blocs de code (dans les annotations) sont spécifiés dans des fichiers d'entête (*header*, `.h`).

On ajoute donc le contenu de fichiers d'entête avec `#include` pour pouvoir utiliser dans le code source des fonctionnalités définies ailleurs, par ailleurs dans des librairies (comme `iostream` dans l'exemple Bonjour).



## Édition de liens

Différents fichiers objet (`.o`) peuvent être combinés en un seul executable par un éditeur de lien (*linker*).
Les modules de bibliothèques (*libraries*) sont aussi intégrés à l'executable. L'intégration des différents fichier objets par le *linker* se fait grace aux annotations.

L'ensemble du processus de compilation, pour un projets avec plusieurs fichier sources (`.cpp`) est donc le suivant:

![width:50px height:50px](img/cpp_separate_compilation_03.png)

En pratique, dans la création d'un executable, il n'est nécessaire de recompiler que les fichiers sources (`.cpp`) qui on été modifiés depuis la dernière compilation. Pour les autres, ainsi que les bibliopthèques, on utilise uniquement les fichiers objets pré-compilés.





## Code de retour de `main`

comme indiqué par sa déclaration `int main()`,  
`main` est une fonction qui retourne une valeur entière.

Cette valeur (par défault 0) est la valeur de retour de la commande dans le shell.  
La valeur de retour de la dernière commande est affichée par  
```> echo $?```

en compilant le programme suivant, on obtient une autre valeur de retour.
```cs
#include <iostream>

int main(void)
{
    std::cout << "Bonjour" << std::endl;
    return 1;
} 
```



##  Environnement de programmation

- un langage de programmation (lexique, syntaxe, sémantique des instructions)
- **éditeur** de code source
  avec coloration syntaxique, 
  complétion automatique de code...
- un outil de traduction en langage machine : 
  **compilateur** ou **interpréteur** selon le langage concerné
- outils d'**automatisation**
  *moteur de production*, cf. `Make`, `CMake`
  ordonnance et pilote les étapes de construction de l'executable
  - preprocessing,
  - compilation,
  - édition de liens,
  - gestion de détection des dépendances
  - configuration suivant la plateforme cible
- un **débugueur**
  et autres outils d'analyse
- outils de recherche et **navigation** de code
  pour les projets composés de nombreux fichiers
- gestion de **versions**
- des **bibliothèques** :
  collections de fonctions déjà sous forme exécutable et prêtes à être utilisées pour réaliser 
  - des opérations courantes, telles que les entrées/sorties ou 
  - des opérations plus spécialisées telle que la manipulation de certains types de nombres)
  
- de la documentation (references)
  - https://en.cppreference.com
  - https://cplusplus.com/reference
  



On peut éditer les programme source avec un éditeur de texte simple.

- linux : `vim`,  `emacs`,  `gedit`...
- macOS : `TextEdit`

- ou directement dans le terminal : `vi`, `nano`

et compiler avec `g++` puis lancer l'exécutable dans le terminal.



## IDE

**IDE** = environnement de programmation intégré :
une application intégrant les différents outils d'un environnement de programmation.

cf. [`../configuration/README.md`](../configuration/README.md)

exemples:

- `VS Code` (Microsoft) (avec extension `C++`)
- `OnlineGDB` (en ligne)
- `XCode` (Apple)
- `Eclipse`
- `Geany`
- `Code::Blocks`
- `QTCreator`

## Références

cf. [`../references/README.md`](../references/README.md)

