## Quelque erreurs courantes

en compilation `C++`



Reprendre le programme de `bonjour.cpp`

et essayer de le compiler avec chacune des modifications suivantes.

On analysera les messages du compilateur.



- C++ est un langage sensible à la casse
  - changer `cout` par `Cout`
  - changer `main` par `Main`
    il est obligatoire d'avoir une fonction `main`  dans tout programme
- point virgule
  - enlever un point virgule
- guillemets
  - remplacer les guillements doubles `"`  pas des guillements simples `'`
     `'` est pour les caractères, `"` est pour les chaînes de caractères

- opérateurs de flux : il existe deux opérateurs de flux, attention à la direction!
  - décomposer la ligne d'affichage en 2 lignes (cf. `bonjour2.cpp`)
  - inverser `<<` en `>>`
    problème de typage: 
    `cout` est un flux de sortie (`ostream`)



