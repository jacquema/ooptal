# Programmation orientée objet C++ 

# M1 TAL INALCO 



## Références

- The C++ Programming Language (Le langage C++)   
  Bjarne Stroustrup (auteur du C++)  
  Addison-Wesley
  http://www.stroustrup.com 
- C++ Coding Standards (Standards de programmation en C++)  
  101 Rules, Guidelines, and Best Practices (C++ In-Depth)
  Herb Suttler, Andrei Alexandrescu  
  Pearson Education

**cours**

- Initiation à la programmation en C++
  Jean-Cédric Chappelier, Jamila Sam, Vincent Lepetit
  [BOOC EPFL](https://www.epflpress.org/product/805/9782889143962/initiation-a-la-programmation-en-c)

- Introduction à la programmation orientée objet en C++

  Jean-Cédric Chappelier, Jamila Sam, Vincent Lepetit
  [BOOC EPFL](https://www.epflpress.org/product/814/9782889143993/introduction-a-la-programmation-orientee-objet-en-c)

- http://casteyde.christian.free.fr/

**manuels de référence**

- http://cppreference.com
- http://www.cplusplus.com/reference

**aide, FAQs**

- https://stackoverflow.com 
- https://isocpp.org/faq 

**avancé**

- Effective Modern C++ (Programmer efficacement en C++)
  Scott Meyers
  O'Reilly
