// Dans une boucle, demander à l'utilisateur d'entrer 10 entiers,
// puis afficher leur somme et leur moyenne.
 
 #include <iostream>

int main()
{
  const int NB = 10; // on fixe ici le nombre d'entiers
  double sum = 0;

  for (int i = 0; i < NB; ++i) 
  {
    int n; // variable locale à la boucle
    std::cout << "entier " << i << ": ";
    std::cin >> n;
    sum += n;
  }
  std::cout << "somme:   " << sum << std::endl;
  std::cout << "moyenne: " << (sum / NB) << std::endl;
  return 0;
}