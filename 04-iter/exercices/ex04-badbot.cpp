
#include <iostream>
#include <string>

int main ()
{

  while (true) // boucle d'interaction
  {
	std::string str;

    std::cout << "quoi? ";
    std::cin >> str; 

	// analyse de cas de str
    if (str == "Bonjour" || str == "bonjour")
    	std::cout << "au revoir" << std::endl;
    else if (str == "STOP") // sortie de boucle
    	break;
	else // réponse par défaut
		std::cout << "je ne comprend pas: " << str << std::endl;
  } 
  return 0;
}
