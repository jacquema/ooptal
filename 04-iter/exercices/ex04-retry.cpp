// Demander à l'utilisateur d'entrer un entier `N` entre `0` et `20`, bornes incluses, et afficher N+2.  
// Si une valeur erronée est entrée, on renouvelera la demande, jusqu'à obtenir une valeur correcte (dans les bornes).

#include<iostream>
 
int main()
{
    int n; // non initialisé

    do {
        std::cout << "entrer un entier entre 0 et 20: ";
        std::cin >> n;
    } while (n < 0 || 20 < n);

    std::cout << "voila: " << n + 2 << std::endl;
    return 0;
}