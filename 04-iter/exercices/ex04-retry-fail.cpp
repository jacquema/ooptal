// Demander à l'utilisateur d'entrer un entier `N` entre `0` et `20`, bornes incluses, et afficher N+2.  
// Si une valeur erronée est entrée, on renouvelera la demande, jusqu'à obtenir une valeur correcte (dans les bornes).

#include<iostream>
 
int main()
{
 	int n; // pas initialisée

    while (true) 
    {
        std::cout << "entrer un entier entre 0 et 20: ";
        std::cin >> n;

        if (std::cin.fail()) 
        {
           std::cout<<"pas un entier!" << std::endl;
			// https://stackoverflow.com/questions/5131647/why-would-we-call-cin-clear-and-cin-ignore-after-reading-input
			// https://www.geeksforgeeks.org/how-to-use-cin-fail-method-in-cpp/
            // remet cin dans son état normal
            std::cin.clear();
            // vide le buffer de saisie
            std::cin.ignore(10000,'\n');       
            //std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');       } 
		}
        else if (0 <= n && n <= 20)
            break;
        else
           std::cout<<"j'ai dit entre 0 et 20" << std::endl;
    }

    std::cout << "voila: " << n + 2 << std::endl;

    return 0;
}



