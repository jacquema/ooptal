// Dans une boucle, demander à l'utilisateur d'entrer 10 entiers, 
// puis afficher le plus petit d'entre eux.
 
 #include <iostream>

int main()
{
  const int NB = 10; // on fixe ici le nombre d'entiers
  int nmin;          // non initialisé : sa valeur est inderminée

  for (int i = 0; i < NB; ++i) 
  {
    int n; // variable locale à la boucle
    std::cout << "entier " << i << ": ";
    std::cin >> n;
    if ((i == 0) || (n < nmin)) // (n < nmin) n'est evalué que si (i != 0)
		  nmin = n;

	// variante equivalente
    // if (i == 0) 
	// 	 nmin = n;
    // else if (n < nmin) 
	// 	 nmin = n;
  }
  // invariant de boucle: 
  // nmin est le plus petit entier rencontré jusqu'ici

  std::cout << "plus petit: " << nmin << std::endl;
  return 0;
}