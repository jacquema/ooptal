#include <iostream>

int main()
{
	for (int j = 1; j < 10; ++j)     // j: numéro de ligne
	{
		for (int i = 1; i < 10; ++i) // i: numéro de colonne
		{
			std::cout << i;
		}
		std::cout << std::endl;
	}
	return 0;
}