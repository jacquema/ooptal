# Exercices - cours 04

## Instructions itératives



>  **Exercice 1**.  
>  Écrire "Bonjour" (avec retour à la ligne) 10 fois avec une boucle `for` .
>
>  Même question avec une boucle  `while`. 
>
>  Ajouter un délai de 1 seconde à chaque itération  à l'aide de  l'instruction `sleep_for`
>  https://cplusplus.com/reference/thread/this_thread/sleep_for



>  **Exercice 2**.  (moyenne)
>  Dans une boucle, demander à l'utilisateur d'entrer 10 entiers,
>  puis afficher leur somme et leur moyenne.



>  **Exercice 3**.  (min)
>  Dans une boucle, demander à l'utilisateur d'entrer 10 entiers,
>  puis afficher le plus petit d'entre eux.



> **Exercice 4**.  (retry)
> Demander à l'utilisateur d'entrer un entier `N` entre `0` et `20`, bornes incluses, et afficher N+2.  
> Si une valeur erronée est entrée, on renouvelera la demande, jusqu'à obtenir une valeur correcte (dans les bornes).



> **Exercice 5**. (boucles imbriquées)
>
> 1. Écrivez un programme qui affiche les valeurs 1 à 9 en ligne, à l'aide d'une boucle for: 
>
> ```
> 123456789
> ```
>
> 2. Modifiez le programme pour qu'il affiche 9 lignes similaires, à l'aide de 2 boucles for imbriquées:
>
> ``` 
> 123456789 
> ...
> 123456789
> ```
>
> 3. Modifier le programme pour qu'il affiche un triangle:
>
> ```
> 1
> 12
> 123
> 1234
> 12345
> 123456
> 1234567
> 12345678
> 123456789
> ```
>
> 4. Modifiez une dernière fois votre programme, pour qu'il affiche une pyramide inversée:
>
> ```
>         1 
>        12 
>       123 
>      1234 
>     12345 
>    123456 
>   1234567 
>  12345678 
> 123456789
> ```



>  **Exercice 6**. bot à bonne mémoire.  
>  Demander en boucle à l'utilisateur d'entrer un prénom, 
>  et lui répondre `"Bonjour "` suivi de tous les prénoms entrés jusqu'ici.
>  On arrêtera la boucle quand l'utilisateur a entré la chaîne `"STOP"`.
>  On utilisera la concaténation de chaîne de caractères (`std::string`) avec l'opérateur binaire `+`.
>  https://cplusplus.com/reference/string/string/operator+



> **Exercice 7**. bad bot (robot de dialogue grossier)
>
> Se mettre en attente d'une entrée de l'utilisateur.
>
> - si l'utilisateur a écrit "bonjour", lui répondre "au revoir".
> - si l'utilisateur a écrit "STOP", terminer le programme.
> - si l'utilisateur a écrit une autre ligne non vide, lui répondre un message d'erreur.



> **Exercice 8**.  (boucle sur rang)
> Déterminer si une chaîne de caractères donnée contient la lettre
> `e`, à l'aide d'une boucle `for` sur rang  `std::string`.
>
> L'énumération sur rang est une extension de C++11, con compilera avec l'option `-std=c++11`.



> **Exercice 9**. **DEVOIR À LA MAISON**
>
> Écrire un programme qui affiche la table de multiplication des entiers de 1 à 10.
>
> On pourra perfectionner les alignements dans la table avec `std::setw` de la bibliothèque `iomanip`.
> voir _e.g._ https://cplusplus.com/reference/iomanip/setw



> **Exercice 10**. 
> Coder en `C++` l'algorithme d'Euclide pour le calcul du plus grand diviseur commun de deux entiers naturels non-nuls, présenté au premier cours.  Dans le programme, 
>
> - les deux entiers seront saisis par l'utilisateur, 
> - il sera vérifié qu'ils sont strictement positifs,  
>   dans le cas contraire, le programme terminera avec un message d'erreur,
> - le PGCD sera affiché.



>  **Exercice 11**. **DEVOIR À LA MAISON**
>
> Écrire une programme affichant toutes les manières possibles d’obtenir un euro avec des pièces de 2, 5, 10, 20 et 50 cents, en comptant combien de possibilités ont été trouvées.
> On pourra procéder par force brute, en énumérant (et testant) toutes les combinaisons de pièces (combien de 2c, de 5c, *etc*) pertinentes.


