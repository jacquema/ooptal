// TD 04 exercice 9
// Écrire un programme qui affiche la table de multiplication des entiers de 1 à 10.
// On pourra perfectionner les alignements dans la table avec `std::setw` de la bibliothèque `iomanip`.
// voir _e.g._ https://cplusplus.com/reference/iomanip/setw

#include <iostream>
#include <iomanip>


int main()
{
    for (int i = 1; i <= 10; ++i)
    {
        for (int j = 1; j <= 10; ++j)
          	std::cout << std::setw(3) << i*j << " ";       
        std::cout << std::endl;
    }
	return(0);
}
