#include <iostream>

int main()
{
	for (int j = 1; j < 10; ++j)      // j: numéro de ligne
	{
		for (int i = 1; i <= j; ++i)  // i: numéro de ligne
		{
			std::cout << i;
		}
		std::cout << std::endl;
	}
	return 0;
}