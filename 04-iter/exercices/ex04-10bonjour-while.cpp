// Écrire "Bonjour" (avec retour à la ligne) 10 fois.

#include <iostream>
#include <thread>    // std::this_thread::sleep_for
#include <chrono>    // std::chrono::seconds

int main()
{
	int n = 0;

	while (n < 10) 
	{
		std::cout << "Bonjour" << std::endl;
		//std::this_thread::sleep_for (std::chrono::seconds(1));
		++n;
	}

	std::cout << n << std::endl;
	return 0;
}