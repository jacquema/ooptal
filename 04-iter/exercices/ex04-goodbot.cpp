// Demander en boucle à l'utilisateur d'entrer un prénom, 
// et lui répondre "Bonjour " suivi de tous les prénoms entrés jusqu'ici. 
// On arrêtera la boucle quand l'utilisateur a entré la chaîne "STOP". 
// On utilisera la concaténation de chaîne de caractères avec l'opérateur binaire +.
 
#include <iostream>
#include <string>

int main ()
{
	std::string all;

	while (true)
  	{
		std::string str; // variable locale
		std::cout << "Comment t'appelles tu? ";
		std::cin >> str;

		if (str == "STOP") // sortie de boucle
			break;
		else
		{
			all += str; // all = all + str; 
			all += ' '; 
			std::cout << "Bonjour " << all << std::endl;
		}
  }
  std::cout << "Au revoir" << std::endl;
  return 0;
}