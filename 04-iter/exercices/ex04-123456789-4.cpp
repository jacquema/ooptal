#include <iostream>

int main()
{
	for (int j = 1; j < 10; ++j)
	{
		for (int i = 1; i <= 9-j; ++i)
			std::cout << ' ';

		for (int i = 1; i <= j; ++i)
			std::cout << i;
		std::cout << std::endl;
	}
	return 0;
}