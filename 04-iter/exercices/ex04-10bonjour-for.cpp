// Écrire "Bonjour" (avec retour à la ligne) 10 fois.

#include <iostream>
#include <thread>    // std::this_thread::sleep_for
#include <chrono>    // std::chrono::seconds

int main()
{
	// nest une variable locale à la boucle (sa portée est limitée au for)
	for (unsigned int n = 0; n < 10; ++n) 
	{
		std::cout << "Bonjour" << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	return 0;
}