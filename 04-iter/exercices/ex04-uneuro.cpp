// TD 04 exercice 11
// Écrire une programme affichant toutes les manières possibles d’obtenir un euro 
// avec des pièces de 2, 5, 10, 20 et 50 cents, 
// en comptant combien de possibilités ont été trouvées.

#include <iostream>

int main()
{
    int nb = 0;  // compteur du nombre de façons de faire 1 euro
    // int n50;     // nombre de pièces de 50 centimes
    // int n20;     // nombre de pièces de 20 centimes
    // int n10;     // nombre de pièces de 10 centimes
    // int n5;      // nombre de pièces de 5 centimes
    // int n2;      // nombre de pièces de 2 centimes

    for (int n50 = 0; n50 <= 2; ++n50)
		for (int n20 = 0; n20 <= 5; ++n20)
			for (int n10 = 0; n10 <= 10; ++n10)
				for (int n5 = 0; n5 <= 20; ++n5)
					for (int n2 = 0; n2 <= 50; ++n2)
						if ( 2*n2 + 5*n5 + 10*n10 + 20*n20 + 50*n50 == 100)
						{ 
							nb++;
							std::cout << "1 euro = "; 
							if (n50 > 0) std::cout << n50 << "*50c "; 
							if (n20 > 0) std::cout << n20 << "*20c "; 
							if (n10 > 0) std::cout << n10 << "*10c "; 
							if (n5 > 0)  std::cout << n5  << "*5c "; 
							if (n2 > 0)  std::cout << n2  << "*2c ";
							std::cout << std::endl;
						}
    std::cout << "En tout, il y a " << nb << " façons de faire 1 euro" << std::endl;
	return 0;
}
