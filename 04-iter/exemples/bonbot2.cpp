#include <iostream>
#include <string>

int main ()
{
  std::string str;

  while (true)
  {
    std::cout << "Comment t'appelles tu? ";
    std::cin >> str; 
    if (str == "Bye")
      break;
    else 
      std::cout << "Bonjour " << str << std::endl;
  } 
  return 0;
}
