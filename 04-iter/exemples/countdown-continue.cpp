#include <iostream>

int main()
{
	for (int n = 10; n > 0; --n) 
	{    
		if ((n == 8) || (n == 4))
		continue;

		std::cout << n << ", ";
	}
	std::cout << "ZERO!" << std::endl;
  return 0;
}
