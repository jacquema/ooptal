#include <iostream>
#include <string>

int main ()
{
  std::string str = "Bonjour.";

  for (char c : str)
    std::cout << '(' << c << ')';

  std::cout << std::endl;
}