#include <iostream>       // std::cout, std::endl
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

int main ()
{
	float x = 0.1;
	while (x != 1) 
	{
		std::cout << "x = " << x << std::endl;
		x += 0.1;
		std::this_thread::sleep_for (std::chrono::milliseconds(100));
	}
	return 0;
}