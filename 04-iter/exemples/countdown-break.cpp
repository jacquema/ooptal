#include <iostream>

int main()
{
  for (int n = 10; n > 0; --n) 
  {    
    std::cout << n << ", ";
    if (n==3)
    {
      std::cout << "annulation! ";
      break;
    }
  }
  std::cout << "FIN" << std::endl;
  return 0;
}