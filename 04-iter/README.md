# Cours de programmation objet

INALCO - Master TAL première année

---

# Cours 04 instructions itératives

jeudi 10 octobre 2024  
Florent Jacquemard  `florent.jacquemard@inria.fr`

sources: 

- cours de Marie-Anne Moreaux, INALCO
- cours de Christian Casteyde
  - http://casteyde.christian.free.fr/cpp/cours/online/x1100.html (`for`)
  - http://casteyde.christian.free.fr/cpp/cours/online/x1133.html (`while`)
  - http://casteyde.christian.free.fr/cpp/cours/online/x1145.html (`do` `while`)
  - http://casteyde.christian.free.fr/cpp/cours/online/x1199.html (`break`, `continue`)
- https://www.geeksforgeeks.org/cpp-loops/
- https://cplusplus.com/doc/tutorial/control/
- https://hannes.hauswedell.net/post/2019/11/30/range_intro/



---

## Les instructions itératives

Après les instructions conditionnelles, nous voyons un deuxième type d'instructions de contrôle: les *instructions itératives*.

Un ordinateur est a priori destiné à exécuter des tâches répétitives. Pour signifier au processeur qu'une même action – composée d'une ou plusieurs instructions – doit être exécutée plusieurs fois, on utilise une instruction dite d'itération.

Une instruction d'itération, aussi appelée une **boucle**, est composée d'une séquence d'instructions  (*bloc*)  devant être exécutées plusieurs fois dans le même ordre et ceci de manière répétitive.

Le processus de répétition lui-même est appelé une **itération**.

Répéter certaines opérations peut servir à compter, accumuler, rechercher une valeur (maximum ou minimum), énumérer... 

Une **condition** est chargée de contrôler **l’arrêt** de la répétition, restreignant le nombre de répétitions à effectuer. Elle est représentée par une expression booléenne.



> **Exemple**:  pgcd 
> on revient encore à l'algorithme d'Euclide pour le calcul du pgcd présenté au premier cours. 
> Une boucle contient toute les instructions (on remarque le retour à la premiere insruction par la flèche de droite). La condition d'arrêt est le test que le reste est nul.
>
> ![](img/pgcd.png)



Avant de construire une itération puis la programmer sous la forme d'une boucle, il faut connaître

- la ou les opérations à répéter,
- la condition d'arrêt / la condition de continuation.



## Tant que ... répéter

L'instruction `while` permet de faire répéter un bloc d'instructions tant qu'une condition et vraie.

```c++
while (expression bool)
{
   // bloc d'instructions (corps)
}
// [suite]
```

Le bloc d'instructions entre `{` et `}` est appelé **corps** de la boucle.

L'instruction  `while` ci-dessus opère comme suit:

1. elle évalue l'expression booléenne entre `()`  (**condition**)

   - si elle est évaluée à `true`, alors les instructions du corps sont exécutées
     puis on retourne au point 1 pour une nouvelle **itération** de la boucle.

   - si elle évaluée à `false` alors on va au point 2

2. **sortie** de boucle: le programme continue avec la `[suite]`.



> **Exemple**: compte à rebours. 
>
> cf.  [`exemples/countdown-while.cpp`](exemples/countdown-while.cpp)
>
> ```c++
> int n = 10; // compteur de boucle
> 
> while (n > 0) 
> {
> 	std::cout << n << ", ";
>   	--n;     // décrémentation du compteur
> }
> 
> std::cout << "ZERO!" << std::endl;
> ```
>
> Dans le code de l'exemple ci-dessus:
>
> 1. `n` se voit assigner la valeur `10` (à sa déclaration)
> 2. entrée de boucle `while`: 
>    - si l'expression `n > 0` (condition) est vraie, aller en 3.
>    - sinon, aller en 5. (sortie)
> 3. éxécuter les instructions corps de boucle (bloc entre `{` `}` après le `while`):
>    - `n` est affiché
>    - `n` est décrémenté
> 4. retour à 2.
> 5. sortie: continuer le programme après le corps de boucle (*i.e.* après la `}` fermante):
>    - afficher `ZERO` et terminer.
>
> L'affichage obtenu est: 
>
> ```
> 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, ZERO!
> ```



Comme pour `if`, les accolades sont facultatives si le bloc ne contient qu'une instruction.



## Répéter ... tant que

La boucle `do-while` est une variante de l'instruction `while` précédente, dans laquelle l'expression booléenne de condition est évaluée après l'exécution du corps de boucle.
Cela garantie que le corps sera exécuté (itéré) au moins une fois.

```c++
do {
   // bloc d'instructions (corps)
} while (expression bool);
/// [suite]
```

1. exécution des instructions du corps de boucle,
2. évaluation de l'expression booléenne entre `()` (**condition**)
   - si elle est évaluée à `true`, 
     alors retour au point 1 (pour une nouvelle **itération** de la boucle).
   - si elle évaluée à `false`, 
     alors aller au point 3. 
3. **sortie** de boucle: le programme continue avec la `[suite]`.




> **Exemple**: bot poli.  
>
> cf.  [`exemples/bonbot.cpp`](exemples/bonbot.cpp)
>
> ```c++
> #include <iostream>
> #include <string>
> 
> int main ()
> {
>     std::string str;
>     do 
>     {
>         std::cout << "Comment t'appelles tu? ";
>         std::cin >> str; 
>         std::cout << "Bonjour " << str << std::endl;
>     } while (str != "STOP");
>     return 0;
> }
> ```
>
> Le type `std::string` est celui des chaînes de caractères, défini dans la bibliothèque standard `<string>`  cf. https://cplusplus.com/reference/string/string
> Un prochain cours sera consacré à cette bibliothèque.
> Immédiatement après sa déclaration, la variable `str`, qui n'a pas été initialisée, contient une chaîne vide.
>
> Ici, le corps de la boucle sera exécuté tant que l'utilisateur n'a pas répondu `STOP`.



L'intérêt ici d'utiliser `do-while` de préférence à `while` est que la variable  `str` testée dans la condition est renseignée dans le corps de boucle. Donc il faut exécuter le corps de la boucle avant de tester la condition.





## Boucles infinies

Il est important que l'expression booléenne du `while`  s'évalue à `false` après un nombre fini d'itérations, sinon la boucle se répète indéfiniment!

<img src="/Users/jacquema/Library/Mobile Documents/com~apple~CloudDocs/Enseignement/INALCO/04-iter/img/infiniteloop_applepark.jpg" alt="infiniteloop_applepark" style="zoom:50%;" />

​                         L'adresse du *campus Apple* est 1 Infinite Loop, Cupertino, California, United States.



> **Example**: boucle infinie
>
> ```c++
> while (true) 
> {
> 	std::cout << "c'est pas fini " << std::endl;
> }
> ```

Cette situation généralement peu souhaitable peut cependant être voulue dans le contexte de programmes qui ne s'arrêtent jamais, par exemple en programmation embarquée.

Pour permettre la sortie de boucle, il convient de modifier, dans le corps de la boucle, les variables évaluées dans la condition booléenne. 



> **Exemple**: compte à rebours, suite
>
> Dans l'exemple du compte à rebours, la variable entière `n`, qui est testée dans la condition `n > 0` est décrémentée par `--n` à la fin du corps. Donc l'expression devient fausse après un  certain nombre d'itérations.

> Que se passe-t-il si on commence avec une valeur négative pour `n`?



#### Quelques erreurs classiques

Une boucle infinie être causée par une erreur d'inattention.



> **Example**:  cf. [`exemples/boucle-erreur.cpp`](exemples/boucle-erreur.cpp)
>
> on a modifié le code de compte à rebours ci-dessus pour afficher le passage à la moitié du décompte... avec une erreur fatale.
>
> ```c++
> int n = 10; // compteur de boucle
> while (n > 0) 
> {
>   std::cout << n << " ";
> 	if (n = 5) 
>  		std::cout << "la moitié" << std::endl;
> 
>   --n;      // décrémentation du compteur
> }
> std::cout << "ZERO!" << std::endl;
> ```



Une boucle infinie peut être causée par une erreur d'arrondi.

> **Example**:  arrondi des décimaux.
> cf. [`exemples/boucle-float.cpp`](exemples/boucle-float.cpp)
>
> ```c++
> float x = 0.1;
> while (x != 1) 
> {
>      std::cout << "x = " << x << std::endl;
>      x += 0.1;
> }
> ```
>
> ```
> x = 0.10000000149011611938
> x = 0.20000000298023223877
> x = 0.30000001192092895508
> x = 0.40000000596046447754
> x = 0.50000000000000000000
> x = 0.60000002384185791016
> x = 0.70000004768371582031
> x = 0.80000007152557373047
> x = 0.90000009536743164062
> x = 1.00000011920928955078
> x = 1.10000014305114746094
> x = 1.20000016689300537109
> ...
> ```



#### Correction d'erreurs

Lorsque le programme entre dans une boucle infinie, l'utilisateur perd la main.

Pour forcer l'interruption de l'execution dans un terminal, faire `^C`  (control-C).



Pour analyser une boucle qui ne termine pas, on peut ajouter un délai à chaque itération de la boucle,  avec l'instruction `sleep_for`
cf. https://cplusplus.com/reference/thread/this_thread/sleep_for/
et  [`exemples/boucle-float.cpp`](exemples/boucle-float.cpp) pour un exemple.



L'utilisation du debugger, par exemple [onlinegdb.com](onlinegdb.com), 
permet aussi d'interrompre l'execution de la boucle infinie (avec un *breakpoint*) et d'avancer pas-à-pas 
 (en mode *stepper*).



> Essayer de debugger le programme  [`exemples/boucle-erreur.cpp`](exemples/boucle-erreur.cpp)  (compte-à-rebours avec affichage à la moitié).



## Boucle `for`

La boucle `for` offre une syntaxe plus explicite pour faire une itération un nombre déterminé de fois.

```c++
for (initialisation; expression bool; changement) 
{
   // bloc d'instructions (corps)
}
/// [suite]
```

Comme la boucle `while`, la boucle `for` répète le bloc d'instruction tant qu'une expression booleénne (condition) est évaluée à `true`.
Mais en outre, elle impose dans sa syntaxe trois éléments entre   `()`, séparés par des `;` 

- une instruction d'**initialisation**, 
  éxécutée avant le début de la boucle,
- une **condition**, expression booleénne évaluée après l'initialisation, 
  et avant chaque itération (éxécution du bloc d'instructions),
- une instruction de **changement**, 
  éxécuté après chaque itération.

Pour résumer, l'exécution de l'instruction `for` ci-dessus se déroule comme suit:

1. **initialisation** (premier élément entre les `()` après `for` )

2. évaluation de la **condition**  (expression booléenne, deuxième élément entre `()` )

   - si l'expression est évaluée à `true`, alors 

     - les instructions du corps de boucle sont exécutées,

     - le **changement** est opéré (troisième élément entre `()` ),

     - puis on retourne au point 2 pour une nouvelle **itération** de la boucle.

   - si l'expression est évaluée à `false` alors on va au point 3. 

3. **sortie** de boucle: le programme continue avec la `[suite]`.



Les blocs d'initialisation et de changement portent typiquement sur les variables de l'expression booléenne:

- l'initialisation est la déclaration d'une variable (*e.g.* un compteur), 
  et l'affectation d'une valeur initiale,
- le changement est la modification de cette variable (*e.g.* incrémentation ou décrémentation).

Dans ce cas typique, la condition booléenne portera sur la variable.



>  **Exemple**: boucle for typique
>
>  ```c++
>  for (unsigned int i = 0; i < 10; ++i) 
>  {
>  	std::cout << i << ", ";
>  }
>  std::cout << "DIX!" << std::endl;
>  ```
>
>  1. **initialisation**: déclaration de `i` et initialisation de `i` à 0.
>
>  2. évaluation de la **condition**  `i < 10` 
>
>     - si l'évaluation donne  `true`, alors 
>
>       - afficher `i`
>
>       - incrémenter `n`  (**changement** `++i`)
>
>       - retourner à 2.  (pour une nouvelle **itération** de la boucle).
>
>     - si l'expression est évaluée à `false` alors aller à 3. 
>
>  3. **sortie** de boucle: le programme continue en affichant `DIX!`
>
>
>  Affichage obtenu:
>
>  ```
>  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, DIX!
>  ```



Dans l'exemple précédent, `i` est ce que l'on appelle une *variable locale* àla boucle : 
sa **portée** est limitée au corps de la foucle `for`
et aux instructions d'initialisation, condition et changement.
En dehors de cela, la variable `i` n'est pas accessible par le programme, 
pour le compilateur `i` n'existe pas en dehors de la boucle.



>  **Exemple**: re-compte à rebours 
>  (équivalent au précédent codé avec `for` au lieu d'un `while`).
>
>  cf.  [`exemples/countdown-for.cpp`](exemples/countdown-for.cpp)
>
>  ```c++
>  for (int n = 10; n > 0; --n) 
>  {
>  	std::cout << n << ", ";
>  }
>  std::cout << "ZERO!" << std::endl;
>  ```
>
>  1. **initialisation**: déclaration de `n` et initialisation de `n` à 10.
>
>  2. évaluation de la **condition**   `n > 0` 
>
>     - si l'évaluation est `true`, alors 
>
>       - afficher `n`
>
>       - décrémenter `n`  (**changement**  `--n`)
>
>       - retourner à 2.  (pour une nouvelle **itération** de la boucle).
>
>     - si l'expression est évaluée à `false` alors aller à 3. 
>
>  3. **sortie** de boucle: le programme continue en affichant `ZERO!`
>
>  Affichage obtenu:
>
>  ```
>  10, 9, 8, 7, 6, 5, 4, 3, 2, 1, ZERO!
>  ```



Comme précédemment, les `{` `}` sont facultatives autour du corps de la boucle lorsque celui ne contient qu'une instruction.

> Exemple: l'exemple précédent   [`exemples/countdown-for.cpp`](exemples/countdown-for.cpp) peut aussi s'écrire:
>
> ```C++
> for (int n = 10; n > 0; --n) 
> 	std::cout << n << ", ";
> std::cout << "ZERO!" << std::endl;
> ```



On rappelle que les indentations sont facultatives: 
les espaces et tabulations sont ignorées par le compilateur, 
elle sont cependant utiles pour la lisibilité du code.



Les blocs d'initialisation et de changement sont eux-mêmes des instructions. 

Ils peuvent même contenir des blocs d'instructions. 
Dans ce cas, les instruction du bloc sont séparées par des `,`  (et non des `;` comme d'habitude, pour des raisons de syntaxe).



Chacun des éléments initialisation, condition et  changement est optionnel.



>  **Exemple**. variante compte à rebours
>
>  ```c++
>  int n = 10;
>  for ( ; n > 0; ) 
>  {
>      std::cout << n << ", ";
>      --n;
>  }
>  std::cout << "ZERO!" << std::endl;
>  ```
>
>  Ce code est équivalent à celui de l'exemple précédent, la variable `n` étant initialisée juste avant la boucle, et décrémentée dans le corps de celle-ci.



> **Exemple**: boucle for infinie
>
> ```c++
> for (;;)
> {
> 	// corps
> }
> ```
>
> fera une boucle infinie, de la même manière que `while (true)`.



## Boucles imbriquées

On peut imbriquer une boucle dans une autre.

> Exemple: boucle imbriquée. cf. `exemples/nested.cpp`
>
> ```C++
> for (unsigned int i = 0; i < 10; ++i) 
> {
> 	for (unsigned int j = 0; j < 10; ++j) 
> 		std::cout << i << '.' << j << ", ";
> }
> std::cout << "fini" << std::endl;
> ```
>
> (accolades écrites pour lisibilité)
>
> Ce code affichera: 
>
> ```
> 0.0, 0.1, 0.2, (...), 1.0, 1.1, (...), 9.8, 9.9, fini
> ```

Chacune des $n$ itérations de la boucle extérieure fera $m$ itérations de la boucle interne. 
On aura donc au total $n \times m$ itérations (100 dans l'exemple précédent).
Le nombre total d'itérations est donc **exponentiel** en le nombre d'imbrications.



## Instructions `break` et `continue`

Des instructions spéciales permettrent d'interrompre le flot des instructions de contrôle conditionnelles  (`switch`) et itératives (`for` et `while`).



#### Sortie de boucle forcée avec `break`

L'instruction`break`, dans le corps d'une boucle, provoque une **sortie immédiate** de la boucle en cours, 
même si la condition booléenne est encore valide.

Cela permet par exemple d'arrêter une boucle infinie:  
La variante inconditionnelle de boucle suivante, avec une condition triviale toujours vraie, 
a une condition d'arrêt testée à l'intérieur de la boucle, qui provoque une sortie de la boucle (saut direct à la `[suite]`).

```c++
while (true)
{
    // bloc d'instructions

    if (expression bool) // condition d'arrêt
        break;

    // bloc d'instructions
}
/// [suite]
```

Dans cet exemple, on aurait pu aussi écrire `for( ; ; )` qui est équivalent à `while (true)`.



>  **Exemple**: bot éternel
>
>  cf. [`exemples/bonbot2.cpp`](exemples/bonbot2.cpp)
>
>  ```C++
>  std::string str;
>  
>  while (true)
>  {
>    std::cout << "Comment t'appelles tu? ";
>    std::cin >> str; 
>    if (str == "STOP")
>      break; // sortie de boucle
>    else 
>      std::cout << "Bonjour " << str << std::endl;
>  } 
>  ```
>
>  Le résultat est le même que dans l'exemple précédent avec `do` `while`.



L'exemple précédent est une *boucle infinie d'interaction*: la sortie de boucle (ou non) dépend d'intéractions avec l'extérieur (ici un utilisateur) qui ont lieu pendant celle-ci. C'est aussi le cas dans les boucles infinie en programmation embarquée.



L'instruction `break` permet aussi de forcer une boucle à terminer avant sa fin naturelle.

> **Exemple**: compte-à rebours annulé.
>
> cf. [`exemples/countdown-break.cpp`](exemples/countdown-break.cpp)
>
> ```c++
> for (int n = 10; n > 0; --n) 
> {    
>      std::cout << n << ", ";
>      if (n == 3)
>      {
>        std::cout << "annulation! ";
>        break;
>      }
> }
> std::cout << "FIN" << std::endl;
> ```
>
> 1. initialisation de `n` à 10.
>
> 2. évaluation de `n > 0`  (condition)
>
>    - si l'expression est évaluée à `true`, alors 
>
>      - afficher `n`
>
>      - si `n` = 3
>        - afficher `annulation!` 
>        - aller à 3. (`break` = **sortie** de boucle).
>
>      - sinon 
>        - décrémenter `n`  (`n--`)
>        - retourner à 2.  (pour une nouvelle **itération** de la boucle).
>
>    - si l'expression est évaluée à `false` alors aller à 3.
>
> 3. **sortie** de boucle: le programme continue avec la `[suite]`
>
> affichage: 
>
> ```
> 10, 9, 8, 7, 6, 5, 4, 3, annulation! FIN
> ```




#### Sortie d'itération avec  `continue`

L'instruction `continue` provoque l'**interruption de l'itération** en cours, c'est à dire que 

- ont sort du corps de la boucle, pendant une itération, comme si l'itération était terminée (les instructions restantes sont ignorées), 
- on démarre l'itération suivante (avec test de la condition).



>  **Exemple**: compte-à rebours abbrégé.
>
>  cf. [`exemples/countdown-continue.cpp`](exemples/countdown-continue.cpp)
>
>  ```c++
>  for (int n = 10; n > 0; --n) 
>    {    
>      if ((n == 8) || (n == 4))
>        continue;
>  
>      std::cout << n << ", ";
>    }
>    std::cout << "ZERO!" << std::endl;
>  ```
>
>  ```
>  10, 9, 7, 6, 5, 3, 2, 1, ZERO!
>  ```
>



## Boucle sur rang

Un **rang** (*range*) est une suite d'élements d'un certain type, par exemple un tableau, ou une chaîne de caractères. Tous les éléments du rang sont du même type.
Formellement, un rang autorise la définition de fonctions `begin()` et `end()` utilisables pour l'itération.

C'est le cas en particulier des objets de type `std::string` (chaîne de caractères) de la bibliothèque `<string>`.

Il existe, depuis `C++ 11`, une syntaxe simplifiée de `for` pour ces objets rangs.

```c++
  for (déclaration : range)
```

où 

- `range` est un rang et
-  `déclaration` est la déclaration d'une variable (du type des éléments de `range`) qui recevra 
  les valeur des éléments de `range` un par un.



>  **Exemple**:  itération de rang sur une chaîne de caractères.
>
>  cf. [`exemples/forange.cpp`](exemples/forange.cpp)
>
>  ```c++
>  std::string str = "Bonjour.";
>  
>  for (char c : str)
>    	std::cout << '(' << c << ')';
>  ```
>    
>  La chaîne `str` est une suite de caractères (type `char`).
>    chaque élément `c` considèré par la boucle a donc pour type `char`.
>    Ce code produit l'afichage suivant:
>  
>  > ```(B)(o)(n)(j)(o)(u)(r)(.)```



La variable utilisée dans la boucle est **déclarée** à gauche du `:`
on doit donc donner explicitement son type (comme dans toute déclaration de variable).

Pour éviter d'avoir à donner son type, on peut utiliser le mot clé `auto`,  à la place du type.
Le compilateur va alors inférer lui même le type de la variable (à partir du rang).

Dans l'exemple précédent, cela donne:

```c++
for (auto c : str)
```

Le compilateur devinera que `c` a pour type `char`.

