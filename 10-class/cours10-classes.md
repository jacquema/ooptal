# Cours de programmation orientée objet 

INALCO, Master TAL première année
Florent Jacquemard  `florent.jacquemard@inria.fr`
mardi 28 janvier 2025  



# Classes et objets en C++

Nous avons cu dans la première partie du cours (introduction) que:

- une **classe**:

  - est la définition d'une catégorie d'objets,  par le programmeur **concepteur**,
  - est le résultat d'une encapsulation,
  - définit un type.

- un **objet**:

  - est une donnée du type défini par une classe,
  - crée par le programmeur **utilisateur**:  
    c'est la réalisation concrête d'une classe on parle d'**instance** de la classe,
  - est une variable:  
    l'objet existe à l'exécution, et a une durée de vie limitée.

  

## Classes

Leur déclaration se fait avec le mot clé `class`, suivi de l'identificateur de classe. 

```c++
class Nom
{
   /// definition 
};
```

ne pas oublier le `;` après la `}`!

Cela définit un nouveau **type de donnée** `Nom`.
Il est d'usage (mais pas obligatoire) de commencer l'identificateur de classe (de type) par une majuscule.

Une variable peut être déclarée avec le type de classe:

```c++
Nom x;
```

Rappel de vocabulaire: 
la variable `x` désigne (contient) une **instance** de la classe `Nom`, c'est à dire un **objet** de type `Nom`.



> **Exemple 1** .  Déclaration de la classe `Rectangle`.
>
> ```c++
> class Rectangle
> {
>  // corps de la définition de la classe
>  // dans cet exemple on n'a encore rien défini...
> };
> 
> int main()
> {
>   Rectangle r;
>   return 0;
> }
> ```
>
> La classe Rectangle est vide mais ce code compile.



## Attributs

Leur déclaration se fait  dans le corps de la définition de la classe, comme une déclaration de variable, avec un type et un identificateur.

```c++
class Nom
{
   Type1 attribut1;
   ...
};
```



> **Exemple 2**.  Attributs de la classe `Rectangle`.
> Il s'agit de la hauteur et la largeur d'un rectangle.
> Leur type est flottant (`double`) dans les deux cas.
>
> ```c++
> class Rectangle
> {
>      double height;
>      double width;
> };
> ```



L'accès aux attributs d'une instance de classe se fait par notation postfixe, avec un point.

Ainsi:

```c++
instance.attribut
```

est la valeur de l'attribut pour l'instance donnée.



> **Exemple 3**.
> Déclarons un objet  `r`  de type `Rectangle`:
>
> ```c++
> Rectangle r;
> ```
>
> La valeur de l'attribut `height`  pour l'instance `r` est:
>
> ```c++
> r.height
> ```




> **Exemple 4**.  Programme avec un rectangle, cf. [exemples/rectangle-pub.cpp](exemples/rectangle-pub.cpp)
> Dans la fonction `main` du programme suivant, les attributs hauteur et largeur de  l'objet  `r`  (de type `Rectangle`) se voient affectés les valeurs (`double`) respectives `2.5` et `4`.
>
> ```c++
> #include <iostream>
> class Rectangle
> {
> public:   // voir le sens de public plus loin.
>  double height;
>  double width;
> };
> 
> int main()
> {
>   Rectangle r;
>   r.height = 2.5;
>   r.width  = 4.0;
>   std::cout << "hauteur = " << r.height << std::endl;
>   std::cout << "largeur = " << r.width << std::endl;
> }
> ```



## Méthodes, définition

Les méthodes sont des fonctions définies au sein d'une classe.
Leur définition se fait suivant la syntaxe usuelle de définition des fonctions:

```c++
type_retour identificateur(type1 parametre1, ...)
{
    // corps de la méthode
}
```

Dans l'**entête** de la méthode (*header*) sont définis, dans cet ordre:

- le type de retour,
- le nom de la méthode (identificateur),
- la liste des paramètres, avec leurs types respectifs, entre `(`, `)`.

Le **corps** de la méthode, entre `{` `}`, contient les instructions.



> **Exemple 5**.  Définition de la méthode `surface` de la classe `Rectangle`.
>
> ```c++
> class Rectangle
> {
>   double height;
>   double width;
> 
>   double surface()
>   {
>    	return height * width;
>   }
> };
> ```
>
> La méthode `surface` est définie à l'intérieur de la classe `Rectangle`.
> Elle s'appliquera aux instances de cette classe.

L'**encapsulation** consiste donc à définir à la fois les données (attributs) et fonctions (méthodes) au sein d'une même entité (classe) à portée unifiée.

On note que la méthode `surface` n'a pas de paramètre:  elle prend implicitement en entrée les attributs de sa classe  (`height` et `width`) et retourne leur produit.
En effet, tous les attributs d'une classe sont accessibles à l'intérieur de la classe, en particulier dans les définition de méthodes.
On dit que ces attributs  sont dans la **portée** de la classe.

Par conséquent, il n'est pas nécessaire de passer les attributs comme paramètres aux méthodes. C'est une différence importante par rapport aux fonctions usuelles.

> **Exemple 6**.  
> Dans le cas d'une définition impérative de la fonction `surface`, on passe deux paramètres `height`,  `width`,  donc la portée est le corps de la fonction.
>
> ```c++
> double surface(double height, double width)
> {
>   	return height * width;
> }
> ```



Une méthode peut aussi recevoir des paramètres extérieurs, qui ne sont pas des attributs de la classe.

> **Exemple 7**. Paramètres extérieur de méthode: mise à l'échelle d'un rectangle.
> Le facteur d'échelle `factor` est un paramètre externe (à la classe `Rectangle`).
>
> ```c++
> class Rectangle
> {
>   //...
>   void scale(double factor)
>   {
>        height = height * factor;
>   }
> };
> ```
>
> Cette méthode `scale` modifie l'objet  auquel elle est appliqué (plus précisément, l'attribut `height` de cet objet), contrairement à `surface`.



Nous aborderons dans la suite une manière de différencier les méthodes qui modifient l'objet auquel elles sont appliquées des autres.




## Appel des méthodes

L'**appel** de méthodes d'une classe se fait de la même manière que l'utilisation des attributs:
pour appeler une méthode de classe sur un objet instanciant la classe, on utilise la notation postfixe, avec un point suivant le nom de l'objet.

```c++
instance.methode(val_parametre1, ...)
```



> **Exemple 8**.  Appel de la méthode `surface` de `Rectangle`.
> Pour une variable pour `r`  de type `Rectangle`, l'appel `r.surface()` retourne la valeur (`double`) calculée par `surface` sur l'instance `r`.
>
> Ci-dessous un exemple de programme  complet avec un tel appel dans `main`, , cf. [exemples/rectangle-pub.cpp](exemples/rectangle-pub.cpp)
>
> ```c++
> #include <iostream>
> class Rectangle
> {
> public:  // voir plus loin.
>   double height;
>   double width;
>   double surface() const
>   {
>     return height * width;
>   }
> };
> 
> int main()
> {
>   Rectangle r;
>   r.height = 2.5;
>   r.width = 4.0;
>   std::cout << "hauteur = " << r.height << std::endl;
>   std::cout << "largeur = " << r.width << std::endl;
>   std::cout << "surface = " << r.surface() << std::endl;
> }
> ```

Dans l'appel de l'exemple précédent,  `surface()` est appliquée à l'objet `r`.

Cela est analogue à un appel où `r` est passé, par référence à une fonction externe  `surface_ext(r)`, définie en dehors de la classe par:

```c++
double surface_ext(const Rectangle& rect)
{
   return rect.height * rect.width;
}
```

On note que l'objet `rect` n'est pas modifié par l'appel (attribut `const`).




> **Exemple 9**.  quelques exemples d'appels de méthodes déjà vus dans les cours précédents...
> Pour `s` de type `std::string`, 
>
> ```c++
> s.size();
> s.append(s1);
> ```
>
> Pour `v` de type `std::vector<int>`, 
>
> ```c++
> v.size();
> v.push_back(1);
> ```



## Définition externe des méthodes

Pour des questions de lisibilité,  on définit en général le corps des méthodes à l'extérieur de la classe.  C'est à dire que l'on a:

- l'entête de la méthode dans la définition de la classe, 
- le corps de la méthode après la définition de la classe.

```c++
class Nom
{
   type  attribut;
   // prototype (entête) de la méthode.
   type_retour methode(type1 parametre1, ...);
};

// definition extérieure de la methode
type_retour Nom::methode(type1 parametre1, ...)
{
   // corps
}
```

Pour une telle définition à l'extérieur de la classe, on utilise l'**opérateur de résolution de portée**  `Nom::`, qui donne à la méthode accès aux attributs de la classe,  comme si elle était définie à l'intérieur de la classe.



> **Exemple 10**. définition externe de méthode, cf.  [`rectangle.cpp`](exemples/rectangle.cpp)
>
> Ci-dessous la définition externe de la méthode surface de notre rectangle:
>
> ```c++
>class Rectangle
> {
> public:  // voir plus loin.
>  double height;
>    double width;
>     double surface() const;
>    };
>    
> double Rectangle::surface() const
> {
> 	return height * width;
> }
>   ```
> 
> La méthode peut faire référence aux attributs `height` et `width` de la classe `Rectangle` grâce à l'opérateur de résolution de portée.

Souvent, la définition de la classe et les définitions du corps des méthodes se trouvent dans des fichiers séparés,

- `Rectangle.h` pour la classe (`h` comme `header`)
  c'est ce fichier `.h` qui est chargé par `#include`,
- `Rectangle.cpp` pour les définitions des méthodes.

Nous verrons cela dans le cours sur la compilation séparée.




## Méthodes `const`

Certaines méthodes, les **actions**,  vont modifier les instances (objets) auxquelles elles s'appliquent.
D'autres méthodes, les **prédicats**, ne les modifient pas.



> **Exemple 11**. actions et prédicats du rectangle.
>
> - la méthode `surface` est un prédicat de `Rectangle`: elle ne modifie pas la hauteur et la largeur, 
> - la méthode `scale` est une action de `Rectangle`.



On peut indiquer au compilateur qu'une méthode est un prédicat en ajoutant le mot-clé `const` à la fin de l'entête de la méthode.

```c++
type_retour identificateur(type1 parametre1, ...) const
```



> **Exemple 12**.
>
> ```c++
> class Rectangle
> {
>   //...
>     double surface() const;       // prédicat
>     void scale(double factor);    // action
> };
> ```



Déclarer comme prédicat (`const`) une méthode qui modifie des attributs de la classe (comme `scale` dans l'exemple 7 ci-dessus) provoquera une erreur de compilation.

```c++
assignement of data member is read-only structure
```

Ce mot-clé `const` n'est pas obligatoire mais il est vivement recommandé de l'utiliser autant que possible, car il donne des garanties quand au traitement effectué par les méthodes sur les objets.




## Droits d'accès

La distinction entre parties visible et cachée des objets, du point de vue du programmeur et de l'utilisateur, est faite à l'aide des mots clés suivants.

- `public:` (*interface*)  
  tout ce qui suit, dans la définition de la classe,  
  est visible à l'extérieur de la classe.
- `private:` (caché, détails d'implémentation)  
  tout ce qui suit, dans la définition de la classe,  
  est visible uniquement à l'intérieur de la classe.

Par défaut tout ce qui est déclaré dans une classe est `private`.



> **Exemple 13**. cf. [`rectangle-pub.cpp`](exemples/rectangle-pub.cpp)
> On revient à l'usage de `public` dans un exemple précédent.
>
> ```c++
> #include <iostream>
> class Rectangle
> {
> public:  
>   double height;
>   double width;
> 
>   double surface() const
>   {
>     return height * width;
>   }
> };
> 
> int main()
> {
>   Rectangle r;
>   r.height = 2.5;
>   r.width = 4.0;
>   std::cout << "hauteur = " << r.height << std::endl;
>   std::cout << "surface = " << r.surface() << std::endl;
> }
> ```
>
> grâce à au mot clé  `public`,  les attributs `height`, `width`, et la méthode `surface()` sont visibles à l'extérieur de la classe (ici dans `main`).
>
> Sans ce mot clé, le code ci-dessus ne compile pas, car par défaut, `height`, `width`, et `surface()` sont `private`, donc pas accessibles à l'extérieur de la classe




## `struct`

Le `struct` présenté au cours 9 est en réalité synonyme de classe (et peut contenir des méthodes).

La principale différence entre `struct` et `class` est que par défaut,  tout ce qui est déclaré dans `struct` est `public`.



> standard c++98 §11.2  
> *Member of a class defined with the keyword class are private by default. Members of a class defined with the keywords struct or union are public by default.*



> **Exemple 15**.  `Rectangle` comme un  `struct`

```c++
#include <iostream>

struct Rectangle
{
   double height;
   double width;
   double surface() const
   {
      return height * width;
   }
};

int main()
{
   Rectangle r;
   r.height = 2.5;
   r.width = 4.0;
   std::cout << "hauteur = " << r.height << std::endl;
   std::cout << "surface = " << r.surface() << std::endl;
}
```

Il y a quelques autres différences entre `class` et `struct` (concernant l'héritage et les templates).

La recommendation est d'utiliser `struct` pour les classes avec uniquement des attributs.

>  Google *coding style*:
>  https://google.github.io/styleguide/cppguide.html  
>  *Use a struct only for passive objects that carry data; everything else is a class.*



**Exercices**:  →   exercices 1 à 4 de la feuille.



## Abstraction

Les droits d'accès permettent de distinguer une partie de la classe qui relève du concepteur (partie privée) et la partie à destination de l'utilisateur (partie publique).

Communément, il est considèré comme bonne pratique d'abstraction de 

- déclarer `private`

  - tous les attributs (représentation interne des objets)
  - les méthodes d'usage **interne** à la classe.

- déclarer `public`

  - quelques méthodes choisies pour faire l'**interface** avec l'utilisateur.

  

|                                              | attributs             | méthodes                          |
| -------------------------------------------- | --------------------- | --------------------------------- |
| **Interface** <br />visibles (`public`)      |                       | méthodes pour<br />utilisateur    |
| **Implémentation**<br />cachés   (`private`) | données<br />internes | méthodes concepteur<br />internes |

Ainsi, l'utilisateur a une vision abstraite de la classe: il n'a pas besoin de connaître la représentation interne des objets qu'il manipule mais sait quelles fonctions il peut leur appliquer.

Ce  principe d'abstraction est généralement employé pour les **librairies**, par exemple pour les classes de la librairie standard que nous avons utilisées.
Par exemple, en créant une chaîne de caractère de  [`std::string`](https://cplusplus.com/reference/string/string/)

```c++
std::string s("abc");
```

on ne connaît pas les détail de la représentation de cette chaîne `s` en mémoire mais on peut en extraire une sous-chaîne avec `substring` ou la modifier avec `push_back` ou  `insert`, qui sont des méthodes documentées de l'interface de   [`std::string`](https://cplusplus.com/reference/string/string/).

Si le concepteur de `std::string`  décide de modifier la représentation des chaînes de caractères, cela n'aura pas d'incidence sur le code de l'utilisateur (tant que l'interface n'est pas modifiée).




> **Exemple 14**.
> Pour `Rectangle`, cette méthode d'abstraction donne le schéma suivant:
>
> |                                              | attributs         | méthodes |
> | -------------------------------------------- | ----------------- | -------- |
> | **Interface** <br />visibles (`public`)      |                   | surface  |
> | **Implémentation**<br />cachés   (`private`) | height<br />width |          |
> |                                              |                   |          |
>
> - La méthode `surface` est accessible à tous (interface).
> - La largeur et hauteur sont dans la partie cachée (détails d'implémentation).
>
> Concrêtement, cela se définit comme suit (cf [`exemples/rectangle.cpp`](exemples/rectangle.cpp)):
>
> ```c++
> class Rectangle
> {
> public: 
>    double surface() const
>    {
>       return height * width;
>    }
> 
> private:
>    double height;
>    double width;
> };
> ```
>
> Le mot clé `private` s'applique à tout ce qui suit, ici: `height` et `width` qui sont rendus inaccessibles à l'extérieur de la classe.
>
> Le mot clé `public` s'applique à `surface()`, qui peut être appelée à l'extérieur de la classe.




> **Exemple 15**.
>
> Avec la définition de `Rectangle` de l'exemple précédent, le code suivant produit une erreur à la compilation:
>
> ```c++
> int main()
> {
>      Rectangle r;
>      std::cout << "hauteur = " << r.height << std::endl;
> }
> ```
>
> parce que `height` est privé.
>
> Mais le code:
>
> ```c++
> int main()
> {
>   Rectangle r;
>   std::cout << "surface = " << r.surface() << std::endl;
> }
> ```
>
> compile correctement.



## Accesseurs, modificateurs

Avec cette pratique d'abstraction, les attributs, déclarés comme `private`,  deviennent inaccessibles en dehors de la classe. Pour y remédier, on peut définir des méthodes spécialement pour accéder aux valeurs d'attributs privés, ou les modifier.

- un **accesseur** (ou *getter*) est un prédicat retournant la valeur d'un d'attribut privé.  
  Le profil type d'un accesseur est:

```c++
type getAttr() const;
```



> **Exemple 16**. Accesseur.
> On définit comme suit un accesseur à l'attribut hauteur d'un rectangle.
>
> ```c++
> double getHeight() const { return height; }
> ```
>
> Cet accesseur s'appelle comme suit:
>
> ```c++
> Rectangle r;
> ...
> double h = r.getHeight(); 
> ```
>
> Lors de l'appel de cet accesseur, l'objet `r` n'est pas modifié (comme indiqué par le mot clé `const`).




- un **modificateur** (*setter*) est une action modifiant la valeur d'un d'attribut privé,  
  Le profil type d'un modificateur est:

```c++
void setAttr(type param);
```



> **Exemple 17**. modificateur.
>
> ```c++
> void setHeight(double h) { height = h; } // copie
> ```



La définition d'accesseurs et modificateurs ne doit pas être systématique, pour tous les attributs.
Elle concerne seulement les attributs pour lesquels l'accès est nécessaire.



> **Exemple 18**. Rectangle (presque) complet. cf.  [`rectangle.cpp`](exemples/rectangle.cpp)
>
> ```c++
>#include <iostream>
> class Rectangle
> {
> public: 
>   double surface() const;
>   double getHeight() const { return height; }
>      void setHeight(double h) { height = h; } 
>      double getWidth()  const { return width; }
>      void setWidth(double h)  { width = h; } 
>    
>    private:
>   double height;
>   double width;
>    };
>    
> double Rectangle::surface() const;
> {
> 	return height * width;
> }
>   
> int main()
> {
>   Rectangle r;
>   r.setHeight(2.5);
>      r.setWidth(4.0);
>      std::cout << "hauteur = " << r.getHeight() << std::endl;
>      std::cout << "surface = " << r.surface() << std::endl;
>    }
>    ```



## Intégrité et robustesse

Les principaux intérêts des accesseurs et modificateurs sont:

- le **contrôle d'intégrité** des données.
  les méthodes modificateur de l'interface vérifient que les données fournies sont cohérentes.
- la **robustesse** aux changements dans la classe.
  on peut changer la représentation interne sans changer l'interface.

Cela prend son sens particulièrement dans le cadre de programmation de gros projets, en équipe.

> **Exemple 19**.  problème d'intégrité des données.
> supposons tous les attributs de rectangle publics.
>
> ```c++
> class Rectangle
> {
> public:   
>   double height;
>   double width;
>   double surface() const { return height * width; }
> };
> ```
>
> L'assignation suivante est possible.
>
> ```c++
> Rectangle r;
> r.height = -5;
> ```
>
> On peut imaginer que cela posera des problèmes.



On peut contrôler dans le modificateur la valeur donnée aux attributs privés  `height` et `width`, avant affectation.



> **Exemple 20**. contrôle d'intégrité.
>
> ```c++
> void setHeight(double h) 
> { 
>      if (height < 0)
>      {
>       std::cerr << "setHeight: hauteur non valable " << h << std::endl;
>       return; // on quitte sans changer le rectangle
>      }
>      height = h; 
> } 
> ```

Ainsi, le passage obligatoire par un modificateur garantit que le rectangle sera bien formé (fait de données intègres).




> **Exemple 21**.  robustesse.
> supposons que le rectangle a un identificateur  (`label`) 
>
> ```c++
> #include <string>
> class Rectangle
> {
>   public:   
>      std::string label;
>      // ... 
> };
> ```
>
> et supposons que ce label est utilisé dans le code (extérieur à la classe).
>
> ```c++
> Rectangle r1, r2;
> if (r1.label.empty())
> 	// ...
> ```
>
> Si le programmeur concepteur décide de changer le type de `label`,  par exemple pour `int`,  le programmeur utilisateur devra réviser son code extérieur.



On  évite le problème de cet exemple en cachant `label` avec `private`, et en le rendant accessible au travers d'accesseurs (par exemple une méthode booléenne `undefLabel()`).




## Fonction `assert` (contrôle d'intégrité)

La fonction `assert` est déclarée dans la librarie `assert.h`.
Elle termine le programme, avec un message, quand son paramètre est évalué à *false*.
Elle est typiquement utilisée pour faire du contrôle d'intégrité, durant une phase de débuggage.

> **Exemple 22**.
>
> ```c++
> #include <assert.h> 
> // ...
> void setHeight(double h) 
> { 
>     assert(height >= 0);
>     height = h; 
> } 
> ```



On peut demander au compilateur d'ignorer les asserts, quand on est suffisamment sûr de son programme.



## Méthodes `inline`

Dans la version de  [`rectangle.cpp`](exemples/rectangle.cpp) du `Rectangle`,  les accesseurs et modificateurs `getHeight`, `setHeight`, `getWidth`, `setWidth` sont définis en ligne,  dans la définition de la classe (en principe dans fichier `.hpp`), contrairement à `surface`.

Avec le mot clé `inline`,  le compilateur fera un **remplacement syntaxique** du code des méthodes à leur appel.

L'exécution sera (légèrement) plus rapide, mais l'exécutable plus gros et la compilation plus longue.



> **Exemple 23**.
>
> ```c++
> inline double getHeight() const { return height; }
> inline void setHeight(double h) { height = h; } 
> inline double getWidth()  const { return width; }
> inline void setWidth(double h)  { width = h; } 
> ```



En pratique, cela peut être utile pour de petites fonctions appelées fréquemment, comme les accesseurs.  On peut aussi laisser le compilateur décider ce qui est `inline`.




## Masquage de variable

Il peut arriver que la portée de classe (d'attributs) masque  le nom d'une variable.



> **Exemple 24**. masquage.
>
> Dans la classe `Rectangle`, on peut défnir le modificateur de hauteur comme suit:
>
> ```c++
> void setHeight(double height) 
> { 
> 	height = height; // correct mais pas clair
> } 
> ```
>
> Le `height` de gauche est l'attribut de la classe `Rectangle`, 
> le `height` de droite est le paramètre de `setHeight`.



Pour désambiguer, on peut:

- changer le nom du paramètre de `setHeight`

```c++
void setHeight(double h) 
{ 
   height = h; // c'est plus clair
} 
```

- changer le nom de l'attribut.  
  Certaines conventions (*coding style*) proposent d'utiliser un `_`
  dans les identificateurs d'attributs privés, au début ou à la fin.  
  cf. https://google.github.io/styleguide/cppguide.html#Variable_Names

```c++
class Rectangle
{
   // ... 
private:   
   double height_;
   double width_;
   std::string label_;
};
```

- expliciter l'attribut

```c++
void setHeight(double height) 
{ 
   this->height = height; 
} 
```

Le mot clé `this` est un pointeur sur l'objet courant de type `Rectangle`. Il a pour type `Rectangle*`.

Donc `this->height` est  l'attribut `height` de  l'objet `Rectangle` sur lequel on appelle `setHeight`.

