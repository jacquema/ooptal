# Cours de programmation objet 
INALCO, Master TAL première année
Florent Jacquemard  florent.jacquemard@inria.fr
mardi 28 janvier 2025  
https://gitlab.inria.fr/jacquema/ooptal



sources:

- [Cours de C/C++](http://casteyde.christian.free.fr/cpp/cours/index.html), Christian Casteyde.

- [Programmation orientée objet](https://perso.telecom-paristech.fr/elc/inf224/), Eric Lecolinet, Télécom Paris

- Introduction à la programmation orientée objet en C++, EPFL Press.

  

# Concepts de programmation orientée objet

La programmation orienté objet est une manière particulière de programmer (un *paradigme* de programmation), visant à apporter des propriétés intéressantes en matière de:

- clarté conceptuelle (organisation des programmes):
  - lisibilité
  - modularité
- maintenabilité:
  - robustesse par rapport aux extensions et modifications.
  - robustesse par rapport aux données erronées ou erreurs de manipulation.
  
  

## Programmation procédurale
Jusqu'à, nous avons écrit en **programmation impérative** (procédurale), dans laquelle les données et traitements sont séparés.  
Typiquement:
- les données sont identifiés par des variables,
- le traitement est défini dans des fonctions.



> **Exemple**.
> pour un calculer une propriété d'un rectangle, on lui passe ses caractéristiques:
>
> ```c++
> double surface(double width, double height)
> {
>   return width * height;
> }
> ```
> l'appel à cette fonction sera par exemple:
> ```c++
> int main()
> {
>   double height = 2.5;
>   double width  = 4;
> 
>   std::cout << "surface du rectangle: ";
>   std::cout << surface(height, width) << std::endl;
> }
> ```



En terme de **clarté conceptuelle**, ce code n'explicite pas:

- le lien sémantique entre les données `width` et `height`,
- ni le lien entre ces données et le rectangle qu'elles caractérisent.

La fonction `surface` pourrait tout aussi s'appeler `product`!

Du point de vue de la **maintenabilité** / **évolutivité**, l'écriture d'extensions de ce code n'est pas pratique.
Pour ajouter un attribut aux rectangles, par exemple une couleur, il faudra ajouter partout dans le code une variable correspondante.




## Concepts clés de la programmation objet

Les concepts clés suivants sont communs à tous les langages orientés objet (pas spécifiques au `C++`):
- encapsulation 
- abstraction
- héritage
- polymorphisme

Dans ce premier cours, nous parlerons des deux premiers concepts, qui sont liés.
Les deux autres seront étudiés dans les cours suivants.



## Encapsulation
Le principe des de grouper dans une seule entité :
- des données, appelées **attributs** (*class members*)
- des fonctionnalités (traitements) qui leur sont spécifiques, appelées **méthodes**.

<img src="img/capsule_anote.jpg" alt="encapsulation" style="zoom:25%;" />

> **Exemple** .
> pour un rectangle:
>
> - attributs: `height`, `width`
> - méthode: `surface`



Une telle entité est appelée objet.

La définition d'un type d'entité (type de donnée) se fait au travers d'une classe.
- une **classe** est un nouveau type de donnée, utilisable dans un programme.
  il s'agit d'un type de donnée complexe (composé), définit par le programmeur.
- un  **objet** est une donnée utilisable, de ce type (on parle d'**instance** de la classe).



> **Exemple**.
>
> `std::string` est une classe.
>
> `size()`, `substr()` sont des méthodes de cette classe, de même que `+` (binaire, application infixe, pour la concaténation) et `[_]` (accès direct) .
>
> Après la déclaration
> ```c++
> std::string s;
> ```
> `s` est un objet de type `std::string` (une instance de la classe `std::string`),
> auquel on peut appliquer les méthodes ci-dessus.



## Abstraction

Il s'agit de la définition *générique* d'un type d'entité.
On procède par identification, pour tous les objets de ce type:

- de caractéristiques communes,
- de traitements, mécanismes communs

Objectif:
- se focaliser sur l'essentiel: ce qui sera utile à un utilisateur,
- cacher les détails d'implémentation.

![iceberg](img/iceberg.jpg)



> **Exemple**.  Programmation procédurale.  
> En programmation procédurale, pour créer des rectangles: 
> on déclare autant de variables de hauteur et largeur que de rectangles.
>
> ```c++ 
> double width1  = 2;
> double height1 = 4;
> 
> double width2  = 3.5;
> double height2 = 2;
> ```
> La manipulation de rectangles (e.g. calcul de surface de rectangles),  se fait par appel de `surface`  sur les variables correspondantes.
> ```c++
> surface(width1, height1);
> surface(width2, height1);
> ```
> Noter l'erreur dans le deuxième appel: cet appel ne concerne aucun des 2 rectangles définis ci-dessus.



> **Exemple**.  Programmation orientée objet.
> En programmation orientée objet, on définit une notion abstraite (classe) `Rectangle`, qui contient une largeur et une hauteur.
> Des objets de type `Rectangle` peuvent  alors être crées par:
>
> ```c++
> Rectangle rectangle1(2, 4);
> Rectangle rectangle2(3.5, 2);
> ```
> Les deux objets `rectangle1` et `rectangle2` sont crées en leur passant leur valeurs respectives de largeur et hauteur.
>
> On peut invoquer des calculs sur ces objets avec des méthodes de la classe `Rectangle`,  qui servent d'*interface d'utilisation*:
>
> ```c++
> rectangle1.surface();
> rectangle1.surface();
> ```
> On notera que les données spécifiques, internes à un rectangle, sont cachées à l'utilisateur,  qui n'a pas à s'en soucier.



## Visibilité, Interface
L'abstraction permet d'organiser une collaboration entre deux types de programmeurs:

- un programmeur **utilisateur**
  qui va utiliser des objets du type de la classe, 
  sans se soucier des détails d'implémentation,
- un programmeur **concepteur** (développeur)
  qui implémente une classe, avec tous les détails nécessaires.

Dans le cas d'une classe de la librairie standard comme `std::string`, `std::vector` *etc*, le concepteur est le (ou les) rédacteurs (trices) de cette classe, et les utilisateurs sont les programmeurs faisant appel à cette classe dans leur programme, avec `#include <string>`.

L'abstraction définit deux niveaux de visibilité pour les composantes des objets:
- niveau **externe** (ou *public*) : 
    ce qui est visible par le programmeur **utilisateur**.  
    C'est le résultat du processus d'abstraction.
- niveau **interne** (ou *privé*) : 
    les détails d'implémentation, visibles exclusivement par le programmeur **concepteur**.
    - corps de définition des méthodes,
    - attributs et méthodes cachés, visibles uniquement par l'objet.

L'**interface**, constituée des entêtes du niveau externe (visible), est en quelque sorte un *contrat* entre le concepteur et l'utilisateur.



> **Exemple**.  Interface de rectangle.
> pour un rectangle, l'interface est constituée  de l'entête de la méthode `surface`:
>
> ```c++
> double surface()
> ```
> En revanche, les attributs `width` et `height` sont au niveau interne, cachés à l'utilisateur.



Un objet a donc la forme suivante:

|          | attributs      | méthodes                                  |
| -------- | -------------- | ----------------------------------------- |
| visibles |                | interface                                 |
| cachés   | données cachés | méthodes internes<br />corps des méthodes |




> **Exemple**.  Programmation procédurale.
>
> ```c++ 
> double width = 2;
> double height = 4;
> surface(width, height);
> ```
> - on travaille sur des données de bas niveau
> - le lien données - traitement se fait par passage des arguments `width` et `height`, il est donc indirect.




> **Exemple**.  Programmation orienté objet.
>
> ```c++
> Rectangle r(2, 4);
> std::cout << r.surface();
> ```
> - `Rectangle`: est une donnée de haut niveau, encapsulée (information de type)
> - les lien  `Rectangle` - `surface` est explicite.
> - code robuste: l'utilisation (de `Rectangle`) n'est pas impacté par changement de code de la méthode `surface`.



Ainsi, le programme gagne en 

- lisibilité
- cohérence
- modularité

