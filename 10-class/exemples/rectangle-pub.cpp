#include <iostream>

class Rectangle
{
public:
   double height;
   double width;

   double surface() const
   {
      return height * width;
   }
};

int main()
{
   Rectangle r;
   r.height = 2.5;
   r.width = 4.0;
   std::cout << "hauteur = " << r.height << std::endl;
   std::cout << "largeur = " << r.width << std::endl;
   std::cout << "surface = " << r.surface() << std::endl;
}

