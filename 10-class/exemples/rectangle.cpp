#include <iostream>

class Rectangle
{
public: 
   double surface() const;

   double getHeight() const;
   double getWidth()  const;

   void setHeight(double h);
   void setWidth(double h);

private:
   double height;
   double width;
};

double Rectangle::surface() const
{
   return height * width;
}

double Rectangle::getHeight() const
{ 
	return height; 
}

double Rectangle::getWidth() const
{ 
	return width; 
}

void Rectangle::setHeight(double h)
{ 
	height = h; 
}

void Rectangle::setWidth(double h)
{ 
	width = h; 
}

int main()
{
   Rectangle r;
   r.setHeight(2.5);
   r.setWidth(4.0);
   // std::cout << "hauteur = " << r.height << std::endl; // erreur
   std::cout << "hauteur = " << r.getHeight() << std::endl;
   std::cout << "surface = " << r.surface() << std::endl;
}
