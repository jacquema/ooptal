#include <iostream>

class Rectangle
{
public: 
   	double height;
	double width;

	double surface() const;
};

double Rectangle::surface() const
{
	return height * width;
}

int main()
{
   Rectangle r;
   std::cout << "donner une hauteur: ";
   std::cin >> r.height;   

   std::cout << "donner une largeur: ";
   std::cin >> r.width;   

   std::cout << "hauteur = " << r.height << std::endl;
   std::cout << "largeur = " << r.width << std::endl;
   std::cout << "surface = " << r.surface() << std::endl;
}

// def. externe surface
// suppr. public

