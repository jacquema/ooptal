#include <iostream>

class Rectangle
{
public: 
   double getHeight() const { return _height; }
   void setHeight(double h) { _height = h; } 

   double getWidth()  const { return _width; }
   void setWidth(double h)  { _width = h; } 

   unsigned int getColor()  const { return _color; }
   void setColor(unsigned int c)  { _color = c; } 

   double surface() const;
   double perimeter() const;
   bool square() const;

private:
   double _height;
   double _width;
   unsigned int _color;
};

double Rectangle::surface() const
{
	return _height * _width;
}

double Rectangle::perimeter() const
{
	return 2 * (_height + _width);
}

bool Rectangle::square() const
{
	return (_height == _width);
}

int main()
{
   Rectangle r;
   double d;

   std::cout << "donner la hauteur: ";
   std::cin >> d;   
   r.setHeight(d);

   std::cout << "donner la largeur: ";
   std::cin >> d;   
   r.setWidth(d);

   std::cout << "hauteur = " << r.getHeight() << std::endl;
   std::cout << "largeur = " << r.getWidth() << std::endl;
   std::cout << "surface = " << r.surface() << std::endl;
}