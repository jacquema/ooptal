#include <iostream>
#include <cmath>

class Disc
{
public:
	void getCenter(double& x, double& y) const;
	void setCenter(double x, double y);
	double getRadius() const;
	void setRadius(double r);

private: // data	
	double _x; // abscisse du centre
	double _y; // ordonnée du centre
	double _radius;
};

void Disc::getCenter(double& x, double& y) const
{
	x = _x;
	y = _y;
}

void Disc::setCenter(double x, double y)
{
	_x = x;
	_y = y;
}

double Disc::getRadius() const
{
	return _radius;
}

void Disc::setRadius(double r)
{
	if (r < 0.0)
	{
		std::cerr << "erreur: rayon négatif " << r << " mis à 0." << std::endl;
		r = 0.0;
	}
	_radius = r;
}

int main()
{
	Disc d;
	double val1, val2;

   	std::cout << "coordonnées du centre: ";
   	std::cin >> val1 >> val2;   
   	d.setCenter(val1+1, val2+1);

	std::cout << "rayon: ";
   	std::cin >> val1;   
   	d.setRadius(val1);

   	d.getCenter(val1, val2);
   	std::cout << "centre = (" << val1 << ", " << val2 << ")" << std::endl;
   	std::cout << "rayon = " << d.getRadius() << std::endl;
}