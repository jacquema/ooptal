#include <iostream>

class Rectangle
{
public: 
	double surface() const;
	double perimeter() const;

	double height;
	double width;
};

double Rectangle::surface() const
{
	return height * width;
}

double Rectangle::perimeter() const
{
	return 2 * (height + width);
}

int main()
{
	Rectangle r;
	std::cout << "donner la hauteur: ";
	std::cin >> r.height;   

	std::cout << "donner la largeur: ";
	std::cin >> r.width;   

	std::cout << "hauteur = " << r.height << std::endl;
	std::cout << "largeur = " << r.width << std::endl;
	std::cout << "surface = " << r.surface() << std::endl;
	std::cout << "perimetre = " << r.perimeter() << std::endl;
}
