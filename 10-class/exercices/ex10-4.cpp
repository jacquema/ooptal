#include <iostream>

class Rectangle
{
public: 
	double height;
	double width;
	unsigned int color;
};


int main()
{
	Rectangle r;
	std::cout << "donner la hauteur: ";
	std::cin >> r.height;   

	std::cout << "donner la largeur: ";
	std::cin >> r.width;  

	std::cout << "donner la couleur: ";
	std::cin >> r.color;  
	
	std::cout << "hauteur = " << r.height << std::endl;
	std::cout << "largeur = " << r.width << std::endl;
	std::cout << "color = "   << r.color << std::endl;
}
