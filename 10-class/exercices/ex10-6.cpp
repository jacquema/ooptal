#include <iostream>
#include <cmath>

class Disc
{
	// private par défaut
	double _x; // abscisse du centre
	double _y; // ordonnée du centre
	double _radius;
};

int main()
{
	Disc d;
	// d._radius = 5; // interdit car _radius est private
	return 0;
}
