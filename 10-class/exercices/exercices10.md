# Exercices - cours 10

## Classes et objets



> **Exercice 10.1**. 
>
> Écrire une classe `Rectangle` vide et créer dans la fonction `main` une instance de cette classe (un objet de type `Rectangle`).
>
> Ajouter à cette classe:
>
> - des attributs  de hauteur et largeur,
> - une fonction `surface` 
>
> comme dans l'exemple 13 du cours. Les attributs et la méthode seront déclarés publics.
>
> Dans la fonction `main` :
>
> - demander à l'utilisateur de saisir des valeurs (`double`) de hauteur et largeur,
> - affecter ces valeurs aux attributs correspondants de l'instance de `Rectangle` crée.
>
> - afficher la surface du rectangle.



> **Exercice 10.2**.
> Ajouter dans la classe `Rectangle` une méthode (publique) qui calcule le périmètre. Afficher le périmètre dans `main`.



> **Exercice 10.3**. 
> Ajouter dans la classe `Rectangle` une méthode (publique) booléenne `square`  qui teste si un rectangle est un carré.
> Afficher le résultat de `square` dans `main`.



> **Exercice 10.4**.
>
> Ajouter à la classe `Rectangle` de  l'exercice précédent un attribut couleur, codé comme un entier non-signé. Modifier le `main` pour changer la couleur du rectangle crée. 
>



> **Exercice 10.5**. 
>
> - Changer tous les attributs de la classe `Rectangle` de `public` à `private`.
> - Essayer de compiler le programme avec la nouvelle classe.
> - Ajouter un accesseur (`get`) et un modificateur (`set`) pour chaque attribut. Ces méthodes seront publiques.
> - Utiliser ces accepteurs et modificateurs dans `main`.
> - Ajouter le mot clé `const` aux méthodes pour lesquelles il est approprié.



---



> **Exercice 10.6**. disque.  (cf. exercice 46 de [C++ par la pratique](https://www.epflpress.org/produit/118/9782889143672/c-par-la-pratique), EPFL press).
>
> Définir dans un fichier `disc.cpp` une classe `Disc` dont les attributs **privés** sont 
>
> - les coordonnées cartésiennes `x` et `y` du centre (de type `double`)
> - le rayon (de type `double`).
>
> Pour compiler cette classe,  écrire une fonction `main` qui ne contiendra qu'une déclaration de variable de type `Disc`.




> **Exercice 10.7**. disque, accesseur et modificateur.
> Écrire pour la classe `Disc` des accesseur et modificateur.
> Pour le centre, ils auront pour entêtes respectives:
>
> ```cpp
> void getCenter(double& x, double& y) const;
> void setCenter(double x, double y);
> ```
> L'accesseur assignera les valeurs des coordonnées du centre aux deux variables passées par référence. 
>
> Le modificateur du rayon vérifiera que le rayon est positif.




> **Exercice 10.8**. disque, méthodes. **DEVOIR MAISON. pour 04/02**
> Écrire pour la classe `Disc` une méthode `surface`, sans paramètre, qui retourne la surface du disque.
>
> On utilisera la constante `M_PI` du module `cmath` (`#include <cmath>`).
> Si cette constante n'est pas définie dans votre installation, ajouter
> les lignes suivantes après les `#include`.
>
> ```cpp
> #ifndef M_PI
> #define M_PI 3.14159265358979323846 
> #endif
> ```
> Dans la fonction `main`, instancier deux objets de type `Disc` et tester la méthode définie ci-dessus.



>  **Exercice 10.9**. disque, méthodes.  **DEVOIR MAISON pour 11/02.**
> Écrire pour la classe `Disc` une méthode `contains`, booléenne, qui, prenant en paramètres les coordonnées d'un point, répond ce point est situé dans le disque ou pas.
> Dans la fonction `main`, instancier deux objets de type `Disc` et tester la méthode `contains`.
