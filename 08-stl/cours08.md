# Cours de programmation objet 
INALCO  
Master TAL première année

---
## Cours 08 Conteneurs
jeudi 5 décembre 2024  
Florent Jacquemard  `florent.jacquemard@inria.fr`

sources:
- cours de Marie-Anne Moreaux, INALCO (09, 10, 12, 13)
- https://cplusplus.com/reference/vector/vector
- https://cplusplus.com/reference/stack/stack

Les conteneurs sont des types de donnée complexes permettant de stocker et organiser des collections d'objets.
Nous verrons ici quelques classes de conteneurs `C++`,  le premier issu du langage `C` et les suivants offerts par la librairie standard STL.



## Tableaux statiques `C`

Les tableaux du langage `C` peuvent aussi être utilisés en `C++`.
Il s'agit de tableaux de taille fixe, qui contiennent des valeurs du même type.

En quelque sorte, un tel tableau de taille $n$ est équivalent à $n$ variables du même type.

### Tableaux `C`: déclaration

```c++
  type nom[taille];
```
- `type` est le type de chaque élement stocké,
- `nom` est l'identificateur du tableau,
- `taille` est le nombre d'élements stockés. 



> **Exemple**: pour un tableau d'entiers de taille la constante `NB_ELT`
>
> ```c++
> const unsigned int NB_ELT = 3;
> int table[NB_ELT];
> ```
>
> La constante `NB_ELT`, utilisée pour spécifier la taille du tableau à l'initialisation, 
> pourra ensuite être utilisée pour se rappeler la taille du tableau (par exemple dans une boucle paracourant ce tableau).

Les chaînes de caractères de `C`,  vues au chapitre 07, sont un un exemple de tableau statique, de type  ` char[]` ou `char*` (pointeur sur le premier élément du tableau).



À la déclaration, une zone mémoire contigüe est allouée pour le tableau. 
Le contenu des cases du tableau est indéterminé après une initialisation comme dans l'exemple ci-dessus.

Il est aussi possible d'initialiser les valeurs contenues dans le tableau à la déclaration. Le contenu est présenté entre accolades.

> **Exemple**:
>
> ```c++
> int table[] = { 10, 11, 12 };
> ```
> - `{ 10, 11, 12 }` est un tableau d'entiers de taille 3, 
>
> - il est copié dans `table`,  qui est donc initialisé  comme un tableau de taille 3.   

Après sa déclaration, la taille d'un tableau statique ne peut pas être modifiée (le tableau ne peut pas être redimensionné).

### Tableaux `C`: accès
L'accès aux éléments d'un tableau statique (lecture et écriture) se fait de manière **indexée**: `t[i]` est l'élément du tableau `t` d'indice `i`.  
Le premier indice d'un tableau est `0`.

> **Exemple**: cf. [`exemples/tab0.cpp`](exemples/tab0.cpp)
> 
>```c++
> int tab[10];
> tab[0] = 100;
> for (size_t i = 0; i < 10; ++i)
> 	std::cout << i << ':' << tab[i] << ' ';
>   ```
> 0:100 1:73896 2:73896 3:73896 4:73896 5:73896 6:73896 7:73896 8:73896 9:1

Noter que dans l'exemple ci-dessus, on utilise `size_t` comme type des indices, par exemple pour le parcour d'un tableau à l'aide d'une boucle `for`.

> **Exemple 2**:  cf. [`exemples/tab1.cpp`](exemples/tab1.cpp)
> 
>```c++
> const unsigned int NB_ELT = 10;
> int tab[NB_ELT];
> for (size_t i = 0; i < NB_ELT; ++i)
>  	tab[i] = 100 - i;
>    ```
> 0:100 1:99 2:98 3:97 4:96 5:95 6:94 7:93 8:92 9:91

Il faut veiller à ne pas acceder à un index supérieur à la taille d'un tableau, 
sous peine de provoquer une erreur à l'exécution, ou d'accéder à une zone de mémoire non réservée pour le tableau, ce qui pourra être source de comportements imprévisibles.


### Tableaux `C` et fonctions
Un tableau statique est toujours passé par référence, même si le paramètre correspondant n'est pas déclaré avec `&`.
Le type pour un tableau en paramètre est de la forme:

```c++
  type id[]
```
Il peut être nécessaire de passer, par un autre paramètre, la taille du tableau, si celle-ci n'est pas connue de la fonction, cf. exercice 1.

Une fonction ne peut pas retourner un tableau statique.
Pour récupérer un tableau après l'appel d'une fonction, il faut le passer en paramètre.



## STL: tableaux dynamiques (`std::vector`)

La librarie standard `C++` (STL, Standard Template Library) offre différentes classes de conteneurs. Le plus couramment utilisé est `vector`. 

La classe `vector` de la STL définit un type de tableaux **dynamiques**, qui peuvent être redimensionné. Cela signifie qu'ils sont alloués dynamiquement: la place occupée en mémoire pour le tableau n'est pas fixée à la compilation.

STL contient d'autres types de conteneurs  (associatifs, ordonnés...).
Dans le doute, il est recommandé d'utiliser `vector` si l'on n'a pas besoin de fonctionnalités particulières.

### Ajout de la librairie `std::vector`

Pour définir et utiliser des vecteurs, il faut inclure le fichiers d'en-tête suivant:
```c++
#include <vector> 
```

### Déclaration de vecteur

Déclaration d'un vecteur vide:
```c++
  std::vector<type> nom;
```
Ce conteneur sera destiné à stocker des objets  du type spécifié entre `<` et `>`. Ce type peut être un type de base ou un type composé, défini par une classe.


> **Exemples**:
>
> - Vecteur d'entiers (vide):
> ```c++
> std::vector<int> v;
> ```
> - Vecteur de chaînes de caractères:
> ```c++
> std::vector<std::string> v;
> ```
> - Vecteur d'objets de type  `Patient` (cours 09).
> ```c++
> std::vector<Patient> v;
> ```



> **Remarque**:
> Il n'est pas possible de définir un vecteur de références, car les références ne sont pas assignables  (élément de droite d'une expression `l = r`), ce qui est exigé des éléments de containeurs.   
>
> En revanche, il est possible de définir un vecteurs de pointeurs, ou de pointeurs intelligents, ce que nous feront pour définir des vecteurs polymorphiques en programmation orientée objet.



Un vecteur peut être déclaré avec une la taille initiale `n` en paramètre:

```c++
  std::vector<type> nom(n);
```
Le vecteur `v` contiendra initialement `n` copies de la valeur par défaut pour `type`.

> **Exemple**: avec
>
> ```c++
> std::vector<int> v(10);
> ```
> `v` contient 10 entiers égaux à 0.

Nous verrons dans les cours sur les classes et objets comment spécifier la valeur par défaut d'un type défini par utilisateur (classe),  avec un constructeur par défaut.

On peut aussi passer un objet pour initialisation du tableau avec  `n` copies de cet objet.
```c++
  std::vector<type> nom(n, objet);
```

> **Exemple**: 
>
> ```c++
> std::vector<int> v(10, -1);
> ```
>
> Initialisation d'un tableau d'entiers de longueur 10 (initialement), chaque élément valant -1.



Un vecteur peut aussi être déclaré  avec une liste d'éléments initiaux, donnés in-extenso,
dans le standard `C++11` (compilation avec l'option `-std=c++11` ou plus).



> **Exemple**:  cf.  [`exemples/vecinit1.cpp`](exemples/vecinit1.cpp)
> 
>```c++
> std::vector<int> v{10, 11, 12};
> ```
> le contenu du vecteur est:
> 0:10 1:11 2:12
> 
>```c++
> std::vector<std::string> v{"ba", "bu"};
> ```
> le contenu du vecteur est:
> 0:ba 1:bu

On notera l'usage des accolades `{`, `}`  autour de la définition du contenu initial (sans parenthèses).

Il est aussi possible d'utiliser la syntaxe:

```C++
std::vector<int> v = {10, 11, 12};
```

Cela revient en fait à initialiser le vecteur déclaré par copie d'un tableau statique `C`.

> **Exemple**  cf.  [`exemples/vecarr.cpp`](exemples/vecarr.cpp)
>
> ```c++
> int tab[] = { 10, 11, 12 };
> std::vector<int> v(tab, tab+3);
> ```



Enfin, un vecteur peut être initialisé  par copie d'un autre vecteur `v0`.
```c++
  std::vector<type> v(v0);
```
ou 
```c++
  std::vector<type> v = v0;
```

Les éléments de `v0` sont alors copiés un à un dans `v`.



### Types paramétrés (templates)

Le type des éléments contenu dans un vecteur doit être fixé (même pour vecteur vide), en le spécifiant entre  `<` et `>`. Cette syntaxe est celle des **templates** (patrons), ou types paramétrés (par un autre type).
Cet outil `C++` puissant permet de définir des fonctions ou des classes travaillant sur un type d'objets générique (le type paramètre donné entre les  `<` et `>` lors de l'utilisation de la fonction ou la classe).
`std::vector` est donc un template.

En donnant à ce template un nom de type en paramètre  (entre `<`, `>`), on obtient, à la compilation, une classe (type composé), avec des méthodes (fonctions) associées.  Ce procéssus est appelé **instanciation**.  Pour déclarer un vecteur, il est nécessaire de donner au compilateur le nom du type des objets qui seront stocké dans ce conteneur (il a besoin de cette information pour instancier la classe que nous voulons utiliser).

Le type `std::vector<int>` est donc "vecteur  d'entiers", utilisable dans une déclation (de variable ou de fonction).  En revanche, `std::vector` n'est pas un nom de type.

Il est possible de définir des vecteurs polymorphes, contenant des objets de différents types, grâce à la notion d'héritage que nous verrons en programmation objet.



### Accès direct aux éléments d'un vecteur

Un vecteur est un conteneur de type **séquentiel**: l'ordre des objets dans le vecteur dépend de l'ordre dans lequel ils ont été insérés par le programmeur.

L'accès aux objets contenus peut se faire de manière **indexée**, avec la même syntaxe que pour un tableau statique ou les chaînes de caractères (operateur [`[ ]`](https://cplusplus.com/reference/vector/vector/operator[]/)): pour un vecteur `v` et un entier `i`, 
```c++
  v[i]
```
retourne une référence sur l'élément de `v` d'indice `i`.  
L'indice du  premier élément est `0`.

Il n’y a pas de vérification que `n` est  plus petit que la taille de `v`. L’accès à un élément hors des bornes du vecteur provoque un comportement indéterminé du programme à l’exécution.

L'alternative suivante [`at`](https://cplusplus.com/reference/vector/vector/at/):
```c++
  v.at(i)
```
retourne aussi une référence sur l'élément  d'indice `i`, mais fait une vérification que `i`  est dans les bornes de `v`et dans le cas contraire une exception `out_of_range` est levée à l'exécution.

### Type des index

Le type `std::vector<type>::size_type`  est un alias de type (`typedef`) utilisable (de manière sûre) pour désigner la taille d'un vecteur d'élément de `type` (nombre d'éléments). Dans les implémentations standards de STL, il s'agit d'un alias pour `size_t`,  un type d'entiers non signés (`unsigned`) suffisamment grand pour pouvoir représenter la taille de tout objet. 

Il est sûr (et recommandé) d'utiliser le type `size_t` pour les index dans les conteneurs STL comme `vector` (somme pour les chaînes de  `std::string`).



### Modification de vecteurs

Pour ajouter un élément à la fin du vecteur `v`, on appellera [`push_back`](https://cplusplus.com/reference/vector/vector/push_back/):
```c++
  v.push_back(element)
```



> **Exemple**:
>
> ```c++
> std::vector<int> v;
> for (int b = 0; b < 10; ++b) 
> 	v.push_back(b);
> ```
>
> `v` = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }




Suppression de l'élément à la fin du vecteur `v` peut se faire avec [`pop_back`](https://cplusplus.com/reference/vector/vector/pop_back/):

```c++
  v.pop_back()
```
Attention: `v` ne doit pas être vide à l'appel de `pop_back()`.

On notera que les 2 fonctions ci-dessus changent la taille du vecteur (de 1 élément).




La fonction [`back`](https://cplusplus.com/reference/vector/vector/back/) retourne une référence sur l'élément à la fin du vecteur `v`:
```c++
  v.back()
```
Là aussi `v` ne doit pas être vide.
L'élément à la fin de `v` peut être modifié à l'aide de cette fonction, par exemple, pour un vecteur d'entier:

```c++
  v.back() += 1;
```
Mais `back()` ne change pas la taille du vecteur.



On peut également obtenir une référence sur l'élément en tête du vecteur `v` avec [`front`](https://cplusplus.com/reference/vector/vector/front/):

```c++
  v.front()
```



La taille du vecteur `v` est retournée par la fonction [`size`](https://cplusplus.com/reference/vector/vector/size/).

```c++
  v.size()
```
Le type de retour de `size` est `size_type` (donc en fait  `size_t`).



La fonction booléenne [`empty`](https://cplusplus.com/reference/vector/vector/empty/):

```c++
  v.empty()
```
vérifie si le vecteur `v` est vide.

et [`clear`](https://cplusplus.com/reference/vector/vector/clear/): 

```c++
  v.clear()
```
supprime tous les éléments de `v`, dont la taille devient `0`.



### Vecteurs et fonctions
Les vecteurs peuvent être passés aux fonctions par valeur (copie) ou par référence, comme n'importe quel autre type.  

- Lors d'un passage par valeur, l'intégralité du contenu du vecteur est copié dans un nouveau vecteur (variable locale).

- Le passage par référence évite une copie complète du vecteur (comme c'est le cas pour  `std::string`).
- Le passage par référence constante (mot clé `const`)  assure en outre que le vecteur passé ne sera pas modifié.

Une fonction peut avoir pour type de retour `std::vector<type>` pour un certain `type`.
Attention, cela génère une copie de vecteur au moment du retour de la fonction.  
Pour l'éviter, on pourra passer en paramètre à la fonction,  par référence, un vecteur à remplir.



## Itérateurs

Les éléments de vecteurs sont aussi accessibles à l'aide d'itérateurs (`iterator`). Ce type abstrait est employé par la plupars des conteneurs de la STL, pour parcourir leurs éléments dans un certain ordre.  Certaines fonctions de containeurs  n'acceptent que des paramètres de  type `iterator` ou retournent un type `iterator`.

Chaque type de conteneur (patron)  définit son propre type « itérateur » pour accéder aux éléments d'une instance de ce type de conteneur.

Intuitivement, un itérateur "*pointe*" sur un élément d'un conteneur, semblable donc à un pointeur. Il peut être déréférencé pour récupérer l'objet pointé, incrémenté ou décrémenté pour passer à l'élément suivant ou précédent.

Pour les vecteurs, la syntaxe générale pour la déclaration d'un itérateur est:
```c++
  std::vector<type>::iterator id;
```
où `id` est l'identificateur de l'itérateur.

> **Exemple**:
>
> ```c++
> std::vector<std::string>::iterator it;
> ```
> définit un itérateur `it` sur les éléments d'un vecteur de chaînes de caractères.



#### Création d'itérateurs

Chaque conteneur (`vector` en particulier) définit une paire de fonctions `begin()` et `end()`  qui renvoient chacune une valeur de type `iterator`.

Pour un vecteur `v` de type `std::vector<type>`, [`begin`](https://cplusplus.com/reference/vector/vector/begin/)

```c++
  v.begin()
```

retourne un itérateur pointant sur le premier élément  de `v`, si `v` n'est pas vide, et [`end`](https://cplusplus.com/reference/vector/vector/end/)

```c++
  v.end()
```

retourne un itérateur "au-delà" du dernier élément de `v` (il pointe un élément qui n'existe pas).
Cet itérateur est un indicateur que l'on est à la fin du vecteur.

Si `v` est vide, on a `v.begin() == v.end()`.



> **Exemple**:
>
> ```c++
> std::vector<std::string> v;
> std::vector<std::string>::iterator it = v.begin();
> ```
>
> `v` est un vecteur de chaîne de caractères et `it` pointe sur son premier élément.




#### Déréférencement d'itérateur
Un itérateur "pointe" sur un élément de containeur.
L'élément pointé est accessible par l'opérateur de  déréférencement `*`.

> **Exemple**:
> pour l'itérateur `it` de l'exemple précédent, 
> l'élément pointé est accessible par `*it`.
>
> |      | `it`                                 | `*it`       |
> | ---- | ------------------------------------ | ----------- |
> | type | `std::vector<std::string>::iterator` | std::string |
>



#### Modification d'itérateur

L'opérateur `++` modifie l'itérateur  pour accéder à l'élément suivant celui que pointe l'itérateur.

On peut donc parcourir un vecteur à l'aide d'une boucle for comme suit:
```c++
for (std::vector<type>::iterator it = v.begin(); it != v.end(); ++it)
```



> **Exemple**: cf. [`exemples/iterator.cpp`](exemples/iterator.cpp)
>
> ```c++
> std::vector<int> v;
> for (int b = 0; b < 10; ++b) 
>   	v.push_back(b*10);
> ```
> `v` = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90 }
>
> La boucle suivante parcourt `v` en affichant chaque élément:
>
> ```C++
> for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
> 	std::cout << *it;
> ```
>
> Noter le déréférencement pour obtenir l'élément à partir de `it`.
>
> La boucle suivante parcourt (et incrémente) les éléments de `v`:
>
> ```c++
> for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
> 	*it += 1;
> ```
> `v` = { 1, 11, 21, 31, 41, 51, 61, 71, 81, 91 }



L'opérateur de décrémentation  `--` modifie l'itérateur  pour accéder à l'élément précédent.



**Arithmétique des itérateurs**

Par l'addition `it += n` (resp. la soustraction `it -= n`) d'un entier `n` à un itérateur `it`,  on obtient un itérateur sur un élément du conteneur  qui se trouve à +`n` (resp. -`n` positions) de l'élément  pointé par `it`.
L'élément pointé par le nouvel itérateur est une référence sur un élément appartenant au conteneur  ou pointe immédiatement après le dernier élément (fin du conteneur).

Il est possible de comparer des itérateurs avec:

- `==`, `!=`,
- `<`, `<=`,  `>`, `>=`.



#### Type `auto`

Le type `std::vector<int>::iterator`, un peu long,  peut-être remplacé par le mot-clé `auto`.
Depuis `C++11`, `auto` peut être utilisé avant un nom de variable (y compris un paramètre de fonction)  ou nom de fonction pour spécifier que le type (de la variable ou type de retour de la fonction) doit être déduit automatiquement par le compilateur.

> **Exemple**. `auto`
> Avec le type `auto`, la dernière boucle de l'exemple précédent devient:
>
> ```c++
> for (auto it = v.begin(); it != v.end(); ++it)
> 	*it += 1;
> ```

La déduction du type peut cependant échouer.




#### Itérateur constant
Les fonction `cbegin()` et `cend()` sont des analogues de `begin()` et `end()` retournant des `const_iterator`. 

Avec de tels itérateurs, l'objet pointé ne peut pas être modifié, mais seulement accédé en lecture.

> **Exemple**:  cf. [`exemples/const_iterator.cpp`](exemples/const_iterator.cpp)
>
> Dans la boucle suivante, on accède aux élément du vecteur `v` en lecture seulement, pour les afficher, avec un `const_iterator`:
>
> ```c++  
> for (std::vector<int>::const_iterator it = v.cbegin(); it != v.cend(); ++it)
> 	std::cout << *it << ' ';
> ```
>
> On aurait pu écrire simplement  `auto` à la place de `std::vector<int>::const_iterator`.



#### Itérateurs inversés

Un itérateur inversé, `reverse_iterator`, permet de parcourir un containeur en sens inverse.

Les fonction `rbegin()` et `rbegin()` retournent de tels itérateurs.

> **Exemple**: cf. [`exemples/reverse_iterator.cpp`](exemples/reverse_iterator.cpp)  
> 
>```c++  
> std::vector<int> v;
> for (int b = 0; b < 10; ++b) 
> 	v.push_back(b*10);
>   
> for (std::vector<int>::reverse_iterator it = v.rbegin(); it != v.rend(); ++it)
>      std::cout << *it << ' ';
>    ```
> 90 80 70 60 50 40 30 20 10 0 



Les fonctions `crbegin()` et `crend()` retournent des `const_reverse_iterator`.



### Boucle range for

Depuis la version `C++11`, le parcours d’un vecteur `v` de type `std::vector<type>`
peut se faire à l’aide d’une boucle `for` dite *range-based* dont la syntaxe est la suivante:

```c++
  for (type x : v)
  {  }
```

Ce type d'itération est possible en général  pour les conteneurs STL qui définissent des functions `begin` et `end`.



## STL vector: autres fonctions d'ajout et suppression

Dans les exemples suivant, on considére: 
- un vecteur `v` de type `std::vector<type>`, 
- un itérateur `it` de type  `std::vector<type>::iterator`
- un valeur `val` de type `type`,  

L'appel à la fonction  [`insert`](https://cplusplus.com/reference/vector/vector/insert/)
```c++
v.insert(it, val)
```
insère un nouvel élément `val` (copie) juste avant l'élément pointé par `it`,  augmentant la taille de `v` de `1`. Elle retourne un itérateur pointant sur l'élément nouvellement inséré.



> **Exemple**: insertion en tête d'un vecteur.
>
> ```c++
> std::vector<int> v; // vide
> v.insert(v.begin(), 0);
> v.insert(v.begin(), 1);
> ```
> insère `0`  puis `1` en tête de `v`.



Le type de `insert` est le suivant (cf. https://cplusplus.com/reference/vector/vector/insert/):

```c++
std::vector<type>::iterator insert(std::vector<type>::iterator position, const type& val);
```

La variante 
```c++
  v.insert(it, n, val)
```
pour un entier `n`, insère `n` copies de `val`. La valeur de retour est alors un itérateur sur le premier élément inséré.

La troisième variante

```C++
v.insert(it, it_first, it_last)
```

va insérer avant l'élément pointé par `it` tous les éléments entre les itérateurs `it_first` (inclu) et `it_last` (exclu), qui peuvent être des intégrateurs dans un autre vecteur du même type que `v`.



L'appel de la fonction [`erase`](https://cplusplus.com/reference/vector/vector/erase/) 
```c++
  v.erase(it)
```
supprime de `v` l'élément pointé par `it`, diminuant la taille de `v` de `1`. La valeur de retour est un itérateur pointant l'élément qui suivait le dernier élément effacé, ou `end` si tous les élément ont été effacés.

La variante:

```c++
v.erase(it_first, it_last)
```
supprime de `v` tous les éléments entre les itérateurs `it_first` (inclu)  et  `it_last` (exclu).

L’ajout ou la suppression d’un élément  ailleurs qu’à la fin d'un vecteur peut avoir un coût plus important  et nécessiter des opérations d’allocation et de désallocation (déplacement de tous les éléments après la position d'insertion).



## STL: tableaux statiques (`std::array`)
STL propose aussi un type de  conteneur séquentiel pour  les tableaux de taille fixe,  avec accès indexé.

La librairie qui gère cela s'appelle [`array`](https://cplusplus.com/reference/array/array/)
```c++
#include <array>
```

La déclaration d'un tableau de taille fixe se fait par
```c++
std::array<type, taille> id;
```
où `taille` est le nombre d'éléments.

> **Exemple**:
>
> ```c++
> std::array<int, 5> myarray = { 2, 16, 77, 34, 50 };
> ```

Les accès indexés aux éléments  se font avec l'opérateur [`[]`](https://cplusplus.com/reference/array/array/operator[]/),  ou [`at`](https://cplusplus.com/reference/array/array/at/), [`front`](https://cplusplus.com/reference/array/array/front/), [`back`](https://cplusplus.com/reference/array/array/back/), comme pour `vector`.

La librairie `array` propose les même fonctions d'itérateurs que pour `vector`: [`begin`](https://cplusplus.com/reference/array/array/begin/), [`end`](https://cplusplus.com/reference/array/array/end/), et leurs variantes, 

En revanche, les ajouts ou suppressions d'éléments  ne sont pas possibles pour les conteneurs `array`.

Les tableaux statiques `array` peuvent être avantageusement utilisés pour définir des **tuplets** d'éléments de même type (vecteurs de longueur fixe), `std::array` est plus approprié (plus efficace) pour cela que `std::vector`.



## STL: tuplets  (`std::tuple`)

Il existe une librairie STL [`tuple`](https://cplusplus.com/reference/tuple/tuple/) qui permet de définir des tuplets d'éléments de types différents.


```c++
#include <tuple>        
```

Le type, pour la **déclaration** d'objets tuplets, est donné avec la liste des types des différentes composantes du tuplet:

```
std::tuple<type_1, ..., type_n> t;  
```

La longueur d'un tuplet, ainsi que le type de ses composants, sont fixés.



> **Exemple**: déclaration de tuplet:
>
> ```c++
> std::tuple<int, char> t(10, 'a');
> ```



Un tuplet peut aussi être construit à l'aide de la fonction `make_tuple`.

> **Exemple**: 
>
> ```c++
> std::tuple<int, char> t = std::make_tuple(10, 'a');
> ```
>
> ou plus simplement,
>
> ```c++
> auto t = std::make_tuple(10, 'a');
> ```

On note qu'il n'est pas nécessaire de donner à  `make_tuple` les types des composantes du tuplet. Ceux-ci sont inférés par le compilateur.



L'**accès** (direct) aux éléments d'un tuplet se fait avec la fonction [`get`](https://cplusplus.com/reference/tuple/get/), à laquelle on passe l'indice de l'élément non pas par paramètre mais entre `<` et `>`.

> **Exemple**: accès aux éléments d'un tuplet:
>
> ```c++
> std::tuple<int, char> t(10, 'x');
> int i  = std::get<0>(t); // 10
> char c = std::get<1>(t); // 'x'
> ```



**Paires**

Un cas particulier de tuple est les paires d'objets, que l'on peut déclarer par:

> **Exemple**: déclaration de paire:
>
> ```c++
> std::pair<int, char> p(10, 'a');
> ```



Les variables  `first` et `second` donnent accès au premier et deuxième élément d'une paire.
Il s'agit donc de raccourcis pour `get<0>` et `get<1>`.



> **Exemple**: paire int, char (suite)
>
> ```c++
> std::pair<int, char> p(10, 'a');
> p.first += 5;
> std::cout << p.first << ", " << p.second << std::endl;
> ```
>
> 15, a



Pour la documentation de référence, cf. https://cplusplus.com/reference/utility/pair/





## STL: Piles (`std::stack`)

Une pile (*stack*) est une collection d’objets, tous de même type, auxquels on peut accéder en mode « **LIFO** » (Last In, First Out),  soit « dernier entré, premier sorti ».

<img src="img/lifo.png" alt="lifo" style="zoom:50%;" />

Ce type de donnée est particulièrement  utile  pour gérer les parenthèses dans les mots et, par extension, pour la représentation d'arbres sous forme de mots, donc pour le *parsing* (analyse syntaxique).

En programmation, les piles sont utilisées pour gérer les appels de fonctions, en particulier les fonction récursives. On parle de *pile d'appel*. Toutes les variables locales sont stockées dans cette pile.



### Ajout de la librarie `stack`

L'utilisation de piles se fait  par inclusion du fichier d'en-tête de la bibliothèque [`stack`](https://cplusplus.com/reference/stack/stack/) : 
```c++
  #include <stack> 
```

### Déclaration de pile
La déclaration d'une pile vide,  destinée à recevoir des objets  d'un certain `type` (de base ou composé) se fait par:
```c++
  std::stack<type> nom;
```
La valeur d'une pile `p` peut être initialisée par copie  d'une autre pile `p0`:
```c++
  std::stack<type> p(p0);
```
ou
```c++
  std::stack<type> p = p0;
```

### Opérations sur les piles
Soit `p` un pile de type `std::stack<type>`.  La fonction [`push`](https://cplusplus.com/reference/stack/stack/push/)


```c++
  p.push(x)
```
ajoute un élément `x` de type `type` au sommet  de la pile `p`,  augmentant la taille de la pile de 1.

```c++
  p.pop()
```
retire l'élément au sommet  de la pile `p`, réduisant la taille de la pile de 1.
La pile ne doit pas être vide à l'appel, ce qui se vérifie avec la fonction booléenne [`empty`](https://cplusplus.com/reference/stack/stack/empty/):

```c++
p.empty()
```

retourne `true` si la pile `p` est vide et  `false` sinon.

On peut accéder à l'élément au sommet de la pile avec [`top`](https://cplusplus.com/reference/stack/stack/top/):

```c++
  p.top()
```
retourne une référence sur l'élément au sommet  de la pile `p`.
La taille de la pile est inchangée par l'appel à `top`.  La pile ne doit pas être vide à l'appel.  

Il n'y a pas d'accès indexé aux éléments d'une pile (avec `[]` ou `at`).
Seul l'élément du dessus est accessible.

```c++
p.size()
```
retourne la taille de `p` (de type `size_type` donc aussi `size_t`).



## STL: files d'attente  (`std::queue`)

Dans ces conteneurs séquentiels de la bibliothèque STL [queue](https://cplusplus.com/reference/queue/queue/), on peut 

-  ajouter un élément à la fin, par [`push`](https://cplusplus.com/reference/queue/queue/push/)
- supprimer l'élément du début, par [`pop`](https://cplusplus.com/reference/queue/queue/pop/)

Ils sont typiquement utilisés pour implémenter des files d'attentes de type  « **FIFO** »  (_First In First Out_).

<img src="img/fifo.png" alt="fifo" style="zoom:50%;" />



On peut accéder au premier et dernier élément respectivement par [`front()`](https://cplusplus.com/reference/queue/queue/front/) et [`back()`](https://cplusplus.com/reference/queue/queue/back/).





## STL: files d'attente doubles (`std::deque`)

`deque` est l'acronyme de _**d**ouble-**e**nded **que**ue_. Il s'agit d'un conteneur séquentiel plus général que `queue`  permettant l'ajout et la suppression d'objets au début ou à la fin. (Formellement, `queue` est un cas particulier de `deque`, aussi appelé _container adapter_).



Les fonctionalités de `deque` sont semblables à celles de `vector`, mais l'implémentation des deux classes est assez différente (du point de vue de la représentation interne et des algorithmes). 

- En particulier, l'ajout au début est plus efficace pour `deque` que pour  `vector`. 
- En revanche, l'ajout/effacement d'éléments à des positions arbitraires (autre que début) est moins efficace pour `deque` que pour  `vector`. 



## STL: files de priorité  (`std::priority_queue`)

Dans une file de priorité (bibliothèque [`priority_queue`](https://cplusplus.com/reference/queue/priority_queue/)), les éléments inséré sont automatiquement ordonnés, et on a accès à l'élément le plus grand.

La relation d'ordre peut être passée en paramètre à la déclaration de la file.

-  on peut ajouter un élément dans la file d'attente avec la fonction [`push`](https://cplusplus.com/reference/queue/priority_queue/push/), 
-  la fonction `top` retourne une référence sur le plus grand élément de la file, 
-  et [`pop`](https://cplusplus.com/reference/queue/queue/pop/)  supprime cet élément de la file.

De plus, [`size`](https://cplusplus.com/reference/queue/priority_queue/size/) et [`empty`](https://cplusplus.com/reference/queue/priority_queue/empty/) retournent les même valeurs que pour les conteneurs précédents.



> Exemple: cf. [`exemple/priority.cpp`](exemple/priority.cpp)
>
> ```C++
>  std::priority_queue<int> q;
> 
>   q.push(10);
>   q.push(90);
>   q.push(25);
>   q.push(50);
> 
>   while (!q.empty())
>   {
>      std::cout << ' ' << q.top();
>      q.pop();
>   }
> ```
>
>   90 50 25 10



## STL: listes (`std::list`)

[`list`](https://cplusplus.com/reference/list/list/) est encore un autre conteneur séquentiel.

Son implémentation (listes doublement chaînées) permet un parcours dans les deux directions, avec des itérateurs, et favorise les ajouts et suppressions d'éléments à des positions arbitraires (par rapport aux classes vues plus haut).

En revanche, il n'y a pas d'accès direct (par `[]` ou `at`) aux éléments des listes. Pour accèder à un élément dans la liste, il faut partir du début et avancer (avec un itérateur) jusqu'à cet élément.



## STL: listes d'association (`std::map`)

**map**

Dans le type de containeur [`map`](https://cplusplus.com/reference/map/map/) sont stockées des paires (c'est à dire des [`std::pair`](https://cplusplus.com/reference/utility/pair/)) formées de:

- une clé (_key value_)
- une valeur associée à cette clé (_mapped value_).

En quelque sorte, du point de vue de l'utilisateur,  `map` généralise `vector` en employant pour les index un type de clé arbitraire, plutôt que des entiers. C'est aussi l'analogue des dictionnaires (*dictionaries*) Python. On parle aussi de liste d'association, ou *conteneurs d'association* dans le vocabulaire STL C++.

Concrètement, l'accès aux éléments se fait avec l'opérateur [`[ ]`](https://cplusplus.com/reference/map/map/operator[]/) ou de la fonction [at](https://cplusplus.com/reference/map/map/at/), mais l'argument passé est du type des clés, au lieu d'être obligatoirement un entier comme pour `vector`.

> **Exemple**:  cf. [`exemples/map.cpp`](exemples/map.cpp)
>
> ```C++
> std::map<std::string, int> mymap; // vide
> mymap["alpha"] = 10;
> mymap["beta"]  = 20;
> mymap["gamma"] = 30;
> ```
>
> Les clés sont ici des chaînes de caractères ("alpha",  "beta",  "gamma").
>
> La map `mymap` défini donc les associations
> "alpha" → 10, "beta" → 20, "gamma" → 30.



Une map est ordonnée par les valeurs des clés, et peut être parcourue dans cet ordre à l'aide d'un *itérateur*. De plus, la relation d'ordre sur les clés peut être passée en paramètre à la déclaration d'un objet [`map`](https://cplusplus.com/reference/map/map/map/).

Les `map` sont typiquement implémentés à l'aide d'arbres binaires de recherche.



> **Exemple**:  cf. exemples/map.cpp
>
> On peut itérer sur la map de l'exemple précédent à l'aide d'une boucle sur rang.
>
> ```C++
> for (auto& x : mymap) 
> 	std::cout << x.first << " : " << x.second << std::endl;
> 
> ```
>
> On utilise les variables `first` et `second` du type `std::pair`  pour l'accès aux valeurs *key value* et *mapped value* de chaque élément.
>
> L'affichage obtenu est:
>
> ```
> alpha : 10
> beta : 20
> gamma : 30
> ```
>
> On note que l'ordre d'affichage est bien l'ordre alphabétique sur les clés.

Chaque clé peut apparaître au plus une fois dans une liste `map`.



**unordered_map**

Les conteneurs de la bibliothèque [`unordered_map`](https://cplusplus.com/reference/unordered_map/unordered_map/)  contiennent le même type de données (ce sont des conteneurs d'association) que `map`, mais les paires <clé, valeur> n'y sont pas ordonnées. Elles sont en fait organisées à l'aide de fonction de *hachage* des valeurs de clés, ce qui permet un accès rapide aux éléments, plus rapide qu'avec `map`. 
En contrepartie, l'itération sur le contenu est moins efficace avec  [`unordered_map`](https://cplusplus.com/reference/unordered_map/unordered_map/)   qu'avec `map`.



**multimap**

La bibliothèque [`multimap`](https://cplusplus.com/reference/map/multimap/) (*multiple-key map*) est une variante dans laquelle on peut avoir plusieurs paires avec la même clé. 

Il n'y a pas d'accès direct dans   [`multimap`](https://cplusplus.com/reference/map/multimap/) (pas d'opérateur `[ ]` ou fonction `at`).

- La fonction [`count`](https://cplusplus.com/reference/map/multimap/count/), à laquelle on passe une clé `k` (par référence) en paramètre, 
  retourne le nombre d'éléments (paires) ayant pour clé `k`.

- La fonction [`find`](https://cplusplus.com/reference/map/multimap/find/), à laquelle on passe aussi une clé `k`  par référence, 
  retourne un itérateur sur les éléments (paires) ayant pour clé `k`.

Les `multimap` sont ordonnées, tout comme les `map`, ce qui permet une itération sur le contenu. 

> **Exemple**: cf. [`exemples/multimap.cpp`](exemples/multimap.cpp)
>
> ```C++
>   std::multimap<std::string, int> mymm;
>   mymm.insert(std::make_pair("x", 10));
>   mymm.insert(std::make_pair("y", 20));
>   mymm.insert(std::make_pair("y", 30));
>   
>   std::multimap<std::string, int>::iterator it = mymm.find("y");
>   for ( ; it != mymm.end(); ++it)
>     std::cout << "y => " << it->second << std::endl;
> ```
>
> Ici, on itère sur les 2 éléments de clé "y".
>
> L'affichage est :
>
> ```
> y => 20
> y => 30
> ```



**unordered_multimap**

La bibliothèque [`unordered_multimap`](https://cplusplus.com/reference/unordered_map/unordered_multimap/) est la variante non ordonnée de `multimap` .

Les avantages et inconvénients en terme d'effecacité sont les même que pour `map` et `unordered_map`.



## STL: ensembles(`std::set`)

Les conteneurs de la bibliothèque [set](https://cplusplus.com/reference/set/set/) (ensembles) stockent des éléments d'un type donné, sans doublons.

Il est possible d'ajouter ([`insert`](https://cplusplus.com/reference/set/set/insert/)) et effacer ([`erase`](https://cplusplus.com/reference/set/set/erase/)) des éléments dans un ensemble `set` .

Il n'y a pas d'accès direct mais on peut itérer sur les éléments de l'ensemble.

> **Exemple**: cf. exemples/set.cpp
>
> ```C++
> std::set<int> myset;
> for (int i = 1; i < 5; ++i) myset.insert(i*3);
> 
> for (std::set<int>::iterator it = myset.begin(); it != myset.end(); ++it)
>     std::cout << ' ' << *it;
> std::cout << std::endl;
> ```
>
> affiche: 
>
> ```
>  3 6 9 12
> ```



On peut vérifier qu'un ensemble contient un certain élément à l'aide de :

- [`find`](https://cplusplus.com/reference/set/set/find/) qui prend un objet en paramètre (par référence), 
  et retourne un itérateur sur l'élément dans l'ensemble, si il existe, où l'itérateur [`end`](https://cplusplus.com/reference/set/set/end/) sinon.
-  [`count`](https://cplusplus.com/reference/set/set/count/) qui retourne 1 ou 0 suivant que l'ensemble contient ou pas l'élément en paramètre.



> **Exemple**: cf. [`exemples/set.cpp`](exemples/set.cpp)
>
> Avec l'ensemble de l'exemple précédent:
>
> ```C++
> for (int i = 0; i < 10; ++i)
> {
>     std::cout << i << (myset.count(i) == 1 ? " est" : " n'est pas");
>     std::cout << " dans myset" << std::endl;
> }
> ```
>
> affiche: 
>
> ```
> 0 n'est pas dans myset
> 1 n'est pas dans myset
> 2 n'est pas dans myset
> 3 est dans myset
> 4 n'est pas dans myset
> 5 n'est pas dans myset
> 6 est dans myset
> 7 n'est pas dans myset
> 8 n'est pas dans myset
> 9 est dans myset
> ```
>
> On obtient le même résultat avec:
>
> ```C++
> for (int i = 0; i < 10; ++i)
> {
>     std::cout << i << (myset.find(i) != myset.end() ? " est" : " n'est pas");
>     std::cout << " dans myset" << std::endl;
> }
> ```

