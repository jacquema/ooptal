// fonction vérifiant qu'un mot (en paramètre) est un palindrome, 
// à l'aide d'une pile.

#include <iostream>
#include <string>
#include <stack>

bool palindrome(const std::string& m)
{
    size_t len = m.size();
    std::stack<char> pile; // vide initialement

    for (size_t i = 0; i < len; ++i)
    {
		// on empile la premiere moitié du mot
		if (i < len / 2)
            pile.push(m[i]);
        // pour un mot de longueur impaire, on peut ignorer la lettre du milieu
        else if ((i == len / 2) && (len%2 == 1))
            continue;
		// on dépile en vérifiant en vérifiant la deuxième moitié de m
		else
        {
            if (m[i] == pile.top())
                pile.pop();
            else
                return false;
        }                    
    }
	// assert(pile.empty());
    return true;
}

void test(const std::string& m)
{
	std::cout << m << (palindrome(m)?" est ":" n'est pas ");
	std::cout << "un palindrome." << std::endl;
}

int main()
{
    test("abccba");
	test("abcdcbc");
	test("abccba");
	test("abccb");
	test("kayak");
	test("abcdedcba");
	test("a");
	test("");
}
