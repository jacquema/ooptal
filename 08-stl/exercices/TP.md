# TP vector



- Déclarez un vecteur contenant des chaîne de caractères `std::string`.

- Ajouter dans ce vecteur, un par un, les mots d'une phrase, exterminant par un symbole de ponctuation.
- Affichez la taille de votre vecteur.
- Écrire une fonction pour afficher le contenu d'un vecteur
  - en utilisant l'accès direct (par index),
  -  en utilisant un itérateur.

- Remplacer le caractère de ponctuation (à la fin) par une chaine contenant 3 fois ce symbole.
- Échangez le premier et le deuxième mot.
- Ajouter une virgule à la fin de chaque mot du vecteur.

- Triez le vecteur en utilisant la fonction [std::sort](https://cplusplus.com/reference/algorithm/sort/) de la STL (inclure opur cela l'en-tête `algorithm`). 

  L'ordre de tri par défaut est celui de la comparaison sur des std::string. 
