// fonction vérifiant le bon parenthèsage multiple d'une chaîne 
// à l'aide d'une pile.

#include <iostream>
#include <string>
#include <stack>

// match de parenthèses ouvrante - fermée
bool match(char c1, char c2)
{
    if (c1 == '(')
        return (c2 == ')');
    else if (c1 == '[')
        return (c2 == ']'); 
    else if (c1 == '{') 
        return (c2 == '}');
    return false;
}

// le mot m est bien multi-parenthesé
bool parenthese(const std::string& s)
{
	// pile de parenthèses (des 3 types) ouvertes et pas refermées
    std::stack<char> pile; // vide initialement

	// parcours de s
    for (char c : s)
    {
		// empile parenthèse ouvrante
        if (c == '(' || c == '[' || c == '{')
            pile.push(c);
		// dépile parenthèse fermante en testant la par. ouvrante correspondante
        else if (c == ')' || c == ']' || c == '}')
        {
            if (!pile.empty() && match(pile.top(), c))
                pile.pop();
            else
                return false;
        }
    }
    return pile.empty();
}

void test(const std::string m)
{
	std::cout << m << (parenthese(m) ? " " : " pas ") << "bien parenthesée." << std::endl;
}

int main()
{
    test("(a{b}cc)");     // OK 
    test("[a(b)c]{{c}}"); // OK
    test("[a(b)c]{{c})"); // NO
    test("[a(b)c{c}]");   // NO
    test("(a(b)c(");      // NO
    test("]a(b)c[");      // NO
	test("[]");			  // OK
	test("a");			  // OK
}