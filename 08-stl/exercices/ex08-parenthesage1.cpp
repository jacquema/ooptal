// fonction vérifiant que dans une chaîne de caractères, chaque parenthèse ouvrante est fermée.

#include<iostream>
#include <string>

													  // le mot m est bien parenthesé
bool parenthese(const std::string& s)
{
	// compteur de parenthèses ouvertes et pas refermées
    int cpt = 0;

	// parcours de s en comptant les parenthèses ouvertes
	// cpt < 0 si une parenthèse est fermée 
	// alors qu'elle n'a pas été ouverte = cas d'erreur provoquant une sortie de boucle
    for (size_t i = 0; i < s.size() && cpt >= 0; ++i)
    {
        if (s[i] == '(')
            ++cpt;
        else if (s[i] == ')')
            --cpt;
    }
	// toutes les parenthèses on été fermées
    return (cpt == 0); 
}

void test(const std::string m)
{
	std::cout << m << (parenthese(m)?" ":" pas ") << "bien parenthesée." << std::endl;
}


int main()
{
    test("(a(b)cc)");     // OK 
    test("(a(b)c)((c))"); // OK
    test("(a(b)c(c))");   // OK
    test("(a(b)c(");      // NO
    test(")a(b)c(");      // NO
}