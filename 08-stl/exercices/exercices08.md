# Exercices - cours 08



## Containers : vectors



> **Exercice 1**.  
> Écrire une fonction prenant un entier positif n en paramètre, et retournant un vecteur contenant les entiers 0, 1,..., n.
>
> Même question mais avec une fonction booléenne et le vecteur (à remplir) passé par référence.
> La fonction vérifiera que le vecteur est initialement vide et retournera `true`  si c'est le cas (après l'avoir rempli) ou `false` sinon (sans le remplir).



> **Exercice 2**. 
>
> En définissant un vecteur des jours de la semaine, écrire un programme qui demande à l’utilisateur de lui fournir un nombre entier entre 1 et 7 et qui affiche le nom du jour de la semaine ayant le numéro indiqué (lundi pour 1, mardi pour 2, ... dimanche pour 7).



> **Exercice 3**.
>
> Écrire une fonction recevant un tableau (`vector`) d'entiers en paramètre et qui retourne l'indice de l'élément  minimum du tableau. En cas de doublons, on retournera le plus petit indice. 



> **Exercice 4**.  
> Écrire une fonction booléenne  vérifiant qu'un vecteur d'entiers passé en paramètre est rangé dans l'ordre croissant.



> **Exercice 5**.  fusion
> Écrire une fonction prenant en paramètre deux vecteurs d'entiers `v1` et `v2` supposés rangés dans l'ordre croissant, et remplissant un troisième vecteur (passé en paramètre) avec la fusion de leur contenus, rangée dans l'ordre croissant.
> On pourra vérifier que `v1` et `v2` sont rangés dans l'ordre croissant à l'aide de la fonction définie dans l'exercice 9.



> **Exercice 6.** 
> On va implémenter une méthode de **tri par sélection** d'un tableau qui fonctionne comme suit: rechercher dans le tableau le plus petit élément, et l'échanger avec le premier, recommencer la même opération avec le deuxième (échanger le deuxième élément avec le plus petit élément situé après le deuxième), puis le troisième *etc* jusqu'au dernier élément du tableau.
>
> 1. Écrire une fonction `swap` qui reçoit en paramètre un tableau (`vector`) d'entiers et deux indices $i$ et $j$, et qui échange dans le tableau les deux entiers aux positions $i$ et $j$. 
>    Le tableau sera modifié en place. On vérifiera au début de la fonction que $i$ et $j$ sont dans les bornes du tableau.
> 2. Écrire une fonction `minswap` qui reçoit en paramètre un tableau d'entiers et un indice $i$, qui recherche dans le tableau le plus petit entier situé après l'indice $i$ (inclu) et qui échange cet entier avec l'entier à l'indice  $i$. Pour l'échange, on utilisera la fonction `swap`.
> 3. Écrire une fonction `sort` qui fait le tri par sélection d'un tableau d'entiers reçu en paramètre. Elle utilisera la fonction `minswap`.
> 4. Tester la fonction `sort` dans `main`.



> **Exercice 7**.  
> Écrire une fonction prenant comme paramètre trois vecteurs d'entiers et remplissant le troisième avec la concaténation des deux premiers.



## Piles



> **Exercise 8**.
> Écrire une fonction vérifiant qu'un mot (en paramètre)  est un palindrome, à l'aide d'une pile.
>
> On procédera en 2 étapes:
>
> - stockage de la première moitié du mot sur une pile
> - comparaison de la seconde moitié du mot avec le contenu de la pile.



> **Exercise 9**. bon parenthèsage (1)
> Écrire une fonction vérifiant que dans une chaîne de caractères, chaque parenthèse ouvrante est fermée.
> On pourra compter le nombre de parenthèses ouvertes à chaque instant, lors du parcours de la chaîne.



> **Exercise 10**. bon parenthèsage (2)
> On généralise à trois type de parenthèses: `(` et `)`,  `[` et `]`,  `{` et `}`.
> On va écrire une fonction vérifiant le bon parenthèsage multiple d'une chaîne à l'aide d'une pile.
>
> - écrire une fonction booléenne  `match` à laquelle on passe deux caractères `c1` et `c2` et qui vérifie si `c1` est une des 3 parenthèses ouvrantes et `c2` est la fermante associée.
> - écrire une fonction vérifiant qu'une chaîne est bien parenthèsée, à l'aide d'une pile dans laquelle on place les parenthèse ouvrante, et vérifiant le match des parenthèse fermantes rencontrée (avec la fonction précédente).
>

