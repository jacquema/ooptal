#include <iostream>
#include <vector>
#include <algorithm> // swap, sort
#include <assert.h>

// affichage du contenu avec access indexé
void printi(const std::vector<std::string>& v)
{
  for (size_t i = 0; i < v.size(); ++i)
    std::cout << v[i] << ' ';
  std::cout << std::endl;
}

// affichage du contenu avec itérateur
void print(const std::vector<std::string>& v)
{
  for (auto i = v.begin(); i != v.end(); ++i)
    std::cout << *i << ' ';
  std::cout << std::endl;
}

int main()
{
  // déclaration et ajout des mots. pour ce type d'initialisation, compiler avec -std=c++11
  std::vector<std::string> v{ "Bonjour", "à", "tous", "comment", "aller", "vous", "?" };

  // affichage de la taille et du contenu du vecteur
  std::cout << "taille: " << v.size() << std::endl;
  print(v);

  // remplacement du caractère de ponctuation(à la fin)
  // par une chaîne contenant 3 fois ce symbole.
  // assert(! v.empty());
  // assert(! v.back().empty());
  if (v.empty() || v.back().empty()) return 1;
  v.back() = std::string(3, v.back().at(0));
  print(v);

  // échange des premier et deuxième mots (4 variantes)
  if (v.size() < 2) return 2;
  // std::swap(v[0], v[1]);
  // ou
  // std::iter_swap(v.begin(), v.begin()+1);
  // ou
  // std::string tmp = v[0];
  // v[0] = v[1];
  // v[1] = tmp;
  // ou 
  v.insert(v.begin(), v[1]);
  v.erase(v.begin()+2);
  print(v);

  // ajout d'une virgule à la fin de chaque mot du vecteur
  for (auto i = v.begin(); i != v.end(); ++i)
	  i->push_back(','); // déréférencement, identique à (*i).push_back(',');
  print(v);  

  std::sort(v.begin(), v.end());
  print(v);  
  
  return 0;
}
