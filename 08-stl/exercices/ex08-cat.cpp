// fonction ayant comme paramètre trois vecteurs d'entiers 
// et remplissant le troisième avec la concaténation des deux premiers

#include <iostream>
#include <vector>

// avec des itérateurs et push_back
void cat(const std::vector<int>& u, const std::vector<int>& v, std::vector<int>& w)
{
    for (auto it = u.begin(); it != u.end(); ++it)
        w.push_back(*it);

    for (auto it = v.begin(); it != v.end(); ++it)
        w.push_back(*it);
}

// variante avec insert
void cat2(const std::vector<int>& u, const std::vector<int>& v, std::vector<int>& w)
{
	w.insert(w.end(), u.cbegin(), u.cend());
	w.insert(w.end(), v.cbegin(), v.cend());
}

void print(const std::vector<int>& v)
{
    for (size_t i = 0; i < v.size(); ++i)
        std::cout << i << ':' << v[i] << ' ';
    std::cout << std::endl;
}

int main()
{
    std::vector<int> u = { 0, 1, 2, 3 };
    std::vector<int> v = { 5, 6, 7 };
    std::vector<int> z; // vide
    print(u);
    print(v);
    cat(u, v, z);
    print(z);
}
