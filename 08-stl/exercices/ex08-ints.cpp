#include <iostream>
#include <vector>

// la fonction retourne un vecteur rempli des entiers 0..n
// on passe en paramètre le plus grand entier à écrire dans le vecteur
std::vector<int> make(int n)
{
    std::vector<int> ret;

    for (int i = 0; i <= n; ++i)
        ret.push_back(i);
        
    return ret;
}

// on passe en paramètres
// - le plus grand entier à écrire dans le vecteur v
// - le vecteur v à remplir (passé par par référence)
// la fonction retourne un booléen = pas d'erreur
bool fill(int n, std::vector<int>& v)
{
	// precondition: le parametre v doit être vide
   	if (!v.empty())
	{
		std::cerr << "remplir: le vecteur n'est pas vide" << std::endl;
		return false; // retour avec erreur
	}

	// remplissage de v avec 0..n
	for (int i = 0; i <= n; ++i)
		v.push_back(i);

	return true; // retour sans erreur
}

// affichage du vecteur v en paramètre
void print(const std::vector<int>& v);

int main()
{
    std::vector<int> v;
	fill(10, v);
    print(v);
}    

void print(const std::vector<int>& v)
{
    for (size_t i = 0; i < v.size(); ++i)
        std::cout << i << ':' << v[i] << ' ';
	std::cout << std::endl;
}
