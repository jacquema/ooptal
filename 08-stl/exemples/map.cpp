#include <iostream>
#include <map>

int main()
{
   std::map<std::string, int> mymap = { { "beta", 0 },
	                                    { "gamma", 0 },
                                        { "alpha", 0 } };

    mymap["alpha"] = 10;
    mymap["beta"]  = 20;
    mymap["gamma"] = 30;

    for (auto& x: mymap) 
      std::cout << x.first << " : " << x.second << std::endl;

    return 0;
}