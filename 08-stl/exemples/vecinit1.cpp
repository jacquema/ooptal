#include <iostream>
#include <vector>

int main()
{
    std::vector<int> v{10, 11, 12};

    for (size_t i = 0; i < v.size(); ++i)
        std::cout << i << ':' << v[i] << ' ';
    
    std::cout << std::endl;        
}    