#include <iostream>

int main()
{
    const unsigned int NB_ELT = 10;
    int table[NB_ELT];
    for (size_t i = 0; i < 100; ++i)
      table[i] = 100 - i;

    for (size_t i = 0; i < 100; ++i)
        std::cout << i << ':' << table[i] << std::endl;

    return 0;     
}