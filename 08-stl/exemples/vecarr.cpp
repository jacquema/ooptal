#include <iostream>
#include <vector>

int main()
{
    int tab[] = { 10, 11, 12 };
    std::vector<int> v(tab, tab+3);

    for (size_t i = 0; i < v.size(); ++i)
        std::cout << i << ':' << v[i] << std::endl;
    
}    