#include <iostream>
#include <set>

int main()
{
  std::set<int> myset;
  for (int i = 1; i < 5; ++i) myset.insert(i*3);

  for (int i = 0; i < 10; ++i)
  {
    std::cout << i << (myset.count(i) == 1 ? " est" : " n'est pas");
    std::cout << " dans myset" << std::endl;
  }

  // nouvel élément
  std::pair<std::set<int>::iterator,bool> ret = myset.insert(4);  
  std::cout << " 4 " << (ret.second ? "ajouté" : "déjà là") << std::endl;

  // élément pas ajouté.
  ret = myset.insert(6);  
  std::cout << " 6 " << (ret.second ? "ajouté" : "déjà là") << std::endl;
  
  for (int i = 0; i < 10; ++i)
  {
    std::cout << i << ( myset.find(i) != myset.end() ? " est" : " n'est pas");
    std::cout << " dans myset" << std::endl;
  }

  return 0;
}