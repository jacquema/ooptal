#include <iostream>
#include <string>
#include <vector>

int main()
{
    std::vector<std::string> v{"ba", "bu"};

    for (size_t i = 0; i < v.size(); ++i)
        std::cout << i << ':' << v[i] << ' ';
    
    std::cout << std::endl;        
}    