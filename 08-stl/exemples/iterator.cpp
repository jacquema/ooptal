#include <iostream>
#include <vector>


int main()
{
    std::vector<int> v; // vide
    
    // on initialise le vecteur
    for (int b = 0; b < 10; ++b) 
      v.push_back(b*10);

    // on affiche le contenu du vecteur
    for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
    	std::cout << *it << ' ';
    std::cout << std::endl;

    // on modifie chaque élément du vecteur   	
    for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
	    *it += 1;

    // on affiche à nouveau le contenu
    for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
    	std::cout << *it << ' ';
    std::cout << std::endl;

    return 0;
}