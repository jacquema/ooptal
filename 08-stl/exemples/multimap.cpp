#include <iostream>
#include <map>

int main()
{
  std::multimap<std::string, int> mymm;

  mymm.insert (std::make_pair("x", 10));
  mymm.insert (std::make_pair("y", 20));
  mymm.insert (std::make_pair("y", 30));
  
  std::multimap<std::string, int>::iterator it = mymm.find("y");
  for ( ; it != mymm.end(); ++it)
    std::cout << "y => " << it->second << std::endl;

  return 0;
}