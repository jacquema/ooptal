#include <iostream>
#include <vector>

int main()
{
    std::vector<int> v;

    for (int b = 0; b < 10; ++b) 
        v.push_back(b*10);

    for (std::vector<int>::const_iterator it = v.cbegin(); it != v.cend(); ++it)
        std::cout << *it << ' ';        
    std::cout << std::endl;

    return 0;
}
