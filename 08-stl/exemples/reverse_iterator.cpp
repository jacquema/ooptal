#include <iostream>
#include <vector>

int main()
{
    std::vector<int> v;
    for (int b = 0; b < 10; ++b) 
        v.push_back(b*10);

    for (std::vector<int>::reverse_iterator it = v.rbegin(); it != v.rend(); ++it)
        std::cout << *it << ' ';        
    std::cout << std::endl;

    return 0;
}
