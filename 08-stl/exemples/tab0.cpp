#include <iostream>

int main()
{
    const int TABSIZE = 10;
    int tab[TABSIZE];
    tab[0] = 100;

    for (size_t i = 0; i < TABSIZE; ++i)
        std::cout << i << ':' << tab[i] << ' ';
    
    std::cout << std::endl;    
}    