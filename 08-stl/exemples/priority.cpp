#include <iostream>
#include <queue> 


int main()
{
 std::priority_queue<int> q;

  q.push(25);
  q.push(90);
  q.push(10);
  q.push(50);

  while (!q.empty())
  {
     std::cout << ' ' << q.top();
     q.pop();
  }
  std::cout << std::endl;

    return 0;
}