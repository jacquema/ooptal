#include <iostream>

int main()
{
    const unsigned int NB_ELT = 10;
    int tab[NB_ELT];

    for (size_t i = 0; i < NB_ELT; ++i)
        tab[i] = 100 - i;

    for (size_t i = 0; i < NB_ELT; ++i)
        std::cout << i << ':' << tab[i] << ' ';
    
    std::cout << std::endl;    
}    