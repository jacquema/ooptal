#include <iostream>

void swap(int& x, int& y)
{
    int tmp = x;
    x = y;
    y = tmp;
}

int main()
{
    int x = 0;
    int y = 1;
    swap(x, y);
    std::cout << "x = " << x << " y = " << y << std::endl;
    
    return 0;
}