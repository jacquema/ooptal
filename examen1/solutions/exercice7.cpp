#include <iostream>
#include <vector>

void saisir(size_t n, std::vector<int>& v)
{
    size_t c = 0;
	while (c < n)
	{
	    int i;
	    std::cout << "entier " << c+1 << ": ";
	    std::cin >> i;
	    if (std::cin.fail()) // bonus
		{
			std::cin.clear();
			std::cin.ignore(10000, '\n');
		}
	    else if (i >= 0)
	    {
	        v.push_back(i);
	        ++c;
	    }
	}
}


int main()
{
    std::vector<int> v;
    saisir(3, v);
    std::cout << v.size() << std::endl;
	return 0;
}