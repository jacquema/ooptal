#include <iostream>

int main()
{
    int A, B, C, D, maxAC, minBD;
    std::cout<< "saisir A: ";
    std::cin >> A;
    std::cout<<"saisir B: ";
    std::cin >> B;
    std::cout<<"saisir C: ";
    std::cin >> C;
    std::cout<<"saisir D: ";
    std::cin >> D;

    if (A < C)
        maxAC = C;
    else 
        maxAC = A;

    if (B < D)
        minBD = B;
    else 
        minBD = D;
        
    if (maxAC > minBD)
        std::cout << "intersection vide" << std::endl;
    else if (maxAC == minBD)   
        std::cout << "intersection = " << maxAC << std::endl;
    else
        std::cout << "intersection = [" << maxAC << ", " << minBD << "]" << std::endl;

    return 0;
}
