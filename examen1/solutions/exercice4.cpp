#include <iostream>
#include <vector>

double mean(const std::vector<double> &v)
{
	if (v.empty())
		return 0;
	double sum = 0;
	for (size_t i = 0; i < v.size(); ++i)
		sum += v[i];
	return sum / v.size();
}

int main()
{
	std::cout << mean({1, 5, 10}) << std::endl;
	std::cout << mean({1}) << std::endl;
	return 0;
}