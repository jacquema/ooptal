#include <iostream>
#include <vector>

std::string cat(const std::vector<std::string>& v)
{
    std::string res; // vide
	for (size_t i = 0; i < v.size(); ++i)
	{
	    res += v[i];
	    res.push_back(' ');
	}

	return res;
}

void cat2(const std::vector<std::string>& v, std::string& r)
{
	for (size_t i = 0; i < v.size(); ++i)
	{
	    r += v[i];
	    r.push_back(' ');
	}
}


int main()
{
	// compilation avec option -std=c++11
	std::cout << cat({"ba", "be", "bi"}) << std::endl;
	
    std::string s;
    cat2({"bu", "ba", "bi"}, s);
	std::cout << s << std::endl;
	return 0;
}