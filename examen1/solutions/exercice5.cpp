#include <iostream>
#include <vector>

size_t skip(const std::string& s, size_t i, char c)
{
	for (size_t j = i; j < s.size(); ++j)
		if (s[j] != c)
			return j;
	return i;
}

size_t next(const std::string& s, size_t i, char c)
{
	for (size_t j = i; j < s.size(); ++j)
		if (s[j] == c)
			return j;
	return i;
}

void capitalize(std::string& s)
{
	for (size_t i = 0; i < s.size();)
	{
		i = skip(s, i, ' ');
		if ('a' <= s[i] && s[i] <= 'z')
			s[i] -= 32; // conversion en majuscule
		size_t j = next(s, i, ' ');
		if (j == i) // dernier mot
			break; 
		else
			i = j;
	}
}

std::vector<std::string> split(const std::string &s)
{
	std::vector<std::string> r; // initialement vide
	for (size_t i = 0; i < s.size(); ++i)
	{
		i = skip(s, i, ' ');

		std::string w;
		for (; i < s.size() && s[i] != ' '; ++i)
			w.push_back(s[i]);
		if (!w.empty())
			r.push_back(w);
	}
	return r;
}

int main()
{
	std::string s;
	std::cout << "saisir du texte: ";
	getline(std::cin, s);
	capitalize(s);
	std::cout << s << std::endl;

	std::vector<std::string> v = split(s);
	std::cout << v.size() << std::endl;

	return 0;
}
