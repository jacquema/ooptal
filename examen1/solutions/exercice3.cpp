#include <iostream>

bool chiffre(char c)
{
    return ('0' <= c && c <= '9');
}

bool nombre(const std::string& s)
{
    for (size_t i = 0; i < s.size(); ++i)
        if (! chiffre(s[i]))
            return false;
    return true;
}

int decode(const std::string& s)
{
    int res = 0;
    for (size_t i = 0; i < s.size(); ++i)
    {
        if (chiffre(s[i]))
            res = res*10 + (s[i]-'0');
        else 
            return -1;
    }
    return res;
}

bool sdecode(const std::string& s, int& r)
{
    r = 0;
    if (s.empty())
        return true;
    bool neg = false;

    for (size_t i = 0; i < s.size(); ++i)
    {
        if (s[i] == '-' && i == 0 && s.size() > 1)
            neg = true;
        else if (chiffre(s[i]))
            r = r*10 + (s[i]-'0');
        else 
        {
            r = 0;
            return false;
        }
    }
	if (neg) r = r * -1;
    return true;
}

int main()
{
    std::cout << chiffre('0') << std::endl;
    std::cout << chiffre('a') << std::endl;

    std::cout << nombre("00") << std::endl;
    std::cout << nombre("-1") << std::endl;  
    
    std::cout << decode("2145") << std::endl;
    std::cout << decode("a1") << std::endl;  
    
    int x;
    std::cout << (sdecode("-213", x)?"OK ":"NO ") << x << std::endl;  
    std::cout << (sdecode("0213", x)?"OK ":"NO ") << x << std::endl;
    std::cout << (sdecode("-", x)?"OK ":"NO ") << x << std::endl;
    std::cout << (sdecode("a213", x)?"OK ":"NO ") << x << std::endl;
    
    return 0;
}

