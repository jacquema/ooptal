# Examen de programmation orientée objet 

INALCO  - Master TAL première année, semestre 1 
19 décembre 2024 9:00 - 11:00 (11:40)

- tous documents autorisés.
- rédiger chaque exercice numéro `n` dans un fichier `nom_exercicen.cpp` 
- déposer les fichiers sur moodle.inalco.fr, de préférence dans un zip,  
  ou les envoyer par mail à  `florent.jacquemard@inria.fr`



> **Exercice 1.**
> Écrire une fonction `swap` ayant pour paramètres deux variables entières, et qui échange leurs contenus respectifs (sans utiliser `std::swap` !).
> Tester cette fonction dans `main`.



> **Exercice 2.**
> Écrire un programme qui demande à l’utilisateur de saisir 4 entiers `A`, `B`, `C` `et` `D`, puis indique quelle est l'intersection des intervalles [AB] et [CD].
> On vérifiera préalablement que `A < B` et `C < D`.
> *hint*:  on pourra utiliser deux variables auxiliaires contenant respectivement le max de `A` et `C` et le min de `B` et `D`.



> **Exercice 3.**  
>
> 1. Écrire une fonction booléenne de paramètre un caractère `c` qui retourne vrai si `c` est un chiffre.
>
> 2. Écrire une fonction booléenne ayant pour paramètre une chaîne de caractères et qui qui teste  si  celle-ci code un entier non signé (c'est à dire qu'elle ne contient que des chiffres).
>
> 3. Écrire une variante de la fonction précédente, avec pour paramètre une chaîne de caractères `s`, et qui retourne la valeur de l'entier codé par `s` ou -1  si `s` ne code pas un entier non signé.
>    On n'utilisera pas de fonction de librairie comme  `stoi` ou `atoi`
>
> 4. Écrire une seconde variante, boolèenne, qui testera si la chaîne de caractères en paramètre code un entier *signé* (qui peut commencer par le caractère `-` ). 
>    De plus, dans l'affirmative, la valeur de l'entier sera calculée et affectée à un second paramètre passé par référence. Sinon, 0 sera affecté à ce paramètre.
>
>    Par exemple., en appelant cette fonction `f`, 
>    `f("10", x)` retourne `true` et affecte 10 à `x`, 
>
>    `f("-1", x)` retourne `true` et affecte -1 à `x`, 
>
>    `f("-", x)` retourne `false` et affecte 0 à `x`, 
>
> On testera chaque fonction dans `main` avec des valeurs littérales.



> **Exercice 4**.  
> Écrire une fonction retournant la moyenne des éléments d'un vecteur (`std::vector`) de `double` passé en paramètre. 



> **Exercice 5**.
>
> 1. Écrire une fonction `skip` de paramètres une chaîne de caractère `s`,  un entier `i` et un caractère `c`  (séparateur) et qui retourne le premier index après `i`  (inclus) qui est différent de `c` dans `s` ou `i` si il n'y en a pas.
> 2. Écrire une fonction `next` de paramètres une chaîne de caractère `s`,  un entier `i` et un caractère `c`  (séparateur) et qui retourne le premier index après `i`  (inclus) qui est  `c` dans `s` ou `i` si il n'y en a pas.
> 3. À l'aide des deux fonctions précédentes, écrire une fonction qui reçoit une chaîne de caractère en paramètre et capitalise la première lettre de chaque mot de celle-ci (les mots étant séparés par des espaces).
> 4. Écrire  une fonction qui reçoit une chaîne de caractère `s` en paramètre et retourne un vecteur (`std::vector`) contenant chaque mot de `s`.
> 5. Tester les 2 fonctions précédentes dans `main` en utilisant `getline(std::cin, s)` pour assigner à la variable `s`  une  chaîne de caractère avec des espaces.



> **Exercice 6.**  
>
> 1. Écrire une fonction `cat` ayant pour paramètre un vecteur (`std::vector`) de chaînes de caractères et retournant la concaténation de celles-ci.
>    Les éléments seront séparés par un espace  dans la concaténation.
> 2. Écrire une variante où la concaténation est ajoutée à la fin d'une variable de type `std::string` passée par référence.
>
> 3. Tester chaque fonction dans `main` avec des valeurs littérales.



> **Exercice 7**.
>
> Écrire une fonction qui a pour paramètres un entier `n` et un vecteur d'entiers `v` et qui remplira `v` avec `n` entiers positifs saisis par l'utilisateur.
> Les éventuels entiers négatifs saisis seront ignorés.
>
> **bonus:**  une saisie qui ne serait pas un entier sera aussi ignorée.


