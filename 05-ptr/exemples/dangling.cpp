// exemple 18

#include<iostream>
#include<string>
 
int main()
{
	
	std::string* p1(new std::string("M1 TAL"));
	std::string* p2 = p1;
	delete p1;
	p1 = nullptr;

	std::cout<< "p2 = " << *p2 << std::endl;

    return 0;
}

