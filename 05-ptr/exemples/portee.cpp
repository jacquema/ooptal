// exemple 3

#include<iostream>
 
int main()
{
	
	int x = 0;
	std::cout<< "x=" << x << std::endl;

	{ 
		int x = 1; // nouveau x de portée limitée au bloc
		std::cout<< "x=" << x << std::endl;
	}
	
	std::cout<< "x=" << x << std::endl;

    return 0;
}