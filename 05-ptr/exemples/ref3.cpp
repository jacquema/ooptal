// exemple 3

#include<iostream>
 
int main()
{

	int var(0);
	int copie(var); // autre variable, initialisée à la valeur de var
                    // var == 0  copie == 0
	std::cout<< "var=" << var << " copie=" << copie << std::endl;

	var = 1; // var == 1  copie == 0
	std::cout<< "var=" << var << " copie=" << copie << std::endl;
	
	copie = 2; // var == 1  copie == 2
	std::cout<< "var=" << var << " copie=" << copie << std::endl;

    return 0;
}