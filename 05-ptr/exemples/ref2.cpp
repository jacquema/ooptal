// exemple 2

#include<iostream>
 
int main()
{

	int var(0);
	int& ref(var); // var == 0  ref == 0
	std::cout<< "var=" << var << " ref=" << ref << std::endl;
	var = 1; // var == 1  ref == 1
	std::cout<< "var=" << var << " ref=" << ref << std::endl;
	ref = 2; // var == 2  ref == 2
	std::cout<< "var=" << var << " ref=" << ref << std::endl;

    return 0;
}