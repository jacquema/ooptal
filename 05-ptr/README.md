# Cours de programmation objet

INALCO  
Master TAL première année

---

## Cours 05 Références et Pointeurs

jeudi 24 novembre 2024  
Florent Jacquemard  `florent.jacquemard@inria.fr`

sources: 

- [Initiation à la programmation en C++](https://www.epflpress.org/produit/805/9782889143962/initiation-a-la-programmation-en-c)
  Jean-Cédric Chappelier, Jamila Sam, Vincent Lepetit, EPFL
  
  

## Introduction

Un pointeur est un lien vers un objet existant en mémoire, c'est à dire le contenu d'une variable.

On peut faire l'analogie avec un URL, qui est un lien sur une page Web:
l'URL est une adresse qui permet d'accéder au contenu de la page, 
mais ne contient pas lui même le contenu de la page (c'est juste un lien).

De la même manière, les pointeurs (et références) permettent d'utiliser ou partager des objets sans copier tout le contenu de ces objets.

On va considérer dans ce cours trois catégories qui correspondent à trois cas d'utilisation.

- les **références** ne sont pas de vrai pointeurs.
  Elles sont totalement gérés par le compilateur, donc d'utilisation sûre, 
  mais leur usage est limité.

- les **pointeurs de base** (pointeurs "à la C" ou *raw pointers*), 
  à l'inverse, ils sont totalement gérés par le programmeur. 
  Ils sont très puissants mais aussi dangereux (fréquente source d'erreurs).

- les **pointeurs intelligents** (*smart pointeurs*), introduit dans `C++11`
  ils sont gérés par le programmeur mais avec une aide du compilateur, 
  qui apporte des gardes fous. 
  Ils sont donc tout aussi puissants que les raw pointers mais plus sûrs.
  
  

Dans les cours suivants, on utilisera uniquement des références (surtout) ou des pointeurs intelligents.

- Les références seront en particulier importantes pour la définition de **fonctions**: 
  pour passer un **argument** à une fonction sans avoir à copier son contenu, 
  ce que l'on appelle *passage par référence*, comme nous le verrons dans le cours sur les fonctions.
- Les pointeurs (intelligents) seront utilisés dans le cadre de la programmation objets, 
  pour écrire des listes d'objets **polymorphes** (il n'existe pas de liste de références).



## Références

Une référence est autre nom (pseudonyme, surnom, *alias*...) pour un objet qui existe déjà en mémoire (le contenu d'une variable).

#### Déclaration de référence

Elle se fait avec la syntaxe:

```C++
	type& nom_reference(identificateur);
```

ou 

```C++
	type& nom_reference = identificateur;
```

`type&` est le type "référence à un objet" du type donné. Le signe `&` caractérise les références.

Après cette déclaration, `nom_reference` est un alias pour la variable `identificateur`. 
C'est à dire que le même objet (contenu de  la variable `identificateur`) pourra être désigné indifféremment par plusieurs noms:  `nom_reference` ou  `identificateur` .



> **Exemple 1** : 
>
> ```c++
> int var = 0;
> int& ref(var);
> ```
>
> La variable `var`, de type `int` contient la valeur `0`.
> `ref`, de type `int&`,  est un autre nom pour `var`.
> Toute opération sur `ref` sera aussi une opération sur `var`.
>
> On illustre comme suit la représentation en mémoire de la variable `var` et la référence `ref`:
>
> ```mermaid
> classDiagram
> class Var["var, ref"]
> Var: 0             
>     
> ```



> **Exemple 2** : pour une assignation:
>
> ```c++
> int var(0);
> int& ref(var);
> var = 1; // var == 1  ref == 1
> ref = 2; // var == 2  ref == 2
> ```



**Attention**: une référence n'est pas une (autre) variable qui copie la première.



> **Exemple 3**: référence ≠ copie de variable.
>
> Ici, on déclare deux variables: `val` et `copie`:
>
> ```c++
> int var = 0;
> int copie(var); // autre variable, initialisée à la valeur de var
>                    // var == 0; copie == 0
> var = 1;   // var == 1 copie == 0
> copie = 2; // var == 1 copie == 2
> ```
>
> modifier `var`  ne modifie pas `ref` et réciproquement.
>
> Aprés l'execution de ces 4 lignes, on a en mémoire:
>
> ```mermaid
> classDiagram
> class V["var"]
> V: 1  
> class C["copie"]
> C: 2
> ```



#### Référence constante

C'est une référence déclarée avec le mot clé `const`:

```c++
const type& ref(identificateur);
```

Dans ce cas, le contenu référencé ne peut pas être modifié via le nom `ref`.

Celà dit, le contenu peut être modifié via un autre nom, si ce dernier n'est pas déclaré comme `const`.

> **Exemple 4**:
>
> ```c++
> int var(0);
> const int& ref(var);
> var = 1; // var == 1 ref == 1
> ref = 2; // ERREUR car `ref` est céclaré const
> ```

Donc le qualificateur `const` s'applique au nom de variable concerné, mais pas au contenu lui même.



#### Restrictions des références

1. Toute référence doit être initialisée vers un objet existant.

L'objet existant est la variable dont on passe l'identificateur à la déclaration.

> **Exemple 5**:  `var` est un objet existant (variable) qui peut être référencé 
> (même si son contenu ici est indéterminé).
>
> ```c++
> int var;
> int& ref(var);
> ```
>
> En revanche, Une déclaration de la forme:
>
> ```c++
> int& ref;
> ```
>
> sera rejetée par le compilateur.
>



2. Une référence ne peut être liée qu'à un seul objet:
   on ne peut pas changer le nom dont la référence est un alias.

> **Exemple 6**:
>
> ```c++
> int x(0);
> int& ref(x); // OK, ref est un alias de x. il vaut 0.
> int y(1);
> ref = y;  // x == ref == 1; y == 1
>              // ref est toujours un alias de x (et pas de y)
>              // il prend la valeur de y (1).
> y = 2;    // x == ref == 1; y == 2
>              // y change de valeur, pas ref
> ```
>
> Évolution de l'état des variables pas à pas, 
> Aprés les 3 déclarations/initialisations:
>
> ```mermaid
> classDiagram
> class X["x, ref"]
> X: 0
> class Y[" y "]
> Y: 1
> ```
>
> Aprés l'avant-derniere ligne:
>
> ```mermaid
> classDiagram
> class X["x, ref"]
> X: 1
> class Y[" y "]
> Y: 1
> ```
>
> Aprés la dernière ligne:
>
> ```mermaid
> classDiagram
> class X["x, ref"]
> X: 1
> class Y[" y "]
> Y: 2
> ```



3. une référence ne peut pas être référencée.

> **Exemple 7**:
>
> ```c++
> int x(0);
> int& ref(x);     // OK, ref est un alias de x
> int& rref(x);    // OK, rref est un second alias de x
> int& rref(ref);  // rref rest une reference sur x, pas sur ref
> int&& rref(ref); // ERREUR 
> ```

Une référence n'est pas un objet en mémoire,
c'est juste un nom supplémentaire pour un objet en mémoire.

Une conséquence est que l'on ne peut pas construire de tableau (vecteur, liste) de références.

On utilisera (en programmation objet) des tableaux de pointeurs.



## Pointeurs



#### Définition

Un pointeur est une **variable** qui contient une **adresse** en mémoire.

Donc, un pointeur est bien une variable,  contrairement à une référence qui n'est qu'un alias.

Physiquement, le contenu d'une variable est stocké en mémoire à une certaine adresse.
Donc un pointeur pour contenir l'adresse en mémoire d'un autre objet, 
et, de cette manière, référencer cet objet.


> **Exemple 8**:
>
> ```c++
> int x(1);
> ```
>
> en mémoire on a 
>
> ```
>              -------------------
>  0x85 0xEE 0x00 0x00 0x00 0x01 0xFF 0x2E
>              ↑
>              adresse de x
> ```
>
> le contenu de `x`, l'entier 1,  est codé sur 4 octets en mémoire,  commençant à l'adresse indiquée ci-dessus. Un pointeur sur `x` contiendra cette adresse.



#### Analogie

Pour illustrer les concepts sur les pointeurs, 
on utilisera l'analogie avec un un carnet d'adresses qui contient une adresse par page.

- un pointeur, en tant que variable, est une page du carnet destinée à recevoir une adresse.
- une adresse est le contenu d'une page.
- une page peut être blanche (on ne lui a pas encore donné d'adresse).



#### Déclaration

La déclaration d'un pointeur se fait suivant la même syntaxe que pour les autres variables.

```c++
T* identificateur;
```

- `T*` est le type de pointeur sur un objet de type `T`.
- `identificateur` est le nom (de variable) du pointeur.



> **Exemple 9**:
>
> ```c++
> int* p;
> ```
>
> `p` est pointeur sur une valeur de type `int`.



> Analogie du carnet d'adresse:
> la déclaration est comme la création d'une page blanche.



#### Initialisation et affectation

La déclaration avec initialisation se fait aussi avec la même syntaxe que pour les autres variables.

```c++
	type* identificateur(adresse);
```

- la valeur d'initialisation du pointeur doit être une adresse mémoire.

Il en est de même pour l'affectation d'une valeur d'adresse à un pointeur déclaré.

> ```c++
> type* p;
> p = adresse;
> ```

Nous verrons trois moyens d'obtenir une valeur d'**adresse** à affecter à un pointeur.

> Exemples:
>
> - pointeur "nul"
>
> ```c++
> int* p(nullptr);
> ```
>
> - adresse de variable
>
> ```c++
> int i;
> int* p(&i);
> ```
>
> - allocation dynamique
>
> ```c++
> int* p(new int(5));
> ```



#### `nullptr`

`nullptr` est une adresse spéciale qui ne pointe vers rien.

Elle peut être affectée à n'importe quel type de pointeur 
et signifie que ce pointeur ne contient pas d'adresse.



> **Exemple 10**: déclaration et initialisation à `nullptr`
>
> ```c++
> int* p(nullptr);
> ```
>
> `p` ne pointe vers rien (en attendant d'être modifié).
>  Analogie du carnet d'adresse:
>  création d'une page blanche 
>  (`nullptr` est le contenu d'une page blanche).



#### Opérateur `&`

L'opérateur `&` est appliqué à un identificateur.
Il retourne l'adresse mémoire d'une variable.

> **Exemple 11**:
>
> ```c++
> int x(0);
> int* p(&x); // pointeur sur un int
> ```
>
> le pointeur `p` se voit affecté l'adresse de la variable entière  `x`.
>
> ```mermaid
> classDiagram
> class P["p"]
> P: adresse de x
> class X["x"]
> X: 0
> P --> X
> ```
>
> 



> **Exemple 12**: Le résultat est le même si `p` est initialisé et affecté ensuite.
>
> ```c++
> int x(0);
> int* p(nullptr);
> p = &x;
> ```
>  Analogie du carnet d'adresse:
>  `x`  est un terrain (sur lequel on a construit `0`) et  `&x` est son adresse.
>  `p` est une page du carnet d'adresse, initialement blanche (valeur `nullptr`).
>  On écrit l'adresse `&x` de `x` sur la page `p` du carnet d'adresse.



#### Opérateur `*` (déréférencement)

L'opérateur `*` retourne la valeur pointée par une variable de type pointeur.

Si `p` est de type `T*`, `*p` est une valeur de type `T`.

> **Exemple 13**:
>
> ```c++
> int x(0);
> int* p(nullptr);
> p = &x; // p pointe sur x
> std::cout << *p << std::endl; 
> ```
>
> affiche `0` (le contenu de `x`).

**remarque**:
pour une variable `int i` ,  l'expression `*&i` est équivalente à `i` 
(valeur contenue dans `i`).

**syntaxes** et confusions:
Le symbole `&` est malheureusement utilisé en `C++` pour des usages différents.

- dans `type& ref(x)`, le `&` caractérise le type d'une référence `ref` sur la variable `x`.
- dans `&x`, l'opérateur `&` retourne l'adresse de la variable  `x`.
- il existe aussi un opérateur `&` sur les booléens.

De même  pour `*`.

-  `type* p;` déclare une variable `p` de type pointeur sur `type`.
-  dans  `*p`, quand `p` est un pointeur,  l'opérateur `*` retourne le contenu pointé par `p`.
-  il existe aussi un opérateur binaire `*` (multiplication).



#### Allocation statique 

Lors d'une déclaration de variable, le compilateur évalue la taille de mémoire nécessaire et la réserve pour la variable. 

On parle d'**allocation statique** de mémoire, *i.e.* d'allocation décidée (et gérée) à la **compilation**.



#### Allocation dynamique 

Certains objets ont une taille variable.
Par exemple, c'est le cas des chaînes de caractères de `std::string`: auxquelles on peut ajouter un caractère.

À l'exécution de la commande `s += '!'` (ou `s.push_back('!')`) pour ajouter le caractère `!` à la fin de la chaîne `s`, l'emplacement de mémoire `s` devra (possiblement) être modifié pour pouvoir contenir le caractère supplémentaire. On parle d'**allocation dynamique** (décidée à l'**exécution**).

En C++, l'allocation dynamique se fait à l'aide de deux opérateurs sur les pointeurs

- `new` pour l'allocation de mémoire,
- `delete` pour sa libération.



#### Allocation dynamique: `new`

```c++
new type(args...);
```

alloue une zone mémoire pour un objet de `type` donné,  et retourne l'adresse de cette zone mémoire.

`args...` est une liste (optionnelle) d'arguments utilisée pour créer un objet du `type` donné dans la zone de mémoire allouée.

Cette adresse peut être stockée dans un pointeur:

- par affectation:

```c++
type* pointeur;
pointeur = new type(args...);
```

- ou bien par initialisation directement lors de la déclaration:

```c++
type* pointeur(new type(args...));	
```

```C++
type* pointeur = new type(args...);	
```

Dans les deux cas:

- La variable `pointeur` est  allouée statiquement. 
  Elle pourra contenir l'adresse d'un objet du `type` spécifié. 
- À l'exécution de `new`, un emplacement mémoire est alloué (dynamiquement). 
  Un objet du `type` donné y est construit à l'aide des arguments de la liste `arg...` 
  
- L'adresse de cet  emplacement mémoire  est affectée à `pointeur`. 




>  Analogie du carnet d'adresse:
>
>  - acheter un terrain (à une certaine adresse.
>  - construire un batiment dessus (avec le matériel `arg...`).
>  - écriture de son adresse à la page `p` du carnet d'adresse.



> **Exemple 14**: allocation dynamique
>
> ```c++
> int* p;
> p = new int;
> ```
> Ici, l'adresse stockée dans `p` contient un entier indéterminé (qui correspond à ce qui se trouvait à cet endroit de la mémoire au moment de l'appel de `new`).
>
> ```mermaid
> classDiagram
> class P["p"]
> P: adresse
> class X["  "]
> X: int indéterminé
> P --> X
> ```
>
> avec:
> ```c++
> *p = 4;
> ```
> on copie l'entier `4` l'adresse mémoire  stockée dans `p`.
>
> ```mermaid
> classDiagram
> class P["p"]
> P: adresse
> class X["  "]
> X: 4
> P --> X
> ```
>
> On peut faire l'allocation et construction d'une valeur (dans cette mémoire) en une seule ligne (à la place des lignes 2 et 3) par:
> ```c++
> int* p;
> p = new int(4);
> ```
> ou pour faire encore plus court, grouper cela avec la déclaration du pointeur `p` 
>
> ```c++
> int* p(new int(4));
> ```



>  **Exemple 15**:
>
>  ```c++
>  std::string* p;
>  p = new std::string("abc");
>  ```
>
>  `p` contient l'adresse du début d'une chaîne de caractère `abc`.



#### Désallocation dynamique: `delete`

Quand `p` est un pointeur sur un certain type, 

```c++
delete p;
```

va libérer la zone mémoire à l'adresse stockée dans `p`. Cette zone de mémoire pourra ensuite être ré-utilisée pour y stocker d'autre données.



>  Analogie du carnet d'adresse:
>
> Le propriétaire d'un terrain peut libérer (revendre) ce terrain, et détruire le bâtiment qu'il y avait construit, pour permettre qu'il soit réutilisé.



#### Portée et cycle de vie

Une notion importante qui différencie les pointeurs des autres variables (et qui fait leur intérêt)  est la notion de portée et durée de vie de variables.

Dans le cas de l'**allocation statique**:

- la **portée** d'une variable `x` est le bloc de code où elle est déclarée.
- sa **durée de vie** est le temps durant lequel elle existe en mémoire.
  C'est la période durant laquelle le code du bloc est exécuté.
  Quand le contrôle sort du bloc, la variable `x` est supprimée, 
  on dit qu'elle quitte la portée. 
  On ne peut alors plus utiliser sous peine d'erreur de compilation.

```c++
{ // bloc délimitant la portée de x
  int x;
  ... // ici on peut acceder à x 
} 
// ici, x n'est plus accessible
```

C'est le compilateur qui gère l'allocation d'une zone mémoire pour la variable `x` et sa libération à la fin de vie de `x`.

Notons que l'on peut avoir plusieurs variables de même nom dans des portées différentes!

cf.  [`exemples/portee.cpp`](exemples/portee.cpp)

```c++
int x = 0;
{ 
  int x = 1; // nouveau x de portée limitée au bloc
  ... // ici x = 1 (c'est le x du bloc)
} 
// ici, x = 0 (c'est le premier x, extérieur au bloc).
```

```mermaid
classDiagram
class X["x"]
X: 0
class Xbloc["x du bloc"]
Xbloc: 1
```



Dans le cas de l'**allocation dynamique**, la durée de vie d'un objet pointé par un pointeur peut en revanche dépasser le bloc où il est alloué.

Considérons par exemple une zone mémoire allouée dynamiquement (par `new`) dans un bloc:

```c++
int* p; // pointeur sur un int
{
  ...
  p = new int(0); // allocation d'une zone mémoire, adresse stockée dans p
  ...
}
// ici on peut encore acceder à la zone pointée *p
```

La zone n'est pas libérée, même à la sortie du bloc. 

Cela peut être très utile pour faire utiliser un même objet dans plusieurs blocs de code (plusieurs fonctions par exemple), sans que l'objet soit détruit quand on passe d'un bloc à l'autre.

En contrepartie, la libération de cette mémoire doit être faite manuellement par le programmeur avec `delete` (le programmeur doit gèrer lui même ces allocations mémoire).

Donc les pointeurs apportent plus de pouvoir mais aussi plus de responsabilité!



#### Erreurs courantes liées à l'utilisation des pointeurs

Quand le programmeur oublie de libérer la mémoire allouée dynamiquement, on parle de **fuite de mémoire** (*memory leak*), qui peut conduire à une saturation de la mémoire.

De plus, après `delete p`, le pointeur `p` contient toujours l'adresse mémoire (qui vient d'être libérée). Si l'on accède ensuite à cette adresse via `*p`, on risque de ne pas trouver ce que l'on attend et provoquer une erreur.

> Analogie du carnet d'adresse.
>
> Le propriétaire d'un terrain peut libérer (revendre) ce terrain, et détruire le bâtiment qu'il y avait construit. Si il a conservé son adresse dans son carnet et y retourne ensuite, il pourra y trouver un autre batiment que celui qu'il connaissait. 

On utilise dans ce cas le terme de ***dangling pointer*** (ou *wild pointer*) pour `p`. Cela correspond à une violation de sécurité de la mémoire, dans la mesure où, via `p`, on peut accéder à du contenu mémoire auquel on ne devrait pas accéder. 
Concrètement, si la zone de mémoire a été ré-allouée et que l'on y accède par  `*p`, cela produira une erreur d'execution (*segmentation fault*) ou tout au moins un comportement imprévisible. C'est une erreur très courante.

Pour éviter cela, il est conseillé de faire suivre `delete p;` de 

```c++
p = nullptr;
```

pour éviter un accés ultérieur (illicite) à l'adresse stockée dans `p`.

> Analogie du carnet d'adresse.
>
> Le propriétaire efface l'adresse de son carnet  après avoir revendu le terrain (`p` redevient une page blanche).



> **Exemple 16**:
>
> ```c++
> int* p(nullptr);
> p = new int; // allocation
> *p = 4;      // construction de l'objet
> std::cout << *p << std::endl; // accès
> delete p;    // destruction de l'objet (libération de la mémoire)
> p = nullptr; // effacement de l'adresse
> ```
>
> Rappel: on peut fusionner les lignes 1 et 2 en une seule
>
> ```c++
> int* p(new int);
> ```
>
> ou aussi fusionner les lignes 1, 2, 3:
>
> ```c++
> int* p(new int(4));
> ```



Une autre cause de *segmentation fault* est l'accès à un emplacement mémoire non alloué.

> **Exemple 17** (*segmentation fault* )
> cf.  [`exemples/segfault.cpp`](exemples/segfault.cpp)
>
> ```c++
> int* p;
> *p = 10;
> ```
>
> la deuxième ligne demande un accès à une adresse mémoire qui n'est pas une adresse mémoire accessible.
>
> Ce code va (malheureusement) compiler correctement mais son execution provoquera une erreur brutale *segmentation fault*  (ou *bus error*).



Il est conseillé de toujours initialiser les pointeurs. En l'absence d'adresse connue, on utilisera `nullptr`.

```c++
type* pointeur(nullptr);
```

Ainsi, il sera possible d'appliquer un garde-fou avant d'accéder au pointeur.

```c++
if (pointeur != nullptr)
  *pointeur = 10;
```

ou bien encore:

```c++
assert(pointeur != nullptr);
*pointeur = 10;
```

`assert` évalue une expression booléenne et provoque la fin du programme si sa valeur est `false`.

Par abus, on peut aussi écrire:

```C++
assert(pointeur);
```

avant un déréférencement de `pointeur`.

Donc `assert` peut provoquer aussi l'arrêt du programme mais l'avantage  (par rapport à un crash par `segmentation fault`) est qu'un message d'erreur explicite est fourni, avec en particulier le numéro de ligne de l'`assert`. Il est donc beaucoup plus facile ensuite de corriger l'erreur incriminée.

Un autre avantage d'`assert` (par rapport au `if`) est que l'on peut (par une option) compiler sans les assert lorsqu'on est sûr de son code (pour ne plus ralentir l'exécution par les tests).



## Pointeurs intelligents

On a vu que les pointeurs sont polyvalents et puissants mais potentiellement dangereux, et leur utilisation demande beaucoup de rigueur.

> **Exemple 18**: *dangling pointer* (2)
>
> ```c++
> std::string* p1(new std::string("M1 TAL"));
> std::string* p2 = p1;
> delete p1;
> p1 = nullptr;
> ```
>
> On a initialement (après les 2 premières lignes) deux pointeurs `p1` et `p2` contenant la même adresse d'une chaîne de caractères.
>
> ```mermaid
> classDiagram
> class P1["p1"]
> P1: adresse
> class P2["p2"]
> P2: adresse
> class X[" (str) "]
> X: M1 TAL
> P1 --> X
> P2 --> X
> ```
>
> 
>
> La zone mémoire de cette chaîne est libérée par `delete p1;`  et `p1` est bien effacé.
> Mais, alors que la machine reprend la main sur cette zone mémoire, `p2` pointe encore dessus.
>
> ```mermaid
> classDiagram
> class P1["p1"]
> P1: nullptr
> class P2["p2"]
> P2: adresse
> class X["  "]
> X: ???
> P2 --> X
> ```
>
> Par la suite, cela pourra causer des problèmes de violation de segment en cas de déréférencement `*p2` de `p2` , dans une portion de code qui ne sait pas que la mémoire pointée a été libérée.



`C++11` introduit une notion de pointeurs intelligents (*smart pointers*) pour éviter les soucis liés à `delete`. 
Définis dans la bibliothèque `memory`, ils ont l'avantage de faire eux-mêmes leur propre `delete`, au moment opportun, grâce à des techniques de *garbage collection* (GC).

Intuitivement, il est recensé, pour une adresse mêmoire pointée, le nombre de pointeurs intelligents contenant cette adresse. Lorsque le décompte tombe à zéro, la zone mémoire correspondante est automatiquement libérée.

Ainsi

- il n'est plus besoin de désallocation manuelle par  `delete`
- les problèmes de *dangling pointers* comme dans l'exemple ci-dessus ne peuvent plus se produire.



#### `unique_ptr`

`unique_ptr` est une famille de pointeurs intelligents tels qu'il peut y avoir au plus 1 tel pointeur pointant sur une zone mémoire (on l'appelle le **propriétaire**).

Par conséquent, les `unique_ptr`  ne peuvent être copiés (voir plus loin).

Leur déclaration suit la syntaxe suivante:

```c++
std::unique_ptr<type> pointeur(new type(args...));
```

- `type` est le type de l'objet pointé (dont le pointeur contient l'adresse),
- `args...` est la liste des arguments pour la création d'un objet à l'adresse pointée
  (comme pour les pointeurs standards, avec `new`).

Après cette déclaration, un `unique_ptr` peut être utilisé comme un pointeur standard, 
à l'exception de la **copie** qui est **interdite**.

Le pointeur intelligent peut être déréférencé par `*pointeur` pour accéder au contenu de l'adresse pointée.

On peut aussi utiliser la fonction `std::make_unique` pour créer (allouer) l'adresse pointée.

```c++
std::unique_ptr<type> pointeur = std::make_unique<type>(args...);
```



> **Exemple 19**:
>
> ```c++
> #include<memory>
> 
> int main()
> {
> 	std::unique_ptr<int> p(new int(0));
> 	*p = *p + 2;
> 	std::cout << *p << std::endl;
>  	return 0; 
> }
> ```
>
> `p` est un pointeur unique sur un entier.  
>
> il n'est besoin libérer la mémoire pointée, c'est fait automatiquement quand `p` tombe hors de portée, ici à la ligne  `return 0;`
>
> `p` ne peut être copié.
>
> ```c++
> std::unique_ptr<int> p2(p);  // ERREUR!
> std::unique_ptr<int> p2 = p; // ERREUR AUSSI!
> ```



Un `unique_ptr`  `p`  peut être **déplacé** vers un autre `unique_ptr` `p2`, 
le pointeur initial `p` devenant alors inutilisable (la variable est désallouée). 
Ce mécanisme, nommé *move semantics* est hors du sujet du cours.

La mémoire pointée par un  `unique_ptr`  `p` est libérée automatiquement quand celui-ci devient obsolete (on est sûr qu'il plus utilisé).

Il est possible (mais pas obligatoire) de forcer cette libération par:

```c++
pointeur.reset();
```

- libère la zone de mémoire pointée par `pointeur`
- assigne `nullptr` à `pointeur`.

En revanche, ```delete pointeur``` est interdit pour un   `unique_ptr` .



#### `shared_ptr`

Avec les pointeurs intelligents partagés, `shared_ptr`, plusieurs pointeurs, multiples **propriétaires**, peuvent pointer sur une même zone mémoire. 

- Le premier propriétaire est créé par initialisation avec une adresse (`new`), 
  comme pour `unique_ptr`.
  
- les suivants sont crées par copie.

- Les propriétaires sont **dénombrés** (automatiquement). 
  Lorsque le décompte tombe à 0, la zone mémoire pointée est **libérée**.

Aucun propriétaire n'est privilégié par rapport à un autre. 
Par conséquent, des situations de dépendance cyclique peuvent apparaître, 
ce qui aura pour conséquence que la mémoire ne sera jamais libérée (fuite de mémoire).



> **Exemple 20**:
> https://stackoverflow.com/questions/4984381/shared-ptr-and-weak-ptr-differences
>
> Soit un type `Person` représentant une personne avec un nom (chaine de caractère), qui contient un pointeur intelligent partagé sur un partenaire qui est une autre `Person`.
>
> ```C++
> class Person { std::shared_ptr<Person> partner; }; // partner initially created empty
> ```
>
> On pourra avoir le scénario suivant, avec une fuite de mémoire:
>
> ```c++
> shared_ptr<Person> alice { std::make_shared<Person>("Alice") }; 
> shared_ptr<Person> bob { std::make_shared<Person>("Bob") }; 
> alice->partner = bob;
> bob->partner = alice;
> ```
>
> à la fin, le décompte de propriétaires pour l'objet "Bob" est 2 (`bob` and `alice->partner`).
> et le décompte pour l'objet "Alice" est 2 (`alice` and `bob->partner`).
> Quand `bob` quitte la portée, le décompte de  "Bob"  tombe à 1,
> l'objet "Bob" n'est pas désalloué (le décompte doit être 0 pour cela).
> Quand `alice` quitte la portée, le décompte de  "Alice"  tombe à 1,
> l'objet "Alice" n'est pas désalloué.
>
> Les deux objets neutralisent mutuellement leur désalocation.



#### `weak_ptr`

 `weak_ptr` est proche de  `shared_ptr` mais le pointeur n'est pas compté dans les propriétaires.
Cela sert à casser un éventuel cycle de dépendances de `shared_ptr`.

Dit d'une autre manière, un `weak_ptr` est comme un pointeur standard (pas de libération automatique),
mais pour lequel il est possible de tester (à l'aide d'une fonction `lock()` ) si l'adresse pointée a été libérée. 



> **Exemple 21**: https://stackoverflow.com/questions/12030650/when-is-stdweak-ptr-useful
>
> Reprenons l'exemple 18 où le (raw) pointeur `p2` devient "dangling" après désalocation de `p1` (`delete p1`).
>
> ```C++
> std::shared_pr<std::string> p1;
> p1.reset(new std::string("M1 TAL")); // p1 propriétaire
> std::weak_pr<std::string> p2 = p1;   // p2 n'est pas propriétaire
> p1.reset(new std::string("M2 TAL")); // desallocation, et nouveau pointeur.
> p2.lock() == false; // est vrai (l'objet pointé est expiré)
> std::weak_pr<std::string> p3 = p1;   // nouvelle adresse
> ```



>  **Exemple 22**: redéfinissons la classe `Person`  de sorte le partenaire soit un `weak_ptr:`
>
>  ```C++
>  class Person { std::weak_ptr<Person> partner; };  
>  ```
>
>  avec le code précédent:
>
>  ```C++
>  shared_ptr<Person> alice { std::make_shared<Person>("Alice") }; 
>  shared_ptr<Person> bob { std::make_shared<Person>("Bob") }; 
>  alice->partner = bob;
>  bob->partner = alice;
>  ```
>
>  Ici, le décompte de propriétaires pour l'objet "Bob" est 1 (`bob` seul, car `alice->partner` est `weak`).
>  et le décompte pour l'objet "Alice" est 1 (`alice`).
>
>  Quand `bob` quitte la portée, le décompte de  "Bob"  tombe à 0, et l'objet "Bob" est pas désalloué.
>
>  De même pour l'objet "Alice" quand  `alice` quitte la portée.



## Résumé



| type           | allocation                           |
| -------------- | ------------------------------------ |
| référence      | statique (variable référencée)       |
| smart pointeur | new ou `make_unique`,  `make_shared` |
| pointeur       | new, delete                          |

