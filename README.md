# Cours de programmation orientée objet

cours de programmation orientée objet C++ 
INALCO - Master TAL première année
Florent Jacquemard  `florent.jacquemard@inria.fr`
2024-2025



## Matériel en ligne

- gitlab: notes de cours et exercices 
  https://gitlab.inria.fr/jacquema/ooptal

- MOODLE: questionnaires
  - [S1](https://moodle.inalco.fr/course/view.php?id=2253)
  - [S2](https://moodle.inalco.fr/course/view.php?id=2253)



## Semestre 1

**Fondements de programmation impérative en C++**

TALA440B

13 séances
jeudi 09:00 - 12:00
INALCO salle 3.16 



### 01. introduction

- introduction programmation, langages (de programmation), algorithmes
- premier exemple de compilation 



### 02. variables et expressions

- variables, littéraux, types
- opérateurs, expressions
- instructions simples 
  - déclaration
  - affectation
  - IO standard, flots




### 03. instruction conditionnelles

- alternative  `if` `else` 
- selection   `if`- `else if`- `else`   et  `switch`



### 04. instructions itératives

- boucles  `while`  et `do`-`while`
- boucle `for`  
- sorties de boucle `break` et `continue`



### 05. références et pointeurs intelligents

- références
- pointeurs



### 06. fonctions

- déclaration et définition
- le cas `main`
- appel, contrôle
- passage par valeur ou référence
- surcharge



### 07. chaînes de caractères

- type `std::string`
  - déclaration, initialisation
  - indexation
- fonctions de la bibliothèque sur les chaînes de caractères
- exercices encodages




### 08. conteneurs de la bibliothèque standard (STL)

- tableaux statiques `std::array`
- tableaux dynamiques `std::vector`
- piles `std::stack`
- listes d'association `std::map`



### 09. structures  et types énumérés 

- `struct` (comme introduction aux objets)
- `class enum`



## Semestre 2

**Introduction à la programmation orientée objet en C++**

TALB440B

13 séances
mardi 13:00 - 16:00




### 10. classes et objets

- encapsulation et abstraction
  notion d'interface et de visibilité
- classes, attributs, méthodes
- droits d'accès

- accesseurs / modificateurs



### 11. constructeurs et destructeurs

- constructeurs
  - initialisation
  - constructeur par défaut
  - surcharge
  - constructeur de copie (règle des 3)
- destructeurs
- copie de surface ou profonde



### 11bis. compilation séparée

- modules, ficher interface description, fichier implémentation
  bibliothèques
- espaces de noms
- compilation séparée, édition de liens
  Makefile et Cmake



### 12. variables et méthodes de classe

- attributs de classe *vs* variables globales
- méthodes de classe




### 13. opérateurs et surcharge

- surcharge d'opérateurs
- surcharge externe
- surcharge interne
- surcharge de constructeurs



### 14. héritage

- principe
- héritage simple, dérivation droits accès
- masquage, fonction virtuelles
- constructeurs
- destructeurs
- copie profonde



### 15. polymorphisme

- résolution statique ou dynamique de liens
- méthodes virtuelles
- masquage, substitution, surcharge
- destructeur virtuel
- méthodes virtuelles pures et classes abstraites
- conteneurs hétérogènes
- liste hétérogène (exemple)



### 16. héritage multiple

- concepts, constructeurs
- masquage
- classes virtuelles



### 17. templates

- patrons de fonctions
- patrons de classe





## References

cf. ../references/README.md

