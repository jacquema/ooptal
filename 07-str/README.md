# Cours de programmation objet

INALCO  
Master TAL première année

---

## Cours 07 Chaînes de caractères

jeudi 14 novembre 2024  
Florent Jacquemard  `florent.jacquemard@inria.fr`

sources: 

- cours de Marie-Anne Moreaux, INALCO
- http://casteyde.christian.free.fr/cpp/cours/online/c5501.html
- https://cplusplus.com/reference/string/string/  
- https://en.cppreference.com/w/cpp/string  
- https://www.tutorialspoint.com/cplusplus/cpp_strings.htm  



---

## Chaînes de caractères

Une chaîne de caractères est une liste de caractères finie et ordonnée. 
Le nombre de caractères composant cette liste est appelé sa **longueur**.

La liste peut ne contenir aucun caractère. Une telle liste est dite **chaîne vide** et sa longueur est 0.

En `C++`, une chaîne de caractères peut être implémentée, c’est-à-dire organisée en mémoire, de différentes manières:

- comme en `C`, par un tableau de caractères, de type `char []`, ou `char*`
  dont le dernier élément a la valeur `'\0'` (marque de fin de chaîne).
- par le type `std::string` de la librairie standard (STL).  
  Ce type est propre à `C++` et sera préféré dans notre cas.



## Le type `char[]`

En `C`, un objet de type `char[]` , ou de manière équivalente `char*`, (tableau de caractères) est alloué **statiquement**: le programmeur doit fixer  la longueur de la chaîne de caractères et la spécifier dans une instruction de déclaration de la forme:

```c++
char mot[25];
```

La mémoire sera réservée au moment de la compilation et deviendra accessible au chargement du programme exécutable. Lors de l’exécution, aucune modification de la dimension de cet espace n’est possible. Cela signifie que l'on ne pourra pas changer la longueur de la chaîne.



## Le type `string` (STL)

Le type `string` est un type complexe, défini dans une classe.  
Les objets de type `string` permettent de représenter des chaînes de caractère de longueur variable.

Un objet de type `string` est alloué **dynamiquement** : la dimension de l’espace mémoire nécessaire à son stockage n'est pas fixée dès le début de l'exécution du programme. La mémoire est allouée au fur et à mesure du besoin durant l'exécution, et peut être re-dimensionnée. Cela se fait automatiquement, sans que l'utilisateur de la classe n'ait à s'en préoccuper.

**Utilisation de la librairie string:**

```c++
#include <string>
```



## Déclaration et initialisation d’un objet de type `string`

La déclaration d’une variable de type string a la forme usuelle:

```c++
std::string s;
```

La variable construite contient initialement une chaîne vide.

On peut déclaration une variable de type string en l'initialisant avec une chaîne littérale :

```c++
std::string s("tschüss"); 
```

ou 

```c++
std::string s = "tschüss";
```

L’objet est construit et initialisé avec une copie de la constante chaîne littérale  `"tschüss"` sans ajout du `'\0'`. 

On notera que le type d'un littéral comme `"tschüss"` est `char[]`, ou `char*`, qui est équivalent. On trouvera donc ce type dans les documents de référence quand un littéral peut être employé.



Dans les déclarations/initialisations suivantes, la variable `s1` est initialisé avec une copie d’un autre objet `s0` de type `string`.

```c++
std::string s1 = s0; 
```

ou 

```c++
std::string s1(s0);
```



On peut aussi, dans une déclaration de variable de type `string` , l'initialiser avec `n` caractères identiques.

```c++
unsigned short n = 10;
std::string s(n, 'a'); 
```

Avec `std::string s(n, c)`:

- l’objet construit est de longueur n et est initialisé avec `n` copies d’un caractère `c`,  

- l’objet copié `c` doit être de type `char`,

  il ne peut être ni de type `string`, ni être une constante chaîne littérale.



## Lecture d’une chaîne de caractères depuis un stream

et assignation de cette suite à un objet string

L'opérateur `>>` extrait, depuis l'entrée standard, tous les caractères jusqu’au 1er caractère **séparateur** rencontré (espace , tab `\t`, retour à la ligne `\n`).  

```c++
std::cin >> s;
```

assigne à la variable `s` (de type `string`) les caractères du flot d'entrée
`std::cin` jusqu'au 1er caractère séparateur.  
On ne peut pas simplement imposer à `>>` un autre caractère de séparateur.

L'instruction [`getline`](https://cplusplus.com/reference/string/string/getline/)

```c++
getline(std::cin, s, sep);
```

extrait du flot `std::cin`  une chaîne de caractères jusqu'au caractère séparateur `sep`, ou jusqu'à la fin du flot `std::cin`, et assigne cette chaîne à la variable `s`. Le caractère séparateur n'est pas placé dans `s`.  

- Le premier argument est de type `std::istream` (*input stream*).
  Il peut être un autre flot d'entrée que `std::cin`, typiquement un fichier ouvert en lecture et qui contiendrait un texte.
- Le second argument est de type `std::string`, passé par référence.

- L'argument `sep`, de type `char` est optionnel. 

Dans la version à 2 arguments, le séparateur par défaut est '\n' (retour à la ligne).

```c++
getline(std::cin, s)
```



## Indexation

Puisqu’une chaîne de caractères est une liste de caractères finie et ordonnée, chaque caractère possède un numéro d’ordre dans la séquence de caractères. Ce numéro est appelé son indice.  
La numérotation, c’est-à-dire l’indiçage, commence à 0.

pour:

```c++
std::string s("abracadabra"); 
```

les indices sont:

```
s → ‘a’ ‘b’ ‘r’ ‘a’ ‘c’ ‘a’ ‘d’ ‘a’ ‘b’ ‘r’ ‘a’ 
     0   1   2   3   4   5   6   7   8   9  10
```



## Les types `std::string::size_type` et `size_t`

Il s'agit du type entier « compagnon » du type `std::string`, qui est utilisé pour les indices dans les chaines.

Chaque conteneur de la STL a le sien propre. Ces types « compagnons » permettent de s'affranchir des types entiers qui dépendent de l'architecture d'une machine, en évitant les problèmes de valeur limiteds.

On ne sait pas à quel type de base correspond `std::string::size_type`. On sait seulement que c'est un type d'entier non signé et qu'il est garanti de pouvoir représenter la longueur de n'importe quelle chaîne de caractères de type `std::string`.

On n'aura pas cette garantie avec d'autres types entiers comme `int`. Par exemple, sur une plate-forme 64 bits, on pourra avoir des `int` représentés sur 32 bits (c'est le cas généralement le cas). Alors, avec un `int`, il ne sera pas possible de traiter des chaînes de plus de $2^{31}$ caractères - ce qui n'est pas si fréquent!

`string ::size_type` sera en revanche une valeur de 64 bits sur ces plates-formes, de sorte qu'il peut représenter des chaînes plus grandes sans problème.  Le type  `std::string::size_type` est donc plus sûr que `int` pour les index de chaînes.

Dans les implémentations standards de la librairie `string`, `std::string::size_type` est défini comme un autre type dépendant de la plateforme qui s'appelle  `size_t` . C'est un type d'entier non signé ( `unsigned`) suffisamment grand pour pouvoir représenter la taille de tout objet. On peut donc aussi utiliser  `size_t` au lieu de  `std::string::size_type`, ce que l'on recommande pour la suite.

La constante `std::string::npos` désigne la plus grande valeur possible d'un élément de type `size_type` pour le type `string`.  Elle est parfois retournée comme valeur d'erreur par certaines fonctions sur les chaînes de caractères.

**rem**. `::` est l’opérateur de portée, il précise qu'il s'agit du nom `size_type` appartenant à la classe `string`, elle même dans la librairie `std`andard. cf. la note sur les `namespace` dans le cours 3.



## Utilisation d'alias pour les noms de type

On peut définir un alias de type avec la commande `typedef`.

```C++
typedef type alias;
```

Par exemple, pour donner un nom plus court au type `std::string::size_type`, 

```c++
typedef std::string::size_type str_size;
```

Avec la commande ci-dessus, `str_size` devient un synonyme pour `std::size_type`, et une déclaration de variable d'index se fera par:

```c++
str_size i;
```



En général, `typedef` pourra être utilisé pour 

- simplifier le code,
- une meilleure documentation du programme en choisissant des noms représentatifs de l'utilisation.



## Opérations sur les objets de type `std::string`

Nous allons maintenant voir un certain nombre d'instructions pour le traitement des chaînes de caractères.

De manière générale, dans les exemples ci-dessous,

- `s`, `s1` et `s2` désignent des objets de type `std::string`,
- `c` désigne un objet de type `char`.
- `s_c` est un littéral chaîne.



### Affectation (copie)

```c++
s1 = s2;
```

assigne l’objet `s2` à `s1` (il en fait une copie).
`s2` peut être de type `char []` (littéral) ou de type `string`.

```c++
s1 = "abc";
```



### Propriétés de chaînes

La fonction booléenne [`empty`](https://cplusplus.com/reference/string/string/empty/) teste si une chaîne est vide:

```c++
s.empty()
```

retourne la valeur  `true` si `s` est une chaîne vide, `false` sinon.

L'appel ci-dessus se fait suivant la syntaxe "orientée objet":  `s.empty()` désigne l'application de la fonction booléenne `empty()` à la chaîne `s`, qui est un objet de type `std::string`.  Intuitivement, c'est comme si `empty` était appelée avec l'argument `s`.

Plus généralement, pour une fonction `f` avec ou sans valeur de retour,
`s.f(args)` est l'analogue de `f(s, args)`, l'application de `f` à l'objet `s` et aux arguments de `args`.

Le contenu de la variable `s`  n'est pas modifié par l'appel de `empty()`.



La fonction [`size`](https://cplusplus.com/reference/string/string/size/) (ou `length`, qui est équivalente) retourne la longueur d'une chaîne de caractère.

```c++
s.size()
```

ou 

```c++
s.length()
```

retourne une valeur de type `std::string::size_type` (donc `size_t`) correspondant au nombre de caractères constituant `s`.

Lorsqu’une méthode de valeur de retour de type `std::string::size_type` retourne la valeur `std::string::npos`, cela signifie que l'opération qu'elle réalise a échoué.



La forme typique d'une boucle parcourant tous les indices `i` d'une chaine de caractères `s` est:

```C++
for (size_t i = 0; i < s.size(); ++i) { }
```



### Accès direct

Si `s` est une chaîne de caractères, chacun des caractères qui la compose peut être désigné en utilisant la **notation indicée** : `s[4]` désigne le caractère d’indice `4` dans la chaîne `s`, c'est à dire le 5ieme caractère de `s` (le premier indice étant `0`).

De manière générale,  

- `s[i]` désigne le caractère à l’indice `i` si `0 <= i < s.size()`  
  en dehors de ces bornes, le comportement de l'opérateur est indéterminé.
- `s[i]` est de type `char`. 

L'opérateur [`[]`](https://cplusplus.com/reference/string/string/operator[]/) peut être utilisé pour modifier un caractère.

cf.  [`exemples/direct.cpp`](emples/direct.cpp)

```c++
    std::string s("abcd");
    s[2] = 'x';
```

En effet, le type de retour de l'[opérateur `[]`](https://cplusplus.com/reference/string/string/operator[]/) est `char&`, c\est à dire une référence sur le caractère de la chaîne à l'indice donné.

La fonction [`at`](https://cplusplus.com/reference/string/string/at/) offre une autre possibilité d'accès direct aux caractères d'une chaîne.

```c++
s.at(i)
```

Contrairement à l'opérateur `[]`, `at` effectue préalable un contrôle sur la validité de l'indice `i` . Si celui-ci n'est pas dans les bornes `0 <= i < s.size()` , une exception ` out_of_range` est levée. Dans le cas de `[]`, une telle erreur provoque un arrêt brutal du programme, alors qu'une exception peut être rattrapée (ce qui est hors du sujet de ce cours).



### Comparaison

L'opérateur d'égalité de chaînes est `==`.

```c++
s1 == s2
```

est vrai si 

- les chaînes `s1` et `s2` sont de même longueur 
   `s1.size() == s2.size()` 
- et les caractères aux indices `0..s1.size()`dans `s1` et `s2` sont identiques.

On note que le nom de cet opérateur `==` est le même que pour d'autre types, comme `int`, ce qui est un exemple de surcharge (cf. cours sur les fonctions).



L'opérateur `<` défini un ordre lexicographique sur les chaînes de caractères.

```c++
s1 < s2
```

est vrai dans les 2 cas suivants

- `s1` est un préfixe stricte de `s2`:
  - `s1.size() < s2.size()` 
  - et les caractères aux indices dans `0..s1.size()`dans `s1` et `s2` sont identiques.

ou bien

- il existe un indice `i` de `s1` tel que
  - les caractères aux indices dans `0.i-1` dans `s1` et `s2` sont identiques,
  - et le caractère de `s1` en `i` a un code inférieur à celui du caractère de `s2`en `i`, 
    `s1[i] < s2[i]` 



```c++
s.compare(s1)  
```

retourne une valeur entière signée indiquant la relation entre `s` et `s1` :

- `0` : `s` et `s1` sont identiques
- `< 0`: la longueur de `s1` est < à celle de `s` ou le 1er caractère différent dans `s1` a un code < à celui du caractère de `s`  (ordre lexicographique, ou alphabétique)
- `> 0`: la longueur de `s1` est > à  celle de `s` ou le 1er caractère différent dans `s1` a un code > à celui du caractère de `s`

cf. [`exemples/comparaison.cpp`](exemples/comparaison.cpp)



### Concaténation

L'opérateur de concaténation de chaînes de caractères est [`+`](https://cplusplus.com/reference/string/string/operator+/).

```c++
s1 + s2
s1 + c
```

Le résultat de l’opération de concaténation est un objet de type `string` qui contient une copie des caractères de `s1` suivie d’une copie des caractères de `s2` ou du caractère `c`.  

`s1` et `s2` peuvent être deux objets de type `string` ou l’un des deux peut être de type `string` et l’autre une constante chaîne littérale.

La commande [`+=`](https://cplusplus.com/reference/string/string/operator+=/) suivante

```c++
s1 += s2; // s1 = s1 + s2
s1 += c;
```

copie les caractères de `s2`, ou le caractère  `c`, à la suite des caractères de `s1`, qui est modifiée. `s2` peut être une constante chaîne littérale ou un objet de type `string`. Elle n'est pas modifiée.
Voir le lien sur la référence pour les différentes surcharges de `+=`.

La fonction [`append`](https://cplusplus.com/reference/string/string/append/) permet de concaténer une partie seulement d'une autre chaîne.

```c++
s1.append(s_c, n);
```

copie les `n` premiers caractères de la chaîne litérale `s_c` (type `char*`) à la fin de `s1`, qui est modifiée.

> **Exemple**.  cf. [`exemples/concatenation.cpp`](exemples/concatenation.cpp)
>
> ```C++
> std::string s1 = "abcef";
> s1.append("mnopq9999999", 5);
> ```
>
> `s1` vaut `abcefghijklmnopq`.



```c++
s1.append(s2, i, n);
```

copie à la fin de `s1`  `n`  caractères de la chaîne `s2`, de type `string` , en partant du caractère d'indice `i`.
`s1` qui est modifiée. Une référence sur `s1` est retournée.

>  **Exemple**.  cf. [`exemples/concatenation.cpp`](exemples/concatenation.cpp)
>
> ```C++
> std::string s1 = "abcef";
> std::string s2 = "ghijk";
> s1.append("s2, 2, 3);
> ```
>
> `s1` vaut `abcefijk`



L'argument est `n` optionnel. Par défaut, tous les caractère jusqu'à la fin de `s2`  sont concaténés.



### Extraction 

la fonction [`substr`]()

```c++
s.substr(i, n)
```

retourne la sous-chaîne de `s` de longueur `n` commençant à l'indice `i` (inclus).  

- si `i` est trop grand, une exception `out_of_range` est levée.

- si  `n` est trop grand, la sous-chaîne allant de `i`  jusqu'à la fin de `s` est retournée.

> **Exemple**. cf. [`exemples/extraction.cpp`](exemples/extraction.cpp)
>
> ```C++
> std::string s1 = "abcdefgh";
> std::string s2 = s1.substr(2, 3);
> ```
>
> - `s1.substr(2, 3)` est `"cde"`
>
> - `s1.substr(2, 10)` est `"cdefgh"`

Les deux arguments sont optionnels. Par défaut, 

- `i` est égal à `0`.
- `n = std::string::npos` est la sous-chaîne retournée va de `i`  jusqu'à la fin de `s`
  (la chaîne retournée est un suffixe de `s`).



### Modification

[`push_back`](https://cplusplus.com/reference/string/string/push_back/)

```c++
s.push_back(c);
```

ajoute le caractère  `c` à la fin de `s` (la valeur de cette variable est donc modifiée).  
Pas de valeur de retour.



[`replace`](https://cplusplus.com/reference/string/string/replace/) peut prendre plusieurs formes.

```c++
s.replace(i, n, s1)
s.replace(i, n, s_c)
```

où 

- `s` et `s1` sont des chaîne de type `string`

- `i` et `n` sont des entiers de type  `size_t`
- `s_c` est un littéral de type `char []`

l'instruction supprime dans la chaîne `s`, à partir de l'indice `i`, `n` caractères, puis les remplace par `s1`  (de type `string`)  ou par  `s_c` (de type `char []`).  

Les chaînes `s1` ou `s_c` ne sont pas obligatoirement  de longueur `n`, 
Si ce n'est pas le cas, la longueur de `s` est modifiée par l'opération.

Si `n` est trop grand, tous les caractères de `s` à partir de `i` sont remplacés. 
On obtient aussi ce résultat avec un paramètre `n = string::npos `

> Exemple. cf. [`exemples/remplacement.cpp`](exemples/remplacement.cpp)
>
> ```C++
> std::string s("abcdefg");
> s.replace(3, 2, "xyz");
> ```
>
> la valeur retournée est `"abcxyzfg"`

Avec

```c++
s.replace(i, n, s1, i1, n1)
```

c'est la sous chaîne de `s1` démarrant à l'indice `i1` et de longueur `n1`
qui est utilisée pour le remplacement.

La fonction d'insertion [`insert`](https://cplusplus.com/reference/string/string/insert/) peut aussi avoir plusieurs formes

```C++
s.insert(i, s1)
```

insère les caractères de `s1`  dans `s` à l'indice `i-1` (c'est à dire juste avant le caractère d'indice `i`). 

```C++   
s.insert(i, s_c) 
s.insert(i, s_c, n)  
```

idem avec une chaîne littérale (`char*`). La deuxième version insère les `n` premiers caractères de `s_c`.

```c++
s.insert(i, s1, i1, n1)
```

insère la sous chaîne de `s1` de longueur `n1` commençant en `i1`.

insère le caractère `c` dans `s` à l'indice `i-1` , et 


```c++
s.insert(i, n, c)
```

insère `n` fois le caractère `c` dans `s` à l'indice `i-1` .



> Exemple . cf. [`exemples/insertion.cpp`](exemples/insertion.cpp)
>
> ```C++
> std::string s("abef");
> std::string s1("9cd99");
> s.insert(2, s1, 1, 2)
> ```
>
> `s` vaut `"abcdef"`
>
> ```C++
> s.insert(6, "gh99", 2);
> ```
>
> `s` vaut `"abcdefgh"`  (6 est la position de fin de ``"abcdef"``)



> **Exemple**:  cf. [`exemples/modifs.cpp`](exemples/modifs.cpp)
> Modification d’un objet de type string par ajout
>
> ```C++
> std::string s("abra");
> std::cout << s << std::endl;
> 
> s.append("cd");
> std::cout << s << std::endl;
> 
> s = s + "a";
> //s = s + 'a'; // ou s += 's'
> std::cout << s << std::endl;
> 
> std::string s2("bra");
> s += s2;
> std::cout << s << std::endl;
> 
> s.push_back('s');
> std::cout << s << std::endl;
> 
> s.insert(5, "a");
> std::cout << s << std::endl;
> ```
>
> ```
> abra
> abracd
> abracda
> abracdabra
> abracdabras
> abracadabras
> ```



On peut supprimer des caractères avec [`erase`](https://cplusplus.com/reference/string/string/erase/)

```c++
s.erase(i, n)
```

supprime dans `s`  `n` caractères à partir de l'indice `i` (inclus).  
Retourne une référence sur `s`.

Les deux arguments sont optionnels. Par défaut, 

- si `n` n'est pas mentionné (c'est à dire qu'`erase` est appelé avec un seul paramètre),
  `erase` supprime tous les caractères de `s` à partir de l'indice `i`, jusqu’à la fin.   
  la valeur par défaut de `n` est en effet  `string::npos`.
- si `i` n’est pas spécifié, la suppression se fait à partir de l’indice `0` (jusqu'à la fin).

> Exemple. cf. [`exemples/effacer.cpp`](exemples/effacer.cpp)
>
> ```C++
> std::string s = "abcde999fgh";
> s.erase(5,3);
> ```
>
> `s` vaut `"abcdefgh"`



```c++
s.clear();
```

transforme la chaîne `s` en une chaîne vide.  
Pas de retour.

`s.erase()` est équivalent à `s.clear()`  



### Recherche

```c++
s.find(c, i)
s.find(s2, i)
```

[`find`](https://cplusplus.com/reference/string/string/find/) recherche dans `s`  un caractère `c` ou une sous-chaîne `s2` et retourne une valeur entière (type  `size_t`) correspondant à la position de l'élément recherché si il existe, ou `std::string::npos` s'il n'existe pas.  
On peut préciser à partir de quel indice `i` on recherche dans `s`. Si  `i` n’est pas spécifié, la recherche se fait à partir de l’indice `0`.

avec [`rfind`](https://cplusplus.com/reference/string/string/rfind/), la recherche se fait en partant de la fin de la chaîne.

>  **Exemple**. cf. [`exemples/recherche.cpp`](exemples/recherche.cpp)
>
> ```C++
> std::string s = "bonjour et bonne journée!";
> ```
>
> `s.find("jour");` retourne 3
>
> `s.rfind("jour");` retourne 17



```c++
s.find_first_of(s2, pos)
s.find_first_of(c, pos)
```

recherche à partir de `pos` dans `s` la première occurrence de l'un des caractères de `s2` ou de `c`. Si `pos` n'est pas spécifié, la recherche se fait à partir du début de `s`.  
Retourne l'indice de `c` ou du premier caractère de `s2` dans `s` ou `npos` si l'objet recherché n'existe pas dans `s`.

```c++
s.find_first_not_of(s2, [i])
s.find_first_not_of(c, [i])
```

recherche à partir de `i` dans `s` la première occurrence du caractère qui n'appartient pas à `s2` ou qui n'est pas `c` et retourne son indice ou `string::npos` si tous les caractères de `s2` appartiennent à `s`.  
Si `i` n'est pas spécifié, la recherche se fait à partir du début de `s` (index 0).

```c++
s.find_last_of(s2, [i])
s.find_last_of(c, [i])
```

recherche dans `s` jusqu’à `i` inclus la dernière occurrence de l'un des caractères de `s2` ou de `c`, ou à partir de la fin de `s` si `i` n'est pas spécifié.  
Retourne l'indice de `c` ou du premier caractère de `s2` dans `s` ou `string::npos` si l'objet recherché n'existe pas dans `s`.

```c++
s.find_last_not_of(s2, [i])
s.find_last_not_of(c, [i])
```

recherche à partir de `i` dans `s` la première occurrence de caractère qui n'appartient pas à `s2` ou qui n'est pas `c` et retourne son indice ou `string::npos` si tous les caractères de `s2` appartiennent à `s`.  
Si `i` n'est pas spécifié, la recherche se fait à partir de la fin de `s`.



## Parcours d’une chaîne

### Énumération de tous les caractères

Depuis la version `C++11`, le parcours d’une chaîne peut se faire à l’aide d’une boucle for dite *Range-Based for* dont la syntaxe est la suivante :

```c++
for (declaration : expression)
{
    ... 
}
```

- expression : désigne l’objet string que l’on veut parcourir
- declaration : déclare la variable utilisée pour accéder à chaque élément de la séquence. À chaque itération, cette variable est initialisée à la valeur de l’élément sur laquelle on pointe dans la chaîne.

>  **Exemple**.: cf. [`exemples/parcours.cpp`](exemples/parcours.cpp)
>
>  ```C++
>  std::string str("abracadabra") ;
>  for (auto c : str)
>  {
>  	std::cout << c << ' '; 
>  }
>  std::cout << endl;
>  ```
>
>  La variable `c` est associée à la string `str`. 
>  Cette variable est déclarée auto : c’est le compilateur qui estimera quel doit être le type de `c` d’après l’objet auquel elle est associée, donc ici ce sera le type `char`.
>
>  À chaque itération, le caractère sur lequel on pointe dans `str` sera recopié dans `c`. 



### Modification de tous les caractères d’une chaîne

Si l’on veut modifier tous les caractères d’une chaîne, la variable de boucle doit être une référence. Dans ce cas, à chaque tour de boucle, cette variable est liée à un caractère de la chaîne (est un alias désignant chaque élément de la chaîne).



> Exemple. 
>
> ```C++
> std::string str("abracadabra");
> for (char& c : str)
> {
>   c = toupper(c);
> }
> std::cout << str << std::endl;
> ```

