#include <iostream>
#include <string>

int main(void)
{
    std::string s("abef");
    std::string s1("9cd99");
    s.insert(2, s1, 1, 2);  // insere 2 caracteres de s1 à partir de l'indice 1

	std::cout << s << std::endl; // s = "abcdef"

	s.insert(6, "gh99", 2); // insere les 2 premiers caracteres de "gh99"
                           	// s = "abcdef"

    std::cout << s << std::endl; // s = "abcdefgh"
    return 0;
}