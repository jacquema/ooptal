#include <iostream>
#include <string>

int main(void)
{

	std::string s("abra");
	std::cout << s << std::endl;

	s.append("cd");
	std::cout << s << std::endl;

	s = s + "a"; //s = s + 'a'; // ou s += 'a'
	std::cout << s << std::endl;

	std::string s2("bra");
	s += s2;
	std::cout << s << std::endl;

	s.push_back('s');
	std::cout << s << std::endl;

	s.insert(5, "a");
	std::cout << s << std::endl;

    return 0;
}