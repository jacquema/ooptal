#include <iostream>
#include <string>

int main(void)
{
  std::string str("abracadabra") ;
  for (auto c : str)
  {
    std::cout << c << ' '; 
  }
  std::cout << std::endl;

  return 0;
}

