#include <iostream>
#include <string>

int main(void)
{
    std::string s("abcdefg");
    s.replace(3, 2, "xyz");
    std::cout << s << std::endl;
    return 0;
}