#include <iostream>
#include <string>

using std::cout;
using std::endl;

int main(void)
{
    const char *c1 = "bcderefb";
    const char *c2 = "bcdetab";   // c2 > c1
    const char *c3 = "bcderefas"; // c3 < c1
    const char *c4 = "bcde";      // c4 < c1
    std::string s1 = c1;

    if (s1 < c2) 
		cout << "c1 < c2" << endl;
    else 
		cout << "c1 >= c2" << endl;
    if (s1.compare(c3) > 0) 
		cout << "c1 > c3" << endl;
    else 
		cout << "c1 <= c3" << endl;
    if (s1.compare(0, std::string::npos, c1, 4) > 0)
        cout << "c1 > c4" << endl;
    else 
		cout << "c1 <= c4" << endl;
    return 0;
}