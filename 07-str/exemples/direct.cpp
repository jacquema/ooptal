#include <iostream>
#include <string>

int main(void)
{
    std::string s("abcd");
    s[2] = 'x';
    std::cout << s << std::endl;

    return 0;
}