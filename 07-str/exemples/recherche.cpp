#include <iostream>
#include <string>

int main(void)
{
    std::string s = "bonjour et bonne journée!";

    size_t pos = s.find("jour");  // std::string::size_type
    std::cout << "find jour:  " << pos << std::endl;

    pos = s.rfind("jour");
    std::cout << "rfind jour: " << pos << std::endl;

	return 0;
}

