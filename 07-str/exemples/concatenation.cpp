#include <iostream>
#include <string>

using namespace std;

int main(void)
{
    std::string s1 = "abcef";
    std::string s2 = "ghijk";

	s1.append(s2, 2, 3);
	std::cout << s1 << std::endl;

	s1 += s2;
    std::cout << s1 << std::endl;

	s1.append("mnopq9999999", 5);
	std::cout << s1 << std::endl;

	

	return 0;
}