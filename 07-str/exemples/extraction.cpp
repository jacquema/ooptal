#include <iostream>
#include <string>


int main(void)
{
    std::string s1 = "abcdefgh";
    std::string s2 = s1.substr(2, 3);

    std::cout << s2 << std::endl;
	
    std::cout << s1.substr(2, 10) << std::endl;
    return 0;
}