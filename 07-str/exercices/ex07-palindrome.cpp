// vérifier qu'une une chaîne de caractères est un palindrome.

#include <iostream>
#include <string>

bool palindrome2(const std::string& s)
{
	size_t j = s.size();

	// on élimine les cas triviaux
	if (j <= 1) return true;

	j--; // j est la position du derniere caractere de s

	for (size_t i = 0; i <= j; ++i) // on fait avancer i et reculer j
	{
		if (s[i] != s[j])
			return false;
		else
			--j;
	}
	return true;
}

bool palindrome(const std::string &s)
{
	// on élimine les cas triviaux
	if (s.size() <= 1) return true;

	for (size_t i = 0; i < s.size() - i - 1; ++i) 
	{
		if (s[i] != s[s.size() - i - 1])
			return false;
	}
	return true;
}

int main ()
{
	std::string s;

	// std::cout << "Entrer un mot: ";
	// std::cin >> s;

	if (palindrome(s))
	std::cout << s << " est un palindrome" << std::endl; 
	else
	std::cout << s << " n'est pas un palindrome" << std::endl;

	return 0;
}