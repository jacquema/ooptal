// calcul d'image mirroir (3 versions)

#include <iostream>
#include <string>

std::string miroir(const std::string& s)
{
	std::string mir; // chaîne vide

	for (size_t i = 0; i < s.size(); ++i)
	{
		mir.insert(0, 1, s[i]); // insertion du caractère en tête de mir
	}
	return mir;
}

std::string miroirback(const std::string& s)
{
	std::string mir; // chaîne vide

	for (size_t i = s.size(); i > 0; --i)
	{
		mir.push_back(s[i - 1]); // insertion du caractère à la fin de mir
	}
	return mir;
}

int main ()
{
  std::string s;

  std::cout << "Entrer un mot: ";
  std::cin >> s; 
  std::cout << "image miroir: " << miroir(s) << std::endl;
  return 0;
}