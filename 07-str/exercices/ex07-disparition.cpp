// fonction qui efface tous les ‘e’ d'une chaîne de caracteres
// et retourne le nombre de ‘e’ supprimés.

#include <iostream>
#include <string>

int supprime(std::string& s)
 {
	 int c = 0; // compteur
	 // la taille de s (modifiée apres suppression) est recalculée à chaque itération
	 for (size_t i = 0; i < s.size();) // pas de ++i automatique ici!
	 {
		 if (s[i] == 'e')
		 {
			 s.erase(i, 1); // on supprime le caractère s[i] dans s (la chaîne s est modifiée)
							// i est maintenant l'index du caractère suivant
			 ++c;
		 }
		 else
		 {
			 ++i; // on ne modifie pas et on passe au caractère suivant
		 }
	 }
	 return c;
}

int main ()
{
	std::string s;
  	std::cout << "Entrer une ligne de texte: ";
	getline(std::cin, s);
	int n = supprime(s);
	std::cout << "resultat: " << s;
	std::cout << " (" << n << " caracteres supprimés)" << std::endl;
	return 0;
}