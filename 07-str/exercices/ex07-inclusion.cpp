// fonctions booléennes qui a pour paramètres deux chaînes de caractères 
// et qui teste si la première est incluse dans la seconde.

#include <iostream>
#include <string>

// avec find
bool find1(const std::string& s1, const std::string& s2)
{
	size_t pos = s2.find(s1);
	return (pos != std::string::npos);
}

// avec acces direct
bool find2(const std::string& s1, const std::string& s2)
{
	if (s1.empty()) return true;

	size_t i = 0, j = 0, pos = 0;
	bool found = false;

	while (j < s2.size() && !found)
	{
		// même caractere: on avance en parrallele dans les deux chaines
		if (s1[i] == s2[j])
		{
			i++;
			j++;
			if (i == s1.size()) // end of s
				found = true;
		}
		// on avance dans la deuxieme chaine et revient au debut de la premiere.
		else
		{
			i = 0;
			j++;
			pos = j;
		}
	}
	// pos contient la position de s1 dans s2 (si elle a été trouvee)
	return found;
}

int main()
{
  std::string s, t;

  std::cout << "Entrer un mot: ";
  std::cin >> s; 

  std::cout << "Entrer un autre mot: ";
  std::cin >> t; 

  if (find2(s, t))
    std::cout << t << " contient " << s << std::endl;
  else
	  std::cout << t << " ne contient pas " << s << std::endl;

 return 0;
}