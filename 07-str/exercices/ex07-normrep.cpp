// fonction qui supprime les caractères au delà de deux répétitions 
// dans une chaîne passée en paramètre
// la fonction retourne une valeur booléenne indiquant si la chaîne a été modifiée ou non.
// Par exemple, « cooooool » est transformé en « cool »

#include <iostream>
#include <string>

// version avec lookahead
bool normalise1(std::string& s)
{
	// si la chaîne a deux caractères ou moins, il n'y a rien à faire!
	if (s.size() <= 2) return false;

	int cpt = 0; // compteur de caractères effacés
	for (size_t i = 0; i < s.size() - 2; ) // pas de ++i automatique ici!
	{
		// trois caractères consécutifs identiques: on supprime le premier
		if (s[i] == s[i+1] && s[i+1] == s[i+2])
		{
			s.erase(i, 1); // i est maintenant l'index du caractère suivant
			cpt++;
		}
		// sinon on passe au caractère suivant
		else
		{
			++i;
		}
	}
	return (cpt > 0);
}

// version avec 2 variables buffer
bool normalise2(std::string& s)
{
	// si la chaîne a deux caractères ou moins, il n'y a rien à faire!
	if (s.size() <= 2)
		return false;

	int cpt = 0;
	char c0 = s[0];                   // la chaîne a plus de 2 caractères.
	char c1 = s[1];					  
	for (size_t i = 2; i < s.size();) // pas de ++i automatique ici!
	{
		// trois caractères consécutifs identiques: on supprime le troisième
		if (c0 == c1 && c1 == s[i])
		{
			s.erase(i, 1); // i est maintenant l'index du caractère suivant
			cpt++;
		}
		// sinon on passe au caractère suivant
		else
		{
			c0 = c1;
			c1 = s[i];
			++i;
		}
	}
	return (cpt > 0);
}

int main()
{
	std::string s;
	std::cout << "Entrer une ligne de texte: ";
	getline(std::cin, s);

	bool mod = normalise1(s);
	std::cout << "resultat: " << s;	
	std::cout << " (" << (mod ? "modifiée)" : "inchangée)") << std::endl;
	return 0;
}
