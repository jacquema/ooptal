# Exercices - cours 07

## Chaînes de caractères



> **Exercice 1**:  normalisation de texte (la normalisation (nettoyage) est une première étape dans le pré-traitement des entrées en NLP avec modèles d’apprentissage machine).
>
> Écrire une fonction qui convertit toutes les lettres en minuscules dans une chaîne de caractère passée par référence,
>
> - avec l'opérateur d'accès direct d'accès direct  `[]`,
> - avec `replace`.
>
> Tester dans le `main` du programme (avec `getline`).
>
> variante: la fonction retourne le nombre de lettres modifiées.



> **Exercice 2**. 
> Ecrire une fonction qui prend comme paramètre une chaîne de caractères, en efface tous les ‘e’ et retourne le nombre de ‘e’ supprimés. 
> Tester dans la fonction `main()`  avec une phrase saisie par l'utilisateur (utiliser `getline` pour la saisie d'une phrase).



> **Exercice 3**:  normalisation 2.
> Écrire une fonction qui supprime les caractères au delà de deux répétitions dans une chaîne passée en paramètre. Par exemple, elle transformera « cooooool » en « cool ».
> De plus, la fonction retournera une valeur booléenne indiquant si la chaîne a été modifiée ou non.



>  **Exercice 4**: inclusion  
>  Écrire deux fonctions booléennes avec pour paramètres deux chaînes de caractères et teste si la première est incluse dans la seconde, 
>
>  - avec `find`
>  - uniquement par accès direct avec l'opérateur d'accès direct  `[]`



> **Exercice 5**: **DEVOIR MAISON**
> Écrire deux fonctions qui retournent l'image miroir d'une chaîne de caractères passée en paramètre:
>
> - la première avec `insert`
> - la seconde avec `push_back`
>
> Par exemple, l'image miroir de "master" est "retsam".



> **Exercice 6**: palindrome.
> Écrire une fonction booléenne qui teste si une chaîne de caractères passée en paramètre est un palindrome.

 

> **Exercice 7**: 
> Demander la saisie d'un verbe du premier groupe et afficher sa conjugaison à l'indicatif présent.

