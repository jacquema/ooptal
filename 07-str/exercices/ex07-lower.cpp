// fonction qui convertit toutes les lettres en minuscules dans une chaîne de caractère passée par référence
// - avec l'opérateur d'accès direct d'accès direct
// - ou avec replace

#include <iostream>
#include <string>

bool majuscule(char c)
{
	return ('A' <= c && c <= 'Z');
}

void lower(std::string& s)
{
	for (size_t i = 0; i < s.size(); ++i)
	{
		if (majuscule(s[i]))
		{
			s[i] = s[i] + 32; // conversion de s[i] en minuscule
		}
	}
}

void lower2(std::string& s)
{
	for (size_t i = 0; i < s.size(); ++i)
	{
		if ('A' <= s[i] && s[i] <= 'Z') // majuscule(s[i])
		{
			s.replace(i, 1, 1, s[i]+32);
		}
	}
}

int main()
{
	std::string s;

	std::cout << "Entrer un texte: ";
	getline(std::cin, s);

	lower(s);
	std::cout << s << std::endl;
	return 0;
}