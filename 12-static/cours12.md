# Cours de programmation objet

INALCO  
Master TAL première année, second semestre



---

## Cours 12 - Variables et méthodes de classe

mardi 18 février 2025  
Florent Jacquemard  `florent.jacquemard@inria.fr`



**sources**: 

- "*Introduction à la programmation orientée objet en C++*", 
  Jean-Cédric Chappelier, Jamila Sam et Vincent Lepetit, BOOCs EPFL
  chapitre 3, section 10.



---

## Variables globales (non)

On peut vouloir définir une information globale sur une classe, qui sera la même pour toutes les instances.  Un attribut de la classe ne peut pas être utilisé pour cela,  car les valeurs prises sont propres à chaque instance de cette classe.



> **Exemple**. compteur de rectangles.  
> Dans le cours sur les constructeurs et destructeurs, nous avons défini un compteur des instances de `Rectangle` créés.  En définissant ce compteur comme un attribut de la classe `Rectangle`, chaque instance de `Rectangle` aura son propre compteur, alors que nous voulons un compteur **centralisé**.  
> C'est pourquoi nous avons déclaré `compteur` comme une **variable globale**.
> 
> ```cpp
> long compteur(0);
>
> class Rectangle 
> {
> public:
>    // constructeur
>    Rectangle(double h=0, double w=0): 
>    height(h), 
>    width(w) 
>    { 
>       ++compteur;
>    } 
>    
>    // destructeur
>    ~Rectangle()
>    {
>       --compteur;
>    }
>    
>    private: 
>     double height; 
>     double width; 
>    };
>    ```

Utiliser des variables globales peut poser des problèmes. En effet, la variable globale `compteur` est accessible par différentes parties du programme qui pourront la modifier concurremment, de manière incontrôlée.

Dans l'exemple du comptage de `Rectangle`, on préfererait "lier" la variable `compteur` à la classe `Rectangle`, sans la définir comme un attribut de `Rectangle`.



## Attributs et méthodes de classe

La solution au problème de l'exemple précédent est de définir un **attribut de classe** (on parle aussi de **variable de classe**). 

### Attributs de classe: définition

Il s'agit d'un attribut, défini à l'intérieur de la classe, précédé du mot-clé `static`.

```cpp
class Nom
{
   static Type attribut;
};
```

Cet attribut statique prendra la même valeur pour toutes les instances de la classe.

> **Exemple**: compteur de rectangles défini comme attribut de classe.
>
> ```cpp
> class Rectangle 
> {
> private:
>      double height;
>      double width;
>      static int compteur;
> };
> ```
> Les deux instances suivantes de `Rectangle` occupent deux emplacements mémoire distincts, qui contiennent leurs propres valeurs de hauteur et largeur.
> ```cpp
> Rectangle r1;
> Rectangle r2;
> ```
> En revanche, la valeur de l'attribut de classe `compteur` est stockée dans une zone mémoire unique, séparée des instances de `Rectangle`.

Un attribut de classe existe même si l'on a crée aucune instance de la classe.

### Visibilité

Les règles de visibilité pour un attribut `static` sont les mêmes que pour les autres attributs. Il peut donc être déclaré `public` ou `private`.

> **Exemple**: visibilité d'attribut de classe.  
> L'attribut de classe `compteur`, déclaré `private` ci-dessus,  est accessible à  l'intérieur la classe `Rectangle`, mais pas à l'extérieur.

### Accès aux attributs de classe

L'accès à la valeur d'un attribut de classe se fait au travers de l'opérateur de résolution de portée, précédé du nom de la classe.

```cpp
ClassName::attribut
```



> **Exemple**: pour accèder à l'attribut de classe `compteur` de l'exemple précédent:
>
> ```cpp
> Rectangle::compteur
> ```



Contrairement au cas des autres attributs (accès avec la syntaxe point), il n'est donc pas nécéssaire de passer par une instance de classe pour accèder à un attribut de classe.
C'est cohérent avec le fait qu'un attribut de classe existe même si aucune instance de la classe n'a encore été créée.



### Attributs de classe: initialisation

L'initialisation d'un attribut de classe se fait **en dehors**  de la classe, avec la syntaxe suivante:

```cpp
Type NomClasse::attribut(val);
```



> **Exemple**: initialisation de l'attribut de classe `compteur` de l'exemple précédent.
> cf. [`exemples/compteur-public.cpp`](exemples/compteur-public.cpp)
>
> ```cpp
> int Rectangle::compteur(0);
> ```



De même que pour l'accès, il n'est pas nécéssaire de passer par une instance de classe pour initialiser un attribut de classe.



## Exemple d'attribut de classe

> **Exemple**. base d'employés.  
> Considérons la classe suivante pour représenter un employé.
> ```cpp
> class Employee
> {
>       int age;
>       Date service;
>       int retirement_age;
> }
> ```
> - L'attribut `retirement_age` est censé représenter l'age légal de départ en retraite.
>  Il aura donc une valeur dans chaque instance d'`Employee`.  
>   Comme il s'agira de la même valeur pour tous, il y a **duplication**.  
> - Imaginons que suite à un changement de loi, l'age légal minimal de départ en retraite soit diminué.  Il faudra alors changer la valeur de l'attribut `retirement_age` dans chaque instance de la classe `Employee`, ce qui pose des problèmes de **maintenance**.
>  Une solution pour remédier à ces problèmes est de définir `retirement_age` comme variable de classe.
> ```cpp
> class Employee
> {
>   int age;
>   Date service;
>   static int retirement_age;
> }
> ```
> On évitera ainsi les  problèmes de duplication et de maintenance. L'information de l'age légal minimal de départ en retraite est **centralisée** et accessible à toutes les instances.




### Méthodes de classe
On peut aussi définir des méthodes de classe, accessibles sans objet/instance.

La définition se fait de manière similaire aux attributs de classe, à l'aide du mot clé `static`.

```cpp
class NomClasse
{
    static Type methode(params) 
    { ... }
}
```

Comme pour les attributs de classe, l'appel aux méthodes de classe se fait au travers de l'opérateur de résolution de portée.

```cpp
NomClasse::method(val_params)
```

Une méthode de classe peut donc être invoquée même si il n'existe aucune instance de la classe.

> **Exemple**. affichage d'un compteur privé de rectangles.  
> cf. [`exemples/compteur-private.cpp`](exemples/compteur-private.cpp)



Une méthode statique peut aussi être invoquée au travers d'une instance, s'il en existe une.

```cpp
NomClasse x;
x.method(val_params)
```

Cette syntaxe, un peu ambigue, n'est pas recommandée.



### Méthodes de classe: restriction

Une méthode de classe ne peut accèder qu'à des attributs et méthodes de classe.  
Elle ne peut pas accèder à des attributs et méthodes non `static`,  dans la mesure où cette méthode (de classe) peut être invoquée indépendamment d'un objet.

Une méthodes de classe est donc une fonction usuelle, définie à l'intérieur d'une classe.

De telles méthodes peuvent être utiles par exemple pour:

- accèder à des attributs de classes,
- manipuler des attributs de classes, 
- toute fonction en lien avec une classe qui ne porte pas sur un objet particulier de cette classe
  e.g. méthode *factory* pour la création d'objets (*Design Pattern*)

