#include<iostream>

class Rectangle 
{
public:
	static int compteur;

    Rectangle(double h=0, double w=0): 
    height(h), 
    width(w) 
    { 
        ++compteur;
    } 

	Rectangle(const Rectangle& rhs): 
    height(rhs.height), 
    width(rhs.width) 
    {  
        ++compteur;
    } 

    // destructeur
    ~Rectangle()
    {
        --compteur;
    }

private: 
    double height; 
    double width; 
};

// initialisation de l'attribut de classe
int Rectangle::compteur(0);

int main()
{	
 	std::cout << "compteur.0 = " << Rectangle::compteur << std::endl;
    Rectangle r1;
 	std::cout << "compteur.1 = " << Rectangle::compteur << std::endl;

    {
        Rectangle r2;
	 	std::cout << "compteur.2 = " << Rectangle::compteur << std::endl;
        Rectangle r3(r2); // copie
	 	std::cout << "compteur.3 = " << Rectangle::compteur << std::endl;
    }
 	std::cout << "compteur.4 = " << Rectangle::compteur << std::endl;
}