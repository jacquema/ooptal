#include<iostream>

class Rectangle 
{
private: 
    double height; 
    double width; 
	static int compteur;

public:
    // constructeur
    Rectangle(double h=0, double w=0): 
    height(h), 
    width(w) 
    { 
        ++compteur;
    } 

	Rectangle(const Rectangle& rhs): 
    height(rhs.height), 
    width(rhs.width) 
    {  
        ++compteur;
    } 

    // destructeur
    ~Rectangle()
    {
        --compteur;
    }

	static void showCompteur() 
	{
		std::cout << "compteur = " << compteur << std::endl;
	}

};

// initialisation de l'attribut de classe
int Rectangle::compteur(0);

int main()
{	
	// la ligne suivante est interdite car compteur est privé
 	// std::cout << "compteur = " << Rectangle::compteur << std::endl;
	Rectangle::showCompteur();
    Rectangle r1;
	Rectangle::showCompteur();

    {
        Rectangle r2;
		Rectangle::showCompteur();
        Rectangle r3(r2); // copie
		Rectangle::showCompteur();
    }
	Rectangle::showCompteur();
}