#include <iostream> 

class Rien
{ 
public :
	Rien();
	~Rien();
};

Rien::Rien() // constructeur par défaut
{
	std::cout << "création d'une instance de Rien" << std::endl;
}

Rien::~Rien() // destructeur
{
	std::cout << "destruction d'une instance de Rien" << std::endl;
}


int main() 
{
	Rien x;
	std::cout << "bonjour" << std::endl;
}
