#include "expoint.hpp"

int main()
{ 
	Point a(10,20);
	Point b(30,40);
	Point nuage[6];
	nuage[0] = Point(4);
	nuage[1] = Point(a);
	nuage[2] = Point(0);
	nuage[3] = Point(b);
	nuage[4] = Point();
	nuage[5] = Point();

	for (int i = 0 ; i < 6 ; i++) 
		nuage[i].show();
}
