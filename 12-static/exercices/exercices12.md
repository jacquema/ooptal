# Exercices - cours 12

## constructeurs, destructeurs, variables de classes



> **Exercice 1.**  
> Écrire une classe `Rien` 
> de manière à ce que le petit programme:
>
> ```cpp
> main()
> { 
>      Rien x;
>      std::cout << "bonjour" << std::endl; 
> }
> ```
> affiche les lignes suivantes :
> ```
> création d'une instance de Rien
> bonjour
> destruction d'une instance de Rien
> ```



> **Exercice 2.**  
> Qu'affichera à l’exécution le programme `demo.cpp`?
> On essaiera de répondre sans executer le programme!



> **Exercice 3.**  
> Qu'affichera l'exécution du programme `expoint_a.cpp`
> (il fait référence à `expoint.hpp` et `expoint.cpp`).




> **Exercice 4.**  
> Même question pour `expoint_b.cpp`.  
> **aide**: La déclaration:
>
> ```cpp
> Point nuage[6];
> ```
> Crée en mémoire un tableau de 6 objets de type `Point` en appelant, 
> le cas échéant, le constructeur par défaut pour chacun d’entre eux. 
> Le tableau  `nuage` n’est pas lui-même un objet.



> **Exercice 5.**
> Même question pour `expoint_c.cpp`
> **aide**:
> on peut compléter la déclaration d’un tableau d’objets 
> par un initialiseur contenant une liste, entre `{` et `}`, de valeurs, 
> chacune étant transmise à un constructeur approprié. 
> On peut ne pas préciser de valeurs pour les derniers éléments du tableau,
> auquel cas le constructeur par défaut est appelé pour leur création.



> **Exercice 6.**  **À FINIR POUR PROCHAIN COURS**
> Modifier la classe `Point` définie dans `expoint.hpp` et `expoint.cpp`
> de manière à ce que chaque point possède un attribut privé entier qui est un identifiant unique.  
> La classe comprendra aussi un destructeur.  
> Chaque constructeur, destructeur, ainsi que `show()`, affichera le numéro du point concerné (créé, détruit ou affiché).  
> Tester avec le programme suivant qui créée un tableau statique de 5 points en commentant le résultat.
>
> ```c++ 
> int main()
> {
> 	Point a(10, 20);
> 	Point b(30, 40);
> 	Point nuage[5];
> 	nuage[0] = Point(4);
> 	nuage[1] = Point(a);
> 	nuage[2] = Point(0);
> 	nuage[3] = Point(b);
> 	nuage[0] = Point(1, 2);
> 
> 	for (int i = 0; i < 5; i++)
> 		nuage[i].show();
> }
> ```
>
