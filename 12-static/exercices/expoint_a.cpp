#include "expoint.hpp"

int main()
{ 
	Point a(10,20);
	Point b(30,40);
	Point c;
	Point d(4);
	Point e(a);
	Point f(0);
	Point g(b);

	a.show();
	b.show();
	c.show();
	d.show();
	e.show();
	f.show();
	g.show();
}

