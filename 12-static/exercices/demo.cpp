#include <iostream> 

class Demo
{ 
public :
	Demo(int abs=1, int ord=0);
	Demo(const Demo&); 
	~Demo ();

private:
	int x;
	int y;
};

Demo::Demo(int abs, int ord):
x(abs), 
y(ord) 
{
	std::cout << "construction: " << x << " " << y << std::endl;
}

Demo::Demo(const Demo& d):
x(d.x),
y(d.y) 
{ 
	std::cout << "copie:        " << x << " " << y << std::endl;
}

Demo::~Demo ()
{ 
	std::cout<<"destruction:  " << x << " " << y << std::endl;
}

int main() 
{
	std::cout << "début demo" << std::endl; 
	Demo a;
	Demo b(2);
	Demo c(a);
	Demo d(4,4); 
	c = Demo(5,5);
	std::cout << "fin demo" << std::endl; 
}

