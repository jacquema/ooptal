#include "expoint.hpp"

Point::Point():
x(0),
y(0) 
{ 
	std::cout << "** constructeur 0 argument" << std::endl;
}

Point::Point(int abs):
x(abs), 
y(0)
{ 
	std::cout << "** constructeur 1 argument" << std::endl;
}

Point::Point(int abs, int ord):
x(abs),
y(ord)
{ 
	std::cout << "** constructeur 2 arguments" << std::endl;
}

Point::Point(const Point& p):
x(p.x),
y(p.y)
{ 
	std::cout << "** constructeur de copie" << std::endl;
}

void Point::show() const
{ 
	std::cout << "point de coordonnees : " << x << " " << y  << std::endl; 
}

