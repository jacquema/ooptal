#include <iostream> 
#include <vector> 

class Point
{
public : 
	Point();
	Point(int abs);
	Point(int abs, int ord);
	Point(const Point& p);
	void show() const;
private:	 
	int x;
	int y;
};