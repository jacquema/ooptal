# Cours de programmation objet

INALCO - Master TAL première année

---

# Cours 02: Variables, types, instructions simples et expressions

jeudi 26 septembre 2024  
Florent Jacquemard  
`florent.jacquemard@inria.fr`

https://gitlab.inria.fr/jacquema/ooptal

sources: 

- cours de Marie-Anne Moreaux, INALCO
- [Wikilivre programmation C++](https://fr.wikibooks.org/wiki/Programmation_C%2B%2B)
- cours de Christian Casteyde 
  - http://casteyde.christian.free.fr/cpp/cours/online/x437.html (variables)
  - http://casteyde.christian.free.fr/cpp/cours/online/x251.html (types)
  - http://casteyde.christian.free.fr/cpp/cours/online/x485.html (instructions et opérations)
  - http://casteyde.christian.free.fr/cpp/cours/online/x813.html (IO de base)
- tutos
  - https://cplusplus.com/doc/tutorial/operators/
  - [wikibook](https://fr.wikibooks.org/wiki/Programmation_C++/Les_types_de_base_et_les_déclarations)



## Structure des programmes sources en C++

- Un programme source est la traduction d'un algorithme en un langage de programmation.  
- Le code source est traduit par un compilateur afin d'être interprété et exécuté par une machine. 
- Un programme source écrit en C++ doit respecter les règles de *syntaxe* de ce langage,
  définies formellement par une **grammaire**, *cf.* par exemple:
  - grammaire de C++:
    https://cermics.enpc.fr/polys/info1/More/node1.html
  - les règles de syntaxe font partie du standard C++, défini par un comitté:
    The C++ Standards Committee - ISOCPP
    https://www.open-std.org/jtc1/sc22/wg21/

La vérification qu'un programme source est bien dans le langage définis par la grammaire C++ est une étape de la compilation que l'on appelle **analyse syntaxique** (*parsing*).

- Un programme `C++` est composé d'*instructions* (unité syntaxique de base). 
  Le but de chaque instruction est d'effectuer un traitement sur des données.  

> **Exemple**: (instruction)
>
> L'instruction suivante 
>
> ```c++
> std::cout << "Bonjour";
> ```
>
> signifie: redirige, 
> avec l'opérateur binaire `<<`  
> la donnée `"Bonjour"` (la chaîne de caractère)
> vers le flux sortie standard `std::cout`



- un programme est généralement structuré en blocs d'instructions (unités syntaxiques composées) 
- les  *fonctions* sont des blocs nommés: chaque fonction a un nom,  un type de retour et des arguments. 
  un cours entier sera dédier aux fonctions.

> **Exemple**: (fonction)
>
> ```c++
> int main()
> {
>     std::cout << "Bonjour" << std::endl;
>     return 0;
> } 
> ```
>
> la fonction `main` de l'exemple du cours précédent est sans argument, et a pour type de retour `int`
>
> elle est faite d'un bloc de deux instructions successives:  
>
> - `std::cout << "Bonjour" << std::endl;`
> - `return 0;`



- Le `main` est une fonction particulière qui est **obligatoire** pour qu’un fichier source puisse devenir exécutable. C'est le point d'entrée de l'executable.



## Les données

- Le programme source utilise un espace de travail, en mémoire, qui contient un certains nombre d'objets.
- Ces objets représentent les **données** manipulées dans les différentes opérations du programme.  
- Chaque objet a un **type** précis. 
- Les opérations sont décrites par les **instructions** composant le programme.  



## Les données: variables et constantes

Les objets présents dans l'espace de travail peuvent

- avoir la même valeur pendant tout leur cycle de vie;
  ce sont alors des **constantes**;
- voir leur valeur modifiée par un ou plusieurs calculs ordonnés par les instructions.
  Ces objets sont alors appelés **variables**.



## Les littéraux

Les *littéraux*  sont les valeurs constantes, écrites explicitement (littéralement) dans le code source d'un programme. Par exemple:
`12`,  `7.5`,  `true`,  `'a'`.

On parle aussi de **constantes non-nommées** (pas désignées par un identificateur).

Il peut s'agir de valeurs booléennes, de nombres entiers ou flottant, ou encore de caractères entre deux `'` ou de chaînes de caractères entre `"`.

Le **type** d'un littéral est défini par sa forme syntaxique:

| ex.             | forme                                             | type                 |
| --------------- | ------------------------------------------------- | -------------------- |
| `98`            | une suite de chiffres (notation en base 10)       | `unsigned int`       |
| `0x62`          | `0x` suivi de chiffres (notation hexadécimale)    | idem (*)             |
| `0b1100010`     | `0b` ou `0B` suivi de chiffres (notation binaire) | idem                 |
| `+10`, `-32`    | une suite de chiffres précédée de `+` ou `-`      | `int`                |
| `3.14`          | une suite de chiffres contenant un point          | `double`             |
| 7.6e10          | notation scientifique avec e ou E                 | `double`             |
| `'ß'`           | un et un seul caractère entouré de `'`            | `char`               |
| `'\t'`          | une séquence d'échappement entouré de `'`         | `char`               |
| `true`, `false` | les mots réservés true et false                   | `bool`               |
| `"bla"`         | une suite de caractère entourée de `"`            | chaîne de caractères |

Le type "chaîne de caractères" diffère en `C` (`char*`) et `C++` (`std::string`).  
Nous aurons plus tard un cours sur la classe `std::string`.

(*) pour les formats des litéraux entiers, voir 
https://en.cppreference.com/w/cpp/language/integer_literal



## Littéraux et échappements

Dans le cas des caractères ou des chaînes, certains caractères spéciaux sont écrit à l'aide d'un `\` (on parle de *séquence d'échappement*).

C'est le cas par exemple de `'` qui s'écrit `\'` et `"` qui s'écrit `\"` , pour permettre au compilateur de terminer correctement l'interpretation d'un littéral de type caractère ou chaînes de caractères.

La *séquence d'échappement* `\n` correspond au caractère de saut de ligne
et `\t` à une tabulation. 

Le caractère `\` (backslash) s'écrit `\\`.

> Exemple: échappeme de retour à la ligne 
>
> ```C++
> std::cout << "bonjour\n";
> ```
>
> donne le même résultat que 
>
> ```c++
> std::cout << "bonjour" << std::endl;
> ```



La liste complète des échappements est disponible ici:  
https://en.cppreference.com/w/cpp/language/escape



## Variables

L'usage de variables permet d'écrire des programmes effectuant des tâches avancées
(plus avancées qu'afficher "Bonjour"!).

Une *variable* est une portion de mémoire où est stockée une **valeur**.

Elle est identifiée par un nom (**identificateur**) et a toujours un *type* défini.

<img src="/Users/jacquema/Library/Mobile Documents/com~apple~CloudDocs/Enseignement/INALCO/02-varexp/img/meuble-casier-industriel-métal-01.jpeg" alt="meuble-casier-industriel-métal-01" style="zoom:75%;" />



> **Exemple**: 3 variables entières
>
> ```c++
> int a = 2;
> int b = 3;
> a = a + 1;
> int result = a * b;
> ```
>
> `a`, `b` et `result` sont 3 variables de type `int`.
>
> - Les deux premières lignes sont les *déclarations* (obligatoires) des variables 
>   `a` et `b`.
>   Des valeurs initiales '2' et '3' leurs sont affectées.  
>   Ces affectations initiales sont facultatives mais recommandées: sans cela les variables prendraient des valeurs arbitraires, ce qui peut être source de problèmes par la suite.
> - la ligne suivante est une (ré-)*affectation* de `a`
>
> ```c++
> int a = 2;
> int b = 3;
> a = a + b;
> int result = a * b;
> ```



## Identificateurs

Un identificateur est une sorte de « nom propre ».  
Il permet de faire référence à une donnée (identificateur de variable) ou à une opération (identificateur de fonction).

Formellement, un identificateur est une suite de caractères alphanumériques :  
`[A-Z]` `[a-z]` `[0-9]` `_` (blanc souligné, ou *underscore*)

- Le premier caractère doit être alphabétique ou le blanc souligné 
- l'identificateur ne peut être identique à un mot réservé
- Il est sensible à la casse
- Seuls les 31 premiers caractères sont significatifs



>  **Exemple** :
>
> - `car`   `Voyelle`   `_date` et  `x1` sont des identificateurs bien formés 
>
> - `2c`  et `&k` ne sont pas des identificateurs valides



> Le choix des noms de variables est affaire de style.
>
> Exemple de guide de style de programmation pour les programmeurs C++ de Google:
> https://google.github.io/styleguide/cppguide.html



## Mots réservés

Certains noms ne peuvent être utilisé par le programmeur pour définir un identificateur, car ils correspondent à des opérations ou descriptions du language :

`alignas`, `alignof`, `and`, `and_eq`, `asm`, `auto`, `bitand`, `bitor`, `bool`, `break`, `case`, `catch`, `char`, `char16_t`, `char32_t`, `class`, `compl`, `const`, `constexpr`, `const_cast`, `continue`, `decltype`, `default`, `delete`, `do`, `double`, `dynamic_cast`, `else`, `enum`, `explicit`, `export`, `extern`, `false`, `float`, `for`, `friend`, `goto`, `if`, `inline`, `int`, `long`, `mutable`, `namespace`, `new`, `noexcept`, `not`, `not_eq`, `nullptr`, `operator`, `or`, `or_eq`, `private`, `protected`, `public`, `register`, `reinterpret_cast`, `return`, `short`, `signed`, `sizeof`, `static`, `static_assert`, `static_cast`, `struct`, `switch`, `template`, `this`, `thread_local`, `throw`, `true`, `try`, `typedef`, `typeid`, `typename`, `union`, `unsigned`, `using`, `virtual`, `void`, `volatile`, `wchar_t`, `while`, `xor`, `xor_eq`.



cf. https://en.cppreference.com/w/cpp/keyword



## Types

 `C++` est un langage *typé* : chaque valeur manipulée par un programme appartient à un type de donnée identifiable.

Le programmeur est donc contraint de spécifier les types de tous les objets manipulés.
En particulier, avant d'être utilisée, chaque variable doit être déclarée, afin de définir son type.

En pratique, cela permet d'éviter des erreurs d'exécution (en les détectant à la compilation). En effet, le compilateur pourra vérifier la validité des opérations du programme source.



Les types des objets manipulés par le programme peuvent être:

- fondamentaux (dits type primitifs) qui sont **prédéfinis** dans le langage: 
  une valeur entière, une valeur décimale, un caractère... 
  On parle aussi de valeurs *scalaires*.
- **définis**. Il s'agit de types définis par un utilisateur ou une librairie.
  C'est le cas collections d'objets d'un certain type: 
  - un ensemble de valeurs entières, de valeurs décimales, de caractères, 
  - une chaîne de caractères (type `std::string`), 
  - un flux de sortie (comme `std::ostream` qui est le type de l'objet `std::cout`)
  - une liste d’entiers, 
  - un tableau de listes de chaînes de caractères...

Lorsque le programme exécutable est en cours d’exécution : 

- les objets de type fondamentaux, non-décomposables, occupent un seul emplacement mémoire, défini par son adresse mémoire et la longueur (nombre d'octets) correspondant au type.
- les objets de type définis, composés d'objets plus petits,  occupent plusieurs emplacements mémoire, chaque élément simple ayant une adresse.



## Types prédéfinis

Il existe plusieurs types de données prédéfinis (dits types *primitifs* ou *fondamentaux*)

référence: https://en.cppreference.com/w/cpp/language/types

- le type vide : `void`. Ce type est utilisé pour spécifier le fait qu'il n'y a pas de type. 
  Cela est utile pour écrire des fonctions sans arguments `int f(void)` ou qui ne retournent pas de valeur `void f(...)`

- les booléens : `bool`, qui peuvent prendre deux valeurs `true` et `false` 
  (en `C++`, en C `1` et `0`)

- les caractères : `char` 
  ce type désigne en fait un entier sur 1 octet (8 bits)  (entier entre 0 et 255 ou -127 et 127)
  
  Les litéraux "caratères" tels que `'A'`, `'a'`, `'$'`... peuvent s'interpreter vers un `char`, 
  
  suivant l'encodage défini par la table ASCII 
  https://en.cppreference.com/w/cpp/language/ascii



> **Exemple**: `char` et caractères (cf. [`exemples/char.cpp`](exemples/char.cpp))
>
> ```c++
> char c
> c = 'A'; 
> int a;
> a = c;
> ```
>
> `c`  et `a` valent 65.



![ascii_table](/Users/jacquema/Library/Mobile Documents/com~apple~CloudDocs/Enseignement/INALCO/02-varexp/img/ascii_table.jpg)



- `char16_t` et `char32_t`  permettent l'encodage des caractères Unicode 
  via respectivement UTF-16 (2 octets) ou UTF-32 (4 octets)
- de même que `wchar_t`  (caractères longs), 
  mais ce dernier type n'est pas standard 
  (certaines implémentations sont sur 2 octets uniquement, ce qui est limité pour Unicode).



## Types prédéfinis numériques

- les entiers : `int`
  
  - entiers signés:
    - `signed char` : 1 octet = 8 bits (`-127` à `127`)
    - `short int` :  typiquement 2 octets
    - `int` : 4 ou 8 octets suivant architecture (cf. `limits.cpp`)
      cf. [std::numeric_limits](https://cplusplus.com/reference/limits/numeric_limits)
    - `long int` : 4 ou 8 octets suivant architecture
    - `long long int` : au moins 8 octets
  - entiers non-signés (toujours > 0):
    - `unsigned char` (`0` à `255`)
    - `unsigned short int` 
    - `unsigned int`
    - `unsigned long int`
    - `unsigned long long int`
  
- les nombres à virgule flottante (décimaux)

  -  `float`    (nombres à virgule flottante en simple précision)
    en général [4 octets](https://en.wikipedia.org/wiki/Single-precision_floating-point_format) 
  -  `double`  (nombres à virgule flottante en double précision)

    en général [8 octets](https://en.wikipedia.org/wiki/Double-precision_floating-point_format) 
  - `long double` (double précision étendue)




En fait, la longueur des `int`  (`short int`, `long int`) n'est pas fixée par le langage.
Le langage impose juste que la taille d'un `long int` doit être supérieure ou égale à celle d'un `int` et que la taille d'un `int` doit être supérieure ou égale à celle d'un `short int`.

Il en est de même pour `float`, `double` et `long double`.



Le programme [`exemples/limits.cpp`](exemples/limits.cpp) affichera les valeurs limites des types entiers ci-dessus sur votre architecture.

Par exemple, sur le système sur laquelle ce cours est écrit (macOS Apple M3 pro), cela donne:

```
    short int = -32768 .. 32767                              // 2 octets
          int = -2147483648 .. 2147483647                    // 4 octets
     long int = -9223372036854775808 .. 9223372036854775807  // 8 octets
long long int = -9223372036854775808 .. 9223372036854775807

    unsigned short int = 0 .. 65535
          unsigned int = 0 .. 4294967295
     unsigned long int = 0 .. 18446744073709551615
unsigned long long int = 0 .. 18446744073709551615
```



**Référence**:  https://en.cppreference.com/w/cpp/language/types



## Types des litéraux

On peut forcer l'intepretation des litéraux numériques vers un type spécifique en ajoutant une ou deux lettres en suffixe.

Pour les entiers:

| constante se terminant par | interpretation   |
| -------------------------- | ---------------- |
| `U`                        | `unsigned int`   |
| `L`                        | `long in`        |
| `LL`                       | ` long long int` |

Pour les nombres à virgule flottante:

| constante se terminant par               | interpretation |
| ---------------------------------------- | -------------- |
| `f` ou `F`                               | `float`        |
| `l` ou `L`  (constante contenant un `.`) | `long double`  |

Une valeur constante de caractère long (entre simple quote) ou de chaîne de caractères longs (entre double quote) doit être précédées du caractère `L`.

> Exemple:
>
> ```c++
> wchar_t c = L'é'; // caractère 'é' unicode(16 ou 32 bits);
> wchar_t[] str = L"الكتاب المختصر في حساب الجبر والمقابلة"; // chaîne de caractère unicode
> ```



## Types prédéfinis numériques: `float` ou `int`

Il est recommandé d'utiliser des entiers chaque fois que cela est possible, 
à cause des problèmes d'arrondis causés par la représentation binaire des nombres en mémoire.

```c++
    float f = 0.1f;
    std::cout << "f = " << f << std::endl;
    float sum = f + f + f + f + f + f + f + f + f + f;
    std::cout << "sum = " << sum << std::endl;
    float product = f * 10;
    std::cout << "mul = " << product << std::endl;
    std::cout << "mul2 = " << f * 10 << std::endl;
```

```
sum=1.000000119209290, mul=1.000000000000000, mul2=1.000000014901161
```

cf. [`exemples/float.cpp`](exemples/float.cpp)

Le résultat dépend du compilateur de des options, et du CPU.

<img src="/Users/jacquema/Library/Mobile Documents/com~apple~CloudDocs/Enseignement/INALCO/02-varexp/img/ariane-5-bug.png" alt="ariane-5-bug" style="zoom:50%;" />

https://www.welcometothejungle.com/en/articles/btc-ariane-5-bug-software



Voir aussi:

https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/



## Instructions

L'unité syntaxique minimale d'un programme source `C++` est l'instruction. 

Une instruction peut être simple ou composée :

- une *instruction simple* est une séquence de tokens terminée par `;` 
  `pgcd = diviseur`;
- une *instruction composée* est une combinaison de plusieurs instructions simples.



## Instructions simples

quelques catégories d'instructions simples

- instruction de **déclaration** :
  - annonce le nom (identificateur) et le type des variables utilisées dans les instructions suivantes:
    `unsigned short n;`
  
  - annonce le nom et le type de retour des fonctions:
    `void OrdreCroissant(unsigned short &, unsigned short &);`
  
- instruction d'**affectation** : modifie le contenu d’une variable
  `n = n1;`

- instructions d'**entrée/sortie** avec les opérateurs `<<` et `>>`
  instructions permettant de récupérer des valeurs depuis un périphérique d’entrée ou d’en envoyer sur un périphérique de sortie
  `std::cout << "Erreur" << std::endl;`
  `std::cin >> n1;`



## Instructions composées

quelques catégories d'instructions composées

- **bloc** d'instructions : séquence d'instructions entourée par `{` et `}`  
  par exemple le corps de définition de la fonction `main`.

- instruction **conditionnelle** : branchement
  une telle instruction permet de spécifier quelle est la prochaine instruction à exécuter en fonction du résultat d'un test  
  par exemple sur le contenu de variables `x == 0`.
  Le cours suivant sera dédié aux instructions conditionnelles.
  
- **boucle** : itération
  instruction indiquant qu’il faut répéter l’exécution d’une ou plusieurs instructions.

- **sous-programmes** : procédures et fonctions 
  suite d’instructions portant un nom, destiné à réaliser une opération spécifique, utilisable par un autre programme.

Chacune de ces 3 dernières catégories fera l'objet d'un cours spécifique.



## Instructions de déclaration

Toute variable ou constante doit être déclarée par une instruction qui annonce 

- son nom (identificateur) 
- et son type.

Une **variable** est déclarée par une instruction de la forme

```c++
type identificateur = valeur;
```

L'affection `= valeur` est facultative dans la déclaration d'une variable.   
En son absence, la variable, si elle d'un type simple, reçoit une **valeur indéterminée**.  
La valeur de la variable peut être modifiée après initialisation (par une affectation).

```c++
    unsigned short cpt;
    float pourcentage;
    char initial;
    bool test;
```

voir les programmes d'exemple

- [`exemples/var0.cpp`](exemples/var0.cpp)
- [`exemples/var1.cpp`](exemples/var1.cpp)



Une **constante** est déclarée au moyen du mot clé `const`, dans une instruction de la forme

```c++
const type identificateur = valeur;
```

L'affection `= valeur` est obligatoire dans le cas d'une constante.  
La valeur de la constante ne peut plus être modifiée après son initialisation.



On peut déclarer plusieurs variables (ou constantes) de même type dans la même instruction de déclaration, en séparant leurs noms par des virgules.

```c++
    unsigned short cptLettres, cptChiffres, cptMots; 
    char initial, final;
```



## Déclaration des variables

(du point de vue du compilateur)

De manière effective, une instruction de déclaration s'adresse au compilateur pour l'informer

- du nom de chaque variable utilisée dans les instructions du programme,
- du type de la valeur qui est susceptible d'être stockée à l'adresse représentée par l'identificateur,

et lui demander

- en fonction de ce type de réserver une zone de mémoire nécessaire au stockage de la valeur.

Une variable créée de cette manière est dite allouée statiquement.  
Ni son nom, ni son type, ni la dimension de la zone mémoire réservée ne peuvent plus être modifiés au cours de l’exécution.



## Instruction d'affectation

Toute variable doit être déclarée pour exister, mais la déclaration seule n'assure pas de la valeur qu'elle contiendra (voir [`exemples/var1.cpp`](exemples/var1.cpp)).

L'instruction d'affection va associer (on dit aussi **assigner**) une valeur à une variable.  

Cela pourra être utile avant d'utiliser la variable dans un calcul, 
ou afin de changer l'état de la variable au cours de l'exécution du programme. 



La syntaxe `C++` de l'affectation est la suivante:

```
    identificateur = expression;
```

- Les deux termes se trouvant de part et d'autre de l'opérateur `=` d'affectation doivent représenter des objets de même type.
  
  Dans le cas contraire, il peut parfois y avoir une conversion implicite 
  (cf. `exemples/arith4c`) mais cette situation est à éviter.
- Le terme de gauche doit être un identificateur de variable.
- Le terme de droite doit représenter une valeur.  
  Donc `expression` peut être:
  
  - une constante littérale  
    `c = 'a';`
  - un identificateur de variable (auquel une valeur est déjà associée)  
    `c1 = c2;`
  - une expression (cf. § Les opérations sur les types élémentaires)  
    `result = nb * 3 + 3248 / j;`
  - l'appel d'une fonction  
    `small = min(i, j);`



## Affectation à la déclaration

La combinaison d'une déclaration et d'une affectation de variable dans:

```c++
type identificateur = valeur;
```

est appelée **déclaration avec initialisation**.

Elle est équivalente à

```C++
type identificateur;
identificateur = valeur;
```

La syntaxe suivante: 

```C++
type identificateur(valeur);
```

fait aussi une copie de la valeur donnée dans la variable déclarée.

Nous y reviendrons dans les cours sur les objets (constructeurs).



## Expressions

Une expression est une construction syntaxique faisant partie d'une instruction et qui produit une valeur.
Une expression peut être simple ou composée.

- expression simple

  - une constante littérale
  - un identificateur de variable ou constante.

- expression composée

  - expression simple
  - application d'un **opérateur** ou d'une **fonction** à des expressions composées. 

Puisqu'une expression produit une valeur, elle est typée.  
Le type d'une expression est celui de la valeur qu'elle délivre.

Nous allons dans les paragraphes suivants un certain nombre d'opérateurs usuels.



## Opérateurs arithmétiques

Appliqués à des valeurs numériques et retournant des valeurs numériques (entiers ou flottants).

| symbole | opération      | arité |
| ------- | -------------- | ----- |
| `+`     | addition       | 2     |
| `-`     | soustraction   | 2     |
| `*`     | multiplication | 2     |
| `/`     | division       | 2     |
| `%`     | modulo (reste) | 2     |
| `++`    | incrémentation | 1     |
| `--`    | décrémentation | 1     |



> **Exemple 1**  cf. [`exemples/arith1.cpp`](exemples/arith1.cpp)
>
> ```c++
> int x = 4;
> int y = 1;
> x = x + y;
> ```
>
> x = 5  
> y = 1





>  **Exemple 2**  cf. [`exemples/arith2.cpp`](exemples/arith2.cpp)
>
>  ```c++
>  int x = 5;
>  int y = 3;
>  y = x - y; 
>  ```
>
>  x = 5  
>  y = 2



>  **Exemple 3**  cf. [`exemples/arith3.cpp`](exemples/arith3.cpp)
>
>  ```c++
>  int x = 5;
>  int y = 3;
>  x = (x * y) + x; 
>  ```
>
>  x = 20    
>  y = 3



> **Exemple 4**
>
> ```c++
> int x = 5;
> int y = 3;
> int z = x / y; 
> ```
>
> x = 5    
> y = 3  
> z = 1



En `C++` la division est entière si les deux opérandes sont entiers.  
On obtient donc le même résultat avec (cf. [`exemples/arith4.cpp`](exemples/arith4.cpp)):

```c++
int x = 5;
int y = 3;
float z = x / y; 
```

> z = 1



Si l'un des opérandes est de type flottant, alors le résultat sera flottant (cf. [`exemples/arith4b.cpp`](exemples/arith4b.cpp)):

```c++
float x = 5;
int y = 3;
float z = x / y; 
```

> z = 1.66667



Lorsqu'une valeur de type `float` (ici le résultat de la division `x/y`) est affectée à une variable de type `int`, 
elle est implicitement convertie en un `int `
sa valeur est "tronquée", en supprimant la partie après la virgule: 

```c++
float x = 5;
int y = 3;
int z = x / y; 
```

> z = 1



attention, ce n'est pas un arrondi à l'entier immédiatement inférieur (cf. [`exemples/arith4c.cpp`]((exemples/arith4c.cpp))):

```c++
float x = -5;
int y = 3;
int z = x / y; 
```

> z = -1



L'opérateur `%` en revanche ne peut être appliqué qu'à des entiers.



>  **Exemple 5**  cf. [`exemples/arith5.cpp`]((exemples/arith5.cpp))
>
>  ```c++
>  int x = 5;
>  int y = 3;
>  int z = x % y;
>  ```
>
>  z = 2  
>
>  division entière:on a 5 = (5/3) * 3 + (5%3) avec 5/3 = 1 (quotient) et 5%3 = 2 (reste)

L'exemple suivant donne une erreur à la compilation
(essayer de compiler [`exemples/arith5b.cpp`](exemples/arith5b.cpp)):

```c++
float x = 5;
float y = 3;
float z = x % y;
```



>  **Exemple 6**  cf. [`exemples/arith6.cpp`](exemples/arith6.cpp):
>
>  ```c++
>  int x = 5;
>  int y = 3;
>  x = x / (x % y);
>  y = y - (y / x);
>  ```
>
>  x = 2  
>y = 2



### Opérateurs arithmétiques d'incrémentation et décrémentation

Les opérateurs `++` et `--` peuvent être préfixés ou infixés:

- avec `++<operande>`, l'opérande est d’abord incrémenté puis retourné.
  `y = ++x` est équivalent à `x = x+1; y = x;`
- avec `<operande>++`, l'opérande est d’abord retourné puis incrémenté.
  `y = x++` est équivalent à `y = x; x = x+1;`



>  **Exemple 7**  cf. [`exemples/arith7.cpp`](exemples/arith7.cpp):
>
>  ```c++
>  int x = 1;
>  int y = ++x;
>  int z = x++;
>  ```
>
>  x = 3  y = 2   z = 2



## Opérateurs relationnels

Leur évaluation retourne une valeur booléenne (`true` ou `false`). 
On les appelle aussi  **prédicats**.

Ces opérateurs permettent de comparer deux valeurs,

- des valeurs simples de types numériques, entiers ou réels, de type caractère

- des valeurs composées si l'opérateur est défini pour ce type.
  C'est le cas par exemple des types définis par la STL `C++`, comme le type `std::string`.

  | symbole | opération             | arité   |
  | ------- | --------------------- | ------- |
  | `==`    | égalité               | binaire |
  | `!=`    | différence            | binaire |
  | `>`     | strictement supérieur | binaire |
  | `>=`    | supérieur ou égal     | binaire |
  | `<`     | strictement inférieur | binaire |
  | `<=`    | inférieur ou égal     | binaire |

Précédence:
`<`, `>`, `>=`, `>=` ont une précédence supérieure à `==`, `!=`
(leur évaluation est prioritaire). 

Associativité: gauche - droite.

Attention: l'opérateur d'égalité est `==`, 
à ne pas confondre avec `=` qui est l'affectation.



### Opérateurs logiques

Il s'agit d'opérateurs portant sur les valeurs booléennes  (`true` ou `false`). 
Ils permettet combiner plusieurs expressions booléennes.

| symbole | opération             | arité   |
| ------- | --------------------- | ------- |
| `!`     | négation              | unaire  |
| `&&`    | conjonction           | binaire |
| `||`    | disjonction inclusive | binaire |

Les tables de valeurs des opérateurs sont, pour la conjonction:
|   `&&`  | `true`  | `false` |
| ------- | --------|-------- |
| `true`  | `true`  | `false` |
| `false` | `false` | `false` |

et pour la disjonction:
| `||`    | `true` | `false` |
| ------- | ------ | ------- |
| `true`  | `true` | `true`  |
| `false` | `true` | `false` |

La précédence (priorité) est : `!`, `&&`, `||`  
L'associativité est de gauche à droite.



>  **Exemple 1**
>
>  ```c++
>  bool x = true;
>  bool y = !x;
>  ```
>
>  x = `true`  
>  y = `false`





> **Exemple 2**
>
> ```c++
> bool x = true;
> bool y = true;
> bool z = false;
> bool a = x && y; 
> bool b = x && z; 
> bool c = x && !z; 
> ```
>
> a = `true`  
> b = `false`  
> c = `true`



### Exemples de propositions sur les caractères ASCII

Une *proposition* est une expression de tyle `bool`.

Soit une variable `c` de type `char`.  
On peut appliquer les opérateurs relationnels au type `char`,
l'ordre sur les caractère est défini par leur encodage en entiers, suivant la table ASCII ci-dessus.



- La valeur de la variable `c` est une lettre majuscule ASCII (encadrement)

  ```c++
  ‘A’ <= c && c <= ‘Z’
  ```

pas de parentheses nécessaire car `<=` a la priorité sur `&&`

- La valeur de la variable `c` est une lettre minuscule ASCII  

  ```c++
  ‘a’ <= c && c <= ‘z’
  ```

- La valeur de la variable `c` est une lettre ASCII

  ```c++
  (‘A’ <= c && c <= ‘Z’) || (‘a’ <= c && c <= ‘z’)
  ```

- La valeur de la variable `c` n’est pas une lettre majuscule ASCII 

  ```c++
  !(‘A’ <= c  && c <= ‘Z’)
  ```

  équivalent à

  ```c++
  c < ‘A’ || ‘Z’ < c
  ```

- La valeur de la variable `c` n'est pas une lettre minuscule ASCII

  ```c++
  !(‘a’ <= c && c <= ‘z’)
  ```

  équivalent à

  ```c++
  c < ‘a’ || ‘z’ < c
  ```

- La valeur de la variable `c` n’est pas une lettre ASCII

  ```c++
  !((‘A’ <= c && c <= ‘Z’) || (‘a’ <= c && c <= ‘z’))
  ```

  équivalent à

  ```c++
  !(‘A’ <= c  && c <= ‘Z’) && !(‘a’ <= c  && c <= ‘z’)
  ```

  équivalent à

  ```c++
  (c < ‘A’ || ‘Z’ < c) && (c < ‘a’ || ‘z’ < c)
  ```
  
  



## Opérateur binaires

Ils agissent sur les nombre en représentation binaire, opérant au niveau des 0 et 1.

| symbole | opération                         | arité   |
| ------- | --------------------------------- | ------- |
| `&`     | et bit à bit                      | binaire |
| `|`     | ou bit à bit                      | binaire |
| `^`     | ou exclusif bit à bit             | binaire |
| `~`     | non binaire (inverse les 0 et 1)  | unaired |
| `<<` n  | décalage vers la gauche de n bits | binaire |
| `>>` n  | décalage vers la droite de n bits | binaire |



> **Exemple**: et binaire
>
> ```c++
>   int a = 53;
>   int b = 167;
>   int c = a & b;
> ```
>
> `c` vaut 37 car
>
> ```
>  53 = 0011 0101
> 167 = 1010 0111
>  37 = 0010 0101
> ```



> **Exemple**: ou binaire
>
> ```c++
>   int a = 53;
>   int b = 167;
>   int c = a | b;
> ```
>
> `c` vaut 183 car
>
> ```
>  53 = 0011 0101
> 167 = 1010 0111
> 183 = 1011 0111
> ```



Tables de valeurs du ou exclusif  (c'est comme `!=`): 

| `^`  | `0`  | `1`  |
| ---- | ---- | ---- |
| `0`  | `0`  | `1`  |
| `1`  | `1`  | `0`  |



> **Exemple**: ou exclusif
>
> ```c++
>   int a = 53;
>   int b = 167;
>   int c = a ^ b;
> ```
>
> `c` vaut 146 car
>
> ```
>  53 = 0011 0101
> 167 = 1010 0111
> 146 = 1001 0010
> ```



> **Exemple**: non binaire
>
> ```c++
>   int a = 53;
>   int b = ~a;
> ```
>
> `b` vaut -54
>
> ou 4294967242 si il était typé `unsigned int`
>
> ```
>  53 = 0000 0000 0011 0101    (entiers sur 4 octets)
> -54 = 1111 1111 1100 1010
> ```



> **Exemple**: décalages
>
> ```c++
> int a = 53;
> int b, c;
> b = a << 3;
> c = a >> 1;
> ```
>
> `b` vaut 424 et `c` vaut 26 car
>
> ```
>  53 = 0000 0000 0011 0101
> 424 = 0000 0001 1010 1000
>  26 = 0000 0000 0001 1010
> ```



Nous utiliserons ces opérateurs binaires dans un TP sur les encodages UTF,
où il faudra faire des manipulations de bas niveau des entiers.



## Opérateurs d'affectation

Il s'agit de l'opérateur `=` et de raccourcis.

| symbole | opération                | arité |
| ------- | ------------------------ | ----- |
| `=`     | affectation              | 2     |
| `+=`    | `x += e` est `x = x + e` | 2     |
| `-=`    | `x -= e` est `x = x - e` | 2     |
| `*=`    | `x *= e` est `x = x * e` | 2     |
| `/=`    | `x /= e` est `x = x / e` | 2     |
| `%=`    | `x %= e` est `x = x % e` | 2     |

Une affectation est une expression, dont la valeur de retour est la valeur affectée.

Cela permet d'écrire des **affectations en série**, telles que

```c++
l = m = n = 0;
i = j = k*3;
c1 = c2 = 'L' + 32;
```

L'opérateur `=` est l'opérateur ayant la précédence la plus basse parmi tous les opérateurs et son associativité est droite/gauche, ce qui assure que le terme de droite est évalué avant que la valeur ne soit assignée à l'objet représenté par le terme de gauche.

Concretement, cela veut dire que les affections de l'exemple précédent sont équivalentes à:

```c++
l = (m = (n = 0));
i = (j = (k*3));
c1 = (c2 = ('L' + 32));
```



## Affectation et égalité

**ATTENTION**:  Il ne faut pas confondre l'affectation `x = 'a'` avec un test d'égalité (« `x` est égal à `'a'` »).   

En `C++`, le prédicat d'égalité est représentée par l'opérateur `==`, donc le test  « `x` est égal à `'a'` » s'écrit
 `x == 'a'`.

Pendant d’exécution d'un programme, 

- l'affectation `x = 'a'` produit un changement de l'état de la zone mémoire référencée par la variable `x`. De plus, elle retourne la  valeur affectée (ici le caractère  `a`),
- tandis que `x == 'a'` retourne une valeur booléenne, en fonction de l'état de la zone mémoire référencée par la variable `x` (sans modifier cette variable).

Certains compilateurs produisent un avertissement (*Warning*)  quand `=` est utilisé à la place de `==`.



## Entrées / Sorties standard

Récapitulatif et approfondissement sur les opérateurs d'entrée/sortie que nous avons utilisé dans les exemples jusqu'ici.

### Lecture et écriture des données

Parfois, un programme doit à un moment donné pouvoir afficher des informations dans un format lisible par l'utilisateur. 
Il peut être également nécessaire de lui fournir des données pendant l'exécution : cela correspond à la notion de paramétrisation d’un programme.

Ces échanges se font par l'intermédiaire des périphériques et sont commandés dans le programme par des instructions d'**écriture** (transmission d'informations à l'extérieur du programme, éventuellement à utilisateur) et des instructions de **lecture** (transmission d'informations au programme en cours d'exécution).

Concretement, les opérations qui permettent de gérer les entrées/sorties ne font pas directement partie du langage `C++`, mais sont dans la librairie standard. 
La conséquence de cela est qu'il faut toujours inclure en en-tête un fichier où sont déclarés les objets et les opérations d'entrée/sortie que l'on veut utiliser.

```C++
#include <iostream>
```



## Flux d'Entrée-Sortie

Un flux (flot de données, ou *stream* en anglais) est une suite d'octets en entrée ou en sortie, c'est à dire 

- lue depuis un périphérique d'entrée ou 
- écrite sur un périphérique de sortie.

Concernant les entrée-sorties, la bibliothèque standard déclare 2 types de flux: 

- le type `istream` qui est le type des flux d'entrée
- le type `ostream` qui est le type des flux de sortie

Le terme « flux » suggère le fait que les caractères sont lus ou écrits de manière séquentielle.



## Les flux standard

Pour utiliser les flux d’entée et sortie standard, il faut inclure le fichier d’en-tête « `iostream` »

```c++
   #include <iostream>
```

Lorsque l'exécution d'un programme est lancée, 4 flux sont créés automatiquement et associées aux variables qui permettent de les désigner.

| périphérique support | type      | variable associée | effet                                                        |
| -------------------- | --------- | ----------------- | ------------------------------------------------------------ |
| clavier              | `istream` | `cin`             | transmission de données à partir d’un périphérique           |
| écran                | `ostream` | `cout`            | transmission de résultats vers un périphérique               |
| écran                | `ostream` | `cerr`            | écriture de messages d'erreur                                |
| écran                | `ostream` | `clog`            | écriture de messages concernant le déroulement de l'exécution |

Par ailleurs, on peut définir des flux dirigés vers des fichiers avec `<fstream>`,

ou des flux associés à des chaines de caractères, avec `<sstream>`.



Référence: https://cplusplus.com/reference/iostream/





## Écriture de données en sortie

```c++
std::cout << expression [<< expression << ...] ;
```

L'expression utilise l'opérateur de sortie `<<`. 

Cet opérateur est binaire : 

- l'opérande de gauche doit être une valeur de type `ostream`, 
- celui de droite est la valeur à envoyer sur le périphérique de sortie. 

Le résultat de l'évaluation d'une telle expression est son opérande de gauche, c'est-à-dire le flot où l'on a écrit. 
Cela permet d'enchaîner l'impression de plusieurs valeurs :

```c++
std::cout << a << " + " << b << " = " << r << std::endl;
```

`std::endl` est un « manipulateur ».   
Il termine la ligne (fin de ligne) et vide le buffer de sortie.



## Lecture de données en entrée

```c++
std::cin >> id_var [>> id_varn >> ...];
```

L'expression utilise l'opérateur d'entrée `>>`.  
Cet opérateur est binaire : 

- l'opérande de gauche doit être une valeur de type `istream`, 
- celui de droite l'identificateur de la variable, c'est-à-dire l'adresse de la mémoire où la valeur récupérée va être écrite. 

exemple: pour faire saisir un entier:

```cpp
int a;
std::cin >> a;
```

Le résultat de l'évaluation d'une telle expression est son opérande de gauche, c'est-à-dire le flot d'où l'on a lu. 
Cela permet d'enchaîner l'impression de plusieurs valeurs :

```c++
int a, b;
std::cin >> a >> b ;
```

La ligne d'entrée est alors segmentée en mots (séparés par espaces), affectés à chacune des variables.
cf. [`exemples/cinmultiple.cpp`](exemples/cinmultiple.cpp).

