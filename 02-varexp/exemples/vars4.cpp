/* 
 * vars4.cpp
 */

#include <iostream>

int main(void)
{
    int a = 2;
    int b = 3;
    a = a + b;
    int result = a * b;

    std::cout << "a=" << a << std::endl;
    std::cout << "b=" << b << std::endl;
    std::cout << "result=" << result << std::endl;
}
