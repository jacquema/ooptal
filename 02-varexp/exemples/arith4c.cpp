
#include <iostream>

int main()
{
    float x = -5;
    float y = 3;
    int z = x / y; 

    std::cout << "x = " << x << std::endl;
    std::cout << "y = " << y << std::endl;
    std::cout << "z = " << z << std::endl;

    return 0;
}
