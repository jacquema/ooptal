
#include <iostream>

int main()
{
    int x = 5;
    int y = 3;
    x = x / (x % y);
    y = y - (y / x);

    std::cout << "x = " << x << std::endl;
    std::cout << "y = " << y << std::endl;

    return 0;
}
