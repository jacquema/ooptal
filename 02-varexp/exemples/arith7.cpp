

#include <iostream>

int main()
{
    int x = 1;

	int y = ++x;
	// equivalent à
	// x = x + 1;
	// int y = x;


    std::cout << " x1 = " << x;
    std::cout << " y1 = " << y << std::endl;

	int z = x++;
	//  equivalent à
	// int z = x;
	// x = x + 1;

    std::cout << " x = " << x;
    std::cout << " y = " << y;
    std::cout << " z = " << z << std::endl;

    return 0;
}
