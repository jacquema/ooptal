/* 
 * vars1.cpp
 */

#include <iostream>

int main(void)
{
    int a = 2;
    int b; // b sans affectation initiale

    std::cout << "a=" << a << std::endl;
    std::cout << "b=" << b << std::endl;
	b = a;
	std::cout << "b=" << b << std::endl;
}
