/* 
 * limits.cpp
 * valeurs limites
 */

#include <iostream>
#include <limits>

int main(void)
{

	std::cout << "    short int = ";
	std::cout << std::numeric_limits<short int>::min() << " .. ";
    std::cout << std::numeric_limits<short int>::max() << std::endl;

    std::cout << "          int = ";
    std::cout << std::numeric_limits<int>::min() << " .. ";
    std::cout << std::numeric_limits<int>::max() << std::endl;

    std::cout << "     long int = ";
    std::cout << std::numeric_limits<long int>::min() << " .. ";
    std::cout << std::numeric_limits<long int>::max() << std::endl;

    std::cout << "long long int = ";
    std::cout << std::numeric_limits<long long int>::min() << " .. ";
    std::cout << std::numeric_limits<long long int>::max() << std::endl;

    std::cout << std::endl;

    std::cout << "    unsigned short int = ";
    std::cout << std::numeric_limits<unsigned short int>::min() << " .. ";
    std::cout << std::numeric_limits<unsigned short int>::max() << std::endl;

    std::cout << "          unsigned int = ";
    std::cout << std::numeric_limits<unsigned int>::min() << " .. ";
    std::cout << std::numeric_limits<unsigned int>::max() << std::endl;

    std::cout << "     unsigned long int = ";
    std::cout << std::numeric_limits<unsigned long int>::min() << " .. ";
    std::cout << std::numeric_limits<unsigned long int>::max() << std::endl;

    std::cout << "unsigned long long int = ";
    std::cout << std::numeric_limits<unsigned long long int>::min() << " .. ";
    std::cout << std::numeric_limits<unsigned long long int>::max() << std::endl;

    return 0;
}
