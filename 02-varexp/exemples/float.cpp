/* 
 * float.cpp
 * expériences avec les flottants
 * https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 */

#include <iostream>

int main(void)
{
    float f = 0.1f;
    std::cout << "f = " << f << std::endl;
    float sum = f + f + f + f + f + f + f + f + f + f;
    std::cout << "sum = " << sum << std::endl;
    float product = f * 10;
    std::cout << "mul = " << product << std::endl;
    std::cout << "mul2 = " << f * 10 << std::endl;
}
