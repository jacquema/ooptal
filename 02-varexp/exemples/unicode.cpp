
#include <iostream>

int main()
{
	wchar_t c = L'é'; // caractère 'é' unicode(16 ou 32 bits);
	wchar_t str[] = L"الكتاب المختصر في حساب الجبر والمقابلة"; // chaîne de caractère unicode

	std::cout << "c = " << c << std::endl;
    std::cout << "str = " << str << std::endl;
	std::cout << "str0 = " << str[0] << std::endl;
}