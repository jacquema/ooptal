

#include <iostream>

int main()
{
    float x = 5;
    int y = 3;
    float z = x / y;  

    std::cout << "x = " << x << std::endl;
    std::cout << "y = " << y << std::endl;
    std::cout << "z = " << z << std::endl;

    return 0;
}
