/*
 * cours 02 
 * exercice 3
 */

#include <iostream>

int main()
{
    int x;
    double sum = 0;

    std::cout << "Entrer le premier entier: ";
    std::cin >> x;
    sum += x; // sum = sum + x;

    std::cout << "Entrer le deuxième entier: ";
    std::cin >> x;
    sum += x; // sum = sum + x;

    std::cout << "Entrer le troisième entier: ";
    std::cin >> x;
    sum += x;

    std::cout << "moyenne = " << sum / 3 << std::endl;

    return 0;
}
