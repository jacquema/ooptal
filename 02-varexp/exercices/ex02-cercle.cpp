/*
 * cours 02 
 * exercice 2
 */

#include <iostream>

int main()
{
    float r;
    float perimetre, surface;

    const float PI = 3.14159;

    std::cout << "Entrer le rayon: ";
    std::cin >> r;

    perimetre = 2 * PI * r;
    surface = PI * r * r;

    std::cout << "perimetre = " << perimetre << std::endl;
    std::cout << "surface = " << surface << std::endl;

    return 0;
}
