# Exercices - cours 02

## Variables, types, expressions, IO de base



> **Exercice 1** 
> Écrire un programme qui demande à l'utilisateur la largeur et la longueur d'un rectangle et qui en affiche le périmètre et la surface.



> **Exercice 2**
> Écrire un programme qui demande à l'utilisateur le rayon d'un cercle et qui en affiche le périmètre et la surface.  
> On pourra définir `pi` comme une constante ou utiliser `std::numbers::pi` définie dans la bibliothèque numbers (`#include <numbers>` en début de programme). Dans ce dernier cas il faudra compiler avec l'option  `-std=c++20`  cf. https://en.cppreference.com/w/cpp/numeric



> **Exercice 3**
>
> - Écrire un programme qui demande à l'utilisateur de saisir 3 entiers et qui affiche leur moyenne.  
> - Essayer de faire cela avec 2 variables.



> **Exercice 4**
> Écrire un programme avec deux variables `a` et `b` qui demande à l’utilisateur de saisir des valeurs pour `a` et `b` et ensuite permute leurs valeurs, en affichant `a` et `b` avant et après permutation.



> **Exercice 5**
> Écrire qui demande à l'utilisateur de taper le prix HT d'un article et le nombre d'articles achetés et qui affiche le prix TTC des articles achetés, en appliquant un taux de TVA de 20%.

