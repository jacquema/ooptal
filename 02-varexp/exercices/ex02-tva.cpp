
#include <iostream>

int main()
{
	const double TVA = 20;
    double prixHT, nb, prixTTC;

    std::cout << "Tapez le prix HT d'un article : "; std::cin >> prixHT;
    std::cout << "Tapez le nombre d'articles achetés : "; std::cin >> nb;

    prixTTC = (1 + TVA/100) * prixHT * nb;

    std::cout << "Le prix total TTC est : " << prixTTC << std::endl;

    return 0;
}