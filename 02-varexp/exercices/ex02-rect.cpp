/*
 * cours 02 
 * exercice 1
 */

#include <iostream>

int main()
{
    double longueur, largeur;
    double perimetre, surface;

    std::cout << "Entrer la largeur: ";
    std::cin >> largeur;
    std::cout << "Entrer la longueur: ";
    std::cin >> longueur;

    surface = largeur * longueur;
    perimetre = 2 * (largeur + longueur);

    std::cout << "perimetre = " << perimetre << std::endl;
    std::cout << "surface = " << surface << std::endl;

    return 0;
}
