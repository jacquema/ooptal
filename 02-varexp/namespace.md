# Espaces de nom



réfs.

- https://learn.microsoft.com/fr-fr/cpp/cpp/namespaces-cpp
- https://stackoverflow.com/questions/1452721/why-is-using-namespace-std-considered-bad-practice 



Les espaces de noms servent à distinguer les noms d'identificateurs (variables et fonctions) provenant de différente sources, afin d'éviter les conflits.

Un espace de nom est déclaré par le mot clé  `namespace`

```c++
namespace nom
{
   // code
}
```

Toutes les variables et fonctions déclarées dans l'espace de nom (entre les `{}`) sont accessible dans cet espace directement par leur identificateur. En dehors de cet espace, il faut les préfixer par `nom::`, qui s'appelle une  **qualification**.



> **Exemple**:
>
> ```c++
> #include <iostream>
> 
> namespace exterieur 
> {
>     int var1 = 1;
> }
> 
> int main()
> {
>     int var1 = 2;
> 
>     std::cout << var1 << std::endl;
>     std::cout << exterieur::var1 << std::endl;
> 
>     return 0;
> }
> ```
>
> Dans la fonction `main`,  `var1` et `exterieur::var1` sont deux variables distinctes, avec des valeurs différentes.



On peut définir des espace de noms imbriqués, qui impliquent des qualifications multiples

```c++
#include <iostream>

namespace un {
    int var1 = 1;
}

namespace deux {
    int var1 = 2;

    namespace trois {
        int var1 = 23;
    }
}

int main()
{	
		int var1 = 123;

  	std::cout << var1 << endl;
    std::cout << un::var1 << endl;
    std::cout << deux::var1 << endl;
    std::cout << deux::trois::var1 << endl;

    return 0;
}
```



## Usage

Supposons qu'un programme inclut une bibliothèque

```c++
#include "mylib.hpp"
```

et que cette bibliothèque définit une fonction `open`.

Si le programme déclare une variable  `open`, il y aura conflit de nom et erreur de compilation.

Pour éviter cela, la bibliothèque `mylib.hpp` pourra définir ses fonctions (et variables) dans un espace de nom, par exemple `ml`. Ainsi, dans le programme, 

- la fonction `open` de `mylib`  sera accédée par la qualification `ml::open`. 
- une variable `open` pourra être déclarée dans le programme,  sans conflit, et sera accédée simplement par  `open`. 



L’espace de nom `std` contient les éléments de la **bibliothèque standard** du langage, tels que les entrées/sorties (`iostream`), les chaînes de caractères (`string`), les tableaux (`array`) , ou d'autres conteneurs (`vector`... ) *etc*. 



## Directive `using`

````C++
using namespace nom
````

 permet d'utiliser tous les noms de l'espace `nom` sans quantificateurs explicite.

Il est aussi possible d'utiliser `using` pour un identificateur particulier, 
par exemple:

```c++
using std::cout;
```

permet d'écrire simplement `cout`  au lieu de `std::cout`.



## Le cas `using namespace std`

On trouve fréquemment au début de codes `C++` la directive

```c++
using namespace std;
```

qui permet d'utiliser les objets de la librairie standard sans la qualification `std::` : ou pourra écrire  `cout` ou `cin` au lieu de `std::cout`, `std::cin`. C'est pratique, mais cela peut aussi s'avérer dangereux.

Dans le namespace `std` sont en effet définis de nombreux identificateurs avec des noms relativement communs. Par exemple une fonction `pair`. 

Si l'on définit une variable `pair`, où si l'on appelle une bibliothèque qui le fait, après une directive `using namespace std`, il y aura conflict. Dans le cas d'un appel à  `pair`, on ne peut savoir si c'est le `pair` de `std` ou de l'autre bibliothèque qui est invoqué. Les erreurs de compilation à sujet pouvant être assez obscurs, il pourra être difficile de déterminer d'où vient l'erreur. 

Donc cette directive est déconseillée par [certains](https://stackoverflow.com/questions/1452721/whats-the-problem-with-using-namespace-std). En tout état de cause, si vous l'utilisez, il faut savoir ce qu'elle implique et savoir anticiper les erreurs qui peuvent survenir.

