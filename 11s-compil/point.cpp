#include "point.hpp"

Point::Point(double x, double y): 
x(x),
y(y)
{ }

void Point::move(double dx, double dy)
{
    x += dx;
    y += dy;
}

double Point::distance(const Point& p) const
{
    double dx = p.x - x;
    double dy = p.y - y;
    return sqrt(dx * dx + dy * dy);
}

void Point::print() const
{
    std::cout << "(" << x << ", " << y << ")";
}
