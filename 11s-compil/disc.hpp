#ifndef disc_hpp
#define disc_hpp

#include <iostream>
#include <cmath>
#include "point.hpp"

class Disc
{
public:
    Disc(double x = 0, double y = 0, double r = 0);
    Disc(const Point& p, double r);

    double surface() const;
    void move(double dx, double dy);
    void print() const;

private:
    Point center; 
    double radius;
};

#endif /* disc_hpp */