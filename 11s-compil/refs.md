


## g++
https://hackingcpp.com/cpp/lang/separate_compilation.html
https://earthly.dev/blog/g++-makefile/

## Makefiles

- https://stackoverflow.com/questions/2481269/how-to-make-a-simple-c-makefile

- https://www.tutorialspoint.com/makefile/makefile_quick_guide.htm

- https://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/
  https://www.cs.swarthmore.edu/~newhall/unixhelp/howto_makefiles.html

- https://makefiletutorial.com

- https://www.bogotobogo.com/cplusplus/gnumake.php