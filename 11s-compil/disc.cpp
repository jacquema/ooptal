#include "disc.hpp"

Disc::Disc(double x, double y, double r): 
center(x, y),
radius(r)
{
    if (r < 0)
    {
        std::cerr << "le rayon doit être positif" << std::endl;
        radius = 0;
    }
}

Disc::Disc(const Point& p, double r): 
center(p), // constructeur de copie
radius(r)
{
    if (r < 0)
    {
        std::cerr << "le rayon doit être positif" << std::endl;
        radius = 0;
    }
}

void Disc::move(double dx, double dy)
{
    center.move(dx, dy);
}

double Disc::surface() const
{
    return M_PI * radius * radius;
}

void Disc::print() const
{
    center.print();
    std::cout << " rayon=" << radius;
}

