#ifndef point_hpp
#define point_hpp

#include <iostream>
#include <cmath> // PI

class Point
{ 
public:

    Point(double x=0, double y=0);

    // double getX() const { return x; }
    // double getY() const { return y; }
    void move(double dx, double dy);
    double distance(const Point& p) const;
    void print() const;

private:
    double x;
    double y;
};

#endif /* point_hpp */