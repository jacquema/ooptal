#include <iostream>
#include "point.hpp"
#include "disc.hpp"

void test_inside(const Point& p, const Disc& d)
{
    p.print();
    if (! d.inside(p))
        std::cout << " not";    
    std::cout << " inside disc ";
    d.print();
    std::cout << std::endl;
}

int main()
{
    Point p0;
    Point p1(1, 1);
    Point p2(-0.5, -0.5);
    Disc d1(0, 0, 1);
    Disc d2(0.5, -1, 2);

    std::cout << "disque 1" << std::endl;
    d1.print(); std::cout << " surface = " << d1.surface() << std::endl;
    test_inside(p0, d1);
    test_inside(p1, d1);
    test_inside(p2, d1);
    std::cout << "move" << std::endl;
    d1.move(0.5, 0.5);
    test_inside(p0, d1);
    test_inside(p1, d1);
    test_inside(p2, d1);

    std::cout << "disque 2" << std::endl;
    d2.print(); std::cout << " surface = " << d2.surface() << std::endl;
    test_inside(p0, d2);
    test_inside(p1, d2);
    test_inside(p2, d2);
    std::cout << "move" << std::endl;
    d2.move(2, 2.5);
    test_inside(p0, d2);
    test_inside(p1, d2);
    test_inside(p2, d2);
}
