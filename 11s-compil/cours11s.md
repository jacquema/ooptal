# Cours de programmation objet

INALCO  
Master TAL première année

---

## Cours 11 bis Compilation séparée

mardi 11 février 2025
Florent Jacquemard  `florent.jacquemard@inria.fr`

sources:

- cours de Marie-Anne Moreaux, INALCO (10, 11)
- https://hackingcpp.com/cpp/lang/separate_compilation.html



## Exemple `point` et `disc`

Le fichier `discp.cpp` de la séance d'exercices 11 est devenu volumineux.

Nous allons 

- le séparer en différents fichiers sources:
  - un pour définir `point`, 
  - un pour définir `disc`,
  - un pour le `main` et des fonctions de test.
- compiler les différents fichiers sources séparément.
- en les compilant seulement si nécessaire (i.e. seulement ceux qui ont été modifiés).



## Organisation

- les fichiers `.hpp` (fichier d'entête, ou *header*) contiennent les déclarations: 
  définition d'une classe (par fichier `.hpp` ) qui ne contient que les entêtes des méthodes.
- les fichiers `.cpp` contiennent le code,  
  c'est à dire le corps des méthodes pour chaque classe,
  avec un fichier `.cpp` par classe.
- un autre fichier `.cpp` **unique** contient la fonction `main`.

Les fichiers `.hpp` et `.cpp` associés portent le même nom (celui de la classe définie).  
Le `.hpp` est inclus (avec `#include`)  au début du `.cpp` correspondant.

Pour l'inclusion d'un de vos fichiers `.hpp`  avec la directive `#include`, 
ce fichier est entouré de `"`. Le préprocesseur va chercher le fichier:

- a priori  dans le dossier courant, où réside le fichier source,
- puis (éventuellement) dans les dossiers spécifiés par l'option de compilation `-I` ,
- et dans les dossiers connus dans l'environnement système.

Pour l'inclusion d'un fichier d'entête du système avec `#include`, le fichier est entouré par `<` `>`, 
et préprocesseur va chercher le fichier:

- dans les dossiers spécifiés par l'option de compilation `-I` ,
- et dans les dossiers connus dans l'environnement système.

cf. https://gcc.gnu.org/onlinedocs/cpp/Include-Syntax.html#Include-Syntax

## Compilation séparée

- Un fichier `.cpp` sans `main` ne peut pas devenir exécutable à lui seul.  
  Il peut être compilé, en utilisant lotion `-c`,  
  en un fichier intermédiaire dit *objet*, de suffixe  `.o`.  
  Un tel fichier contient du code machine et des liens vers d'autres fichiers (symboles), 
  et éventuellement des informations utiles au débuggage (option `-g`)
- Le fichier `.cpp` contenant le `main` peut aussi être compilé en un objet.
- Ensuite, tous les fichiers objets sont liés, ensemble et avec des librairies, pour produire un exécutable.

![width:50px height:50px](img/cpp_separate_compilation_03.png)

Dans notre exemple, cela donne la suite de commandes de compilation dans le script shell  [`compile.sh`](compile.sh)

```shell
c++ -Wall -std=c++11 -c point.cpp
c++ -Wall -std=c++11 -c disc.cpp
c++ -Wall -std=c++11 -c tester.cpp
c++ -Wall point.o disc.o tester.o -o test_disc
```



## Précaution sur les includes

La double inclusion (en cas de dépendance mutuelle) peut faire boucler le compilateur.

Pour éviter cela, on ajoute des garde-fou dans les fichiers `.hpp`:

```cpp
#ifndef FILENAME_HPP
#define FILENAME_HPP
...
...
#endif
```



## Makefile

Un fichier Makefile indique comment compiler les fichiers d'un projet,
avec des règles qui spécifient

- un fichier *cible* (target) qui sera produit (exécutable, objet, etc),
- le ou les fichier nécessaires à la production du fichier cible, 
  que l'on appelle *pré-requis* 
- la commande pour produire le fichier cible avec les pré-requis.

Le fichier `Makefile` est lu par la commande `make` qui effectue la compilation.

Le but est de ne compiler que ce qui est nécessaire, 
c'est à dire, que les fichiers d'un projet qui on été modifiés depuis la dernière compilation,
grâce à une inspection des dépendances définies dans les règles.



voir pour notre exemple:

- [`Makefile`](Makefile) qui contient des règles spécifique à l'exemple,
- et  [`Makefile1`](Makefile1)  écrit avec des règles génériques et des variables.

La commande pour compiler avec `Makefile` (dans le dossier où sont les sources) est

```shell
make
```

qui affichera les commandes de compilation invoquées.

Les fichiers objets et executables sont produit dans le dossier courant.

il peuvent être effacés avec:

```
make clean
```

(dernière règle de `Makefile`)

Pour compiler avec `Makefile1`: 

```
make -f Makefile1 
```



Quelques pointeurs pour aller plus loin:

- https://earthly.dev/blog/g++-makefile/
- https://makefiletutorial.com
- https://stackoverflow.com/questions/2481269/how-to-make-a-simple-c-makefile



## Cmake

La commande `cmake` génère un `Makefile`, suivant une spécification donnée dans un fichier  `CMakeLists.txt`.

On trouve un exemple minimaliste dans  [`CMakeLists.txt`](CMakeLists.txt)

```cmake
cmake_minimum_required (VERSION 3.6)
project (DiscPoint)
add_executable(mytest point.cpp disc.cpp tester.cpp)
```

La commande `add_executable` produit l'executable `mytest` à partir des sources spécifiées.



Les 4 commandes suivantes utilisent `cmake` pour notre exemple.
Ce sont les mêmes commandes que pour pratiquement n'importe quel projet.

```shell
mkdir build
cd build
cmake ..
make
```

`cmake` est une commande très puissante qui peut produire des fichiers `Makefile` Unix, 
comme dans l'exemple ci-dessus, 
mais aussi des fichiers de configurations pour différents systèmes de développements (Visual Studio, Xcode, [https://ninja-build.org](https://ninja-build.org)...).



Pour aller plus loin:

- https://cmake.org



## IDEs

Environnement graphique permettant la construction et compilation d'un "projet", semblable à ce qui est décrit dans la `CMakeLists.txt` précédente (avec beaucoup d'autres fonctionnalités!).

- [Visual Studio Code](https://code.visualstudio.com)
- [XCode](https://developer.apple.com/xcode/) (mac)
- [Code::Blocks](https://www.codeblocks.org)
- [Geany](https://www.geany.org)
- [Eclipse](https://eclipseide.org)
- ...

