// fonctions (booléenne) de test divisibilité par 2 et 3.
 
#include <iostream>

// l'entier x est pair
bool pair(int x)
{
  return (x % 2 == 0);
}

// l'entier x est pair
bool div3(int x)
{
  return (x % 3 == 0);
}

int main()
{
	int x;
	std::cout << "Entrer un entier: ";
	std::cin >> x;
	std::cout << x << (pair(x)?" pair":" impair") << std::endl;
	std::cout << x << (div3(x)?" ":" pas ") << "divisible par 3" << std::endl;
	std::cout << x << ((pair(x) && div3(x))?" ":" pas ") << "divisible par 6" << std::endl;
	return(0);
}