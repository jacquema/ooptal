// cours 06 exercice 10
// suite de Fibonacci

#include <iostream>

// calcul récursif du nieme nombre de Fibonacci
int Fib(int n) 
{
    if (n <= 0) 
        return 0;
    else if (n == 1) 
        return 1;
    else
        return Fib(n-1) + Fib(n-2);
}

// calcul itératif du nieme nombre de Fibonacci
// fenêtre glissante de longueur 3
int Fib_iteratif(int n) 
{
    int Fn = 0;    // pour memoriser Fib(n).
	int Fn_1 = 0;  // pour memoriser F(n-1). initialement F(0)
	int Fn_2 = 1;  // pour memoriser F(n-2). 

	for (int i = 1; i <= n; ++i) 
    {
        Fn   = Fn_1 + Fn_2; // pour i == 1, Fn = 1
        Fn_2 = Fn_1;        // 0 pour i = 1
        Fn_1 = Fn;
    }
    return Fn; 
}

int main() 
{
	int n;
	std::cout << "Entrer un entier positif : ";  
	std::cin >> n;

	std::cout << "calcul itératif: ";
	std::cout << "F(" << n << ") = " << Fib_iteratif(n) << std::endl;
	std::cout << "calcul récursif:";
	std::cout << "F(" << n << ") = " << Fib(n) << std::endl;

    return 0; 
}

