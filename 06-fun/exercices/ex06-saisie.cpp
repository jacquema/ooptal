// fonctions de saisie au clavier d'une chaine de caracteres.

#include <iostream>
#include <string>

// retour d'une chaîne saisie par l'utilisateur
std::string saisir()
{
	std::string ret;
	std::cout << "Entrer une chaine de caractères: ";
	std::cin >> ret;
	return ret;
}

// enregistrement dans reference d'une chaîne saisie par l'utilisateur
void saisiref(std::string& s)
{
	std::cout << "Entrer une chaine de caractères: ";
	std::cin >> s;
}

int main()
{
	std::string s = saisir();
	std::cout << "s =  " << s << std::endl;
	saisiref(s);
	std::cout << "s =  " << s << std::endl;
	return 0;
}