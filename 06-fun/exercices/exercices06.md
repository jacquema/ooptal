# Exercices - cours 06

## Fonctions



> **Exercice 1**.
>
> - Écrire une fonction booléenne ayant pour paramètre un entier et qui retourne si cet entier est pair.
> - idem pour tester qu'un entier en paramètre est multiple de 3.
> - Dans le `main`, demander la saisie d'un entier et afficher si il est multiple de 2, de 3, de 6.



> **Exercice 2. ** 
>
> 1. Écrire une fonction retournant la valeur absolue $|i|$ d'un entier $i$ passé en paramètre.
>
> 2. Écrire une fonction retournant la distance $|j - i|$ entre deux entiers $i$ et $j$ passés en paramètres. Elle appellera la fonction de la question précédente.
>



> **Exercice 3. ** **DEVOIR MAISON**
>
> 1. Écrire une fonction $f$ de paramètre un entier $n$, qui retourne la somme de $1$ à $n$ si $n >0$ ou $0$ sinon.
>
> 2. Écrire une fonction  $g$ de paramètre un entier $n$, qui retourne $\frac{n(n+1)}{2}$ si $n \geq 0$ ou 0 sinon.
>
> 3. Écrire une fonction booléenne `test` de paramètre un entier $n$ qui retourne si $f(n) = g(n)$, et affiche un message sur le résultat du test.
>
> 4. Dans la fonction `main`, appeler `test` sur des valeurs positives saisies par l'utilisateur.



>  **Exercice 4**: fonctions qui pourront être utilisées pour des tests.
>
>  1. Écrire une fonction nommée  `saisir`, sans paramètre,  qui demande la saisie au clavier d'une chaîne de caractère et retourne cette chaîne.
>  2. Écrire une fonction nommée  `saisiref` qui reçoit (par référence) une variable de type `std::string`, demande la saisie au clavier d'une chaîne de caractères et l'enregistre dans la variable.
>  3. Appeler les 2 fonctions dans  `main`.



> **Exercice 5**: autre fonction pour les tests.
>
> Écrire une fonction  `saisir`  avec deux paramètre entiers `a` et `b`,  qui demande la saisie au clavier d'un entier entre `a` et `b` et retourne cet entier. On recommencera la saisie tant que l'entier n'est pas entre les bornes. int saisir(int a, int b)
> 
>**bonus**: on recommence aussi si la saisie n'était pas un entier. 
> Ce dernier cas peut être détecté par un appel à `std::cin.fail()`, 
> et le flot `cin` sera remis en état par: `std::cin.clear();` puis `cin.ignore(1000,'\n');`




>  **Exercice 6**:
>
>  - Écrire une fonction booléenne ayant pour paramètre un caractère et testant si celui-ci est une lettre majuscule.
>
>  - Écrire une fonction booléenne ayant pour paramètre une chaîne de caractères et testant si elle contient une lettre majuscule.
>  - Tester dans `main` avec une fonction de l'exercice 4.



> **Exercice 7.** 
>
> 1. Écrire une fonction booléenne `alpha` qui teste qu'un  caractères en paramètre est alphabétique (lettre majuscule ou minuscule).
>
> 2. Écrire une fonction booléenne `eqc` prenant deux caractères en paramètres, et qui teste si ceux ci sont égaux sans tenir compte de la casse, c'est à dire sans faire la différence entre majuscules et minuscules. Par exemple `eqc('a', 'A')` = `eqc('B', 'b')` = `eqc('C', 'C')` = `eqc('!', '!')` = `true` et `eqc('a', 'B')` = `eqc('A', 'B')` = `eqc('C', '!')` = `false`.
>    On ne tiendra pas compte des caractères accentués.
>
> 3. En utilisant la fonction précédente, écrire une fonction prenant en paramètres deux chaines de caractères et qui teste si celles ci sont égales, sans tenir compte de la casse.
>    On la testera dans un `main` en utilisant la fonction  `saisir` de l'exercice 4.




> **Exercice 8**:  Valeur de retour par référence.
> Écrire une fonction ayant pour paramètres un `double` $x$ et une référence `flag` sur un booléen et retourne l'inverse $\frac{1}{x}$ de $x$ si $x \neq 0$ ou $-1$ sinon. 
> De plus, `flag` sera mis à la valeur `true` si $x = 0$  (cas d'erreur)  et `false` sinon.




> **Exercice 9**:  Valeurs de retour par références. **DEVOIR MAISON.**
> Écrire une fonction boolénne ayant pour paramètres une chaîne de caractères `s` et trois références sur des entiers. La fonction retourne `true` si le premier caractère de `s` est une lettre majuscule, et, au retour, les trois variables devront contenir respectivement le nombre de lettres minuscules, de lettres majuscules, et de caractères autres (non-alphabétiques) dans `s`.
> `s` ne devra pas être modifié par la fonction.



> **Exercice 10:** nombres de Fibonacci
>
> La suite de Fibonacci est définie par
> $$
> \begin{align*}
> F(0) & = 0 \\
> F(1) & = 1 \\
> F(n) & = F(n-1)+ F(n-2) \ \mathrm{ pour }\  n>1
> \end{align*}
> $$
> <img src="/Users/jacquema/Library/Mobile Documents/com~apple~CloudDocs/Enseignement/INALCO/06-fun/img/Fibonacci2.png" alt="Fibonacci2" style="zoom:50%;" />
>
> Écrire deux fonctions avec un paramètre entier  $n$ et qui calculent $F(n)$:
>
> - la première comme fonction récursive,
> - la seconde avec une boucle `for`. 
>
> Tester ces deux fonctions dans le `main` , avec des nombres pas trop grands ($≤ 45$.)



> **Exercice 11:**  Dichotomie 
> La  dichotomie est une méthode pour rechercher efficacement un nombre dans un intervalle. Supposons que l'usilisateur choisit un nombre entre 0 et 100. Le programme essaie de le deviner comme suit:
>
> - le programme propose le mileu de l'intervalle, 50, et l'utilisateur lui répond:
>   - ou bien "trouvé" et c'est gagné, 
>   - ou bien "plus grand" et le programme recommence avec l'intervalle entre 51 et 100,
>   - ou bien "plus petit" et le programme recommence avec l'intervalle entre 0 et 49.
>
> On va implémenter cette méthode avec des fonctions.
>
> - Écrire une fonction `propose` avec un paramètre entier, qui propose cet entier à l'utilisateur (en l'affichant) et demande une réponse. 
>   La réponse peut être un des caractères `=` ou `>` ou `<` suivant les 3 cas ci-dessus. La fonction retournera alors `0` ou `1` ou `-1`.
> - Écrire une fonction **récursive**  `dichtomie` , qui prend en paramètre les bornes entières d'un  intervalle et fait la recherche comme si dessus, en appelant `propose` avec le milieu de l'intervalle.
> - Écrire un `main`  qui lance la recherche dans un intervalle initial, définit par des constantes `INF` et `SUP` .

