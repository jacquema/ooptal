
// fonction  `saisir` avec deux paramètre entiers `a` et `b`, 
// qui demande la saisie au clavier d 'un entier entre `a` et `b` et retourne cet entier. 
// On recommence la saisie tant que l' entier n'est pas entre les bornes.

#include <iostream>
#include <limits>


int saisie(int a, int b)
{
	int x;

	while (true)
	{
		std::cout << "entier " << "entre " << a << " et " << b << ": ";
		std::cin >> x;

		if (x < a || b < x)	continue;
		else return x;
	}
}

// bonus: variante avec détection d'échec de parsing d'un entier
// https://stackoverflow.com/questions/5131647/why-would-we-call-cin-clear-and-cin-ignore-after-reading-input
// https://www.geeksforgeeks.org/how-to-use-cin-fail-method-in-cpp/
int saisiefail(int a, int b)
{
	int x;

	while (true)
	{
		std::cout << "entier " << "entre " << a << " et " << b << ": ";
		std::cin >> x;

		if (std::cin.fail())
		{
			// cin est remis dans son état initial
			// Clear the error flags on the input stream.
			std::cin.clear();
			// leave the rest of the line
			// std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
			std::cin.ignore(10000, '\n');
		}
		else if (x < a || b < x)
		{
		} // continue est inutile
		else
		{
			return x;
		}
	}
}

int main()
{
    int x = saisiefail(0, 100);
    std::cout << "x = " << x << " fail: " <<  std::cin.fail() << std::endl;
    
    return 0;
}