#include <iostream> 
#include <string>

bool alpha(char c)
{
  return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z');
}

bool eqc(char c1, char c2)
{
  return (c1 == c2 || 
		 (alpha(c1) && alpha(c2) && (c1 == c2 - 32 || c1 == c2 + 32)));
}

bool eqc(const std::string& s1,
             const std::string& s2)
{
  if (s1.size() != s2.size())
    return false;

  for (size_t i = 0; i < s1.size(); ++i)
    if (! eqc(s1[i], s2[i]))
      return false;

  return true;
}

void test(const std::string& s1,
          const std::string& s2)
{
  std::cout << s1 << ", " << s2 << " : " << eqc(s1, s2) << std::endl;
}

int main()
{
  test("bonjour!", "bonjour!");
  test("Bonjour!", "bonjour!");
  test("bonJour!", "BONJOUR!");
  test("Bonjour!", "Bonjours");
  test("Bonjour!", "Vonjour!");
  test("Bonjour!", "Bon");
  return 0;
}