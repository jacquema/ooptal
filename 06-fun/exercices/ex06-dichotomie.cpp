/* 
 * cours 07 exercice 
 * 
 * Recherche dichotomique
 */

#include <iostream>
#include <assert.h>

// "interface utilisateur"
// propose un nombre a à l'utilsateur et convertit sa réponse en entier (-1 ou 0 ou 1)
int propose(int x)
{
    char rep = 0;

    do
    {
        std::cout << "Le nombre est-il =, >, < à " << x << " ? ";
        std::cin >> rep;
        if (rep == '=')
            return 0;
        else if (rep == '>')
            return 1;
        else if (rep == '<')
            return -1;
        else 
             std::cout << "WHAT!?" << std::endl;
    } while (rep != '=' && rep != '>' && rep != '<');
}

// recherche dichotomique entre les bornes a et b
int dicho(int a, int b)
{
	assert(a <= b);

	if (a == b) // cas trivial: on a trouvé
		return a;

	int m = (a + b) / 2; // milieu de [a, b]

	// interaction avec l'utilisateur. rep est sa réponse.
	int rep = propose(m);

	if (rep == 0)
		return m;               // trouvé!
	else if (rep > 0) 
		return dicho(m + 1, b); // relance une recherche dans la moitié supérieure
	else if (rep < 0)
		return dicho(a, m - 1); // relance une recherche dans la moitié inférieure
}

int main()
{
	const int INF = 0;
	const int SUP = 100;

	std::cout << "Pense à un nombre entre " << INF << " et " << SUP << std::endl;
	int d = dicho(INF, SUP);
	std::cout << "c'est: " << d << std::endl;
	return 0;
}