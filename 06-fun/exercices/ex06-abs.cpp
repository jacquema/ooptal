// valeur absolue et distance d'entiers

#include <iostream>

int abs(int i)
{
    if (i < 0) return -i;
    else return i;
}


int dist(int i, int j)
{
    return abs(j-i);
}


int main()
{
	int i, j;
	std::cout << "Entrer deux entiers: ";
	std::cin >> i >> j;

    std::cout << "valeur absolue " << i << " = " << abs(i) << std::endl;
    std::cout << "valeur absolue " << j << " = " << abs(j) << std::endl;
    std::cout << "distance " << i << ", " << j << " = " << dist(i, j) << std::endl;
	return 0;
}