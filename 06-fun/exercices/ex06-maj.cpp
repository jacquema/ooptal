// - Écrire une fonction booléenne ayant pour paramètre un caractère et testant si celui-ci est une lettre majuscule.
// - Écrire une fonction booléenne ayant pour paramètre une chaîne de caractères et testant si elle contient une lettre majuscule.

#include <iostream>
#include <string>

// le caratere est une majuscule
bool maj(char c)
{
	return ('A' <= c && c <= 'Z');
}

// la chaîne s contient une lettre majuscule
// version avec boucle sur rang
bool maj(const std::string& s)
{
	// parcours de tous les caracteres de s
	for (char c : s)
	{
		if (maj(c))
			return true; // trouvé! on arrête la recherche
	}
  
	return false;    // pas trouvé
}

// la chaîne s contient une lettre majuscule
// version avec boucle sur index dans la chaine
bool maji(const std::string& s)
{
	// parcours de tous les caracteres de s
	for (int i = 0; i <  s.size(); ++i) // i = index dans la chaine = numéro de caractere  
	{
		if (maj(s[i]))   // s[i] est le ieme caractere de s
			return true; // trouvé! on arrête la recherche
	}
  
	return false;    // pas trouvé
}

// function de l'exercice 4 pour la saisie
void saisiref(std::string& s)
{
	std::cout << "Entrer une chaine de caractères: ";
	std::cin >> s;
}

int main()
{
	std::string s;
	saisiref(s);
	std::cout << s << ((maj(s))? " contient une majuscule":" ne contient pas de majuscule") << std::endl;
	return 0;
}