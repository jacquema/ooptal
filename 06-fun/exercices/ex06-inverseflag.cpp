// cours 06 exercice 8
// fonction inverse ayant pour paramètres un double x et une référence "flag" sur un booléen 
// et qui retourne 1/x si x != 0 ou -1 sinon.
// de plus, flag est mis à la valeur true si x = 0 (cas d'erreur) et false sinon.

#include <iostream>

		 // retour de l'inverse, signalant une erreur avec le flag
		 double
		 inv(double x, bool &flag)
{
    if (x != 0)
    {
        flag = false; // pas d'erreur
        return 1/x;
    }
    else
    {
        flag = true; // erreur
        return -1;
    }
}


void testinv(double x)
{
    bool erreur;
	std::cout << "inv: " << x << " = " << inv(x, erreur) << " ";
	if (erreur)
		std::cout << "(ERROR)" << std::endl;
	else
       std::cout << "(OK)" << std::endl;
}

int main()
{
    testinv(1);
    testinv(2);
    testinv(0);
    testinv(-10);
    return 0;
}
