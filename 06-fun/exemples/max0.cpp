
#include <iostream>

// définition de la function max
int max(int x, int y = 0)
{
    if (x > y)
        return x;
    else
        return y;
}

int main()
{
    int a = 100;
    int b = 200;

    // appel de max (et affectation)
    std::cout << "max " << a << " et " << b << " = " << max(a, b) << std::endl;
    std::cout << "max " << a << " = " << max(a);
    return 0;
}