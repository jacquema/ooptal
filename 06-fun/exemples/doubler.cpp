
#include <iostream>

// définition de la function max
void doubler(int& x)
{
    x *= 2;
}
 
int main () 
{
    int n = 7;
    std::cout << "n = " << n << std::endl;

    doubler(n);
    std::cout << "n = " << n << std::endl;
    return 0;
}