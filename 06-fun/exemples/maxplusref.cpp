
#include<iostream>

int maxplus(int& x, int& y)
{
  if (x > y) { y++; return x; }
  else { x++; return y; }    
}

int main() 
{
  int a = 100;
  int b = 200;
  int m;

  m = maxplus(a, b); // appel de max (et affectation du retour à m)
	                 // a est modifié
  std::cout << "Le max est: " << m << std::endl;
  std::cout << "a=" << a << std::endl;
  std::cout << "b=" << b << std::endl;

  return 0;
}