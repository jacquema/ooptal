
#include <iostream>
#include <string>

bool plusgrand(int x, int y)
{
    return (x >= y);
}

bool plusgrand(const std::string& x, const std::string& y)
{
    return (x >= y);
}

int main()
{
    std::cout << "max " << "cat" << " et " << "dog" << " = ";
    std::cout << plusgrand("cat", "dog") << std::endl;

    std::cout << "max " << 11 << " et " << 6 << " = ";
    std::cout << plusgrand(11, 6) << std::endl;

    return 0;
}