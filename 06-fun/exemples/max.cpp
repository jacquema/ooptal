
#include <iostream>

// définition de la function max
int max(int x, int y)
{
    if (x > y)
        return x;
    else
        return y;
}
 
int main () 
{
   int a = 100;
   int b = 200;
   int ret;
 
   // appel de max (et affectation)
   ret = max(a, b);
   std::cout << "Le max de " << a << " et " << b << " est: " << ret;
   std::cout << std::endl;

   return 0;
}