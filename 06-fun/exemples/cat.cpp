#include <iostream>
#include <string>


std::string concatenate(std::string& x, std::string& y)
{
  return x + y;
}

int main()
{
  std::string s1 = "the ";
  std::string s2 = "cat";

  std::cout << concatenate(s1, s2) << std::endl;
  std::cout << s1 << std::endl;
  std::cout << s2 << std::endl;
  return 0;
}