#include <iostream>
#include <string>


std::string concatenate(const std::string& x, const std::string& y)
{
  return x + y;
}

std::string concatenate(const std::string &x, const std::string &y, const std::string &z)
{
  return x + y + z;
}

int main()
{
  std::string s1 = "the ";
  std::string s2 = "cat ";

  // le compilateur sait qui appeler
  std::cout << concatenate(s1, s2) << std::endl;
  std::cout << concatenate(s2, s1, s2) << std::endl;
  return 0;
}