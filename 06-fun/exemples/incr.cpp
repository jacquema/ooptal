#include <iostream>

int incr(int& x)
{
  x = x+1;
  return x;
}

int main()
{
  int a=2, b=2;
  std::cout << incr(a) << std::endl;
  std::cout << a + incr(b) << std::endl;
  incr(b);
  std::cout << b << std::endl;
}