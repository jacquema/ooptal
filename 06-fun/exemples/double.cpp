#include <iostream> 

int doubler(const int&);

int main()
{ 
  const int p(2);
  std::cout << " double " << p << " = " << doubler(p) << std::endl;
}

int doubler(const int& n)
{  
  return 2*n;
}