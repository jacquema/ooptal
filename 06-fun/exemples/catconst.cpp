#include <iostream>
#include <string>

std::string concatenate(const std::string& x, const std::string& y)
{
  x = x + y;
  return x;
}

int main()
{
  std::string s1 = "the ";
  std::string s2 = "cat";

  std::cout << concatenate(s1, s2) << std::endl;
  std::cout << s1 << std::endl;
  std::cout << s2 << std::endl;
  return 0;
}