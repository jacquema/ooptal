
#include <iostream>

// définition de la function max
void doublerpas(int x)
{
    x *= 2;
}
 
int main () 
{
    int n = 7;
    std::cout << "n = " << n << std::endl;

    doublerpas(n);
    std::cout << "n = " << n << std::endl;
    return 0;
}