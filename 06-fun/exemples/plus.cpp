/**
 * échec à la compilation
 *
 * plus.cpp:19:18: error: call to 'plus' is ambiguous
 * 
 * plus.cpp:4:5: note: candidate function
 * int plus(int a)
 * 
 * plus.cpp:12:5: note: candidate function
 * int plus(int a, int b = 0)
**/

#include <iostream>

int plus(int a)
{
    if (a < 0)
        return -a;
    else
        return a;
}

int plus(int a, int b = 0) 
{
   return (a+b);
}

int main()
{
    std::cout << plus(1);
    return 0;
}
