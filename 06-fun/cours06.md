# Cours de programmation objet

INALCO  
Master TAL première année

---

## Cours 06 Fonctions

jeudi 5 novembre 2024  
Florent Jacquemard  `florent.jacquemard@inria.fr`

sources: 

- cours de Christian Casteyde
  - http://casteyde.christian.free.fr/cpp/cours/online/x600.html (fonctions)
  - http://casteyde.christian.free.fr/cpp/cours/online/x792.html (`main`)
- https://cplusplus.com/doc/tutorial/functions/
- https://www.tutorialspoint.com/cplusplus/cpp_functions.htm
- https://www.geeksforgeeks.org/functions-in-cpp/ 
- cours de Marie-Anne Moreaux, INALCO (cours 09)

---

## Généralités

Une fonction est une suite d'instructions qui, ensemble, réalisent une tâche particulière.
Concrètement, il s'agit d'un **bloc** de code auquel est donné un **nom**, et qui s'exécute que lorsque ce nom est **appelé**.

De plus, il est possible de passer à la fonction des valeurs d'entrée (appelés **paramètres** ou **arguments**) 
et la fonction peut **retourner** une valeur après avoir exécuté ses instructions (mais ce n'est pas obligatoire).



## Définition

La **définition** d'une fonction se fait, en `C++`,  avec la syntaxe  suivante.

```c++
type identificateur(paramètres)
{
    // instructions de la fonction.
}
```

- La première ligne est appelée **entête** de la fonction (*header*).  
- La seconde partie, le bloc de code entre `{` `}`, qui contient les instructions, est appelée **corps** de la fonction (*body*).

L'entête est composée de:

- **type de retour**: une fonction peut renvoyer une valeur. Le type de retour est le type de la valeur de donnée renvoyée par la fonction. 
  
  Certaines fonctions effectuent des opérations sans renvoyer de valeur (on parle de *procédures*). 
  Dans ce cas, le type de retour est le mot-clé `void`. 
  
- **nom de la fonction**: il s'agit d'un **identificateur** utilisé par la suite pour appeler la fonction. 
  
- **liste des paramètres** (ou arguments): c'est une suite de déclarations de variables, séparées par des virgules:
  `type1 id1, type2 id2... `.
  Chacun des identificateurs `id1` agit dans le corps de la fonction comme une variable, **locale** à la fonction (sa portée se limite à la fonction).   
  Une fonction peut n'avoir aucun paramètre. Dans ce cas, dans l'entête, son nom doit quand même être suivi de parenthèses, ou de `(void)`.
  
  > **Exemple**:
  >
  > ```c++
  > void exit();
  > ```
  
  Le rôle des paramètres est de permettre de passer des arguments à la fonction à partir de l'emplacement d'où elle est appelée. 
  
- Le **corps** de la fonction contient une collection d'instructions qui définissent ce que fait la fonction.



L'instruction `return` termine l'exécution de la fonction. 

- pour une fonction qui retourne une valeur (type de retour autre que `void`), l'instruction `return` est obligatoire. On lui passe la donnée à retourner.
- pour une fonction de type de retour `void`, l'instruction `return` (sans argument) est optionnelle.
  En l'absence de `return`, la fin de l'execution est la fin du bloc (`}` fermante).



> **Exemple**:
>
> ```c++
> int max(int x, int y)
> {
> 	if (x > y)
> 		return x;
> 	else
>     return y;    
> }
> ```
>
> `max` a pour type de retour `int` et deux paramètres: les variables `x` et `y` de type `int`.
> Le corps de la fonction est composé d'une seule instruction `if`.
> On remarque que chaque branche du `if` se termine par `return` .



## La fonction `main`

Tout programme `C++` contient au moins une fonction: la fonction `main`, qui est le **point d'entrée** du programme, c'est à dire que la machine commencera  l'exécution du code à partir du début de la fonction `main`.

Le **type de retour** de `main` est entier (`int`). 
Le nombre entier renvoyé est utilisé pour informer le programme appelant du résultat de l'exécution du programme.  
Si le corps de la fonction `main` ne contient pas `return`, le compilateur ajoute une instruction finale (implicite) `return 0`.

> **Attention**: ceci ne s'applique qu'à la fonction `main`! Toutes les autres fonctions avec un type de retour doivent se terminer par une instruction `return` appropriée, avec pour argument la valeur de retour, même si celle-ci n'est pas utilisée.



Par convention, une valeur de retour `0` signale qu'il n'y a pas eu de problèmes, et une autre valeur correspond à un **code d'erreur**.  

Certains environnements donnent accès aux valeurs de retour à l'appelant. Les valeurs pour `main` qui sont garanties d'être interprétées de la même manière sur toutes les plateformes sont:

- `0` le programme a été exécuté avec succès
- constante `EXIT_SUCCESS` (définie dans `<cstdlib>`): 
  le programme a été exécuté avec succès
- constante `EXIT_FAILURE` (définie dans `<cstdlib>`): 
  l'exécution du programme a échoué.

cf. https://cplusplus.com/reference/cstdlib/EXIT_SUCCESS/

La fonction `main` peut être définie sans paramètres:

```c++
int main() 
{ 
    ... 
    return 0; 
}
```

ou avec des paramètres décrivant les options de la ligne de commande:

```c++
int main(int argc, char* const argv[]) 
{ 
    ... 
    return 0; 
}
```



## Appel

L'utilisation d'une fonction dans un programme se fait par un **appel** (*call* ou *invokation*).

L'appel est fait à l'aide du nom de la fonction (identificateur), suivit des valeurs des paramètres requis, entre `(` `)`, en respectant l'ordre spécifié à la définition.  
Les `(` `)` sont obligatoires, même pour une fonction sans paramètres.

Si la fonction retourne une valeur, l'instruction d'appel est évaluée comme la valeur de retour.

> **Exemple** : fonction `max`
> cf. [ `exemples/max.cpp`](exemples/max.cpp)
>
> ```c++
> // définition de la function max
> int max(int x, int y)
> {
> 	if (x > y) return x;
> 	else return y; 
> }
> 
> int main() 
> {
> 	int a = 100;
> 	int b = 200;
> 	int m;
> 
> 	// appel de max et affectation de la valeur de retour à m
> 	m = max(a, b);
> 	std::cout << "Le max est: " << m << std::endl;
> 
> 	return 0;
> }
> ```



## Contrôle

Lorsqu'un programme appelle une fonction, le contrôle du programme est transféré au début du corps de la fonction. 

La tâche définie par la fonction est exécutée, et lorsque l'instruction `return` est rencontrée, 
ou bien lorsque l'accolade fermante `}` marquant la  fin de la fonction est atteinte, 
le contrôle est renvoyé au point d'appel (à l'instruction suivant l'appel).



## Déclaration

Une **déclaration** de fonction est composée simplement de son entête, suivie d'un point virgule.

Elle renseigne le compilateur sur le nom, le type de retour et les paramètres (nombre et types) d'une fonction. 
Celle ci peut être appelée aussitôt après sa déclaration (même si elle n'est pas encore définie).

La définition de la fonction, qui décrit fournit le corps réel de la fonction peut être faite séparément, après la déclaration.

Les identificateurs de paramètres peuvent être omis dans la déclaration, mais 

- leur types sont obligatoires, et
- leur ordre est important.



> **Exemple**: déclaration de fonction
>
> ```c++
> // déclaration de la function max
> int max(int, int);
> 
> // à partir d'ici, la fonction max peut être appelée
> // (dans la définition d'autres fonctions).
> int main () 
> {
>    	std::cout << "Le max est : " << max(100, 200) << endl;
>    	return 0;
> }
> 
> // définition de la fonction max (ne pas l'oublier!)
> int max(int x, int y)
> {
>     if (x > y)
>         return x;
>     else
>         return y;
> }
> ```



## Remarques

de l'importance d'écrire des fonctions

Créer des fonctions permet de rassembler certaines tâches courantes ou répétées, de sorte qu'au lieu d'écrire le même code encore et encore pour différentes valeurs d'entrées, il est possible d'appeler la fonction. 

Ainsi, le code est

- plus concis:  
  on évite des redondances de code - blocs répétés (copiés/collés) à différents endroits,

- plus facile à maintenir:  
  modifier le corps d'une fonction (pour le corriger ou l'améliorer) revient à modifier le résultat de chaque appel, alors que dans le cas de blocs copiés/collés, il faut réécrire chaque bloc!

- plus fiable:  
  lorsqu'une fonction a été bien vérifiée / testée, 
  elle peut être appelée sereinement.

Vous pouvez diviser votre code en fonctions distinctes. La façon dont vous divisez votre code entre différentes fonctions dépend de vous. Typiquement, la division est généralement telle que chaque fonction effectue une tâche spécifique.



## Fonctions d'utilisateurs ou de bibliothèques

### Fonctions d'utilisateurs

Les fonctions définies par l'utilisateur sont des blocs de code personnalisés, définis spécialement  pour réduire la complexité des gros programmes.  
Elles sont communément appelés « fonctions sur mesure », et sont construites pour satisfaire les conditions dans lesquelles l'utilisateur veut résoudre des problèmes tout, en réduisant la complexité de l'ensemble du programme.

### Fonctions de bibliothèques

Les fonctions de bibliothèque, également appelées « *builtin* » font partie d'un paquet pré-défini pour certaines fonctionnalités. 
Les fonctions *builtin* peut être appelées  directement, après avoir chargé leurs déclaration 
par une `#include`.

> **Exemples**:
> 
> - les opérateurs binaires de redirection de flux `<<` et `>>` définis dans la librairie ` <iostream>`.
> - les fonctions de la librairie `<string>` vues au cours 07.



## Passage de paramètres par valeur ou référence

### Passage par valeur

Dans les exemples de fonctions vus jusqu'ici, les arguments sont passés par valeur. 
Cela signifie que, lors de l'appel d'une fonction, comme `max(a, b)`, 
ce qui est passé à la fonction `max` sont les valeurs (entières), au moment de l'appel,
des variables `a` et `b` (`100` et `200` dans l'exemple précédent).  
Ces valeurs sont **copiées** dans les variables représentées par les paramètres de la fonction.
Une valeur peut alors être calculée à partir de ces variables, dans le corps de la fonction) et retournée.



> **Exemple**:  dans l'exemple ci-dessus avec une fonction `max` 
>
> ```C++
> int max(int x, int y)
> {
> 	if (x > y) return x;
> 	else return y; 
> }
> ```
>
> appelée comme suit:
>
> ```C++
> 	int a = 100;
> 	int b = 200;
> 	int m = max(a, b);
> ```
>
>  on aura en mémoire, pendant l'appel, les 5 variables suivantes:
>
> ```mermaid
> classDiagram
> class A["a"]
> A: 100
> class B["b"]
> B: 200
> class M["m"]
> M: ??
> class X["x"]
> X: 100
> class Y["y"]
> Y: 200
> ```
>
> Les deux dernières variables sont les variables locales `x` et `y` de `max`.
> Elle contiennent une copie du contenu de `a` et `b` respectivement. 
>
> Après l'appel, `x` et `y` sont sorties de portée, 
> et `m` s'est vu assignée la valeur de retour de `max:
>
> ```mermaid
> classDiagram
> class A["a"]
> A: 100
> class B["b"]
> B: 200
> class M["m"]
> M: 200
> ```



## Passage par référence

Dans certains cas, on préférera accéder directement aux variables passées à la fonction 
(pas à une copie de leur valeur). 

Par exemple, cela peut être utile: 

- pour modifier les valeurs de ces variables dans le corps de la fonction.
  Typiquement, pour échanger le contenu de 2 variables.
- pour éviter de copier le contenu d'une variable lors d'un appel, 
  lorsque celui-ci est un gros objet.

Pour être passé par référence, un paramètre est déclaré dans l'entête d'une fonction comme une référence.



##### Rappel du cours 05: 

En `C++` une **référence** est un alias, c'est à dire un surnom (un identificateur alternatif) pour une variable existante. À la déclaration, le type d'une référence est suivit du caractère `&`.

> **Exemple**:
>
> ```c++
> int n = 1;
> int& ref = n;
> ```
>
> `ref` est une référence sur `n` (un surnom pour `n`).
> Ici, la valeur de `ref` est 1 (tout comme `n`).
>
> ```c++
> n = n + 1;
> ```
>
> maintenant, 
>
> - la valeur de `n` est 2,
> - la valeur de `ref` est 2.
>
> ```c++
> ref = 5;
> ```
>
> et maintenant, 
>
> - la valeur de `n` est 5,
>
> - la valeur de `ref` est 5.



Le passage de paramètres par référence s'écrit, dans la déclaration de la fonction,  
en utilisant la syntaxe avec `&` après les types des paramètres. 



> **Exemple**:  revenons à l'exemple de la fonction `max` , cette fois avec un passage de paramètres par référence (types `int&` dans l'entête).
>
> ```C++
> int max(int& x, int& y)
> {
> 	if (x > y) return x;
> 	else return y; 
> }
> ```
>
> Pendant l'appel suivant:
>
> ```C++
> 	int a = 100;
> 	int b = 200;
> 	int m = max(a, b);
> ```
>
>  on aura en mémoire les variables:
>
> ```mermaid
> classDiagram
> class A["a, x"]
> A: 100
> class B["b, y"]
> B: 200
> class M["m"]
> M: ??
> ```
>
> Donc ici, `x` et `y` sont des alias pour `a` et `b` respectivement, 
> et non pas des nouvelles variables, copies de `a` et `b` comme dans le cas précédent du passage par valeur.



Dans le cas de la fonction `max`, le résultat est le même que l'on passe les paramètre par valeur ou référence. Une différence importante dans le second cas est néanmoins que les variables initiales `a` et `b` pourraient être modifiées par la fonction.



### Modification de variable

Le passage par référence permet de modifier, dans le corps de la fonction, une variable extérieure à celle-ci.

Lors d'un passage de variable par référence, ce qui est passé à la fonction est non pas une copie, 
mais la variable elle-même; la variable passée à l'appel est identifiée par le nom du paramètre de fonction,
qui devient une référence (surnom) sur cette variable dans le corps de la fonction.

Donc toute modification du paramètre dans le corps de la fonction est reflétée dans les variables passées en tant qu'arguments dans l'appel.

Cela s'appelle un *effet de bord* de la fonction.



> **Exemple**: passage par reference avec modification de la variable référencée.
>
> cf.  [`exemples/doubler.cpp`](exemples/doubler.cpp)
>
> ```c++
> void doubler(int& x)
> {
>   	x *= 2;
> }
> ```
>
> Cette fonction `doubler` ne retourne rien, mais la valeur de la variable en argument est doublée.
>
> ```c++
> int main()
> {
>   	int n = 7;
>   	doubler(n);
>   	std::cout << "n = " << n << std::endl;
> }
> ```
>
> on obtient l'affichage
>
> ```
> n = 14
> ```



En redéfinissant la fonction avec un appel par valeur, on perd cette fonctionnalité.

> **Exemple**:
>
> ```c++
> void pasdoubler(int x)
> {
>   	x *= 2;
> }
> 
> int main()
> {
>    	int n=7;
>     	pasdoubler(n);
>     	std::cout << "n=" << n;
> }
> ```
>
> on obtient l'affichage
>
> ```
> n = 7
> ```
>
> En fait, ici, la fonction `doublepas` ne fait rien du tout!



> **Exemple**:  revenons encore à l'exemple de la fonction `max` , avec passage des paramètres par référence et, cette fois, modification d'un des paramètres.
>
> ```C++
> int max(int& x, int& y)
> {
> 	if (x > y) 
>     return x;
> 	else 
>   {
>     x += 1;
>     return y; 
>   }
> }
> ```
>
> Au début de l'appel suivant:
>
> ```C++
> 	int a = 100;
> 	int b = 200;
> 	int m = max(a, b);
> ```
>
>  on aura en mémoire les variables:
>
> ```mermaid
> classDiagram
> class A["a, x"]
> A: 100
> class B["b, y"]
> B: 200
> class M["m"]
> M: ??
> ```
>
> et après l'appel, `a` (alias `x` pendant l'appel) a été incrémentée (par `x += 1` dans la fonction).
>
> ```mermaid
> classDiagram
> class A["a"]
> A: 101
> class B["b"]
> B: 200
> class M["m"]
> M: 200
> ```
>
> Les références `x` et `y` ont disparues à la sortie de portée.



### Efficacité du passage par référence

Une autre utilité du passage par référence est l'économie de temps de copie, 
en particulier dans le cas de paramètres qui représentent de gros objets.

L'appel d'une fonction avec des paramètres passés par valeur nécessite des copies des valeurs. 
Cela est relativement peu coûteux pour les types fondamentaux tels que `int`. 
En revanche, si le paramètre est d'un type composé volumineux  (image...), cela peut entraîner une certain coût. 



> **Exemple**: passage de chaînes par valeur.
>
> ```c++
> std::string concatenate(std::string x, std::string y)
> {
>   	return x+y;
> }
> ```
>
> La fonction `concatenate` prend deux chaînes comme paramètres (par valeur) et retourne leur concaténation.   
> En passant les arguments par valeur, la fonction assigne à `x` et `y` des copies des arguments passés à l'appel. 
> Et si il s'agit de longues chaînes, cela peut être couteux.



> **Exemple**: passage de chaînes par référence.
>
> cf. [ `exemples/cat.cpp`](exemples/cat.cpp)
>
> On peut éviter le cout de copie avec un passage par référence.
>
> ```c++
> std::string concatenate(std::string& x, std::string& y)
> {
>   	return x+y;
> }
> ```



Le passage des arguments par référence ne nécessite pas de copie. 
La fonction calcule directement sur des (alias de) des chaînes passées en tant qu'arguments. 
Par conséquent, la version de `concatenate` prenant des références est plus efficace que la version prenant des valeurs.



> **Exemple**: passage de chaînes par référence avec modification.
>
> Le contenu des variables peut être modifié lors d'un passage par référence.
>
> cf. [`exemples/catmod.cpp`](exemples/catmod.cpp)
>
> ```c++
> std::string concatenate(string& x, string& y)
> {
>     	x = x+y;
>     	return x;
> }
> ```
>
> La valeur de la première chaine (reference  `x`)  est modifiée, pour devenir la concaténation des valeurs initiales de `x` et `y`.
>
> La valeur la seconde chaîne (référence  `y`)  n'est pas modifiée.



## `const` références

Le mot clé `const`, avant une déclaration de paramètre, spécifie qu'un paramètre passé par référence n'est pas modifié par la fonction.  

> **Exemple**:
>
> cf. [`exemples/catconst.cpp`](exemples/catconst.cpp)
>
> Dans l'exemple précédent, on peut ajouter `const` au paramètre  `y` .
>
> ```c++
> std::string concatenate(string& x, const string& y)
> {
>   	x = x+y;
>   	return x;
> }
> ```



Lorsqu'un paramètre est déclaré comme `const`, il est interdit de le modifier dans le corps de la fonction.

Mais la fonction peut utiliser la valeur du paramètre.

> **Exemple**: dans l'exemple précédent, la fonction `concatenate` 
> utilise la valeur de `y` pour appliquer `+`, 
>  sans avoir à faire de copie réelle de la chaîne `y` (comme pour `x`).  

En d'autre termes, la variable `const` est accessible en lecture seule.
Dans l'exemple sans `const` elle est accessible en lecture et écriture.



L'usage de `const` donne l'assurance à l'appelant que la fonction ne modifie pas `y`.
Le compilateur va s'assurer que tel est bien le cas.

>  **Exemple**:
>
> La définition de fonction suivante provoquera une erreur de compilation.
>
> ```c++
> std::string concatenate(const string& x, const string& y)
> {
>     x = x+y;
>     return x;
> }
> ```
>
> message d'erreur:
>
> ```
> ...argument has type 'const std::string' (...)
> but method is not marked const
> string& operator=(const string& __str);
> ```
>
> ce qui signifie que le paramètre `x` est déclaré comme `const`
> pour `concatenate` mais pas l'opérateur d'affectation `=` qui lui est appliqué.



Donc, une référence déclarée comme `const` garantit la non modification des variables concernées, 
tout comme avec le passage par valeur,  mais avec une meilleure efficacité pour les paramètres de types volumineux. 

C'est pourquoi les déclarations `const` sont extrêmement populaires en `C++` pour les arguments de types utilisateur (*i.e*. autre que types de base). 

Il est recommandé d'utiliser `const` de manière **pro-active** (chaque fois que possible).

Notez cependant que pour la plupart des types de base, il n'y a pas de différence notable d'efficacité 
(dans certains cas, les références `const` peuvent même être légèrement moins efficaces!).



## Surcharge de fonctions

En `C++`, il est possible de définir plusieurs fonctions 

- de même nom 
- et même type de retour 
- mais avec un nombre et/ou des types de paramètres différents.

On parle alors de surcharge.



> **Exemple** 1: cf. [`exemples/catoverload.cpp`](exemples/catoverload.cpp).
>
> On a parfaitement le droit de définir les deux fonctions `concatenate` suivantes:
>
> ```c++
> std::string concatenate(const string& x, const string& y)
> {
>   	return x+y;
> }
> 
> std::string concatenate(const string& x, const string& y, const string& z)
> {
>   	return x+y+z;
> }
> ```
>



> **Exemple** 2:  cf. [`exemples/maxoverload.cpp`](exemples/maxoverload.cpp).
>
> ou des fonctions `plusgrand` portant sur des objets de différents types (ici `int` ou `string`)
>
> ```c++
> bool plusgrand(int x, int y)
> {
>    	return (x >= y);
> }
> 
> bool plusgrand(const std::string& x, const std::string& y)
> {
>    	return (x >= y);
> }
> ```
>



Rem: En `C`, la surcharge est interdite.

L'avantage de la surcharge de fonctions est qu'elle augmente la lisibilité du programme car il n'est pas nécessaire d'utiliser des noms différents pour la même action.

**Résolution d'ambiguité**: Pour différencier des fonctions surchargées, le compilateur regarde la liste des types de leurs paramètres respectifs (on parle de **signature** des fonction).



## Valeurs de paramètres par défaut

Lors de la déclaration d'une fonction, il est possible de spécifier une valeur par défaut pour chacun des derniers paramètres. Cette valeur sera utilisée si l'argument correspondant est laissé vide lors de l'appel de la fonction.

Cela se fait en utilisant l'opérateur d'affectation `=` pour "affecter" des valeurs aux paramètres de la fonction. Si aucune valeur n'est transmise pour le paramètre concerné lorsque la fonction est appelée, la valeur donnée par défaut est utilisée. Si une valeur est spécifiée, la valeur par défaut est ignorée et la valeur transmise est utilisée à la place.

> **Exemple**: cf. [`exemples/max0.cpp`](exemples/max0.cpp)
>
> ```c++
>int max(int x, int y=0)
> {
>  if (x > y) return x;
>  else return y;
>    }
>    
>    int main()
>    {
>   int a = 100;
>   int b = 200;
> 
>   // appel de max (et affectation)
>      std::cout << "max " << a << " et " << b << " = " << max(a, b) << std::endl;
>      std::cout << "max " << a << " = " << max(a) << std::endl;
>   return 0;
>    }
>    ```
>    
>    ```
> max 100 et 200 = 200
> max 100 = 100
>```



Attention au mélange de surcharge et spécification de valeur par défaut.
Cela peut aboutir à des ambiguités insolubes pour le compilateur.

> **Exemple**: cf. [`exemples/plus.cpp`](exemples/plus.cpp)
>
> ```c++
>int plus(int a) // = valeur absolue
> {
>   if (a < 0) return -a;
>   else return a;
>    }
>    
>    int plus(int a, int b = 0) 
>    {
> 	return (a + b);
> }
> 
> int main()
>    {
>   std::cout << plus(1);
>   return 0;
> }
> ```
>    
>    ne compile pas:
> 
> ```
>plus.cpp:19:18: error: call to 'plus' is ambiguous
> 
>plus.cpp:4:5: note: candidate function int plus(int a)
> plus.cpp:12:5: note: candidate function int plus(int a, int b = 0)
> ```



## Récursivité

Une fonction est dite **récursive** lorsqu'elle comporte un appel à elle-même dans le corps de sa définition.

Ce type de fonctions, dont la définition est autorisée en `C` et `C++`, est utile par exemple pour des calculs définis suivant un principe de récurrence.



> **Exemple**: la fonction factorielle:
> $$n! = 1 \times 2 \times ... \times (n-1) \times n$$
>
> st définie par récurrence comme suit:
>
> - $0! = 1$
> - si $n > 0$, $n! = n \times (n-1)!$
>
> Sa définition en `C++` est la suivante.
>
> ```c++
> long factorial(long x)
> {
>   if (x > 0)
>      return (x * factorial(x-1));
>   else
>     return 1;
> }
> ```

