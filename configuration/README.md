# Environnement pour le cours de Programmation Orientée Objet



## Compilateurs

il existe plusieurs compilateurs `C++` en particulier

- `g++` : compilateur C++ de la GNU Compiler Collection (GCC) 
  https://gcc.gnu.org

- `clang++` : autre compilateur C++ du projet LLVM
http://clang.org  



Compilateurs `C` (pas pour ce cours!) :

- GNU `gcc`, de la GCC
- `clang` de LLVM



#### macOS

dans un terminal

- vérifier que le compilateur `clang++` est installé  
  `clang++ --version`

- sinon l'installer   
  `xcode-select --install`  

- vérifier l'installation

  `g++ --version`

  

#### Linux (Ubuntu)

dans un terminal

- vérifier que `g++` est installé  
`g++ -v`


- sinon l'installer   
`sudo apt-get update`    
`sudo apt-get install g++`    

- installer les GNU compiler tools

  `sudo apt-get install build-essential` 

- installer le debugger GDB 

  `sudo apt-get install gdb` 

- vérifier

  `g++ --version`



## Terminal

la commande de compilation suivante peut servir de test d'installation du compilateur.



- télécharger le fichier [`bonjour.cpp`](./bonjour.cpp) du cours `01-intro` (dossier exemples)
  dans un dossier 
- aller dans ce dossier (`cd dossier`)
- compiler avec la commande : 
  - [macos] `g++ ./bonjour.cpp -o bonjour` ou  `clang++ ./bonjour.cpp -o bonjour` 
  - [Linux]  `g++ ./helloworld.cpp -o hello`

- sauf erreur, un fichier executable appelé  `bonjour` a été généré (dans le dossier)
  on peut le vérfier avec `ls`
- executer avec `./bonjour` 
  ce qui devrait afficher `Bonjour!` dans le terminal



## OnlineGDB

IDE en ligne (pas de signup)

https://www.onlinegdb.com

- éditeur
- débuggueur
- compilation dans différents langages 
- execution sur console
- possibilité de sauvegarder le programme localement



## VS Code

éditeur de code source Visual Studio Code

- https://code.visualstudio.com
- voir aussi https://learn.microsoft.com/fr-fr/training/modules/introduction-to-visual-studio-code/

**macOS**

https://code.visualstudio.com/docs/setup/mac

- installation:
  [download](https://go.microsoft.com/fwlink/?LinkID=534106), copier app
- installer commande terminal :
  démarrer VSCode, ouvrir Palette `Cmd+Shift+P`, taper `shell command`

**Linux**

https://code.visualstudio.com/docs/setup/linux

pour Ubuntu desktop :

- menu *applications*, appli *Ubuntu Software*
- chercher "code"
- installer

**version en ligne**
https://vscode.dev



**extension C++**

- installer l'extension C++ :
  https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools

- configuration de l'extension:

  - Linux gcc
    https://code.visualstudio.com/docs/cpp/config-linux  
  - mac / clang  
    https://code.visualstudio.com/docs/cpp/config-clang-mac  

- compilation:  `Run` en haut à droite de la fenêtre d'édition  
  la 1ere fois choisir

  - pour linux `g++ build and debug active file`
  - pour macOS  `C/C++: clang++ build and debug active file`

- IntelliSense
  - info par mouse over
  -  auto complétion par `Tab`

- debugger :
  placer des *breakpoints* en cliquant a coté des numéro de ligne

   `Debug` en haut à droite de la fenêtre d'édition
  la 1ere fois choisir

  - pour linux `g++ build and debug active file`

  - pour macOS  `C/C++: clang++ build and debug active file`



## Geany

éditeur et IDE 

#### Mac OS

http://www.geany.org 
Download, Releases

#### Linux (Ubuntu)

```
sudo apt-get install geany
```

#### Configuration

- charger un fichier `.cpp`
- menu `Construire` (`Build`)
  `Définir les commandes de construction` (`Set Build Commands`)
- rubrique `Commandes pour C++`, 
  `Command`, `Compile` et 
  `Command`, `Build`
  ajouter à la fin de la commande `-std=c++17`.



## Eclipse

https://www.eclipse.org/ide/

IDE pour Java et aussi C++



## Code::Blocks

 IDE open source populaire  (écrite en C++)

attention, la dernière version n'est pas disponible sous macOS.



## QTCreator

https://www.qt.io/product/development-tools

particulièrement adapté au développement d'interfaces graphiques 

recommandé dans les TD d'É. Lecolinet
https://perso.telecom-paristech.fr/elc/cpp/TP-C++.html



