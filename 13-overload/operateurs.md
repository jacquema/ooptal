## Les opérateurs pouvant être surchargés en C++

source:
"*Exercices en langage C++*", Claude Delannoy, Eyrolles
tableau d'opérateurs en page 157.

opérateurs classés par priorité décroissante

| Arité   | Opérateurs                                                                   | Associativité  |
| ------- | ---------------------------------------------------------------------------- | -------------- |
| binaire | `()` `[]` `->`                                                               | g              |
| unaire  | `+` `-` `++` `--` `!` `~` `*` `&` `new` `new[]` `delete` `delete[]` (`cast`) | g              |
| binaire | `*` `/` `%`                                                                  | g              |
| binaire | `+` `-`                                                                      | g              |
| binaire | `<<` `>>`                                                                    | g              |
| binaire | `<` `<=` `>` `>=`                                                            | g              |
| binaire | `==` `!=`                                                                    | g              |
| binaire | `&`                                                                          | g              |
| binaire | `^`                                                                          | g              |
| binaire | `||`                                                                         | g              |
| binaire | `&&`                                                                         | g              |
| binaire | `|`                                                                          | g              |
| binaire | `=` `+=` `-=` `*=` `/=` `%=` `&=` `^=` `|=` `<<=` `>>=`                      | d              |
| binaire | `,`                                                                          | g              |

- `&` et `=` ont une définition par défaut pour les objets de n'importe quelle classe (quand ils ne sont pas surchargés).
  
- `()` `[]` `->` et `=` doivent être définis en interne, comme méthodes 

- pour `++` `--` 
  
  - notation *pre* en arité unaire,
  - notation *post* en arité binaire. 
