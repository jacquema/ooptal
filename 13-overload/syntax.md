# The General Syntax of operator overloading in C++

https://stackoverflow.com/questions/4421706/what-are-the-basic-rules-and-idioms-for-operator-overloading

You cannot change the meaning of operators for built-in types in C++, operators can only be overloaded for user-defined types1. That is, at least one of the operands has to be of a user-defined type. As with other overloaded functions, operators can be overloaded for a certain set of parameters only once.

Not all operators can be overloaded in C++. Among the operators that cannot be overloaded are: `.` `::` `sizeof` `typeid` `.*` and the only ternary operator in C++, `?:`

Among the operators that can be overloaded in C++ are these:

| Category                     | Operators                                            | Arity and Placement      |
| ---------------------------- | ---------------------------------------------------- | ------------------------ |
| **Arithmetic**               | `+` `-` `*` `/` `%` and `+=` `-=` `*=` `/=` `%=`     | binary infix             |
|                              | `+` `-`                                              | unary prefix             |
|                              | `++` `--`                                            | unary prefix and postfix |
| **Bitwise**                  | `&` `|` `^` `<<` `>>` and `&=` `|=` `^=` `<<=` `>>=` | binary infix             |
|                              | `~`                                                  | unary prefix             |
| **Comparison**               | `==` `!=` `<` `>` `<=` `>=` `<=>`                    | binary infix             |
| **Logical**                  | `||` `&&`                                            | binary infix             |
|                              | `!`                                                  | unary prefix             |
| **Allocation functions**     | `new` `new[]` `delete` `delete[]`                    | unary prefix             |
| **User-defined conversions** | `T`                                                  | unary                    |
| **Assignment**               | `=`                                                  | binary infix             |
| **Member access**            | `->` `->*`                                           | binary infix             |
| **Indirection/Address-of**   | `*` `&`                                              | unary prefix             |
| **Function call**            | `()`                                                 | N-ary postfix            |
| **Subscript**                | `[]`                                                 | N-ary2 postfix           |
| **Coroutine await**          | `co_await`                                           | unary prefix             |
| **Comma**                    | `,`                                                  | binary infix             |

However, the fact that you *can* overload all of these does not mean you *should* do so. See [the next answer](https://stackoverflow.com/questions/4421706/operator-overloading-in-c/4421708#4421708).

In C++, operators are overloaded in the form of ***functions with special names***. As with other functions, overloaded operators can generally be implemented either as a ***member function of their left operand's type*** or as ***non-member functions***. Whether you are free to choose or bound to use either one depends on several criteria.3 A unary operator `@`4, applied to an object x, is invoked either as `operator@(x)` or as `x.operator@()`. A binary infix operator `@`, applied to the objects `x` and `y`, is called either as `operator@(x,y)` or as `x.operator@(y)`.5

Operators that are implemented as non-member functions are sometimes friend of their operand’s type.

1 The term “user-defined” might be slightly misleading. C++ makes the distinction between built-in types and user-defined types. To the former belong for example int, char, and double; to the latter belong all struct, class, union, and enum types, including those from the standard library, even though they are not, as such, defined by users.

2 The subscript operator used to be binary, not N-ary until C++23.

3 This is covered in [a later part](https://stackoverflow.com/questions/4421706/operator-overloading/4421729#4421729) of this FAQ.

4 The `@` is not a valid operator in C++ which is why I use it as a placeholder.

5 The only ternary operator in C++ cannot be overloaded and the only n-ary operator must always be implemented as a member function.