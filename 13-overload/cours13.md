# Cours de programmation objet

INALCO  
Master TAL première année


## Cours 13 - Opérateurs et surcharge

mardi 04 mars 2025  
Florent Jacquemard  `florent.jacquemard@inria.fr`



sources:

- "*Exercices en langage C++*", Claude Delannoy, Eyrolles
  tableau d'opérateurs en page 157 (voir aussi `operateurs.md`)
- "*Introduction à la programmation orientée objet en C++*", 
  Jean-Cédric Chappelier, Jamila Sam et Vincent Lepetit, BOOCs EPFL
  chapitre 3.
- *[Stack Overflow's C++ FAQ](https://stackoverflow.com/questions/tagged/c++-faq)*



## Introduction

Écrire une classe permet de définir un nouveau type de donnée (dit type *utilisateur*).

Dans cette optique, il peut être pratique d'étendre des opérateurs classiques (arithmétique, de comparaison *etc*) au type de donnée défini. Par exemple, nous avons vu que les opérateurs `==`, `<` et `+` s'appliquent à la classe `string`, qui est définie dans la librairie standard (cf. [string::compare](https://cplusplus.com/reference/string/string/compare) pour la signification des deux premiers).

Cela est possible dans certains langages objet grace à un mécanisme de **surcharge** des opérateurs, implémenté grâce à des noms réservés de méthodes (qui différent suivant les langages). Nous verrons ci-dessous les détails de la surcharge d'opérateurs en C++, avec les pratiques courantes. Par ailleurs, nous verrons en TP des exemples de surcharges d'opérateurs Python.



La surcharge d'opérateurs permet de redéfinir l'application d'opérateurs classiques (arithmétique, logique *etc*) pour des instances de classes particulières. 

> **Exemple**: 
> Considérons la définition d'une classe pour la représentation de nombres.
>
> ```cpp
> class Nombre
> {
>    	// ...
> }
> ```
> On peut définir dans `Nombre` une méthode binaire `add` pour l'addition, 
> et écrire ainsi:
>
> ```cpp
> Nombre x1, x2, x3;
> x3 = add(x1, x2);
> x3 = add(add(x1, x2), x3);
> ```
> Il est possible aussi de définir, par *surcharge*, l'application de l'opérateur `+` sur des objets instances de la classe `Nombre`.  Cela permet ensuite d'écrire dans une syntaxe plus naturelle:
> ```cpp
> x3 = x1 + x2;
> x3 = x1 + x2 + x3;
> ```



> **Exemple**:  concaténation de chaines
> l'opérateur de concaténation `+` de la classe `std::string` est défini par surcharge.



> **Exemple**: classe `Nombre` et `<<`
> De même, pour afficher un `Nombre`, on peut définir une méthode `show()` dans `Nombre` et écrire par exemple:
>
> ```cpp
> Nombre x1;
> std::cout << "x1 = ";
> x1.show();
> std::cout << std::endl;
> ```
> Mais on préférerait pouvoir utilise la syntaxe usuelle "one-liner":
> ```cpp
> std::cout << "x1 = " << x1 << std::endl;
> ```
> Cela est possible en définissant (par *surcharge*) l'opérateur `<<` dans  la classe `Nombre`.

La *surcharge d'opérateurs* permet donc d'étendre les opérateurs  usuels aux classes que l'on défini.



## Opérateurs surchargeables
La surcharge peut concerner:
- les opérateurs arithmétiques de base `+`, `*` _etc_
- la version d'auto-affectation de ces opérateurs  `+=`, `*=` _etc_
- l'incrémentation `++`
- l'opérateur d'affichage `<<`
- les opérateurs booléens de comparaison `==`, `!=`, `<` _etc_
- les connecteurs logique `&&`, `||`
- et même l'opérateur d'affectation `=`.

cf. [`operators.md`](operators.md) pour la liste de l'ensemble des opérateurs qui peuvent être surchargés.

Seuls ne sont pas surchargeables: 

- `::`   opérateur de résolution de portée 
-  `.`  opérateur d'accès aux attributs et méthodes



## Surcharge de fonctions

Comme on l'a déjà vu, il est possible de définir deux fonctions ou méthodes de même nom et même type de retour mais avec des paramètres de types différents. 
On parle alors de surcharge de fonctions / méthodes.

> **Exemple**: fonctions surchargées.
>```cpp
> int max(int, int);
> double max(double, double);
>```

La surcharge d'opérateurs rentre dans ce cadre, un opérateur étant considéré comme une fonction.



## Appel d'opérateurs
Les opérateurs sont des fonctions agissant sur un ou plusieurs opérandes.  
L'appel à un opérateur `Op` est donc en réalité  un appel à une fonction ou méthode spécifique d'identifiant `operatorOp`.

| opérateur        | fonction                   | méthode                   |
| ---------------- | -------------------------- | ------------------------- |
| `a + b`          | `operator+(a, b)`          | `a.operator+(b)`          |
| `a <= b`         | `operator<=(a, b)`         | `a.operator<=(b)`         |
| `std::cout << a` | `operator<<(std::cout, a)` | `std::cout.operator<<(a)` |
| `-a`             | `operator-(a)`             | `a.operator-()`           |
| `++a`            | `operator++(a)`            | `a.operator++()`          |
| `a = b`          |                            | `a.operator=(b)`          |

Suivant le type d'appel ci-dessus (fonction ou méthode), l'opérateur sera défini à l'intérieur ou à l'extérieur de la classe.



## Surcharge externe d'opérateurs
Pour surcharger un opérateur défini comme une fonction, on peut écrire cette fonction à l'extérieur de la classe à laquelle cet opérateur s'applique. On parle alors de **surcharge externe**.

> **Exemple**: classe de paires de nombres, avec addition.
> cf.  [`exemples/pair-externe.cpp`](exemples/pair-externe.cpp)
>
> ```cpp
> class Pair
> {
> public:
>     Pair(double a, double b);
>     double get_c1() const;
>     double get_c2() const;
> 
> private:
>     double c1;
>     double c2;
> };
> 
> Pair operator+(Pair, Pair);
> ```
> L'addition par `+` est ici définie en dehors de la classe `Pair`.
> L'assignation:
>
> ```cpp
> x3 = x1 + x2;
> ```
> est alors équivalente à:
> ```
> x3 = operator+(x1, x2);
> ```
> La fonction `operator+` retourne une valeur pour `x3`, suivant le prototype défini plus haut.



L'inconvénient est que cette fonction externe n'a pas accès aux attributs privés de la classe, et que l'on doit avoir recours à des accesseurs.




> **Exemple**: classe de paires de nombres, méthodes.
>
> pour la classe `Pair`, la fonction d'addition externe est définie avec des accepteurs. comme suit:
>
> ```cpp
> Pair operator+(Pair p1, Pair p2)
> {
>      return Pair(p1.get_c1() + p2.get_c1(),
>                  p1.get_c2() + p2.get_c2());
> }
> ```
>
> où `get_c1` et `get_c2` sont les accesseurs suivants:
>
> ```cpp
> double Pair::get_c1() const
> {
>    	return c1;
> }
> 
> double Pair::get_c2() const
> {
>    	return c2;
> }
> ```
> Le constructeur est quand à lui:
>
> ```c++
> Pair::Pair(double a, double b):
> c1(a),
> c2(b)
> { }
> ```
>
> La fonction `operator+` doit passer par les accesseurs `get_c1` et `get_c2`, car les attributs `c1` et `c2` sont privés, et `operator+` est définie en dehors de la classe `Pair`.
> Un autre prototype possible (et meilleur) est:
>
> ```cpp
> Pair operator+(const Pair& p1, const Pair& p2);
> ```
> avec un passage par références constantes.



## Surcharge interne d'opérateurs

Une surcharge d'opérateur peut aussi se faire par définition d'une méthode, écrite à l'intérieur de la classe à laquelle cet opérateur s'applique. On parle alors de **surcharge interne**.



> **Exemple**:  classe `Nombre`
> définissons l'opérateur `+` comme méthode de la  classe `Nombre`, par surcharge interne.
>
> ```cpp
> class Nombre
> {
> public:
>   	Nombre operator+(Nombre) const;    
> };
> ```
> On notera que cette méthode `operator+` n'a qu'un paramètre  (contrairement à une fonction `+` définie par surcharge externe, qui est binaire).
> En effet, cette méthode `+` est appelée à travers un objet, et
>
> - l'objet auquel s'applique la méthode sera le premier opérande de `+`
> - l'argument de la méthode sera le second opérande de `+`.  
>
> En d'autre termes:
> ```cpp
> z3 = z1 + z2;
> ```
> correspond à:
> ```
> z3 = z1.operator+(z2);
> ```



Pour surcharger un opérateur `OP` à l'intérieur une classe `ClassName`, il faut ajouter une méthode `operatorOP` dans la classe concernée.

```cpp
class ClassName
{
    // entête (prototype) de l'opérateur OP
    type_retour operatorOP(param_type) [const];
};

// définition de l'opérateur OP
type_retour ClassName::operatorOP(param_type) [const]
{
    // ...    
}
```

La définition de `operatorOp` se fait de la même manière que pour les autres méthodes.
En particulier, on peut utiliser les attributs de la classe dans la définition de `operatorOp` (sans avoir recours à des getters, comme avec la surcharge externe).

Le premier opérande de `Op` est l'instance courante, sur laquelle est appelée la méthode. Il ne doit pas être passé en paramètre.
Le seul éventuel paramètre de `operatorOp` est le second opérande, dans le cas où `Op` est binaire.

Si `OP` est unaire, le prototype de la méthode est de la forme:

```cpp
type operatorOP() [const];
```



> **Exemple**: opérateur `+=` à l'intérieur de la classe de paires de nombres.
> cf.  [`exemples/pair-interne.cpp`](exemples/pair-interne.cpp)
> `z += y` modifie l'objet `x` en lui ajoutant la valeur de  `y`.
> Il est donc cohérent de le définir par surcharge interne.
>
> ```cpp
> class Pair
> {
> public:
>   	void operator+=(const Pair& rhs); // n'est pas const
> private:
>   	double c1;
>   	double c2;
> };
> 
> void Pair::operator+=(const Pair& rhs)
> {
>    	 c1 += rhs.c1;
>    	 c2 += rhs.c2;
> }
> ```



## Surcharge externe nécessaire (1)

La surcharge externe d'un opérateur binaire comme `operator+` ou `operator*` est nécessaire  lorsque le premier paramètre (opérande de gauche) n'est pas du type de la classe.

> **Exemple**: produit d'une paire de nombre par un `double`.
> cf. [`exemples/pair-externe.cpp`](exemples/pair-externe.cpp)
>
> ```cpp
> double x;
> Pair p1, p2;
> p2 = x * p1;
> ```
> l'initialisation de `p2` se fait par 
> ```cpp
> p2 = operator*(x, p1);
> p2 = x * p1;
> ```
> Elle ne peut pas se faire par 
> ```cpp
> p2 = x.operator*(p1);
> ```
> parce que `x` n'est pas un objet (instance de classe) mais un double (type de base).
> La définition externe de la fonction utilisée pourra avoir la forme suivante:
>
> ```cpp
> Pair operator*(double a, const Pair& p)
> {
> 	return Pair(a * p.get_c1(), a * p.get_c2());
> }
> ```
>
> On utilise ici les accesseurs `get_c1` et `get_c2`, la définition étant externe.



## Surcharge externe nécessaire (2)

La surcharge externe peut aussi être nécessaire dans le cas d'entrées/sorties dans des flots (streams) de la librairie standard (STL).

> **Exemple**: écriture sur `cout`.
> ```cpp
> Pair p1;
> std::cout << p1;
> ```
> La deuxième ligne correspond à un appel de méthode (interne)
> ```cpp
> std::cout.operator<<(p1);
> ```
> ou bien un appel de fonction (externe)
> ```cpp
> operator<<(std::cout, p1);
> ```
> La méthode correspondant au cas de méthode interne doit être définie dans la classe `ostream` de `cout` de la librairie standard, mais on ne veut (ou peut) pas la modifier.
>
> Elle ne peut pas être définie dans la classe `Pair` car le premier paramètre de `operateur<<` (opérande de gauche) est un `cout`.



Dans l'exemple précédent, on utilisera donc la définition externe.



> **Exemple**: écriture sur `cout`, définition externe.
>
> cf. [`exemples/pair-externe.cpp`](exemples/pair-externe.cpp)
>
> ```cpp
> std::ostream& operator<<(std::ostream& o, const Pair& p);
> ```
> le premier paramètre `cout`, de type `std::ostream` est passé par référence *non-constante* parce qu'il est modifié par l'affichage de `p` (qui est écrit dedans).
> Le type de retour est `std::ostream&` de manière à pouvoir enchainer les `<<` (voir plus loin).
> La définition externe de la fonction utilisée peut être:
>
> ```cpp
> std::ostream& operator<<(std::ostream& o, const Pair& p)
> {
>    		o << '(' << p.get_c1() << ", " <<  p.get_c2() << ')';
>    		return o;
> }
> ```



> **Exemple: ** méthodes internes pour l'affichage dans `cout`.
>
> cf.  [`exemples/pair-interne.cpp`](exemples/pair-interne.cpp)
>
> Si l'on a définit dans la classe `Pair` une méthode pour la conversion d'une paire en une chaine de caractère.
>
> ```cpp
> class Pair
> {
> public: 
>     std::string tostring() const; // serialisation
> };
> ```
> la fonction `operator<<` pourra simplement appeler cette méthode:
> ```cpp
> std::ostream& operator<<(std::ostream& o, const Pair& p)
> {
>   	o << p.to_string();
>   	return o;
> }
> ```
> De même, si l'on a définit dans la classe `Pair` une méthode d'affichage qui prend un stream en paramètre:
> ```cpp
> class Pair
> {
> public: 
>     std::ostream& show(std::ostream& o) const;
> };
> ```
> la fonction `operator<<` pourra aussi appeler cette méthode:
> ```cpp
> std::ostream& operator<<(std::ostream& o, const Pair& p)
> {
> 		return p.show(o);
> }
> ```
> Avec de telle méthodes, on n'a plus besoin des accesseurs `get_c1()` et `get_c2()` dans la définition de la fonction `operator<<`



## Surcharge d'un opérateur à l'aide d'un autre, surchargé en interne

Pour contourner le problème des droits d'accès dans la surcharge externe, on peut passer par un autre opérateur défini en interne.



> **Exemple**: classe `Pair`.  cf.  [`exemples/pair-interne.cpp`](exemples/pair-interne.cpp)
>
> On peut définir un opérateur `+` en externe, en utilisant `+=`.
>
> ```cpp
>const Pair operator+(const Pair& lhs, const Pair& rhs)
> {
>    Pair tmp(lhs);
>    tmp += rhs; // réutilisation de += défini en interne dans la classe Pair
>       return tmp;
>    }
>    ```
> Cette définition de `+` nous épargne l'utilisation d'accesseurs `get_c1()`, `get_c2()`.
> ```cpp
> Pair p1(1, 2);
> Pair p2(5, 6);
> Pair p3 = p1 + p2;
> ```
> à la fin de l'éxécution:
> - `p3` vaut `(6, 8)`
> - `p1` vaut `(1, 2)` (inchangé)
> - `p2` vaut `(5, 6)` (inchangé)

L'opérateur `+=` est plus léger que `+`, car il ne crée pas de nouvel objet, c'est pourquoi nous l'avons défini en premier.

Utiliser  `+=`  dans la définition de `+` permet d'assurer aussi une certaine cohérence: l'addition elle même n'est définie qu'une fois, dans `+=` et la réutilisation de `+=` assure que tous les opérateur en `+` auront le même comportement.



> **Exemple**: définition de `+` à partir de `+=` avec passage par valeur.
> On obtient le même résultat que dans l'exemple précédent avec:
>
> ```cpp
> const Pair operator+(Pair lhs, const Pair& rhs)
> {
>      lhs += rhs; // réutilisation de +=
>      return lhs;
> }
> ```
> En particulier, dans `p3 = p1 + p2`, la variable `p1` n'est pas modifiée, car elle est copiée dans `lhs`.
> En effet, le premier argument étant passé par valeur, `lhs` est une variable locale qui joue le même rôle que la variable `tmp` de l'exemple précédent.
> Elle est initialisée avec une copie de la valeur de l'instance de `Pair` passée en paramètre.
> Il serait donc inutile d'utiliser une variable (locale) `tmp` à l'intérieur du corps de la méthode.



Certains compilateurs pourront faire des optimisations dans le cas d'un tel passage par valeur.



## Déclaration d'amitié

Pour permettre à une fonction externe d'accéder aux attributs et méthodes déclarés `private` d'une classe, on peut la déclarer comme amie à l'intérieur de la classe:
```cpp
friend prototype
```

On n'écrit dans la classe que le prototype de la fonction dans la classe, la définition de la fonction doit toujours être faite en dehors de la classe.



> **Exemple**: friendship de la classe `Pair`.
> cf.  [`exemples/pair-friend.cpp`](exemples/pair-friend.cpp)
>
> ```cpp
> class Pair
> {
> 	friend Pair operator*(double, const Pair&);
> 	friend std::ostream& operator<<(std::ostream&, const Pair&);
>   
>   private:
>    double c1;
>    double c2;
> };
> ```
> Après avec fait ces déclarations, on pourra compiler:
> ```cpp
> Pair operator*(double a, const Pair& p)
> {
> 	return Pair(a * p.c1, a * p.c2);
> }
> ```
> et
> ```cpp
> std::ostream& operator<<(std::ostream& o, const Pair& p)
> {
>  o << '(' << p.c1 << ", " <<  p.c2 << ')';
>  return o;
> }
> ```



### Méthodologie

- on utilisera la surcharge interne pour les opérateurs "proches de la classe", 
  nécessitant des accès internes.

- on préférera la surcharge externe chaque fois
  
  - qu'elle est nécessaire... (voir plus haut).
  - qu'elle peut être écrite uniquement à l'aide de l'interface de la classe (sans `friend`).
  - sans copies inutiles.



## Surcharge de quelques opérateurs usuels

Nous voyons plus en détails, au cas par cas, la forme que prend la surcharge d'opérateurs usuels.

voir aussi:
https://stackoverflow.com/questions/4421706/what-are-the-basic-rules-and-idioms-for-operator-overloading

#### surcharge interne

**comparaison**

```cpp
bool operator==(const ClassName&) const; 
```

exemple d'appel: `p == q`

dans ce cas, on surchargera aussi `!=`

```cpp
bool operator!=(const ClassName&) const; 
```

en utilisant `==` dans la définition: 
`operator!=` est définit comme `!operator==`.

```cpp
bool operator<(const ClassName&) const;  
```

exemple d'appel: `p < q`

Le dans ce cas, on surchargera aussi 

- `<=` comme `!>`
- `>` comme symétrique de `<`
- `>=` comme `!<`



> **Exercice.**  Écrire des opérateurs `==`, `!=`, `<`, `≤`, `>`, `≥` pour la classe `Paire` des exemples précédents. 



**auto-affectation**

```cpp
ClassName& operator+=(const ClassName&);  
```

exemple d'appel: `p += q`

Le type de retour est `ClassName&`, une référence sur une instance de `ClassName`.
Le but est que la nouvelle valeur de `p` après l'affectation `p += q` soit retournée, ce qui est le comportement standard sur les types de base. La valeur de retour pourra être utilisée dans un appel comme:

```c++
x = (p += q);
```

Pour éviter d'avoir à créer un nouvelle instance de `ClassName`, par copie, on retournera la (nouvelle) valeur de `p`. 



> **Exemple**: auto-affectation de `Pair`.
>
> ```c++
> Pair& Pair::operator+=(const Pair& rhs)
> {
>     c1 += rhs.c1;
>     c2 += rhs.c2;
>     return *this; // l'instance courante
> }
> ```
>
> Pour comparaison, la version suivante fait des copies inutiles:
>
> ```c++
> Pair Pair::operator+=(Pair rhs)
> {
>     c1 += rhs.c1;
>     c2 += rhs.c2;
>     Pair tmp = *this; // copie
>     return tmp;       
> }
> ```
>
> - copie au passage par valeur de l'argument `rhs`,
> - copie au retour,
> - copie pour l'initialisation du `tmp`.



Pour les autres auto-affectations:

```cpp
ClassName& operator-=(const ClassName&);  
```

exemple d'appel: `p -= q`



multiplication par un autre type:

```cpp
ClassName& operator*=(autre_type const); 
```

exemple d'appel: `p *= x`


```cpp
ClassName& operator++();               
```

exemple d'appel: `++p`



```cpp
ClassName& operator++(int optionel);    
```

exemple d'appel: `p++`



```cpp
const ClassName operator-() const;
```

exemple d'appel: `r = -p` (opposé, unaire)



#### surcharge externe

**arithmétique**

```cpp
const ClassName operator+(ClassName, const ClassName&); 
```

exemple d'appel: `r = p + q`

Le type de retour est `const` pour éviter des expressions comme:

```cpp
++(p + q);
p + q = x;
```

```cpp
const ClassName operator-(ClassName, ClassName const&); 
```

exemple d'appel: `r = p - q`



**multiplication à gauche par un autre type**

```cpp
const ClassName operator*(autre_type, const ClassName&);  
```

exemple d'appel: `q = x * p` 



**affichage**

définition externe:

```cpp
ostream& operator<<(ostream&, ClassName& const);
```

exemple d'appel: `std::cout << p` 

Le type de retour est `ostream&`, une référence sur un flot (`stream`) de sortie,
pour pouvoir enchainer des affichages comme dans:

```cpp
std::cout << p << std::endl;
```

qui correspond à 

```cpp
operator<<(operator<<(std::cout, p), std::endl);
```

c'est à dire que la valeur de retour de `operator<<(std::cout, p)` est passé comme premier argument au `operator<<` le plus extérieur.

Noter que la référence `ostream&` n'est pas `const`, car on veut écrire dans le flot de sortie retourné.



## Opérateur d'affectation

C'est l'opérateur utilisé par exemple dans `p = q`.

- il est très proche du constructeur de copie.
  La différence principale est que le constructeur de copie crée un nouvel objet, alors que l'affectation traite un objet existant.

- il existe par défaut pour toute classe, même si il n'est pas surchargé.
  On parle d'*opérateur universel*.

- la version par défaut, fournie par le compilateur en l'absence de surcharge interne de `operator=`, fait une simple copie de surface (comme pour le constructeur de copie par défaut).
  
- il peut être surchargé en interne par:
  
  ```cpp
  ClassName& operator=(const ClassName& rhs);
  ```

- il ne peut **pas** être surchargé en externe.

- la version par défaut peut être supprimé si l'on souhaite interdire l'usage de l'opérateur `=`, par la déclaration suivante (dans la classe `ClassName`):  
  
  ```cpp
  ClassName& operator=(const ClassName&) = delete;
  ```
  
  Le mot clé `delete` est le même que pour effacer le constructeur de copie.

Cela peut être utile par exemple si les instances de `ClassName` sont très grandes en mémoire et que l'on veut éviter de les copier.

Le type de retour est `ClassName&`, une référence sur une instance de `ClassName`, pour la même raison que pour les opérateurs d'auto affectation `+=` *etc*.
C'est à dire que lors d'une affectation `p = q`, la nouvelle valeur de `p` est retournée, ce qui permet d'enchainer des affectations comme dans:

```cpp
s = (p = q);
```

Pour éviter de créer une nouvelle instance de `ClassName`, par copie, on retourne une référence sur la (nouvelle) valeur de `p`. 

### fonction `swap`

Cette fonction (`C++11`) échange la valeur de deux objets en mémoire.
Elle peut être utilisée pour la surcharge (interne) de l'opérateur `=`:

```cpp
ClassName& operator=(ClassName rhs)
{
    swap(*this, rhs);
    return *this;
}
```

On notera que dans ce cas, l'objet à copier est passé par valeur, pour des raisons d'optimisation du compilateur.
