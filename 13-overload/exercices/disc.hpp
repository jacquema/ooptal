#ifndef disc_hpp
#define disc_hpp 

#include <iostream>

#include "point.hpp"

class Disc
{
public:
    Disc(double x = 0, double y = 0, double r = 0);
    Disc(const Point& p, double r);

	std::ostream& show(std::ostream& o) const; 	// exercice 2

private:
    Point center; 
    double radius;
};

// exercice 2
std::ostream& operator<<(std::ostream& o, const Disc& d);

#endif /* disc_hpp */
