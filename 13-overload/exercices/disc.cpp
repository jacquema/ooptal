#include "disc.hpp"

Disc::Disc(const Point& p, double r): 
center(p), // constructeur de copie
radius(r)
{
    if (r < 0)
    {
        std::cerr << "le rayon doit être positif" << std::endl;
        radius = 0;
    }
}

Disc::Disc(double x, double y, double r): 
Disc(Point(x, y), r)
{ }

// exercice 2
std::ostream& Disc::show(std::ostream& o) const
{
	o << "centre=" << center << " rayon=" << radius;
	return o;
}

// exercice 2
std::ostream& operator<<(std::ostream& o, const Disc& d)
{
	return d.show(o);
}
