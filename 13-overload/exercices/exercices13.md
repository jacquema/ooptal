# Exercices - cours 13

## surcharge d'opérateurs



> **Exercice 1** (affichage).
> Réécrire la classe `Point` de l'exercice 3 du cours 11 avec une méthode d'affichage des coordonnées du point
>
> ```cpp
> std::ostream& show(std::ostream& o) const;
> ```
> Définir une fonction externe `operator<<` qui appelle la méthode `show`.



> **Exercice 2** (affichage).
> Même exercice que le précédent pour la classe `Disc` de l'exercice 7 du cours 11 (le centre de `Disc` est un `Point`). La méthode `show` affichera le centre (coordonnées) et le rayon.



> **Exercice 3** (égalité).  
> Définir des opérateurs d'égalité `operator==` et diségalité `operator!=` pour la classe `Point`
>
> - comme méthode, par surcharge interne,
> -  **DEVOIR MAISON**: comme fonction, par surcharge externe. 
>   - avec des méthodes accesseurs. 
>   - avec une déclaration `friend`.



> **Exercice 4** (comparaison).  
> Définir un opérateur d'ordre `<`  pour la classe `Point`, par surcharge interne.
>
> Définir aussi `<=`, `>` et `≥` en utilisant `<`.



> **Exercice 5** (accès indexé).
> Définir par surcharge interne l'opérateur `operator[]` de la classe `Point` donnant accès à la coordonnée `0` (`x`) ou `1` (`y`).
> 
> C'est à dire que pour un `Point p`, `p[0]` est le `x` de `p`, et  `p[1]` est le `y` de `p`.



>  **Exercice 6.** opérateurs sur dates.
>
>  1. Définir une classe `Date` avec trois attributs entiers: année, mois, jour.
>  2. Écrire un constructeur pour cette classe avec trois arguments jours, mois, année, la valeur par défaut du dernier étant 2025. On vérifiera la validité du numéro de mois.
>  3. Écrire un constructeur par copie pour `Date`.
>  4. Écrire une méthode `show(std::ostream&)` qui affichera la date au format `JJ/MM/AAAA` dans le flux de sortie en argument.
>  5. Définir un opérateur externe `<<`  affichant une date dans un flux de sortie, au même format que 4. On utilisera pour cet opérateur et les suivants les profils types.
>  6. Définir des opérateurs booléens `==`, interne, et `!=`, externe,  testant égalité et diségalité de dates.
>  7. Définir des opérateurs internes `<`, `<=`, `>` et `>=` de comparaison de dates. 
>     On utilisera l'un pour définir les 3 autres.
>
>  bonus: vérifier la validité du numéro de jour dans le constructeur en utilisant un vecteur static contenant le nombre de jour de chaque mois.

