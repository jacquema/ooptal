// TD 13 exercice 1

#include <iostream>
#include <cmath>

class Point
{ 
public:
    Point(double x=0, double y=0);
    Point(const Point& p);

	bool operator==(const Point&) const;
	bool operator!=(const Point&) const;
	bool operator<(const Point &) const;
	bool operator<=(const Point&) const;
	bool operator>(const Point&) const;
	bool operator>=(const Point&) const;
	std::ostream &show(std::ostream &o) const;

private:
    double x;
    double y;
};

Point::Point(double x, double y): 
x(x),
y(y)
{ }

Point::Point(const Point& rhs): 
x(rhs.x),
y(rhs.y)
{ }

bool Point::operator==(const Point& rhs) const
{
	return x == rhs.x && y == rhs.y; 
}

bool Point::operator!=(const Point &rhs) const 
{
	return ! operator==(rhs); 
}

bool Point::operator<(const Point& rhs) const
{
	if (x < rhs.x)
		return true;
	else if (x > rhs.x)
		return false;
	else // x == rhs.x
		return (y < rhs.y);
}

bool Point::operator>=(const Point &rhs) const
{
	return !operator<(rhs);
}

bool Point::operator>(const Point& rhs) const
{
	return rhs.operator<(*this); // self
}

bool Point::operator<=(const Point &rhs) const
{
	return !operator>(rhs);
}

std::ostream& Point::show(std::ostream& o) const
{
	o << "(" << x << ", " << y << ")";
	return o;
}

std::ostream& operator<<(std::ostream& o, const Point& p)
{
	return p.show(o);
}



class Disc
{
public:
    Disc(double x = 0, double y = 0, double r = 1);
	std::ostream& show(std::ostream& o) const;

private:
    Point center; 
    double radius;
};

Disc::Disc(double x, double y, double r): 
center(x, y), // appel du constructeur de Point
radius(r)
{
	// test d'intégrité
	if (r < 0)
	{
		std::cerr << "le rayon doit être positif" << std::endl;
		radius = 0;
	}
}

std::ostream& Disc::show(std::ostream& o) const
{
	o << "centre=" << center << " rayon=" << radius;
	return o;
}

std::ostream& operator<<(std::ostream& o, const Disc& d)
{
	return d.show(o);
}

int main()
{
	Point p0;
	Point p1(1, 1);
	Point p2(-1, 2);
	// p0.show(std::cout);
	std::cout << "p0: " << p0 << std::endl;
	std::cout << "p1: " << p1 << std::endl;
	std::cout << "p2: " << p2 << std::endl;

	Disc d1(0, 0, 1);
	Disc d2(1, 1, 2);
	std::cout << "d1: " << d1 << std::endl;
	std::cout << "d2: " << d2 << std::endl;
}
