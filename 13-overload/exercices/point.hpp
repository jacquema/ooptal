#ifndef point_hpp
#define point_hpp 

#include <iostream>

class Point
{
private: // données
	double x;
	double y;

public: // méthodes

    Point(double x=0, double y=0);
	std::ostream& show(std::ostream& o) const; // exercice 1
	
	bool operator==(const Point& rhs) const;   // exercice 3 question 1
	bool operator!=(const Point& rhs) const;   // exercice 3 question 1

	bool operator<(const Point& rhs) const;    // exercice 4
	bool operator<=(const Point& rhs) const;   // exercice 4
	bool operator>(const Point& rhs) const;	   // exercice 4
	bool operator>=(const Point& rhs) const;   // exercice 4

	double operator[](size_t i) const;         // exercice 5
};

// exercice 1
std::ostream& operator<<(std::ostream& o, const Point& p);

// exercice 4 question 2
// bool operator==(const Point& lhs, const Point& rhs);

#endif /* point_hpp */
