#include "point.hpp"

Point::Point(double x1, double y1): 
x(x1),
y(y1)
{ }

// exercice 1
std::ostream& Point::show(std::ostream& o) const
{
	o << "(" << x << ", " << y << ")";
	return o;
}

// exercice 1
std::ostream& operator<<(std::ostream& o, const Point& p)
{
	return p.show(o);
}

// exercice 3 question 1
bool Point::operator==(const Point& rhs) const
{
	return (x == rhs.x and y == rhs.y);
}

// exercice 3 question 1
bool Point::operator!=(const Point& rhs) const
{
	return !operator==(rhs);
}

// ordre lexicographique
bool Point::operator<(const Point& rhs) const
{
	if (x == rhs.x)
		return (y < rhs.y);
	else
		return (x < rhs.x);
}

bool Point::operator<=(const Point& rhs) const
{
	return !operator>(rhs);
}

bool Point::operator>(const Point& rhs) const
{
	return (rhs < *this);
}

bool Point::operator>=(const Point& rhs) const
{
	return !operator<(rhs);
}

// exercice 5
double Point::operator[](size_t i) const
{
	if (i == 0)
		return x;
	else if (i == 1)
		return y;
	else
	{
		std::cerr << "[] sur index hors bornes " << i << std::endl;
		return x;
	}		
}

