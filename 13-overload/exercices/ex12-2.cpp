#include "disc.hpp"

int main()
{
    Point p0;
    Point p1(1, 1);
    Disc d1(0, 0, 1);
    Disc d2(p1, 2);

    std::cout << "d1: " << d1 << std::endl;
	std::cout << "d2: " << d2 << std::endl;
}
