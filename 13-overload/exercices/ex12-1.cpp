#include "point.hpp"

int main()
{
    Point p0;
    Point p1(1, 1);
    Point p2(-1, 2);

	std::cout << "p0: " << p0 << std::endl;
	std::cout << "p1: " << p1 << std::endl;
	std::cout << "p2: " << p2 << std::endl;
}
