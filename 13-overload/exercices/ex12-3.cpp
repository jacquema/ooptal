#include "point.hpp" // question 1
// #include "point_ext1.hpp" // question 2.1
// #include "point_friend.hpp" // question 2.2

int main()
{
    Point p0;
    Point p1(1, 1);
    Point p2(-1, 2);

	std::cout << "p0: " << p0 << std::endl;
	std::cout << "p1: " << p1 << std::endl;
	std::cout << "p2: " << p2 << std::endl;

	std::cout << "p0 = (0, 0): " << (p0 == Point(0, 0)) << std::endl;
	std::cout << "p0 = p1: " << (p0 == p1) << std::endl;
	std::cout << "p0 != p1: " << (p0 != p1) << std::endl;
	std::cout << "p2 = p1: " << (p2 == p1) << std::endl;
	std::cout << "p2 = p2: " << (p2 == p2) << std::endl;
	std::cout << "p0 < p1: " << (p0 < p1) << std::endl;
	std::cout << "p0 <= p1: " << (p0 < p1) << std::endl;
	std::cout << "p2 < p1: " << (p2 < p1) << std::endl;
}
