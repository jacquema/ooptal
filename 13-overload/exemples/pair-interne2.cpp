#include<iostream>

class Pair
{
public:
	Pair(double a=0, double b=0);

	void operator+=(const Pair& rhs); // n'est pas const
    // pas besoin d'accesseurs pour operator+=

	bool operator==(const Pair&) const;
	bool operator!=(const Pair&) const;
	bool operator<(const Pair&) const;
	bool operator<=(const Pair&) const;
	bool operator>(const Pair&) const;
	bool operator>=(const Pair&) const;

	std::ostream& show(std::ostream& o) const;

private:
	double c1;
	double c2;
};

Pair::Pair(double a, double b): 
c1(a),
c2(b)
{ }

void Pair::operator+=(const Pair& rhs)
{
	c1 += rhs.c1;
	c2 += rhs.c2;
}

std::ostream& Pair::show(std::ostream& o) const
{
	o << '(' << c1 << ", " << c2 << ')';
	return o;
}

std::ostream& operator<<(std::ostream& o, const Pair& p)
{
	return p.show(o);
}

// lhs est une copie locale
const Pair operator+(Pair lhs, const Pair& rhs)
{
	lhs += rhs; // réutilisation de +=
	return lhs;
}

bool Pair::operator==(const Pair& rhs) const
{
	return (c1 == rhs.c1) && (c2 == rhs.c2);
}

bool Pair::operator!=(const Pair& rhs) const
{
	return !operator==(rhs);
}

bool Pair::operator<(const Pair& rhs) const
{
	return (c1 < rhs.c1) && (c2 < rhs.c2);
}

bool Pair::operator<=(const Pair& rhs) const
{
	return !operator>(rhs);
}

bool Pair::operator>(const Pair& rhs) const
{
	return (rhs < *this);
}

bool Pair::operator>=(const Pair& rhs) const
{
	return !operator<(rhs);
}

int main()
{
	Pair p1(1, 2);
	std::cout << p1 << std::endl;
	p1 += Pair(1, 1);
	std::cout << p1 << std::endl;
	Pair p2(5, 6);
	Pair p3 = p1 + p2;
	std::cout << p3 << std::endl;
}
