#include<iostream>

class Pair
{
	friend Pair operator*(double a, const Pair& p);
	friend std::ostream& operator<<(std::ostream& o, const Pair& p);

public:
	Pair(double a, double b);

private:
	double c1;
	double c2;
};

Pair::Pair(double a, double b):
c1(a),
c2(b)
{ }

Pair operator*(double a, const Pair& p)
{
	return Pair(a * p.c1, a * p.c2);
}

std::ostream& operator<<(std::ostream& o, const Pair& p)
{
	o << '(' << p.c1 << ", " <<  p.c2 << ')';
	return o;
}

int main()
{

}

