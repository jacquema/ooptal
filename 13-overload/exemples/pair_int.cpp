#include<iostream>

class Pair
{
public:
	Pair(double a, double b);

private:
	double c1;
	double c2;
};

Pair::Pair(double a, double b):
c1(a),
c2(b)
{ }

double Pair::get_c1() const
{
	return c1;
}

double Pair::get_c2() const
{
	return c2;
}

// Pair operator+(Pair p1, Pair p2)
// {
// 	Pair p(p1.get_c1() + p2.get_c1(),
//      	   p1.get_c2() + p2.get_c2());
// 	return p;
// }

Pair operator+(Pair p1, Pair p2)
{
	return Pair(p1.get_c1() + p2.get_c1(),
	         	p1.get_c2() + p2.get_c2());
}

Pair operator*(double a, const Pair& p)
{
	return Pair(a * p.get_c1(), a * p.get_c2());
}

std::ostream& operator<<(std::ostream& o, const Pair& p)
{
	o << '(' << p.get_c1() << ", " <<  p.get_c2() << ')';
	return o;
}

int main()
{

}

