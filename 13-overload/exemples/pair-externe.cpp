#include<iostream>

class Pair
{
public:
	Pair(double a, double b);
	double get_c1() const;
	double get_c2() const;

private:
	double c1;
	double c2;
};

Pair::Pair(double a, double b):
c1(a),
c2(b)
{ }

double Pair::get_c1() const
{
	return c1;
}

double Pair::get_c2() const
{
	return c2;
}

// Pair operator+(Pair p1, Pair p2)
// {
// 	Pair p(p1.get_c1() + p2.get_c1(),
//      	   p1.get_c2() + p2.get_c2());
// 	return p;
// }

Pair operator+(const Pair& p1, const Pair& p2)
{
	return Pair(p1.get_c1() + p2.get_c1(),
	         	p1.get_c2() + p2.get_c2());
}

// nécessairement externe
Pair operator*(double a, const Pair& p)
{
	return Pair(a * p.get_c1(), a * p.get_c2());
}

// nécessairement externe
std::ostream& operator<<(std::ostream& o, const Pair& p)
{
	o << '(' << p.get_c1() << ", " <<  p.get_c2() << ')';
	return o;
}

int main()
{
	Pair p1(1, 2);
	std::cout << p1 << std::endl;
	Pair p2 = 2 * p1;
	std::cout << p2 << std::endl;
	Pair p3 = p1 + p2;
	std::cout << p3 << std::endl;
}
