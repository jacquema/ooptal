#include<iostream>

class Pair
{
public:
	Pair(double a, double b);
	double get_c1() const;
	double get_c2() const;

private:
	double c1;
	double c2;
};