#include <iostream>

class Rectangle
{ 
public:
Rectangle(double h = 1, double w = 1);
void print() const;

private:
   double height;
   double width;
};

Rectangle::Rectangle(double h, double w):
height(h), 
width(w)
{  }

void Rectangle::print() const
{
    std::cout << "hauteur: " << height;
    std::cout << " largeur: " << width << std::endl;
}

class Window
{ 
public:

    Window(int c = 0);

    Window(double h, double w, int c);

    Rectangle rectangle;
    int color;
};

Window::Window(int c):
rectangle(),
color(c)
{  }


Window::Window(double h, double w, int c):
rectangle(h, w),
color(c)
{  }


int main()
{
    Window w;
    Window w1(3.0, 2.5, 1);

    w.rectangle.print();
    w1.rectangle.print();
}

