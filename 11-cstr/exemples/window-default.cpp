#include <iostream>

class Rectangle
{ 
public:
Rectangle(double h = 1, double w = 1);
void print() const;

private:
   double height;
   double width;
};

Rectangle::Rectangle(double h, double w):
height(h), 
width(w)
{  }

void Rectangle::print() const
{
    std::cout << "hauteur: " << height;
    std::cout << " largeur: " << width << std::endl;
}

class Window
{ 
public:
    Rectangle rectangle;
    int color;
};


int main()
{
    // ok, utilise le constructeur par défaut 
    Window w;

    w.rectangle.print();
}

