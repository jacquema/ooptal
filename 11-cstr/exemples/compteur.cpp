#include<iostream>

// variable globale
long compteur(0);

class Rectangle 
{
public:
    // constructeur
    Rectangle(double h=0, double w=0): 
    height(h), 
    width(w) 
    { 
        ++compteur;
    } 

	Rectangle(const Rectangle& rhs): 
    height(rhs.height), 
    width(rhs.width) 
    {  
        ++compteur;
    } 

    // destructeur
    ~Rectangle()
    {
        --compteur;
    }

private: 
    double height; 
    double width; 
};


int main()
{
 	std::cout << "compteur.0 = " << compteur << std::endl;
    Rectangle r1;
 	std::cout << "compteur.1 = " << compteur << std::endl;

    {
        Rectangle r2;
	 	std::cout << "compteur.2 = " << compteur << std::endl;
        Rectangle r3(r2); // copie
	 	std::cout << "compteur.3 = " << compteur << std::endl;
    }
 	std::cout << "compteur.4 = " << compteur << std::endl;
}