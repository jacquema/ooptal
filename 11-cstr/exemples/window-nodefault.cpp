// ne compile pas
// car Window n'a pas de constructeur par defaut

#include <iostream>

class Rectangle
{ 
public:
Rectangle(double h = 1, double w = 1);
void print() const;

private:
   double height;
   double width;
};

Rectangle::Rectangle(double h, double w):
height(h), 
width(w)
{  }

void Rectangle::print() const
{
    std::cout << "hauteur: " << height;
    std::cout << " largeur: " << width << std::endl;
}

class Window
{ 
public:

    // de part l'existence de ce constructeur, 
    // aucun constructeur par défaut n'est ajouté automatiquement
    Window(double h, double w, int c = 0);

    Rectangle rectangle;
    int color;
};

Window::Window(double h, double w, int c):
rectangle(h, w),
color(c)
{  }


int main()
{
    Window w;
    Window w1(3.0, 2.5, 1);

    w.rectangle.print();
}

