// il y a un constructeur de trop 
// dans ce rectangle

class Rectangle
{ 
public:
Rectangle()
{
    height = 2;
    width = 2;
}

Rectangle(double h = 1, double w = 1)
{
    height = h;
    width = w;
}

private:
   double height;
   double width;
};

int main()
{
    // on ne sait pas quel constructeur appeler à l'initialisation de r.
    Rectangle r;
}