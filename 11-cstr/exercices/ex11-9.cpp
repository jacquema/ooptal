#include <iostream>
#include <cmath>

class Point
{ 
public:

    Point(double x=0, double y=0);
    Point(const Point& p);

    void move(double dx, double dy);
    double distance(const Point& p) const;
    void print() const;

private:
    double x;
    double y;
};


Point::Point(double x, double y): 
x(x),
y(y)
{ }

Point::Point(const Point& rhs):
x(rhs.x),
y(rhs.y)
{ }

void Point::move(double dx, double dy)
{
    x += dx;
    y += dy;
}

double Point::distance(const Point& p) const
{
    double dx = p.x - x;
    double dy = p.y - y;
    return sqrt(dx * dx + dy * dy);
}

void Point::print() const
{
    std::cout << "(" << x << ", " << y << ")";
}



class Disc
{
public:
    Disc(double x = 0, double y = 0, double r = 0);
    Disc(const Point& p, double r);

    void move(double dx, double dy);
    double surface() const;
    void print() const;

private:
    Point center; 
    double radius;
};

Disc::Disc(double x, double y, double r): 
center(x, y),
radius(r)
{
    if (r < 0)
    {
        std::cerr << "le rayon doit être positif" << std::endl;
        radius = 0;
    }
}

Disc::Disc(const Point& p, double r): 
center(p), // constructeur de copie
radius(r)
{
    if (r < 0)
    {
        std::cerr << "le rayon doit être positif" << std::endl;
        radius = 0;
    }
}

void Disc::move(double dx, double dy)
{
    center.move(dx, dy); // appel de Point::move
}

double Disc::surface() const
{
    return M_PI * radius * radius;
}

void Disc::print() const
{
    center.print();
    std::cout << " rayon=" << radius;
}

int main()
{
    Point p1(1, 1);
    Point p2(-0.5, -0.5);
    Disc d1(0, 0, 1);
    Disc d2(0.5, -1, 2);

	std::cout << "disque 1: "; d1.print(); std::cout << std::endl;
	std::cout << " surface = " << d1.surface() << std::endl;
    std::cout << " move" << std::endl;
    d1.move(0.5, 0.5);
	std::cout << "disque 1: "; d1.print(); std::cout << std::endl;
	std::cout << " surface = " << d1.surface() << std::endl;

	std::cout << "disque 2: "; d2.print(); std::cout << std::endl;
    std::cout << " surface = " << d2.surface() << std::endl;
    std::cout << "move" << std::endl;
    d2.move(2, 2.5);
	std::cout << "disque 2: "; d2.print(); std::cout << std::endl;
	std::cout << " surface = " << d2.surface() << std::endl;
}
