#include <iostream>
#include <cmath>

class Point
{ 
public:

    Point(double x=0, double y=0);
    Point(const Point& p);

    void move(double dx, double dy);
    double distance(const Point& p) const;
    void print() const;

private:
    double x;
    double y;
};

Point::Point(double x, double y): 
x(x),
y(y)
{ }

Point::Point(const Point& rhs):
x(rhs.x),
y(rhs.y)
{ }

void Point::move(double dx, double dy)
{
    x += dx;
    y += dy;
}

double Point::distance(const Point& p) const
{
    double dx = p.x - x;
    double dy = p.y - y;
    return sqrt(dx * dx + dy * dy);
}

void Point::print() const
{
    std::cout << "(" << x << ", " << y << ")";
}



class Disc
{
public:
    Disc(double x = 0, double y = 0, double r = 1);

    void print() const;

private:
    Point center; 
    double radius;
};

Disc::Disc(double x, double y, double r): 
center(x, y), // appel du constructeur de Point
radius(r)
{
	// test d'intégrité
    if (r < 0)
    {
        std::cerr << "le rayon doit être positif" << std::endl;
        radius = 0;
    }
}

void Disc::print() const
{
	std::cout << " centre=";
	center.print(); // print de la classe Point
	std::cout << " rayon=" << radius;
}

int main()
{
    Disc d1(0, 0, 1);
    Disc d2(0.5, -1, 2);

    std::cout << "disque 1: "; d1.print(); std::cout << std::endl;
    std::cout << "disque 2: "; d2.print(); std::cout << std::endl;
}
