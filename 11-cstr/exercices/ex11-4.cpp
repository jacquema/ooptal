#include <iostream>
#include <cmath>

class Point
{ 
public:
    Point(double x=0, double y=0);
    void move(double dx, double dy);
    void print() const;

private:
    double x;
    double y;
};

Point::Point(double x1, double y1): 
x(x1),
y(y1)
{ }

void Point::move(double dx, double dy)
{
    x += dx;
    y += dy;
}

void Point::print() const
{
    std::cout << "(" << x << ", " << y << ")";
}

int main()
{
    Point p0;
    Point p1(1, 1);
    Point p2(-1, 2);

	std::cout << std::endl << "move point 0" << std::endl;
	std::cout << "p0: "; p0.print(); std::cout << std::endl;
    p0.move(1, 1);
    std::cout << "p0: "; p0.print(); std::cout << std::endl;

	std::cout << std::endl << "move point 1" << std::endl;
	std::cout << "p1: "; p1.print(); std::cout << std::endl;
	p1.move(-1, -1);
    std::cout << "p1: "; p1.print(); std::cout << std::endl;

	std::cout << std::endl << "move point 2" << std::endl;
	std::cout << "p2: "; p2.print(); std::cout << std::endl;
    p2.move(1, -1);
    std::cout << "p2: "; p2.print(); std::cout << std::endl;
}
