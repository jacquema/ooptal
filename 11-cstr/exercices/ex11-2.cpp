#include <iostream>
#include <cmath>

class Disc
{
public:
	Disc(double x = 0, double y = 0, double r = 1);

	void getCenter(double& x_out, double& y_out) const;
	void setCenter(double x_in, double y_in);
	double getRadius() const;
	void setRadius(double r);

	double surface() const;
	bool contains(double x, double y) const;
	void print() const;

private:
	double x_center; // abscisse du centre
	double y_center; // ordonnée du centre
	double radius;
};

Disc::Disc(double x, double y, double r): 
x_center(x),
y_center(y),
radius(r)
{
	if (r < 0)
	{
		std::cerr << "le rayon doit être positif" << std::endl;
		radius = 0;
	}
}

void Disc::getCenter(double& x_out, double& y_out) const
{
	x_out = x_center;
	y_out = y_center;
}

void Disc::setCenter(double x_in, double y_in)
{
	x_center = x_in;
	x_center = y_in;
}

double Disc::getRadius() const
{
	return radius;
}

void Disc::setRadius(double r)
{
	if (r < 0.0)
		r = 0.0;
	radius = r;
}

double Disc::surface() const
{
	return M_PI * radius * radius;
}

bool Disc::contains(double x, double y) const
{
	double dx = x - x_center;
	double dy = y - y_center;
	return (dx * dx + dy * dy <= radius * radius);
}

void Disc::print() const
{
	std::cout << "(" << x_center << ", " << y_center << ") ";
	std::cout << "rayon=" << radius;
}


int main()
{
	Disc d1(0, 0, 1);
	Disc d2(0.5, -1, 2);

	// get center
	std::cout << "get center" << std::endl;
	double x, y;
	d1.getCenter(x, y);
	std::cout << "disc 1: x=" << x << " y=" << y << std::endl;
	d2.getCenter(x, y);
	std::cout << "disc 2: x=" << x << " y=" << y << std::endl;

	std::cout << std::endl
			  << "Disque 1" << std::endl;
	d1.print();
	bool b = d1.contains(0.1, 0.5);
	std::cout << " surface = " << d1.surface() << std::endl;
	std::cout << std::endl;

	std::cout << "Disque 2" << std::endl;
	std::cout << " surface = " << d2.surface() << std::endl;
}