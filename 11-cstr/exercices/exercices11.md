# Exercices - cours 11
## constructeurs




> **Exercice 1**. 
> On considère 4 exemples de classes avec ou sans constructeurs
>
> 1. pas de constructeur explicite  (es attributs sont privés)
> ```cpp
> class Rectangle
> { 
>    double height;
>    double width;
> };
> ```
>
> 2. constructeur par défaut explicite   
> ```cpp
> class Rectangle
> { 
> public:  
>   Rectangle():
>   height(0), 
>   width(0)
>   { }
>   
> private:
>   double height;
>   double width;
> };
> ```
>
> 3. constructeur avec valeurs par défaut des paramètres
> ```cpp
> class Rectangle
> { 
> public:
>    Rectangle(double h = 1.0, double w = 1.0):
>    height(h), 
>    width(w)
>    { }
> private:
>    double height;
>    double width;
> };
> ```
>
> 4. constructeur explicite sans valeurs par défaut des paramètres
> ```cpp
> class Rectangle
> { 
> public:
>    Rectangle(double h, double w):
>    height(h), 
>    width(w)
>    { }
> private:
>    double height;
>    double width;
> };
> ```
> Donner le comportement (compilation et valeur du rectangle à l'exécution) pour chacun des 4 exemples des codes ci-dessus, après des 2 déclaration/initialisation suivantes:
> - a. `Rectangle r1;`
> - b. `Rectangle r2(3.0, 2.5);`



> **Exercice 2**. disque: constructeur.
> Écrire pour la classe `Disc` de la feuille d'exercice 10 un constructeur avec une liste d'initialisation et 3 paramètres: les valeurs (`double`)  `x` et `y` des coordonnées du centre et le rayon.
> On vérifiera que le rayon est positif. Dans le cas contraire, le rayon sera mis à zéro.
> Ajouter dans le constructeur des valeurs par défaut pour chaque attribut.



> **Exercice 3**. point: constructeur.
> Définir dans un fichier `point.cpp` une classe `Point` avec deux attributs, privés de type `double`, contenant les coordonnées cartésiennes, abscisse et ordonnée.
>
> Écrire un constructeur pour la classe `Point` recevant comme paramètres les deux coordonnées (`double`) d’un point.
>
> Écrire une méthode `print()` de `Point` qui affiche les coordonnées du point. 



On ajoutera le qualificatif `const` aux méthodes pour lesquelles c'est approprié.



> **Exercice 4**. point: méthode translation.
>
> Écrire une méthode `move` de `Point`, ayant pour entête:
> ```cpp
> void move(double dx, double dy);
> ```
> qui modifie les coordonnées du point en leur ajoutant respectivement `dx` et `dy` (qui peuvent être négatifs).
> 
>Écrire une fonction `main` qui testera le constructeur et `move` en utilisant `print`.



> **Exercice 5**. points et distance.
> Écrire une méthode `distance` de la classe `Point` calculant la distance euclidienne avec un autre point passé en paramètre (par référence).
>
> La bibliothèque `cmath` contient une fonction `sqrt` de racine carrée.



> **Exercice 6**. point: comptage.
>
> Modifier la class `Point` précédente avec un compteur du nombre d'instances de point existantes, défini comme variable globale.
> En particulier, on ajoutera à la classe `Point`:
>
> - un destructeur
> - un constructeur par copie




> **Exercice 7**. disque de centre encapsulé/abstrait.
> En partant de copies des deux classes précédentes `Disc` et `Point`, 
> définir, dans un nouveau fichier, une nouvelle classe `Disc` dont les attributs (privés) sont 
>
> - le centre, de type `Point`,
> - le rayon (de type `double`).
>
> Écrire, pour cette nouvelle classe `Disc`, un constructeur avec 3 paramètres:
>
> ```cpp
> Disc(double x, double y, double radius);
> ```
>
> Écrire une méthode `print()` pour `Disc` .




> **Exercice 8**. disque de centre encapsulé/abstrait.
>
> Écrire pour la classe `Disc`  de l'exercice 7 un constructeur prenant 2 paramètres: un `Point` et une valeur de  rayon.
>
> ```cpp
> Disc(const Point& centre, double radius);
> ```
>




> **Exercice 9**. disque de centre encapsulé, translation.
>
> Écrire pour la classe `Disc` de l'exercice 7 une méthode `move` qui déplace le centre.
>
> ```cpp
> void move(double dx, double dy);
> ```
>
> Écrire aussi une méthode `surface` pour cette classe.




> **Exercice 10**. disque de centre encapsulé, intérieur. **DEVOIR MAISON**
>
> Écrire pour la classe `Disc` de l'exercice 7 une nouvelle méthode `contains`, qui prend un `Point` en paramètre et utilise `distance` de l'exercice précédent. On placera des `const` là où c'est approprié.
