#include <iostream>
#include <cmath>

class Point
{ 
public:

    Point(double x=0, double y=0);

    void move(double dx, double dy);
    double distance(const Point& p) const;
    void print() const;

private:
    double x;
    double y;
};

Point::Point(double x1, double y1): 
x(x1),
y(y1)
{ }

void Point::move(double dx, double dy)
{
    x += dx;
    y += dy;
}

double Point::distance(const Point& p) const
{
    double dx = p.x - x;
    double dy = p.y - y;
    return sqrt(dx * dx + dy * dy);
	// return sqrt((p.x - x) * (p.x - x) + (p.y - y) * (p.y - y));
}

void Point::print() const
{
    std::cout << "(" << x << ", " << y << ")";
}

int main()
{
    Point p0;
    Point p1(1, 1);
    Point p2(-1, 2);

	std::cout << "p0 = "; p0.print(); std::cout << std::endl;
	std::cout << "p1 = "; p1.print(); std::cout << std::endl;
	std::cout << "p2 = "; p2.print(); std::cout << std::endl;

	std::cout << "dist p0 p1 = " << p0.distance(p1) << std::endl;
	std::cout << "dist p0 p2 = " << p0.distance(p2) << std::endl;
	std::cout << "dist p1 p2 = " << p1.distance(p2) << std::endl;

	std::cout << "dist p0 p1 = " << p0.distance(p1) << std::endl;
	std::cout << "dist p0 p2 = " << p0.distance(p2) << std::endl;
	std::cout << "dist p1 p2 = " << p1.distance(p2) << std::endl;
}

