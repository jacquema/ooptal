#include <iostream>
#include <cmath>

class Point
{ 
public:

    Point(double x=0, double y=0); // header

    void print() const;

private:
    double x;
    double y;
};

Point::Point(double a, double b): 
x(a),
y(b)
{ }

void Point::print() const
{
    std::cout << "(" << x << ", " << y << ")";
}

int main()
{
    Point p0;
    Point p1(1);
    Point p2(-1, 2);

	std::cout << "p0 = "; p0.print(); std::cout << std::endl;
	std::cout << "p1 = "; p1.print(); std::cout << std::endl;
	std::cout << "p2 = "; p2.print(); std::cout << std::endl;
}
