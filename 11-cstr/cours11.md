# Cours de programmation objet

INALCO   Master TAL première année, second semestre
Florent Jacquemard  `florent.jacquemard@inria.fr`
mardi 4 février 2025



# Cours 11 - Constructeurs



# 11.1 Constructeurs: Définition, Appel



## Initialisation individuelle des attributs

Dans le cours précédent, nous avons vu comment créer une instance de classe et avons utilisé des modificateurs (*setters*) pour donner des valeurs aux attributs de cette instance.

> **Exemple 1** (rappel): initialisation "manuelle" d'un rectangle.
>
> ```cpp
> Rectangle r;
> double d;
> 
> std::cout << " donner la hauteur:"; 
> std::cin >> d;
> r.setHeight(d);
> std::cout << " donner la largeur:"; 
> std::cin >> d;
> r.setWidth(d);
> ```

Pour procéder ainsi, il faut veiller à bien initialiser tous les attributs, un à un, 
- soit directement pour les attributs qui sont dans l'interface (`public`)
- soit à l'aide d'un modificateurs pour les attributs cachés (`private`)

C'est fastidieux:  le programmeur utilisateur doit veiller à ne pas oublier d'attribut alors que ce n'est pas son rôle.

De plus, c'est contraire au principe d'abstraction: les attributs cachés ne devraient pas être connus de l'utilisateur!

## Méthode d'initialisation des attributs

Dans une approche alternative, on peut créer une **méthode spécifique** dédiée à l'**initialisation** de tous les attributs d'un objet, en un seul appel. Elle sera dans l'interface de la classe.

> **Exemple 2** : méthode d'initialisation de rectangle.
>
> ```cpp
> #include <iostream>
> class Rectangle
> { 
> public:
>    void init(double h, double w)
>    {
>       height = h;
>       width = w;
>    }
> 
> private:
>     double height;
>     double width;
> };
> ```

La méthode publique `init` peut être appelée pour initialiser une instance de la classe.

> **Exemple 3**: initialisation d'une variable de type `Rectangle` avec `init`:
>
> ```cpp
> int main()
> {
>     	Rectangle r;
>     	r.init(3, 2.5);
> }
> ```



## Constructeurs

`C++` rend systématique l'usage de méthodes dédiées à l'initialisation d'un objet. Une telle méthode spécifique est appelée **constructeur**.

Par convention, un constructeur a le même identificateur (nom) que la classe elle-même.  Cela permet de le distinguer des autres méthodes.

La déclaration d'un constructeur a la forme:
```cpp
ClassName(paramètres)
{
    // initialisation de l'objet (attributs...)
}
```

Contrairement aux autres méthodes (et fonctions), le constructeur n'a pas de type de retour, pas même `void`.

> **Exemple 4**: constructeur de rectangle.
>
> ```cpp
> class Rectangle
> { 
> public:
>     Rectangle(double h, double w)
>     {
>        height = h;
>        width = w;
>     }
> 
> private:
>     double height;
>     double width;
> };
> ```

Le rôle d'un constructeur est de réaliser toutes les opérations nécessaires au début de la vie de chaque objet,  en particulier l'initialisation des attributs. 

Dans l'exemple ci-dessus, le constructeur est dans l'interface. Il peut donc être appelée explicitement par le programmeur utilisateur.

Lors de toute création d'instance d'une classe, un constructeur est appelé. C'est *automatique*.  
L'appel peut être explicite (écrit dans le code) ou implicite.  Par exemple,  à la déclaration d'une variable contenant un objet (variable de type le nom d'une classe), la variable est automatiquement initialisée avec l'appel d'un constructeur sans paramètre. 

> **Exemple 4'** (suite)
>
> ```cpp
> int main()
> {
>   	Rectangle r; // ici un constructeur Rectangle() a été appelé.
> }
> ```



Par la suite, un constructeur sans paramètre sera appelé **constructeur par défaut.**



## Initialisation (explicite) par constructeur

Lors de la construction d'une instance de classe,  on peut passer des paramètres entre `(` `)` à destination du constructeur. 

La syntaxe de déclaration d'un objet  (variable `var` contenant une instance de la classe `ClassName`), avec passage de paramètres au constructeur, a la forme suivante:

```cpp
ClassName var(val1, ..., valn);
```

où `val1`,..., `valn` sont les valeurs des paramètres passés au constructeur.

Si la variable est déclarée sans parenthèses:

```cpp
ClassName var;
```

c'est le constructeur par défaut, `ClassName()`, sans paramètre, qui est appelé.



> **Exemple 5**: pour le constructeur de `Rectangle` précédemment défini, avec:
>
> ```cpp
> Rectangle r(3, 2.5);
> ```
>
> la variable `r` contient, après initialisation,  un `Rectangle` de hauteur `3` et largeur `2.5`.
>
> La déclaration / initialisation suivante
>
> ```cpp
> Rectangle r = Rectangle(3, 2.5);
> ```
>
> est correcte et produira le même résultat, mais passe par plusieurs étapes, avec une copie intermédiaire:
>
> - un objet de type `Rectangle` est créé, pour la variable `r`,
>   avec le constructeur par défaut `Rectangle()`.
>   Admettons que le rectangle construit ainsi a pour hauteur `1.0` et largeur `1.0`.  
> - une autre *instance anonyme* (sans nom de variable ou identificateur) de  `Rectangle` est crée par l'appel au constructeur: `Rectangle(3, 2.5)`. Elle a pour hauteur `3.0` et largeur `2.5`
> - l'instance anonyme est ensuite copiée dans le rectangle `r`, par l'opérateur `=`.
>   Les valeurs initiales de hauteur et largeur sont écrasées, remplacées par celles de l'instance anonyme.

La copie lors de la déclaration avec `=` est inutile et peut être couteuse dans le cas de très gros objets.
Donc, la première forme d'initialisation d'instance, avec parenthèses, est préférable car elle appelle directement le constructeur de `Rectangle`.



## Valeurs par défaut

Les paramètres de constructeurs  peuvent avoir des valeurs par défaut, comme pour les autres fonctions.

> **Exemple 6**: valeurs par défaut pour paramètre d'un constructeur de rectangle.
>
> ```cpp
> class Rectangle
> { 
> public:
>     Rectangle(double h = 1, double w = 1)
>     {
>       height = h;
>       width = w;
>     }
> 
> private:
>   double height;
>   double width;
> };
> ```
>
> Cette déclaration définit un constructeur par défaut `Rectangle()`, qui initialise un rectangle de hauteur `1` et largeur `1`.
> Ce constructeur peut aussi être appelé avec une valeur de hauteur seule:
> `Rectangle(2)` initialise un rectangle de hauteur `2` et largeur `1`.

La présence de paramètres optionnels (avec valeur par défaut) peut causer des surcharges de constructeurs.



## Constructeurs multiples (surcharge)

Il peut exister plusieurs constructeurs pour une même classe.

Comme les autres méthodes, les constructeurs peuvent être **surchargés** : on peut avoir, pour une classe, plusieurs constructeurs, avec un nombre et des types des paramètres différents. Notons que tous les constructeurs ont le même nom, celui de la classe et pas de type de retour.

En cas de surcharge, le choix du constructeur à appeler obéit aux mêmes règles que pour les fonctions surchargées.



> **Exemple 7**. La classe `Rectangle` suivante a 3 constructeurs, avec respectivement 0, 1 et 2 paramètres.
>
> ```cpp
> class Rectangle
> { 
> public:
>     Rectangle() // constructeur par défaut
>     {
>       height = 1; 
>       width(1)
>     }
> 
>     Rectangle(double h)
>     {
>       height = h; 
>       width = 2 * h;
>     }
> 
>     Rectangle(double h, double w)
>     {
>       height = h; 
>       width = w;
>     }
> 
> private:
>     double height;
>     double width;
> };
> ```
> Les déclarations
> ```c++
> Rectangle r1; // constructeur par défaut
> Rectangle r2(1);
> Rectangle r3(3, 2.5);
> ```
>
> initialisent des rectangles avec chacun des 3 constructeurs:
>
> - `r1` est de hauteur `1` et largeur `1` 
>   initialisé par appel du premier constructeur sans paramètre = constructeur par défaut,
> - `r2` de hauteur `1` et largeur `2` 
>   initialisé par appel du deuxième constructeur à 1 paramètre,
> - `r3` de hauteur `3` et largeur `2.5`
>   initialisé par appel du troisième constructeur à 2 paramètres.



L'usage de valeurs par défaut pour les paramètres correspond à une définition implicite de plusieurs constructeurs.


> **Exemple 8**. Avec la définition suivante d'un constructeurs à 2 paramètres avec valeurs par défaut
>
> ```cpp
> class Rectangle
> { 
> public:
>   Rectangle(double h = 1, double w = 1)
>   {
> 	  height = h; 
> 	  width = w;
>   }
> ...
> };
> ```
>
> on définit implicitement 3 constructeurs à 0, 1 et 2 paramètres.
>
> Les déclarations
>
> ```cpp
> Rectangle r1;
> Rectangle r2(2);
> Rectangle r3(3, 2.5);
> ```
>
> initialisent des rectangles
>
> - `r1` est de hauteur `1` et largeur `1` 
>   (les deux paramétres prennent des valeurs par défaut),
> - `r2` de hauteur `2` et largeur `1` 
>   (le deuxième paramétre, de largeur, prend la valeur par défaut),
> - `r3` de hauteur `3` et largeur `2.5`



## Surcharge et ambiguité

La règle dans le cas d'existence de constructeurs multiples est que l'appel doit pouvoir se faire **sans ambiguité**.

> **Exemple 9**. surcharge ambigüe de constructeurs.
> La classe suivante ne peut être compilée, cf.  [`exemples/constructeurs_trop.cpp`](exemples/constructeurs_trop.cpp)
>
> ```c++
> class Rectangle
> { 
> public:
>      Rectangle()
>      {
>          height = 2;
>          width = 2;
>      }
> 
>      Rectangle(double h)
>      {
>          height = h;
>          width = 2*h;
>      }
> 
>      Rectangle(double h, double w)
>      {
>          height = h;
>          width = w;
>      }
> 
>      Rectangle(double h = 1, double w = 1)
>      {
>          height = h;
>          width = w;
>      }
> 
> private:
>      double height;
>      double width;
> };
> ```
>
> On a  à la compilation une erreur comme suit:
>
> ```
> error: call to constructor of 'Rectangle' is ambiguous
> ```
>
> suivie d'une liste des deux candidats constructeurs  entre lesquels le compilateur ne peut choisir.



## Constructeurs et modificateurs

Avec le constructeur `Rectangle(double h, double w)` ci-dessus, les modificateurs `setHeight` et `setWidth` ne sont plus nécessaires pour **initialiser** les attributs d'une instance de `Rectangle`.

Cependant, le programmeur concepteur peut choisir de les inclure dans l'interface de sa classe, pour permettre de **modifier** les attributs de hauteur et largeur, séparément, après la création (s'il juge cela utile).



## Listes d'initialisation

Il existe en C++ une syntaxe particulière pour l'initialisation des attributs par un constructeur. Celle-ci peut s'écrire entre l'entête et le corps du constructeur, de la manière suivante (noter les `:`)

```cpp
ClassName(paramètres):
attribut1(paramètres1), // appel au constructeur de l'attribut 1
...
attributn(paramètresn)  // appel au constructeur de l'attribut n
{
   // corps du constructeur
}
```

L'initialisation des attributs se fait suivant la même syntaxe que pour les déclarations de variables, avec les arguments entre  `(`, `)`.   
Dans la liste d'initialisations , les attributs sont séparées par des virgules.



> **Exemple 10**. Les 3 constructeurs de l'exemple 7 s'écrive comme suit, avec des listes d'initialisation:
>
> ```cpp
> class Rectangle
> { 
> public:
>       Rectangle():
>       height(1), 
>       width(1)
>       { }
> 
>     Rectangle(double h):
>       height(h), 
>       width(2 * h)
>       { }
> 
>     Rectangle(double h, double w):
>       height(h), 
>       width(w)
>       { }
> 
> private:
> double height;
> double width;
> };
> ```
>



Le corps des 3 constructeurs de l'exemple précédent est vide, parce qu'on considère qu'il n'y a plus rien à faire après l'initialisation des attributs. Dans certains cas, on pourra avoir des instructions après initialisation, typiquement, pour faire des tests d'intégrité.



> **Exemple 11**  constructeur de rectangle avec listes d'initialisation et valeurs par défaut.
>
> ```c++
> class Rectangle
> { 
> public:
>   Rectangle(double h = 1, double w = 1):
>   height(h), 
>   width(w)
>   { 
>      // rien d'autre à faire!
>   }
> 
> private:
>   double height;
>   double width;
> };
> ```
> Les valeurs `double` de `h` et `w` sont assignées aux attributs correspondants `height` et `width`.



La différence avec la définition précédente de constructeur est que l'on évite la copie avec l'opérateur `=`. 
Cela peut être important dans le cas d'attributs qui sont des objets et pas des types de base.



> **Exemple 12**. initialisation d'attribut objet.
> Considérons une classe Fenêtre dont un attribut est une instance de `Rectangle`.
>
> ```cpp
> class Window
> { 
> public:
>      Window(double h, double w, Color c):
>      _rec(h, w),
>      _col(c)
>      {  }
> 
> private:
> 	Rectangle _rec;
> 	Color _col;
> };
> ```
> On a déclaré les attributs, dans une section  `private`, dont `_rec`, de type `Rectangle`.
> Dans le constructeur, l'attribut `_rec`, de type `Rectangle`,  est initialisé avec le constructeur `Rectangle`, comme une variable.



Quand on a un objet parmi les attributs, on doit lui passer autant de paramètres que de paramètres du  constructeur correspondant. 
Dans l'exemple 12, pour `_rec`, il y a une hauteur (`h`) et une largeur (`w`).



L'usage de la liste d'initialisation est recommandé :
- évite de faire des copies (avec opérateur `=`)
- regroupe de manière compacte et lisible les initialisation d'attributs, 
- donne des valeurs initiales à chaque attribut avant même la création de l'objet,
  ce qui est une sécurité.

La valeur des attributs peut ensuite être modifiée dans le corps du constructeur
(en général, ce n'est pas nécessaire).



## Constructeur de la même classe dans la liste d'initialisation

Un constructeur peut (depuis `C++11`) invoquer un constructeur de la même classe dans sa liste d'initialisation.



> **Exemple 13**: Le deuxième constructeur appelle le premier.
>
> ```cpp
> class Rectangle
> { 
> public:
> 	Rectangle(double h, double w):
> 	height(h), 
> 	width(w)
> 	{ 
>     //...
>   }
> 
> 	Rectangle():
> 	Rectangle(1.0, 1.0)
> 	{ }
> 
> private:
> 	double height;
> 	double width;
> };
> ```
> Cette solution est équivalente au constructeur précédent avec les valeurs par défaut.
> ```cpp
> Rectangle(double h = 1.0, double w = 1.0);
> ```



Appeler un constructeur de la même classe dans la liste d'initialisation permet de n'écrire qu'une fois (dans le constructeur appelé) certaines instructions (par exemple des tests d'intégrité). Ainsi, on évite de la duplication de code.
Des appels de constructeurs dans la liste d'initialisation seront nécessaires pour l'héritage, ce qui sera vu dans un cours suivant.



# 11.2 Constructeur par défaut


## Définition

Un constructeur par défaut est 
- un constructeur défini sans paramètre, ou bien, 
- un constructeur dont tous les paramètres ont des valeurs par défaut.

Dans les deux cas, le constructeur pourra être appelé sans paramètre, et sans parenthèses dans le cas d'une déclaration/initialisation de variable.

> **Exemple 14**. constructeur par défaut de `Rectangle`.
>
> ```c++
> Rectangle():
> height(1), 
> width(1)
> { }
> ```
>
> Ce constructeur sera appelé automatiquement pour l'initialisation, sans `(`,`)`, d'une 
> variable de type `Rectangle`.
>
> ```c++
> Rectangle r; // appel du constructeur par défaut
> ```
> Donc `r` aura initialement une hauteur `1` et une largeur `1`.



Le constructeur  par défaut est nécessairement unique. Il sera appelé automatiquement pour l'initialisation d'un objet de la classe, lors d'une cléclaration, sans `(`,`)` .

**Attention**: ne pas ajouter des parenthèses pour l'initialisation avec un constructeur par défaut. C'est une erreur fréquente qui pose un problème de syntaxe au compilateur. La déclaration suivante:

```cpp
Rectangle r();
```
n'est **pas** un appel de constructeur par défaut, mais sera considérée par le compilateur comme une déclaration d'un  prototype d'une fonction `r`, sans paramètres, et qui retourne un `Rectangle`. 




> **Exemple 15**. constructeur de `Rectangle` avec valeurs par défaut des paramètres. 
>
> Revenons à un exemple précédent de constructeur:
>
> ```c++
> Rectangle(h = 1, w = 1):
> height(h), 
> width(w)
> { }
> ```
> Avec une déclaration sans valeur entre `(`, `)`:
> ```c++
> Rectangle r;
> ```
> le constructeur ci-dessus est invoqué avec les valeurs par défaut des paramètres, 
> donc `r` aura initialement une hauteur `1` et une largeur `1`.




## Nécessité d'un constructeur par défaut

Nous avons vu qu'un constructeur est toujours appelé lorsqu'on déclare un objet.
Que se passe-t-il quand on déclare un objet **sans passer de valeur d'initialisation** entre `(`, `)` et qu'aucun constructeur par défaut n'est défini dans la classe correspondante? 

> **Exemple 16**: déclaration de `Rectangle` sans constructeur par défaut.
>
> ```cpp
> class Rectangle
> { 
> public:
> 	Rectangle(double h, double w):
> 	height(h), 
> 	width(w)
> 	{ }
>   
> private:
> 	double height;
> 	double width;
> };
> 
> int main()
> {
> 	Rectangle r(3, 2.5);
> 	Rectangle s; // quel constructeur?
> }
> ```
> La variable `r` est initialisée avec une instance de `Rectangle`, construite avec le constructeur défini dans la classe (avec deux paramètres : hauteur `3` et largeur `2.5`).
>
> Avec quel constructeur est initialisée la variable `s` (déclarée sans valeur d'initialisation)?

Malgré tout, il est autorisé de définir des classes sans constructeur.
C'est ce qui a été fait au cours précédent, avec du code qui compile correctement.  
Dans ce cas, quel constructeur est utilisée pour l'initialisation d'instances de la classe?




## Constructeur par défaut par défaut

Si aucun constructeur n'est défini dans une classe, le compilateur en fournit un par défaut, qui ne prend aucun paramètre. Il s'agit donc d'un constructeur par défaut$* 2$.

Dans ce constructeur,  les attributs sont initialisés comme suit :

- pour un attribut objet, appel du constructeur par défaut de la classe correspondante,
- un attribut de type de base restera non initialisé (il prend une valeur non-déterminée).



> **Exemple 17**. pour une classe  `Rectangle` définie sans constructeur, avec la déclaration:
>
> ```cpp
> Rectangle r; // appel du constructeur par défaut par défaut 
> ```
> la variable `r` contiendra un rectangle dont les valeurs de hauteur et largeur sont indéterminées.
> Plus précisément, ce sont les valeurs à l'adresse de l'objet (emplacement mémoire où il est stocké) au moment de l'exécution.



*standard C++3337 § 8.5, note 11 : If no initializer is specified for an object, the object is default-initialized; if no initialization is performed, an object with automatic or dynamic storage duration has indeterminate value. [ Note: Objects with static or thread storage duration are zero-initialized, see 3.6.2. — end note ]*



> **Exemple 18.** cf.  [`exemples/window-default.cpp`](exemples/window-default.cpp)
> avec une classe `Window` dont un des attributs est un `Rectangle`, 
>
> ```c++
> class Window
> { 
> private:
> 	Rectangle _rec;
> 	Color _col;
> };
> ```
>
> et avec le constructeur suivant défini dans la classe `Rectangle`
>
> ```cpp
> Rectangle(h = 1, w = 1):
> height(h), 
> width(w)
> { }
> ```
> après la déclaration
> ```cpp
> Window w;
> ```
> la variable `w` contiendra une fenêtre avec un rectangle de hauteur `1` et de largeur `1`, et une couleur définie par le constructeur par défaut de la classe `Color`.



## Absence de constructeur par défaut

**Attention**:
Le constructeur par défaut sans paramètre n'est fournit par le compilateur que si dans la classe n'est défini **aucun** constructeur. Donc si dans la classe est défini un constructeur avec des paramètres, il n'y a plus de constructeur par défaut par défaut fournit par le compilateur! 

Dans ce cas, si un constructeur par défaut n'est défini explicitement,  on ne pourra plus initialiser une variable à la déclaration sans parenthèses et il y aura erreur à la compilation.

> **Exemple 19**.  cf. [`exemples/window-nodefault.cpp`](exemples/window-nodefault.cpp)
> Si la classe `Window` (de l'exemple suivant) contient un unique constructeur 
>
> ```cpp
> Window(double h, double w, int c = 0);
> ```
> qui doit obligatoirement être appelé avec au moins deux paramètres,  cette classe `Window` n'a pas de constructeur par défaut. La déclaration suivante:
> ```cpp
> Window w;
> ```
> ne compilera pas.
> ```
> window-nodefault.cpp:48:12: error: no matching constructor for initialization  of 'Window'
> ()...)           ^
> window-nodefault.cpp:40:9: note: candidate constructor not viable: requires at least 2 arguments, but 0 were provided
> ```



## Constructeur avec mot clé  `default`

Il est possible (en `C++11`) de forcer le compilateur à fournir un constructeur par défaut même si il existe un autre constructeur, avec le mot-clé `default`:

```cpp
ClassName() = default;
```



> **Exemple 20**. On aura deux constructeurs, avec 0 et 2 paramètres, pour cette classe. 
>
> ```cpp
> class Rectangle
> {
>      Rectangle() = default;
>   
>      Rectangle(h, w):
>      height(h), 
>      width(w)
>      { }
> };
> ```

Attention, avec le constructeur par défaut, les attributs sont initialisés à des valeurs (`double`) indéterminées. Pour avoir des valeurs d'initialisation déterminées, utiliser des valeurs de paramètres par défaut.



## Méthodes `default` et `delete`
L'emploi de `= default` est généralisable à toute méthode (pas seulement les constructeurs) quand cela a du sens.

De manière similaire, le mot-clé `delete` interdit certaines surcharges.

>  **Exemple 21**. mot clé `delete`
>
>  On veut que la fonction `noint` ne s'applique qu'à des arguments de type `double`.
>
>  ```cpp
>  double noint(double x);
>  double noint(int) = delete;
>  ```

On peut aussi utiliser ce mot clé  `delete` pour interdire la création, par le compilateur, d'un constructeur par défaut par défaut.



```cpp
Rectangle() = delete; // interdit la création d'une constructeur par défaut par défaut
```



## Initialisation par défaut des attributs

Il est possible (depuis `C++11`) de donner une valeur initiale par défaut aux attributs.
Si le constructeur invoqué ne modifie pas la valeur de l'attribut, celui-ci sera initialisé à la valeur par défaut indiquée.

> **Exemple 22**.
>
> ```cpp
> class Rectangle
> {
> private:
>       double height = 0.0;
>       double width  = 0.0;
> };
> ```

Cela peut donc être utile pour les attributs avec un type de base (pour éviter les valeurs indéterminées).

Si le constructeur modifie la valeur de l'attribut, la valeur initiale par défaut est ignorée (le constructeur prime).

Donc cela peut compliquer l'écriture et la maintenance de code, car il faut tenir compte de ces valeurs par défaut en plus du code des constructeurs. On pourra avoir des contradictions.
Il est *préférable de définir explicitement un constructeur par défaut pour contrôler les valeurs par défaut des attributs*.



> Exercice 1 feuille 11



# 11.3 Constructeur de copie


## Copie à l'initialisation

`C++` permet d'initialiser un objet par copie d'un autre objet de la même classe.

> **Exemple 23**. copie de `Rectangle`
>
> ```c++
> Rectangle r1(3.5, 2.0);
> Rectangle r2(r1);
> ```
> - L'instance `r1` est initialisée avec le constructeur de `Rectangle`.
> - L'instance `r2` est initialisée avec une copie de `r1`: il y a copie des valeurs des attributs de `r1`, un à un. Après initialisation, `r1` et `r2` sont deux instances distinctes, séparées en mémoire, 
> mais avec les mêmes valeurs d'attributs.



La copie lors d'une initialisation comme ci-dessus se fait à l'aide d'un constructeur spécifique, dit de copie, comme nous allons le voir ci-dessous.




## Copie et passage par valeur

On rappelle que lors du passage de paramètre par valeur, une copie de la valeur des paramètres est effectuée.

> **Exemple 24**. Dans la fonction `surface` suivante, il y a passage par valeur d'une instance de `Rectangle`
>
> ```cpp
> double surface(Rectangle r);
> 
> int main()
> {
>   Rectangle r1;
>   // ...
>   double z = surface(r1);
> }
> ```
> La fonction `surface` prend en paramètre un objet `Rectangle` (une instance).
> Lors de l'appel de `surface`, dans `main`, il a donc copie de l'instance `r1`, dans le paramètre `r`.



La copie lors du passage par valeur d'instance d'une classe se fait aussi à l'aide d'un **constructeur de copie**.
Il est donc important de définir un tel constructeur.




## Constructeur de copie: définition

Il initialise une instance en copiant les attributs  d'une autre instance de la même classe.

La syntaxe pour l'entête d'un constructeur de copie est la suivante.

```cpp
NomClass(const NomClass& rhs);
```

Le paramètre `rhs` (*right-hand-side*) est une autre instance de la classe, passée 
- par référence (pour éviter de faire des copie de copies...)
- par référence constante (mot clé `const`), car cette autre instance ne sera pas modifiée, mais seulement lue.



> **Exemple 25**.  Définition d'un constructeur par copie de `Rectangle`.
>
> ```cpp
> class Rectangle
> { 
> public:
> 		Rectangle(const Rectangle& rhs):
> 		height(rhs.height), 
> 		width(rhs.width)
> 		{ }
>   
> private:
> 		double height;
> 		double width;
> };
> ```
> Les attributs de la nouvelle instance de `Rectangle` sont initialisés avec les valeurs des mêmes attributs dans  `rhs`. 

Cette forme avec une liste d'initialisation des attributs aux valeurs respectives correspondantes dans `rhs` est une définition typique de constructeur par copie.



## Constructeur de copie par défaut

En l'absence de constructeur par copie défini dans une classe, le compilateur fournit un constructeur de copie par défaut, qui fait une copie des attributs un à un. On parle de *copie de surface*.

> **Exemple**. constructeur de copie fourni par défaut.
> Le constructeur de copie fourni par défaut pour  la classe `Rectangle` est équivalent à celui défini dans l'exemple précédent.




## Mot clé `delete`

Depuis `C++11` il est possible de demander explicitement à ne pas avoir de constructeur de copie, à l'aide du mot clé `delete`.

```c++
class Uncopyable
{
    Uncopyable(const Uncopyable&) = delete;
}
```

Cela peut être utile pour une classe dont les instances prennent beaucoup de place 
en mémoire, et pour laquel on veut éviter à tout prix de faire des copies.



## Règle des trois

Dans certains cas, Il peut arriver que le constructeur de copie fourni par défaut  ne soit pas suffisant,  et qu'il faille écrire explicitement un opérateur de copie (avec des opération d'initialisation particulières).

La règle dite des trois stipule que

> Si dans une classe, on définit explicitement l'une des méthodes suivante:
>
> - **constructeur de copie**
> - **destructeur** (présenté à la section suivante)
> - opérateur d'**affectation**  `=` (qui fait aussi une copie)
>
> alors il faut (certainement) définir les deux autres.

 

# 11.4 Destructeurs

Les constructeurs, présentés au cours précédents sont chargés de gérer le début de vie d'un objet (son initialisation en mémoire).
À l'inverse, le rôle d'un destructeur est de de gérer ce qui se passe à la fin de la vie d'un objet, en particulier, comment les ressources utilisées par cet objet sont libérées.

Il peut s'agir de

- zones de mémoire réservées (pointeurs)
- fichiers
- périphériques...



## Destructeur: définition

Un destructeur est une méthode appelée **automatiquement**  à la fin de vie d'une instance. Par exemple, 

- lors de la sortie de la portée d'une variable,
- lors d'une opération `clear` sur un `vector` (qui contient des objets),
- pendant la désallocation de pointeur...

L'existence du destructeur évite à l'utilisateur d'avoir à libérer explicitement les ressources allouées à un objet (ou d'oublier de le faire...).



>  **Exemple 26**. fin de vie de variable
>
>  ```cpp
>  {
>    	Rectangle r;
>    // ...
>  } 
>  // on sort de la portée de r. le destructeur de r est appelé automatiquement ici.
>  ```



La syntaxe de l'entête d'un destructeur a toujours la même forme:

```cpp
~ClassName()
{ 
    // libération des ressources
}
```

L'identificateur est celui de la classe, précédé d'un `~`.

Le destructeur est une méthode **sans paramètre**,  il n'y a donc pas de surcharge possible. 

Il n'y aura donc qu'un destructeur par classe.



## Destructeur fourni

Si le destructeur n'est pas défini explicitement dans la classe (par le programmeur concepteur), le compilateur en génère un automatiquement.
Il s'agit d'une version minimale par défaut.



## Exemple: compteur d'instances

Supposons que l'on veuille compter le nombre d'instances d'une classe `Rectangle` actives dans un programme, à l'aide d'une variable globale (initialisée à 0):

```c++
long compteur = 0;
```

Le résultat souhaité sera par exemple, 

```cpp
int main()
{
    // compteur = 0
    Rectangle r1;
    // compteur = 1 (incrémenté à la construction de r1)
    {
        Rectangle r2;
       // compteur = 2 (incrémenté à la construction de r2)
    }
    // compteur = 1 (décrémenté à la destruction de r2)
}
// compteur = 0 (décrémenté à la destruction de r1)
```

Cela peut être fait à l'aide des constructeurs et destructeurs suivants pour la classe `Rectangle`.

```cpp
long compteur(0);

class Rectangle 
{
public:
    // constructeur
    Rectangle(double h=0, double w=0): 
    height(h), 
    width(w) 
    { 
        ++compteur;
    } 

    // destructeur
    ~Rectangle()
    {
        --compteur;
    }
private: 
    double height; 
    double width; 
};
```

Le comportement est bien celui souhaité car, comme nous l'avons vu, les constructeurs et destructeurs sont appelés automatiquement à la création et la fin de chaque objet `Rectangle`.



## Règle des trois (encore)

Dans l'exemple précédent, le destructeur fourni par défaut n'est pas suffisant car il ne modifie pas le `compteur`.

Restons dans le même exemple et regardons une autre execution avec la classe ci-dessus.

```cpp
int main()
{
    // compteur = 0
    Rectangle r1;
    // compteur = 1
    {
        Rectangle r2;
       // compteur = 2
        Rectangle r3(r2); // copie de r2
       // compteur = ?
    }
    // compteur = ?
}
// compteur = ?
```

Le constructeur appelé pour la copie `Rectangle r3(r2)` est le constructeur de copie fourni par défaut par le compilateur. Celui-ci fait un simple initialisation des attributs, comme dans:

```cpp
Rectangle(const Rectangle& rhs): 
height(rhs.height), 
width(rhs.width) 
{  } 
```

Mais malheureusement, il ne met pas à jour la variable `compteur`!
On a donc,  avec ce constructeur de copie fourni par défaut:

```cpp
int main()
{
    // compteur = 0
    Rectangle r1;
    // compteur = 1
    {
        Rectangle r2;
       // compteur = 2
        Rectangle r3(r2);
       // compteur = 2 (pas d'incrémentation)
    }
    // compteur = 0 décrémentations par le destructeur appelé pour r2 et r3
}
// compteur = -1 (!!) destructeur de r1
```

Dans ce cas, on a donc besoins d'une définition explicite du constructeur de copie, avec opération sur le compteur.

```cpp
    Rectangle(const Rectangle& rhs): 
    height(rhs.height), 
    width(rhs.width) 
    {  
        ++compteur;
    } 
```



> **Règle des trois** (deuxième fois):
> Si dans une classe, on définit explicitement un des
>
> - constructeur de copie
> - destructeur (présenté au cours suivant)
> - opérateur d'affectation `=`
>
> alors il faut (certainement) définir les deux autres.



# 11.5 Copie profonde



## Copie par défaut de surface

Le constructeur fourni par défaut n'est pas toujours adéquat,  et il peut être nécessaire d'un définir un autre explicitement, comme dans l'exemple précédent du comptage d'instances de `Rectangle`.

Le constructeur de copie minimal fourni par défaut réalise une *copie de surface* des attributs de l'objet copié. Elle réalise une copie des attributs un à un.



## Copie de surface, exemple de corruption

La copie par surface peut poser des problèmes quand un attribut est un pointeur.

> **Exemple**. Rectangles avec attributs pointeurs.
>
> ```cpp
> class Rectangle 
> {
> private:
> 	double* largeur; // un pointeur !
> 	double* hauteur;
> public:
> 	Rectangle(double l, double h): 
> 	width(new double(l)), // allocation
> 	height(new double(h)) 
> 	{ }
> 
> ~Rectangle() { delete largeur; delete hauteur; }
>   
> double getWidth() const;
> double getHeight() const;
> // ...
> };
> ```

Les pointeurs alloués dans le constructeur sont désalloués dans le destructeur.
Par ailleurs, la classe ne contient pas de constructeur de copie explicite.

Considérons une simple fonction d'affichage.

```cpp
void show(Rectangle tmp)
{
	std::cout << "Largeur: " << tmp.getWidth() << std::endl;
}
```

et un appel.

```cpp
Rectangle r(3, 4);
show(r);
```

Lors de l'appel  à `show(r)` ci-dessus, il y a copie de `r` dans une variable locale `tmp` (passage par valeur d'un `Rectangle`).
Le constructeur de copie par défaut est utilisé. Celui-ci va copier dans `tmp` les attributs `width` et `height` de `r`, qui sont des pointeurs. Donc les attributs de `r` et `tmp` pointent vers les même zones mémoire.

À la fin de l'appel `show(r)`, la variable `tmp` est supprimée. 
Cela cause l'appel du destructeur `~Rectangle` et donc la désallocation des  attributs `width` et `height`. Or ces attributs sont les mêmes pointeurs que pour `r`. Donc après l'appel, l'instance `r` ne sera plus utilisable (corrompu).



## Copie profonde

Dans le cas d'attributs qui sont des pointeurs, la copie profonde ne copie pas seulement les adresses mais les contenus pointés, avec duplication.

> **Exemple**. copie profonde de `Rectangle`.
> Le constructeur de copie explicite suivant fait une copie profonde.
>
> ```cpp
> Rectangle(const Rectangle& rhs): 
> largeur(new double(*(rhs.width))),
> hauteur(new double(*(rhs.height)))
> {}
> ```
>
> Dans le rectangle copié, les attributs sont de nouvelles adresses, 
> qui contiennent les valeurs d'origine 
> (`*(rhs.width)` et `*(rhs.height)`) de l'objet copié `rhs`.



Donc, dans cette classe avec un destructeur particulier (désallocation), il faut aussi définir explicitement un constructeur de copie particulier, qui réalise une copie profonde.

En toute rigueur il faudrait aussi surcharger dans `Rectangle` l'opérateur d'affectation `operator=`, conformément à la règle des 3.

