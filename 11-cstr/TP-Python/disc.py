
from point import Point

class Disc:
    """disc defined by center and radius"""
    def __init__(self, p, r=0):
        """constructor of disc from center and radius"""
        self.center = p
        r = -r if (r < 0) else r
        self.radius = r

    @classmethod
    def construct(cls, x=0, y=0, r=0):
        """constructor of disc from center's coordinates and radius"""
        return cls(Point(x, y), r)
    
    def __str__(self):
        """str representation for print"""
        return 'center='+str(self.center)+', radius='+str(self.radius)

    def __repr__(self):
        """representation for the prompt"""
        return 'disc: '+str(self) 
    
    def move(self, dx, dy):
        """translation"""
        self.center.move(dx, dy)
        
    def surface(self):
        """surface"""
        return math.pi * self.radius * self.radius
    
    def contains(self, p):
        """the given point is inside this disc"""
        return self.center.distance(p) <= self.radius
    
