import math # sqrt, pi

class Point:
    """2D point with cartesian coordinate"""    
    def __init__(self, x=0, y=0):
        self.__x = x
        self.__y = y
        
    def __str__(self):
        """str representation for print"""
        return '('+str(self.__x)+', '+str(self.__y)+')'

    def __repr__(self):
        """representation for the prompt"""
        return 'point: '+str(self) 
               
    @property
    def x(self):
        """getter of x"""
        print('getter of x called')
        return self.__x

    @x.setter
    def x(self, v):
        """setter of x"""
        print('setter of x called')       
        self.__x = v
        
    @property
    def y(self):
        """getter of y"""
        print('getter of y called')
        return self.__y

    @y.setter
    def y(self, v):
        """setter of y"""
        print('setter of y called')       
        self.__y = v        
            
    def move(self, dx, dy):
        """translation"""
        self.__x += dx;
        self.__y += dy;

    def distance(self, p):
        """euclidian distance to another point"""
        dx = p.__x - self.__x;
        dy = p.__y - self.__y;
        return math.sqrt(dx * dx + dy * dy);

