
from point_abs import Point

class Disc:
    """disc defined by center and radius"""
    def __init__(self, p, r=0):
        """constructor of disc from center and radius"""
        self.__center = p
        self.__radius = -r if (r < 0) else r

    @classmethod
    def construct(cls, x=0, y=0, r=0):
        """constructor of disc from center's coordinates and radius"""
        return cls(Point(x, y), r)
    
    def __str__(self):
        """str representation for print"""
        return 'center='+str(self.__center)+', radius='+str(self.__radius)

    def __repr__(self):
        """representation for the prompt"""
        return 'disc: '+str(self) 

    @property
    def center(self):
        """getter of center"""
        print('getter of center called')
        return self.__center

    @center.setter
    def center(self, p):
        """setter of center"""
        print('setter of center called')       
        self.__center = p
        
    @property
    def radius(self):
        """getter of radius"""
        print('getter of radius called')
        return self.__radius

    @radius.setter
    def radius(self, r):
        """setter of radius"""
        print('setter of radius called')       
        self.__radius = -r if (r < 0) else r   
    
    def move(self, dx, dy):
        """translation"""
        self.center.move(dx, dy)
        
    def surface(self):
        """surface"""
        return math.pi * self.__radius * self.__radius
    
    def contains(self, p):
        """the given point is inside this disc"""
        return self.__center.distance(p) <= self.__radius

