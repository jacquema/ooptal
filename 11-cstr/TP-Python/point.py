
import math # sqrt, pi

class Point:
    """2D point with cartesian coordinate"""    
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        
    def __str__(self):
        """str representation for print"""
        return '('+str(self.x)+', '+str(self.y)+')'

    def __repr__(self):
        """representation for the prompt"""
        return 'point: '+str(self) 
                   
    def move(self, dx, dy):
        """translation"""
        self.x += dx;
        self.y += dy;

    def distance(self, p):
        """euclidian distance to another point"""
        dx = p.x - self.x;
        dy = p.y - self.y;
        return math.sqrt(dx * dx + dy * dy);

